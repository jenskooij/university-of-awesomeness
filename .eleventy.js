const htmlmin = require("html-minifier");

module.exports = function(eleventyConfig) {
    eleventyConfig.setTemplateFormats([
        "njk",
        "ico",
        "png",
        "xml"
    ]);

    eleventyConfig.addTransform("htmlmin", function(content, outputPath) {
        if( outputPath.endsWith(".html") ) {
            let minified = htmlmin.minify(content, {
                useShortDoctype: true,
                removeComments: true,
                collapseWhitespace: true
            });
            return minified;
        }

        return content;
    });

    eleventyConfig.dir = {
        input: "src/views",
        output: "dist/site"
    };

    return eleventyConfig;
};