const cloudinary = require('cloudinary').v2;
const axios = require('axios');
require('dotenv').config();

const baseURL = process.env.URL ? `${process.env.URL}/.netlify/functions/` : 'http://localhost:9000/';
const certificateUrl = `${baseURL}generate-certificate?name=`;

exports.handler = function (event, context, callback) {
    function sendResponse(response) {
        if (!response.header) {
            response.headers = {};
        }
        response.headers['Access-Control-Allow-Origin'] = '*';
        response.headers['Access-Control-Allow-Credentials'] = true;
        callback(null, response);
    }

    // Retrieve certificate and upload it to cloudinary for transformation
    console.log('Retrieve certificate from ' + certificateUrl);
    // Check for required parameter
    if (!(event.queryStringParameters.name && event.queryStringParameters.orderId)) {
        const errMsg = 'Required query parameter `name` or `orderId` not provided';
        console.error(errMsg);
        sendResponse({
                statusCode: 400,
                body: JSON.stringify({
                    status: errMsg
                })
            }
        );
        return;
    }

    if (!(process.env.CLOUDINARY_API_KEY && process.env.CLOUDINARY_API_SECRET && process.env.CLOUDINARY_CLOUD_NAME)) {
        sendResponse({
                statusCode: 500,
                body: JSON.stringify({
                    status: 'Cloudinary not properly configured'
                })
            }
        );
        return;
    }

    // Get the certificate SVG
    axios.get(certificateUrl + event.queryStringParameters.name + `&orderId=${event.queryStringParameters.orderId}`)
        .then(function (response) {
            // Upload a base64 encoded version of the svg to cloudinary
            const buffer = Buffer.from(response.data, 'utf-8');
            const base64data = buffer.toString('base64');
            cloudinary.uploader.upload('data:image/svg+xml;base64,' + base64data, {
                api_key: process.env.CLOUDINARY_API_KEY,
                api_secret: process.env.CLOUDINARY_API_SECRET,
                cloud_name: process.env.CLOUDINARY_CLOUD_NAME
            }, function (error, result) {
                if (error) {
                    console.error(error);
                    sendResponse({
                        statusCode: 500,
                        body: JSON.stringify({
                            status: `Error uploading certificate to cloudinary`,
                            error: error
                        })
                    });
                }
                const certUrl = `https://res.cloudinary.com/${process.env.CLOUDINARY_CLOUD_NAME}/image/upload/w_3500,b_white/${result.public_id}.png`;
                console.log(`Uploading of certificate to cloudinary successful, see ${certUrl}`);
                sendResponse({
                    statusCode: 200,
                    body: JSON.stringify({
                        status: 'ok',
                        certificateUrl: certUrl
                    })
                });
            });
        }).catch((err) => {
        console.error(err);
        sendResponse({
            statusCode: 500,
            body: JSON.stringify({
                status: `Error getting the generated certificate from ${certificateUrl}`,
                error: err
            })
        });
    });
};