require('dotenv').config();

const axios = require('axios');
const qs = require('querystring');

exports.handler = function (event, context, callback) {
  function sendResponse (response) {
    if (!response.header) {
      response.headers = {};
    }
    response.headers['Access-Control-Allow-Origin'] = '*';
    response.headers['Access-Control-Allow-Credentials'] = true;
    callback(null, response);
  }

  function sendErrorResponse (err) {
    console.error(err);
    sendResponse({
        statusCode: 500,
        body: JSON.stringify({
          status: err.msg
        })
      }
    );
  }

  if (!event.queryStringParameters.order_id) {
    const err = {msg: 'One of required query parameters not provided (order_id)'};
    sendErrorResponse(err);
  }

  const details = (event.queryStringParameters.details) && event.queryStringParameters.details == "true";

  const orderId = event.queryStringParameters.order_id;

  axios.post(process.env.PRINTAPI_ENDPOINT + 'oauth', qs.stringify({
    grant_type: 'client_credentials',
    client_id: process.env.PRINTAPI_CLIENT_ID,
    client_secret: process.env.PRINTAPI_SECRET
  }), {
    headers: {
      'Content-Type': 'application/x-www-form-urlencoded'
    }
  })
    .then(response => {
      console.log('Authenticating with printapi successful');

      const accessToken = response.data.access_token;

      const orderInfoRequestConfig = {
        headers: {
          Authorization: 'Bearer ' + accessToken,
          Accept: 'application/json',
          ContentType: 'application/json'
        }
      };

      axios.get(process.env.PRINTAPI_ENDPOINT + 'orders/' + orderId, orderInfoRequestConfig)
        .then(response => {
          if (details) {
            sendResponse({
                statusCode: 200,
                body: JSON.stringify({
                  status: 'ok',
                  orderStatus: response.data.status,
                  paymentStatus: response.data.checkout.status,
                  paymentUrl: response.data.checkout.paymentUrl,
                  shippingMethod: response.data.shipping.method.name,
                  shippingTrackable: response.data.shipping.method.isTrackable,
                  shippingTrackingUrl: response.data.trackingUrl ? response.data.trackingUrl : null,
                  name: response.data.shipping.address.name
                })
              }
            );
          } else {
            sendResponse({
                statusCode: 200,
                body: JSON.stringify({
                  status: 'ok',
                  paymentStatus: response.data.checkout.status,
                  name: response.data.shipping.address.name
                })
              }
            );
          }

        })
        .catch(err => sendErrorResponse(err));

    }).catch(err => sendErrorResponse(err));
};