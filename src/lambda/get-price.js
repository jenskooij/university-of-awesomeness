require('dotenv').config();
const axios = require('axios');
const qs = require('querystring');
const ipapi = require('ipapi.co');

const optionHangingSystem = 'hanging_system';
const optionFrame = 'photo_frame_a4';
const defaultCountry = 'US';
const defaultCurrency = 'USD';
const currencyExchangeUrl = 'https://api.exchangeratesapi.io/latest?base=EUR';
const countryCurrencies = {"BD": "BDT", "BE": "EUR", "BF": "XOF", "BG": "BGN", "BA": "BAM", "BB": "BBD", "WF": "XPF", "BL": "EUR", "BM": "BMD", "BN": "BND", "BO": "BOB", "BH": "BHD", "BI": "BIF", "BJ": "XOF", "BT": "BTN", "JM": "JMD", "BV": "NOK", "BW": "BWP", "WS": "WST", "BQ": "USD", "BR": "BRL", "BS": "BSD", "JE": "GBP", "BY": "BYR", "BZ": "BZD", "RU": "RUB", "RW": "RWF", "RS": "RSD", "TL": "USD", "RE": "EUR", "TM": "TMT", "TJ": "TJS", "RO": "RON", "TK": "NZD", "GW": "XOF", "GU": "USD", "GT": "GTQ", "GS": "GBP", "GR": "EUR", "GQ": "XAF", "GP": "EUR", "JP": "JPY", "GY": "GYD", "GG": "GBP", "GF": "EUR", "GE": "GEL", "GD": "XCD", "GB": "GBP", "GA": "XAF", "SV": "USD", "GN": "GNF", "GM": "GMD", "GL": "DKK", "GI": "GIP", "GH": "GHS", "OM": "OMR", "TN": "TND", "JO": "JOD", "HR": "HRK", "HT": "HTG", "HU": "HUF", "HK": "HKD", "HN": "HNL", "HM": "AUD", "VE": "VEF", "PR": "USD", "PS": "ILS", "PW": "USD", "PT": "EUR", "SJ": "NOK", "PY": "PYG", "IQ": "IQD", "PA": "PAB", "PF": "XPF", "PG": "PGK", "PE": "PEN", "PK": "PKR", "PH": "PHP", "PN": "NZD", "PL": "PLN", "PM": "EUR", "ZM": "ZMK", "EH": "MAD", "EE": "EUR", "EG": "EGP", "ZA": "ZAR", "EC": "USD", "IT": "EUR", "VN": "VND", "SB": "SBD", "ET": "ETB", "SO": "SOS", "ZW": "ZWL", "SA": "SAR", "ES": "EUR", "ER": "ERN", "ME": "EUR", "MD": "MDL", "MG": "MGA", "MF": "EUR", "MA": "MAD", "MC": "EUR", "UZ": "UZS", "MM": "MMK", "ML": "XOF", "MO": "MOP", "MN": "MNT", "MH": "USD", "MK": "MKD", "MU": "MUR", "MT": "EUR", "MW": "MWK", "MV": "MVR", "MQ": "EUR", "MP": "USD", "MS": "XCD", "MR": "MRO", "IM": "GBP", "UG": "UGX", "TZ": "TZS", "MY": "MYR", "MX": "MXN", "IL": "ILS", "FR": "EUR", "IO": "USD", "SH": "SHP", "FI": "EUR", "FJ": "FJD", "FK": "FKP", "FM": "USD", "FO": "DKK", "NI": "NIO", "NL": "EUR", "NO": "NOK", "NA": "NAD", "VU": "VUV", "NC": "XPF", "NE": "XOF", "NF": "AUD", "NG": "NGN", "NZ": "NZD", "NP": "NPR", "NR": "AUD", "NU": "NZD", "CK": "NZD", "XK": "EUR", "CI": "XOF", "CH": "CHF", "CO": "COP", "CN": "CNY", "CM": "XAF", "CL": "CLP", "CC": "AUD", "CA": "CAD", "CG": "XAF", "CF": "XAF", "CD": "CDF", "CZ": "CZK", "CY": "EUR", "CX": "AUD", "CR": "CRC", "CW": "ANG", "CV": "CVE", "CU": "CUP", "SZ": "SZL", "SY": "SYP", "SX": "ANG", "KG": "KGS", "KE": "KES", "SS": "SSP", "SR": "SRD", "KI": "AUD", "KH": "KHR", "KN": "XCD", "KM": "KMF", "ST": "STD", "SK": "EUR", "KR": "KRW", "SI": "EUR", "KP": "KPW", "KW": "KWD", "SN": "XOF", "SM": "EUR", "SL": "SLL", "SC": "SCR", "KZ": "KZT", "KY": "KYD", "SG": "SGD", "SE": "SEK", "SD": "SDG", "DO": "DOP", "DM": "XCD", "DJ": "DJF", "DK": "DKK", "VG": "USD", "DE": "EUR", "YE": "YER", "DZ": "DZD", "US": "USD", "UY": "UYU", "YT": "EUR", "UM": "USD", "LB": "LBP", "LC": "XCD", "LA": "LAK", "TV": "AUD", "TW": "TWD", "TT": "TTD", "TR": "TRY", "LK": "LKR", "LI": "CHF", "LV": "EUR", "TO": "TOP", "LT": "LTL", "LU": "EUR", "LR": "LRD", "LS": "LSL", "TH": "THB", "TF": "EUR", "TG": "XOF", "TD": "XAF", "TC": "USD", "LY": "LYD", "VA": "EUR", "VC": "XCD", "AE": "AED", "AD": "EUR", "AG": "XCD", "AF": "AFN", "AI": "XCD", "VI": "USD", "IS": "ISK", "IR": "IRR", "AM": "AMD", "AL": "ALL", "AO": "AOA", "AQ": "", "AS": "USD", "AR": "ARS", "AU": "AUD", "AT": "EUR", "AW": "AWG", "IN": "INR", "AX": "EUR", "AZ": "AZN", "IE": "EUR", "ID": "IDR", "UA": "UAH", "QA": "QAR", "MZ": "MZN"};

exports.handler = function (event, context, callback) {
    const clientIp = event.headers['client-ip'] ? event.headers['client-ip'] : null;

    function sendResponse(response) {
        if (!response.header) {
            response.headers = {};
        }
        response.headers['Access-Control-Allow-Origin'] = '*';
        response.headers['Access-Control-Allow-Credentials'] = true;
        callback(null, response);
    }

    if (event.queryStringParameters.country) {
        console.log('Country given in querystring: ', event.queryStringParameters.country);
        let country = event.queryStringParameters.country;
        let currency = countryCurrencies[country];
        getPrice(country, currency);
    } else {
        // Determine country and currency using https://ipapi.co/
        console.log('Determine country and currency for client-ip: ' + clientIp);
        let country = defaultCountry;
        let currency = defaultCurrency;
        ipapi.location(loc => {
            if (loc.error === true) {
                country = defaultCountry;
                currency = defaultCurrency;
            } else {
                country = loc.country;
                currency = loc.currency;
            }

            console.log('Country: ' + country);
            console.log('currency: ' + currency);
            getPrice(country, currency);

        }, clientIp);
    }



    function getPrice(country, currency) {
        axios.get(currencyExchangeUrl)
          .then(response => {
              const currencyExchangeRates = response.data.rates;
              console.log('Authenticating');
              axios.post(process.env.PRINTAPI_ENDPOINT + 'oauth', qs.stringify({
                  grant_type: 'client_credentials',
                  client_id: process.env.PRINTAPI_CLIENT_ID,
                  client_secret: process.env.PRINTAPI_SECRET
              }), {
                  headers: {
                      'Content-Type': 'application/x-www-form-urlencoded'
                  }
              })
                .then(response => {
                    console.log('Authenticating with printapi successful');
                    const accessToken = response.data.access_token;

                    const quoteRequestConfig = {
                        headers: {
                            Authorization: 'Bearer ' + accessToken,
                            Accept: 'application/json',
                            ContentType: 'application/json'
                        }
                    };

                    const quoteRequestBody = {
                        "country": country,
                        "items": [
                            {
                                productId: "poster_a4_lig",
                                quantity: 1,
                                options: [
                                    {"id": "hanging_system", "value": "photo_frame_a4"}
                                ]
                            }
                        ]
                    };

                    axios.get(process.env.PRINTAPI_ENDPOINT + 'products/poster_a4_lig', quoteRequestConfig)
                      .then(response => {
                          const basePrice = response.data.checkout.base;
                          let framePrice = 0;

                          response.data.options.forEach(option => {
                              if (option.id === optionHangingSystem) {
                                  option.choices.forEach(choice => {
                                      if (choice.value === optionFrame) {
                                          framePrice = choice.checkout.base;
                                      }
                                  })
                              }
                          });

                          axios.post(process.env.PRINTAPI_ENDPOINT + 'shipping/quote', quoteRequestBody, quoteRequestConfig)
                            .then(response => {
                                const shippingPrice = response.data.payment;
                                const totalEur = basePrice + framePrice + shippingPrice;

                                sendResponse( {
                                    statusCode: 200,
                                    body: JSON.stringify({
                                        basePrise: basePrice,
                                        framePrice: framePrice,
                                        shippingPrice: shippingPrice,
                                        totalEur: totalEur,
                                        totalExchanged: currencyExchangeRates[currency] ? totalEur * currencyExchangeRates[currency] : totalEur,
                                        exchangeRateUsed: currencyExchangeRates[currency] ? currencyExchangeRates[currency] : null,
                                        country: country,
                                        currency: currencyExchangeRates[currency] ? currency : 'EUR'
                                    })
                                });
                            })
                            .catch(err => {
                                console.error(err);
                                sendResponse( {
                                    statusCode: 500,
                                    body: JSON.stringify({
                                        status: 'Something went wrong getting price',
                                        msg: err.message
                                    })
                                });
                            });
                      })
                      .catch(err => console.error(err));

                    /**/

                })
                .catch(err => {
                    console.error(err);
                    sendResponse( {
                        statusCode: 403,
                        body: JSON.stringify({
                            status: 'Something went wrong authenticating'
                        })
                    });
                });
          })
          .catch(err => {
              console.error(err);
              sendResponse( {
                  statusCode: 500,
                  body: JSON.stringify({
                      status: 'Something went wrong getting exchange',
                      msg: err.message
                  })
              });
          });
    }

};