require('dotenv').config();
const axios = require('axios');
const qs = require('querystring');

const hrstart = process.hrtime();

function infoLog (message) {
  const hrend = process.hrtime(hrstart);
  console.info('Execution time (hr): %ds %dms', hrend[0], hrend[1] / 1000000);
  console.info(message);
}

/**
 * Function that handles communication with the order API for shipping a
 * physical copy of the certificate.
 *
 * @param event
 * @param context
 * @param callback
 */
exports.handler = function (event, context, callback) {

  function sendResponse (response) {
    if (!response.header) {
      response.headers = {};
    }
    response.headers['Access-Control-Allow-Origin'] = '*';
    response.headers['Access-Control-Allow-Credentials'] = true;
    callback(null, response);
  }

  infoLog('ordering certificate');

  // Check for required parameter
  if (!(event.queryStringParameters.name
    && event.queryStringParameters.street
    && event.queryStringParameters.zip
    && event.queryStringParameters.city
    && event.queryStringParameters.country
    && event.queryStringParameters.email
  )) {
    const errorMessage = 'One of required query parameters not provided (name, street, zip, city, country)';
    console.error(errorMessage);

    sendResponse({
        statusCode: 400,
        body: JSON.stringify({
          status: errorMessage
        })
      }
    );
    return;
  }

  const address = {
    "address": {
      "name": event.queryStringParameters.name,
      "line1": event.queryStringParameters.street,
      "postCode": event.queryStringParameters.zip,
      "city": event.queryStringParameters.city,
      "country": event.queryStringParameters.country
    }
  };

  // Authenticate with Print Api
  // Request body for retrieval of auth token
  const AuthRequestBody = {
    grant_type: 'client_credentials',
    client_id: process.env.PRINTAPI_CLIENT_ID,
    client_secret: process.env.PRINTAPI_SECRET
  };

  // Request headers for retrieval of auth token
  const authConfig = {
    headers: {
      'Content-Type': 'application/x-www-form-urlencoded'
    }
  };

  // Retrieve the auth token
  axios.post(process.env.PRINTAPI_ENDPOINT + 'oauth', qs.stringify(AuthRequestBody), authConfig)
    .then((result) => {
      infoLog('using access_token: ' + result.data.access_token);
      const accessToken = result.data.access_token;
      const orderConfig = {
        headers: {
          Authorization: 'Bearer ' + accessToken,
          Accept: 'application/json',
          ContentType: 'application/json'
        }
      };
      const orderRequestBody = {
        "email": `${event.queryStringParameters.email}`,
        "items": [
          {
            "productId": "poster_a4_lig",
            "quantity": 1,
            options: [
              {
                id: "hanging_system",
                value: "photo_frame_a4"
              }
            ]
          },
        ],
        "shipping": address
      };
      // Request the order
      axios.post(process.env.PRINTAPI_ENDPOINT + 'orders', orderRequestBody, orderConfig)
        .then((orderResult) => {
          const orderId = orderResult.data.id;
          infoLog('order succesful');

          sendResponse({
            statusCode: 200,
            body: JSON.stringify({
              "orderId": orderId
            })
          });

        })
        .catch((error) => {
          console.error(error);
          sendResponse({
            statusCode: 500,
            body: JSON.stringify({
              "msg": ["An error occured placing the order", error]
            })
          });
        });
    })
    .catch((error) => {
      console.error(error);
      sendResponse({
        statusCode: 500,
        body: JSON.stringify({
          "msg": ["An error occured authenticating with the printapi", error]
        })
      });
    });

};