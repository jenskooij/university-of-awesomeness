const QRCode = require("qrcode-svg");

exports.handler = function (event, context, callback) {
  if (!(event.queryStringParameters.url)) {
    console.error('Required query parameter `url` not provided');
    callback(null, {
        statusCode: 400,
        headers: {
          'Content-Type': 'image/svg+xml'
        },
        body: `<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE svg PUBLIC "-//W3C//DTD SVG 1.0//EN" "http://www.w3.org/TR/2001/REC-SVG-20010904/DTD/svg10.dtd">
<svg version="1.0" id="Layer_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
\t\t width="841.89px" height="595.28px" viewBox="0 0 841.89 595.28" enable-background="new 0 0 841.89 595.28" xml:space="preserve">
<text x="50%" y="50%" dominant-baseline="middle" text-anchor="middle" fill="#e11d74" style="font-family: 'Bangers';" font-size="2em">Url not provided, unable to generate QR</text>
</svg>`

      }
    );
    return;
  }

  const svg = new QRCode(event.queryStringParameters.url).svg();
  callback(null, {
      statusCode: 200,
      headers: {
        'Content-Type': 'image/svg+xml'
      },
      body: svg
    }
  );
};