require('dotenv').config();
const axios = require('axios');
const qs = require('querystring');

const baseURL = process.env.URL || 'http://localhost:8080';

exports.handler = function (event, context, callback) {

  function sendResponse (response) {
    if (!response.header) {
      response.headers = {};
    }
    response.headers['Access-Control-Allow-Origin'] = '*';
    response.headers['Access-Control-Allow-Credentials'] = true;
    callback(null, response);
  }

  // Check for required parameter
  const {certificateCloudinaryUrl, orderId} = event.queryStringParameters;

  if (!(certificateCloudinaryUrl && orderId)) {
    const errorMessage = 'One of required query parameters not provided (certificateCloudinaryUrl, orderId)';
    console.error(errorMessage);

    sendResponse({
        statusCode: 400,
        body: JSON.stringify({
          status: errorMessage
        })
      }
    );
    return;
  }

  // Authenticate with Print Api
  // Request body for retrieval of auth token
  const AuthRequestBody = {
    grant_type: 'client_credentials',
    client_id: process.env.PRINTAPI_CLIENT_ID,
    client_secret: process.env.PRINTAPI_SECRET
  };

  // Request headers for retrieval of auth token
  const authConfig = {
    headers: {
      'Content-Type': 'application/x-www-form-urlencoded'
    }
  };

  // Authenticate
  console.log('Authenticating with printapi');
  axios.post(process.env.PRINTAPI_ENDPOINT + 'oauth', qs.stringify(AuthRequestBody), authConfig)
    .then((result) => {
      const accessToken = result.data.access_token;
      const orderConfig = {
        headers: {
          Authorization: 'Bearer ' + accessToken,
          Accept: 'application/json',
          ContentType: 'application/json'
        }
      };

      console.log(`Retrieve order ${orderId}`);
      axios.get(process.env.PRINTAPI_ENDPOINT + `orders/${orderId}`, orderConfig)
        .then((result) => {
          const assetUploadUrl = result.data.items[0].files.content.uploadUrl;
          const setupUrl = result.data.checkout.setupUrl;
          const address = result.data.shipping.address;

          console.log(`Asset uploadUrl: ${assetUploadUrl}`);
          // Upload asset
          console.log('uploading file');
          axios.get(certificateCloudinaryUrl, {
            responseType: 'arraybuffer' // This is mandatory, this makes it so you get a arraybuffer, rather than
                                        // the actual byte data of the image
          })
            .then(response => {
              console.log('uploading file succesful');
              const assetBinary = response.data;
              axios.post(assetUploadUrl, assetBinary, {
                headers: {
                  Authorization: 'Bearer ' + accessToken,
                  Accept: 'application/json',
                  'Content-Type': 'image/png'
                }
              })
                .then(result => {
                  // Request checkout
                  const checkoutRequestBody = {
                    "returnUrl": `${baseURL}/confirm?order_id=${orderId}`,
                    "billing": {
                      "address": address
                    }
                  };

                  axios.post(setupUrl, checkoutRequestBody, orderConfig)
                    .then((result) => {
                      console.log('checkout requested');
                      // Return the a-okay
                      sendResponse({
                        statusCode: 200,
                        body: JSON.stringify(result.data),

                      });
                    })
                    .catch((error) => {
                      console.error(error);
                      sendResponse({
                        statusCode: 500,
                        body: JSON.stringify({
                          "msg": ["An error occured setting up the checkout", error]
                        })
                      });
                    });
                })
                .catch(err => {
                  console.error(err);
                  sendResponse({
                    statusCode: 500,
                    body: JSON.stringify({
                      "msg": ["An error occured uploading certificate asset to printapi", err]
                    })
                  });
                });
            })
            .catch(err => {
              console.error(err);
              sendResponse({
                statusCode: 500,
                body: JSON.stringify({
                  "msg": ["An error occured downloading certificate asset from cloudinary", err]
                })
              });
            });
        })
        .catch(err => console.error(err));

    })
    .catch((error) => {
      console.error(error);
      sendResponse({
        statusCode: 500,
        body: JSON.stringify({
          "msg": ["An error occured authenticating with the printapi", error]
        })
      });
    });

};