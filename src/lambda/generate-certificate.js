const baseUrl = process.env.URL ? process.env.URL : 'http://localhost:8080';
const lambdaUrl = process.env.URL ? `${process.env.URL}/.netlify/functions/` : 'http://localhost:9000/';

/**
 * Generates a SVG certificate based on the query parameter `name`
 *
 * @param event
 * @param context
 * @param callback
 */
exports.handler = function (event, context, callback) {
  console.log('generating certificate');
  // Check for required parameter
  if (!(event.queryStringParameters.name && event.queryStringParameters.orderId)) {
    console.error('Required query parameter `name` or `orderId` not provided');
    callback(null, {
        statusCode: 400,
        headers: {
          'Content-Type': 'image/svg+xml'
        },
        body: `<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE svg PUBLIC "-//W3C//DTD SVG 1.0//EN" "http://www.w3.org/TR/2001/REC-SVG-20010904/DTD/svg10.dtd">
<svg version="1.0" id="Layer_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
\t\t width="841.89px" height="595.28px" viewBox="0 0 841.89 595.28" enable-background="new 0 0 841.89 595.28" xml:space="preserve">
<text x="50%" y="50%" dominant-baseline="middle" text-anchor="middle" fill="#e11d74" style="font-family: 'Bangers';" font-size="2em">Name or orderId not provided, unable to generate certificate</text>
</svg>`

      }
    );
    return;
  }

  // Print out the SVG, with the query parameter `name` placed in the middle (see the very last lines)
  callback(null, {
    statusCode: 200,
    headers: {
      'Content-Type': 'image/svg+xml'
    },
    body: `<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE svg PUBLIC "-//W3C//DTD SVG 1.0//EN" "http://www.w3.org/TR/2001/REC-SVG-20010904/DTD/svg10.dtd">
<svg version="1.0" id="Layer_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
\t width="841.89px" height="595.28px" viewBox="0 0 841.89 595.28" enable-background="new 0 0 841.89 595.28" xml:space="preserve">
\t<defs>
  <style type="text/css">
    @import url('https://fonts.googleapis.com/css?family=Playfair Display:700,400');
 </style>
</defs>
<g>
\t<g>
\t\t<g>
\t\t\t<g>
\t\t\t\t<g opacity="0.15">
\t\t\t\t\t<path fill="none" stroke="#F0F0F0" stroke-miterlimit="10" d="M420.384,361.753c0.271,0,0.539,0.016,0.807,0.02
\t\t\t\t\t\tc0.267-0.004,0.539-0.02,0.807-0.02H420.384z"/>
\t\t\t\t\t<path fill="none" stroke="#F0F0F0" stroke-width="4" stroke-miterlimit="10" d="M420.384,361.753
\t\t\t\t\t\tc0.271,0,0.539,0.016,0.807,0.02c0.267-0.004,0.539-0.02,0.807-0.02H420.384z"/>
\t\t\t\t</g>
\t\t\t\t<g>
\t\t\t\t\t<path fill="none" stroke="#F0F0F0" stroke-miterlimit="10" d="M418.358,361.753c0.267,0,0.534,0.016,0.801,0.02
\t\t\t\t\t\tc0.272-0.004,0.535-0.02,0.807-0.02H418.358z"/>
\t\t\t\t\t<path fill="none" stroke="#F0F0F0" stroke-width="4" stroke-miterlimit="10" d="M418.358,361.753
\t\t\t\t\t\tc0.267,0,0.534,0.016,0.801,0.02c0.272-0.004,0.535-0.02,0.807-0.02H418.358z"/>
\t\t\t\t</g>
\t\t\t</g>
\t\t\t<g opacity="0.15">
\t\t\t\t<path fill="none" stroke="#F0F0F0" stroke-miterlimit="10" d="M421.194,361.772c-15.395,0.389-27.739,11.907-27.739,26.068
\t\t\t\t\tv106.504h26.272h29.207V387.841C448.93,373.68,436.585,362.161,421.194,361.772z"/>
\t\t\t\t<path fill="none" stroke="#F0F0F0" stroke-width="4" stroke-miterlimit="10" d="M421.194,361.772
\t\t\t\t\tc-15.395,0.389-27.739,11.907-27.739,26.068v106.504h26.272h29.207V387.841C448.93,373.68,436.585,362.161,421.194,361.772z"/>
\t\t\t</g>
\t\t</g>
\t\t<g id="XMLID_2_">
\t\t\t<g>
\t\t\t\t<path fill="none" stroke="#F0F0F0" stroke-miterlimit="10" d="M449.178,386.865v108.136h-58.759V386.865
\t\t\t\t\tc0-15.124,12.889-27.298,29.338-27.716l0.039-0.01l0.044,0.01C436.294,359.567,449.178,371.741,449.178,386.865z
\t\t\t\t\t M445.894,491.722V386.865c0-0.189-0.01-0.38-0.01-0.569h-26.088h-26.078c0,0.189-0.015,0.38-0.015,0.569v104.856H445.894z
\t\t\t\t\t M424.027,383.013h21.536c-0.87-5.082-3.421-9.629-7.161-13.142L424.027,383.013z M421.438,380.924l14.409-13.166
\t\t\t\t\tc-3.998-2.905-8.948-4.79-14.409-5.228V380.924z M418.153,380.161V362.53c-4.343,0.351-8.365,1.613-11.849,3.586
\t\t\t\t\tL418.153,380.161z M416.269,383.013l-12.737-15.09c-4.989,3.702-8.453,9.007-9.498,15.09H416.269z"/>
\t\t\t\t<path fill="none" stroke="#F0F0F0" stroke-miterlimit="10" d="M445.894,386.865v104.856h-52.19V386.865
\t\t\t\t\tc0-0.189,0.015-0.38,0.015-0.569h26.078h26.088C445.884,386.485,445.894,386.676,445.894,386.865z"/>
\t\t\t\t<path fill="none" stroke="#F0F0F0" stroke-miterlimit="10" d="M445.563,383.013h-21.536l14.375-13.142
\t\t\t\t\tC442.143,373.384,444.693,377.931,445.563,383.013z"/>
\t\t\t\t<path fill="none" stroke="#F0F0F0" stroke-miterlimit="10" d="M435.847,367.758l-14.409,13.166V362.53
\t\t\t\t\tC426.898,362.968,431.849,364.853,435.847,367.758z"/>
\t\t\t\t<path fill="none" stroke="#F0F0F0" stroke-miterlimit="10" d="M418.153,362.53v17.631l-11.849-14.045
\t\t\t\t\tC409.788,364.144,413.811,362.881,418.153,362.53z"/>
\t\t\t\t<path fill="none" stroke="#F0F0F0" stroke-miterlimit="10" d="M403.531,367.923l12.737,15.09h-22.235
\t\t\t\t\tC395.078,376.93,398.542,371.625,403.531,367.923z"/>
\t\t\t</g>
\t\t</g>
\t\t<g>
\t\t\t<g opacity="0.15">
\t\t\t\t<path fill="none" stroke="#F0F0F0" stroke-miterlimit="10" d="M420.661,359.971l-0.039-0.005l-0.039,0.005
\t\t\t\t\tc-16.459,0.418-29.343,12.592-29.343,27.71v108.146h58.763V387.681C450.003,372.563,437.11,360.389,420.661,359.971z
\t\t\t\t\t M422.264,363.347c5.45,0.442,10.406,2.327,14.399,5.228l-14.399,13.175V363.347z M439.224,370.692
\t\t\t\t\tc3.735,3.518,6.291,8.064,7.165,13.141h-21.54L439.224,370.692z M418.979,363.347v17.635l-11.858-14.049
\t\t\t\t\tC410.604,364.96,414.627,363.696,418.979,363.347z M404.347,368.744l12.738,15.089h-22.23
\t\t\t\t\tC395.894,377.756,399.362,372.451,404.347,368.744z M446.72,492.537h-52.195V387.681c0-0.189,0.01-0.374,0.02-0.563h26.078l0,0
\t\t\t\t\tl0,0H446.7c0.01,0.189,0.02,0.374,0.02,0.563V492.537L446.72,492.537z"/>
\t\t\t</g>
\t\t\t<g>
\t\t\t\t<path fill="none" stroke="#F0F0F0" stroke-miterlimit="10" d="M419.845,359.145h-0.049h-0.039
\t\t\t\t\tc-16.449,0.423-29.338,12.597-29.338,27.716v108.141h58.759V386.86C449.178,371.741,436.294,359.567,419.845,359.145z
\t\t\t\t\t M421.442,362.53c5.456,0.438,10.401,2.317,14.404,5.223l-14.409,13.171L421.442,362.53z M438.402,369.866
\t\t\t\t\tc3.731,3.522,6.296,8.064,7.161,13.146h-21.531L438.402,369.866z M418.153,362.53v17.631L406.3,366.116
\t\t\t\t\tC409.778,364.144,413.811,362.881,418.153,362.53z M403.531,367.923l12.728,15.09h-22.226
\t\t\t\t\tC395.073,376.939,398.537,371.625,403.531,367.923z M445.894,491.722h-52.19V386.86c0-0.189,0.01-0.379,0.015-0.564h26.078l0,0
\t\t\t\t\tl0,0h26.088c0,0.186,0.01,0.375,0.01,0.564V491.722L445.894,491.722z"/>
\t\t\t</g>
\t\t</g>
\t\t<g>
\t\t\t<g>
\t\t\t\t<g opacity="0.05">
\t\t\t\t\t<rect x="399.878" y="392.495" fill="none" stroke="#F0F0F0" stroke-miterlimit="10" width="17.421" height="24.98"/>
\t\t\t\t</g>
\t\t\t\t<g>
\t\t\t\t\t<rect x="399.057" y="391.679" fill="none" stroke="#F0F0F0" stroke-miterlimit="10" width="17.416" height="24.98"/>
\t\t\t\t</g>
\t\t\t</g>
\t\t\t<g>
\t\t\t\t<g opacity="0.05">
\t\t\t\t\t<rect x="423.75" y="392.495" fill="none" stroke="#F0F0F0" stroke-miterlimit="10" width="17.421" height="24.98"/>
\t\t\t\t</g>
\t\t\t\t<g>
\t\t\t\t\t<rect x="422.929" y="391.679" fill="none" stroke="#F0F0F0" stroke-miterlimit="10" width="17.417" height="24.98"/>
\t\t\t\t</g>
\t\t\t</g>
\t\t\t<g>
\t\t\t\t<g opacity="0.05">
\t\t\t\t\t<rect x="399.878" y="459.017" fill="none" stroke="#F0F0F0" stroke-miterlimit="10" width="17.421" height="24.98"/>
\t\t\t\t</g>
\t\t\t\t<g>
\t\t\t\t\t<rect x="399.057" y="458.191" fill="none" stroke="#F0F0F0" stroke-miterlimit="10" width="17.416" height="24.989"/>
\t\t\t\t</g>
\t\t\t</g>
\t\t\t<g>
\t\t\t\t<g opacity="0.05">
\t\t\t\t\t<rect x="423.75" y="459.017" fill="none" stroke="#F0F0F0" stroke-miterlimit="10" width="17.421" height="24.98"/>
\t\t\t\t</g>
\t\t\t\t<g>
\t\t\t\t\t<rect x="422.929" y="458.191" fill="none" stroke="#F0F0F0" stroke-miterlimit="10" width="17.417" height="24.989"/>
\t\t\t\t</g>
\t\t\t</g>
\t\t\t<g>
\t\t\t\t<g opacity="0.05">
\t\t\t\t\t<rect x="399.878" y="426.541" fill="none" stroke="#F0F0F0" stroke-miterlimit="10" width="17.421" height="24.979"/>
\t\t\t\t</g>
\t\t\t\t<g>
\t\t\t\t\t<rect x="399.057" y="425.715" fill="none" stroke="#F0F0F0" stroke-miterlimit="10" width="17.416" height="24.985"/>
\t\t\t\t</g>
\t\t\t</g>
\t\t\t<g>
\t\t\t\t<g opacity="0.05">
\t\t\t\t\t<rect x="423.75" y="426.541" fill="none" stroke="#F0F0F0" stroke-miterlimit="10" width="17.421" height="24.979"/>
\t\t\t\t</g>
\t\t\t\t<g>
\t\t\t\t\t<rect x="422.929" y="425.715" fill="none" stroke="#F0F0F0" stroke-miterlimit="10" width="17.417" height="24.985"/>
\t\t\t\t</g>
\t\t\t</g>
\t\t</g>
\t</g>
\t<polygon fill="none" stroke="#F0F0F0" stroke-miterlimit="10" points="418.202,264.74 415.764,264.74 267.116,264.74 
\t\t267.116,500.412 415.764,500.412 418.202,500.412 566.85,500.412 566.85,264.74 \t"/>
\t<rect x="402.016" y="294.299" fill="none" stroke="#F0F0F0" stroke-miterlimit="10" width="36.183" height="47.853"/>
\t<rect x="311.368" y="294.299" fill="none" stroke="#F0F0F0" stroke-miterlimit="10" width="36.188" height="47.853"/>
\t<path fill="none" stroke="#F0F0F0" stroke-miterlimit="10" d="M400.98,293.267v49.919h38.247v-49.919H400.98z M437.163,317.19
\t\th-16.026v-21.856h16.026V317.19z M419.072,295.334v21.856h-16.027v-21.856H419.072z M403.045,319.255h16.022v21.861h-16.022
\t\tV319.255z M421.137,341.116v-21.861h16.026v21.861H421.137z"/>
\t<rect x="489.489" y="294.299" fill="none" stroke="#F0F0F0" stroke-miterlimit="10" width="36.184" height="47.853"/>
\t<path fill="none" stroke="#F0F0F0" stroke-miterlimit="10" d="M488.46,293.267v49.919h38.247v-49.919H488.46z M524.643,317.19
\t\th-16.026v-21.856h16.026V317.19z M506.552,295.334v21.856h-16.027v-21.856H506.552z M490.524,319.255h16.027v21.861h-16.027
\t\tV319.255z M508.616,341.116v-21.861h16.026v21.861H508.616z"/>
\t<path fill="none" stroke="#F0F0F0" stroke-miterlimit="10" d="M310.334,293.267v49.919h38.252v-49.919H310.334z M346.521,317.19
\t\th-16.026v-21.856h16.026V317.19L346.521,317.19z M328.425,295.334v21.856h-16.021v-21.856H328.425z M312.403,341.116v-21.861
\t\th16.021v21.861H312.403z M330.495,341.116v-21.861h16.026v21.861H330.495z"/>
\t<path fill="none" stroke="#F0F0F0" stroke-miterlimit="10" d="M372.235,487.752"/>
\t<path fill="none" stroke="#F0F0F0" stroke-miterlimit="10" d="M501.509,487.752"/>
\t<path fill="none" stroke="#F0F0F0" stroke-miterlimit="10" d="M571.698,487.752"/>
\t<line fill="none" stroke="#F0F0F0" stroke-miterlimit="10" x1="394.185" y1="494.957" x2="418.202" y2="494.957"/>
\t<g>
\t\t<polygon fill="none" stroke="#F0F0F0" stroke-miterlimit="10" points="577.47,505.878 577.47,494.957 418.202,494.957 
\t\t\t258.936,494.957 258.936,505.878 229.131,505.878 229.131,516.789 418.202,516.789 607.269,516.789 607.269,505.878 \t\t"/>
\t\t<g>
\t\t\t<rect x="229.131" y="513.835" fill="none" stroke="#F0F0F0" stroke-miterlimit="10" width="189.071" height="2.954"/>
\t\t\t<rect x="418.202" y="502.919" fill="none" stroke="#F0F0F0" stroke-miterlimit="10" width="159.268" height="2.959"/>
\t\t\t<rect x="418.202" y="513.835" fill="none" stroke="#F0F0F0" stroke-miterlimit="10" width="189.066" height="2.954"/>
\t\t\t<rect x="258.936" y="502.919" fill="none" stroke="#F0F0F0" stroke-miterlimit="10" width="159.267" height="2.959"/>
\t\t</g>
\t</g>
\t<g>
\t\t<g opacity="0.1">
\t\t\t<polygon fill="none" stroke="#F0F0F0" stroke-miterlimit="10" points="417.396,125.122 210.428,240.222 233.13,240.222 
\t\t\t\t417.396,141.892 601.663,240.222 624.364,240.222 \t\t\t"/>
\t\t</g>
\t\t<g>
\t\t\t<polygon fill="none" stroke="#F0F0F0" stroke-miterlimit="10" points="417.396,122.7 210.428,237.805 233.13,237.805 
\t\t\t\t417.396,139.47 601.663,237.805 624.364,237.805 \t\t\t"/>
\t\t</g>
\t</g>
\t<polygon fill="none" stroke="#F0F0F0" stroke-miterlimit="10" points="417.396,139.47 239.916,234.183 417.396,234.183 
\t\t594.876,234.183 \t"/>
\t<path fill="none" stroke="#F0F0F0" stroke-miterlimit="10" d="M302.056,487.752"/>
\t<g>
\t\t<g opacity="0.1">
\t\t\t<rect x="265.581" y="267.949" fill="none" stroke="#F0F0F0" stroke-miterlimit="10" width="31.82" height="214.727"/>
\t\t</g>
\t\t<g>
\t\t\t<rect x="267.224" y="267.128" fill="none" stroke="#F0F0F0" stroke-miterlimit="10" width="31.82" height="214.731"/>
\t\t</g>
\t</g>
\t<g>
\t\t<rect x="269.633" y="279.098" fill="none" stroke="#F0F0F0" stroke-miterlimit="10" width="0.821" height="201.888"/>
\t</g>
\t<g>
\t\t<rect x="272.257" y="279.098" fill="none" stroke="#F0F0F0" stroke-miterlimit="10" width="0.825" height="201.888"/>
\t</g>
\t<g>
\t\t<rect x="274.89" y="279.098" fill="none" stroke="#F0F0F0" stroke-miterlimit="10" width="0.816" height="201.888"/>
\t</g>
\t<g>
\t\t<rect x="277.513" y="279.098" fill="none" stroke="#F0F0F0" stroke-miterlimit="10" width="0.826" height="201.888"/>
\t</g>
\t<g>
\t\t<rect x="280.141" y="279.098" fill="none" stroke="#F0F0F0" stroke-miterlimit="10" width="0.821" height="201.888"/>
\t</g>
\t<g>
\t\t<rect x="282.769" y="279.098" fill="none" stroke="#F0F0F0" stroke-miterlimit="10" width="0.821" height="201.888"/>
\t</g>
\t<g>
\t\t<rect x="285.393" y="279.098" fill="none" stroke="#F0F0F0" stroke-miterlimit="10" width="0.825" height="201.888"/>
\t</g>
\t<g>
\t\t<rect x="288.025" y="279.098" fill="none" stroke="#F0F0F0" stroke-miterlimit="10" width="0.821" height="201.888"/>
\t</g>
\t<g>
\t\t<rect x="290.649" y="279.098" fill="none" stroke="#F0F0F0" stroke-miterlimit="10" width="0.825" height="201.888"/>
\t</g>
\t<g>
\t\t<rect x="293.277" y="279.098" fill="none" stroke="#F0F0F0" stroke-miterlimit="10" width="0.821" height="201.888"/>
\t</g>
\t<g>
\t\t<rect x="295.905" y="279.098" fill="none" stroke="#F0F0F0" stroke-miterlimit="10" width="0.821" height="201.888"/>
\t</g>
\t<path fill="none" stroke="#F0F0F0" stroke-miterlimit="10" d="M300.705,480.985h-17.159h-17.148c-3.935,0-7.122,3.521-7.122,7.86
\t\tv6.111h24.276h24.275v-6.111C307.827,484.507,304.64,480.985,300.705,480.985z"/>
\t<g>
\t\t<g opacity="0.1">
\t\t\t<path fill="none" stroke="#F0F0F0" stroke-miterlimit="10" d="M259.275,266.385v6.116c0,4.336,3.188,7.857,7.122,7.857h17.148
\t\t\t\th17.159c3.935,0,7.122-3.521,7.122-7.857v-6.116h-24.281H259.275z"/>
\t\t</g>
\t\t<g>
\t\t\t<path fill="none" stroke="#F0F0F0" stroke-miterlimit="10" d="M259.275,264.74v6.116c0,4.341,3.188,7.86,7.122,7.86h17.148
\t\t\t\th17.159c3.935,0,7.122-3.52,7.122-7.86v-6.116h-24.281H259.275z"/>
\t\t</g>
\t</g>
\t<g>
\t\t<g opacity="0.05">
\t\t\t<path fill="none" stroke="#F0F0F0" stroke-miterlimit="10" d="M300.705,475.938c0,1.88-1.525,3.405-3.41,3.405h-28.779
\t\t\t\tc-1.875,0-3.4-1.525-3.4-3.405l0,0c0-1.88,1.525-3.405,3.4-3.405h28.779C299.18,472.537,300.705,474.058,300.705,475.938
\t\t\t\tL300.705,475.938z"/>
\t\t</g>
\t\t<g>
\t\t\t<path fill="none" stroke="#F0F0F0" stroke-miterlimit="10" d="M300.705,477.584c0,1.876-1.525,3.401-3.41,3.401h-28.779
\t\t\t\tc-1.875,0-3.4-1.525-3.4-3.401l0,0c0-1.885,1.525-3.41,3.4-3.41h28.779C299.18,474.174,300.705,475.699,300.705,477.584
\t\t\t\tL300.705,477.584z"/>
\t\t</g>
\t</g>
\t<g>
\t\t<g opacity="0.1">
\t\t\t<rect x="335.664" y="267.077" fill="none" stroke="#F0F0F0" stroke-miterlimit="10" width="31.82" height="214.729"/>
\t\t</g>
\t\t<g>
\t\t\t<rect x="337.306" y="266.258" fill="none" stroke="#F0F0F0" stroke-miterlimit="10" width="31.825" height="214.728"/>
\t\t</g>
\t</g>
\t<g>
\t\t<rect x="339.822" y="279.098" fill="none" stroke="#F0F0F0" stroke-miterlimit="10" width="0.816" height="201.888"/>
\t</g>
\t<g>
\t\t<rect x="342.445" y="279.098" fill="none" stroke="#F0F0F0" stroke-miterlimit="10" width="0.821" height="201.888"/>
\t</g>
\t<g>
\t\t<rect x="345.069" y="279.098" fill="none" stroke="#F0F0F0" stroke-miterlimit="10" width="0.825" height="201.888"/>
\t</g>
\t<g>
\t\t<rect x="347.702" y="279.098" fill="none" stroke="#F0F0F0" stroke-miterlimit="10" width="0.816" height="201.888"/>
\t</g>
\t<g>
\t\t<rect x="350.325" y="279.098" fill="none" stroke="#F0F0F0" stroke-miterlimit="10" width="0.826" height="201.888"/>
\t</g>
\t<g>
\t\t<rect x="352.958" y="279.098" fill="none" stroke="#F0F0F0" stroke-miterlimit="10" width="0.816" height="201.888"/>
\t</g>
\t<g>
\t\t<rect x="355.582" y="279.098" fill="none" stroke="#F0F0F0" stroke-miterlimit="10" width="0.821" height="201.888"/>
\t</g>
\t<g>
\t\t<rect x="358.205" y="279.098" fill="none" stroke="#F0F0F0" stroke-miterlimit="10" width="0.826" height="201.888"/>
\t</g>
\t<g>
\t\t<rect x="360.839" y="279.098" fill="none" stroke="#F0F0F0" stroke-miterlimit="10" width="0.82" height="201.888"/>
\t</g>
\t<g>
\t\t<rect x="363.462" y="279.098" fill="none" stroke="#F0F0F0" stroke-miterlimit="10" width="0.826" height="201.888"/>
\t</g>
\t<g>
\t\t<rect x="366.095" y="279.098" fill="none" stroke="#F0F0F0" stroke-miterlimit="10" width="0.816" height="201.888"/>
\t</g>
\t<path fill="none" stroke="#F0F0F0" stroke-miterlimit="10" d="M370.885,480.985h-17.149h-17.153c-3.931,0-7.122,3.521-7.122,7.86
\t\tv6.111h24.275h24.271v-6.111C378.007,484.507,374.819,480.985,370.885,480.985z"/>
\t<g>
\t\t<g opacity="0.1">
\t\t\t<path fill="none" stroke="#F0F0F0" stroke-miterlimit="10" d="M329.46,266.385v6.116c0,4.336,3.188,7.857,7.122,7.857h17.153
\t\t\t\th17.149c3.935,0,7.122-3.521,7.122-7.857v-6.116h-24.271H329.46z"/>
\t\t</g>
\t\t<g>
\t\t\t<path fill="none" stroke="#F0F0F0" stroke-miterlimit="10" d="M329.46,264.74v6.116c0,4.341,3.188,7.86,7.122,7.86h17.153h17.149
\t\t\t\tc3.935,0,7.122-3.52,7.122-7.86v-6.116h-24.271H329.46z"/>
\t\t</g>
\t</g>
\t<g>
\t\t<g opacity="0.05">
\t\t\t<path fill="none" stroke="#F0F0F0" stroke-miterlimit="10" d="M371.531,475.938c0,1.88-1.521,3.405-3.405,3.405h-28.779
\t\t\t\tc-1.881,0-3.406-1.525-3.406-3.405l0,0c0-1.88,1.525-3.405,3.406-3.405h28.779C370.011,472.537,371.531,474.058,371.531,475.938
\t\t\t\tL371.531,475.938z"/>
\t\t</g>
\t\t<g>
\t\t\t<path fill="none" stroke="#F0F0F0" stroke-miterlimit="10" d="M371.531,477.584c0,1.876-1.521,3.401-3.405,3.401h-28.779
\t\t\t\tc-1.881,0-3.406-1.525-3.406-3.401l0,0c0-1.885,1.525-3.41,3.406-3.41h28.779C370.011,474.174,371.531,475.699,371.531,477.584
\t\t\t\tL371.531,477.584z"/>
\t\t</g>
\t</g>
\t<g>
\t\t<g opacity="0.1">
\t\t\t<rect x="464.937" y="267.077" fill="none" stroke="#F0F0F0" stroke-miterlimit="10" width="31.82" height="214.729"/>
\t\t</g>
\t\t<g>
\t\t\t<rect x="466.579" y="266.258" fill="none" stroke="#F0F0F0" stroke-miterlimit="10" width="31.82" height="214.728"/>
\t\t</g>
\t</g>
\t<g>
\t\t<rect x="469.091" y="279.098" fill="none" stroke="#F0F0F0" stroke-miterlimit="10" width="0.82" height="201.888"/>
\t</g>
\t<g>
\t\t<rect x="471.719" y="279.098" fill="none" stroke="#F0F0F0" stroke-miterlimit="10" width="0.821" height="201.888"/>
\t</g>
\t<g>
\t\t<rect x="474.343" y="279.098" fill="none" stroke="#F0F0F0" stroke-miterlimit="10" width="0.825" height="201.888"/>
\t</g>
\t<g>
\t\t<rect x="476.976" y="279.098" fill="none" stroke="#F0F0F0" stroke-miterlimit="10" width="0.816" height="201.888"/>
\t</g>
\t<g>
\t\t<rect x="479.599" y="279.098" fill="none" stroke="#F0F0F0" stroke-miterlimit="10" width="0.821" height="201.888"/>
\t</g>
\t<g>
\t\t<rect x="482.227" y="279.098" fill="none" stroke="#F0F0F0" stroke-miterlimit="10" width="0.821" height="201.888"/>
\t</g>
\t<g>
\t\t<rect x="484.854" y="279.098" fill="none" stroke="#F0F0F0" stroke-miterlimit="10" width="0.821" height="201.888"/>
\t</g>
\t<g>
\t\t<rect x="487.483" y="279.098" fill="none" stroke="#F0F0F0" stroke-miterlimit="10" width="0.82" height="201.888"/>
\t</g>
\t<g>
\t\t<rect x="490.111" y="279.098" fill="none" stroke="#F0F0F0" stroke-miterlimit="10" width="0.816" height="201.888"/>
\t</g>
\t<g>
\t\t<rect x="492.735" y="279.098" fill="none" stroke="#F0F0F0" stroke-miterlimit="10" width="0.825" height="201.888"/>
\t</g>
\t<g>
\t\t<rect x="495.363" y="279.098" fill="none" stroke="#F0F0F0" stroke-miterlimit="10" width="0.821" height="201.888"/>
\t</g>
\t<path fill="none" stroke="#F0F0F0" stroke-miterlimit="10" d="M500.158,480.985h-17.149h-17.153c-3.936,0-7.122,3.521-7.122,7.86
\t\tv6.111h24.275h24.271v-6.111C507.28,484.507,504.093,480.985,500.158,480.985z"/>
\t<g>
\t\t<g opacity="0.1">
\t\t\t<path fill="none" stroke="#F0F0F0" stroke-miterlimit="10" d="M458.733,266.385v6.116c0,4.336,3.191,7.857,7.127,7.857h17.148
\t\t\t\th17.149c3.939,0,7.122-3.521,7.122-7.857v-6.116h-24.271H458.733z"/>
\t\t</g>
\t\t<g>
\t\t\t<path fill="none" stroke="#F0F0F0" stroke-miterlimit="10" d="M458.733,264.74v6.116c0,4.341,3.191,7.86,7.127,7.86h17.148
\t\t\t\th17.149c3.939,0,7.122-3.52,7.122-7.86v-6.116h-24.271H458.733z"/>
\t\t</g>
\t</g>
\t<g>
\t\t<g opacity="0.05">
\t\t\t<path fill="none" stroke="#F0F0F0" stroke-miterlimit="10" d="M500.284,475.938c0,1.88-1.525,3.405-3.4,3.405H468.1
\t\t\t\tc-1.88,0-3.405-1.525-3.405-3.405l0,0c0-1.88,1.525-3.405,3.405-3.405h28.784C498.759,472.537,500.284,474.058,500.284,475.938
\t\t\t\tL500.284,475.938z"/>
\t\t</g>
\t\t<g>
\t\t\t<path fill="none" stroke="#F0F0F0" stroke-miterlimit="10" d="M500.284,477.584c0,1.876-1.525,3.401-3.4,3.401H468.1
\t\t\t\tc-1.88,0-3.405-1.525-3.405-3.401l0,0c0-1.885,1.525-3.41,3.405-3.41h28.784C498.759,474.174,500.284,475.699,500.284,477.584
\t\t\t\tL500.284,477.584z"/>
\t\t</g>
\t</g>
\t<g>
\t\t<g opacity="0.1">
\t\t\t<rect x="535.122" y="267.077" fill="none" stroke="#F0F0F0" stroke-miterlimit="10" width="31.824" height="214.729"/>
\t\t</g>
\t\t<g>
\t\t\t<rect x="536.759" y="266.258" fill="none" stroke="#F0F0F0" stroke-miterlimit="10" width="31.83" height="214.728"/>
\t\t</g>
\t</g>
\t<g>
\t\t<rect x="539.275" y="279.098" fill="none" stroke="#F0F0F0" stroke-miterlimit="10" width="0.825" height="201.888"/>
\t</g>
\t<g>
\t\t<rect x="541.903" y="279.098" fill="none" stroke="#F0F0F0" stroke-miterlimit="10" width="0.821" height="201.888"/>
\t</g>
\t<g>
\t\t<rect x="544.531" y="279.098" fill="none" stroke="#F0F0F0" stroke-miterlimit="10" width="0.821" height="201.888"/>
\t</g>
\t<g>
\t\t<rect x="547.155" y="279.098" fill="none" stroke="#F0F0F0" stroke-miterlimit="10" width="0.825" height="201.888"/>
\t</g>
\t<g>
\t\t<rect x="549.788" y="279.098" fill="none" stroke="#F0F0F0" stroke-miterlimit="10" width="0.816" height="201.888"/>
\t</g>
\t<g>
\t\t<rect x="552.411" y="279.098" fill="none" stroke="#F0F0F0" stroke-miterlimit="10" width="0.826" height="201.888"/>
\t</g>
\t<g>
\t\t<rect x="555.04" y="279.098" fill="none" stroke="#F0F0F0" stroke-miterlimit="10" width="0.82" height="201.888"/>
\t</g>
\t<g>
\t\t<rect x="557.668" y="279.098" fill="none" stroke="#F0F0F0" stroke-miterlimit="10" width="0.821" height="201.888"/>
\t</g>
\t<g>
\t\t<rect x="560.296" y="279.098" fill="none" stroke="#F0F0F0" stroke-miterlimit="10" width="0.821" height="201.888"/>
\t</g>
\t<g>
\t\t<rect x="562.925" y="279.098" fill="none" stroke="#F0F0F0" stroke-miterlimit="10" width="0.82" height="201.888"/>
\t</g>
\t<g>
\t\t<rect x="565.548" y="279.098" fill="none" stroke="#F0F0F0" stroke-miterlimit="10" width="0.826" height="201.888"/>
\t</g>
\t<path fill="none" stroke="#F0F0F0" stroke-miterlimit="10" d="M570.348,480.985h-17.154H536.04c-3.936,0-7.122,3.521-7.122,7.86
\t\tv6.111h24.275h24.276v-6.111C577.47,484.507,574.282,480.985,570.348,480.985z"/>
\t<g>
\t\t<g opacity="0.1">
\t\t\t<path fill="none" stroke="#F0F0F0" stroke-miterlimit="10" d="M553.193,266.385h-24.275v6.116c0,4.336,3.187,7.857,7.122,7.857
\t\t\t\th17.153h17.154c3.935,0,7.122-3.521,7.122-7.857v-6.116H553.193z"/>
\t\t</g>
\t\t<g>
\t\t\t<path fill="none" stroke="#F0F0F0" stroke-miterlimit="10" d="M553.193,264.74h-24.275v6.116c0,4.341,3.187,7.86,7.122,7.86
\t\t\t\th17.153h17.154c3.935,0,7.122-3.52,7.122-7.86v-6.116H553.193z"/>
\t\t</g>
\t</g>
\t<g>
\t\t<g opacity="0.05">
\t\t\t<path fill="none" stroke="#F0F0F0" stroke-miterlimit="10" d="M570.469,475.938c0,1.88-1.521,3.405-3.405,3.405h-28.779
\t\t\t\tc-1.875,0-3.4-1.525-3.4-3.405l0,0c0-1.88,1.521-3.405,3.4-3.405h28.779C568.948,472.537,570.469,474.058,570.469,475.938
\t\t\t\tL570.469,475.938z"/>
\t\t</g>
\t\t<g>
\t\t\t<path fill="none" stroke="#F0F0F0" stroke-miterlimit="10" d="M570.469,477.584c0,1.876-1.521,3.401-3.405,3.401h-28.779
\t\t\t\tc-1.875,0-3.4-1.525-3.4-3.401l0,0c0-1.885,1.521-3.41,3.4-3.41h28.779C568.948,474.174,570.469,475.699,570.469,477.584
\t\t\t\tL570.469,477.584z"/>
\t\t</g>
\t</g>
\t<g>
\t\t<rect x="416.608" y="81.778" fill="none" stroke="#F0F0F0" stroke-miterlimit="10" width="1.643" height="41.825"/>
\t</g>
\t<path fill="none" stroke="#F0F0F0" stroke-miterlimit="10" d="M418.144,103.464c0,0,2.944,1.672,8.396-1.148
\t\tc2.273-1.173,4.901-2.376,7.315-2.667c2.799-0.335,6.384-0.387,11.058,3.418c3.935,3.196,9.988,0.397,9.988,0.397v-3.956v-17.06
\t\tc0,0-5.325,3.224-9.988-0.398c-4.751-3.697-8.259-3.75-11.058-3.417c-2.414,0.293-5.042,1.493-7.315,2.669
\t\tc-5.456,2.817-8.396,1.146-8.396,1.146v16.073"/>
\t<circle fill="none" stroke="#F0F0F0" stroke-miterlimit="10" cx="417.435" cy="80.34" r="1.849"/>
\t<g>
\t\t<g>
\t\t\t<g opacity="0.15">
\t\t\t\t<polygon fill="none" stroke="#F0F0F0" stroke-miterlimit="10" points="422.672,235.478 422.672,235.478 242.661,234.651 
\t\t\t\t\t242.661,249.063 420.787,249.063 420.787,249.887 598.262,249.887 598.262,235.478 \t\t\t\t"/>
\t\t\t</g>
\t\t\t<g>
\t\t\t\t<polygon fill="none" stroke="#F0F0F0" stroke-miterlimit="10" points="420.729,234.183 420.729,234.183 240.718,233.357 
\t\t\t\t\t240.718,247.766 418.844,247.766 418.844,248.592 596.323,248.592 596.323,234.183 \t\t\t\t"/>
\t\t\t</g>
\t\t</g>
\t\t<g>
\t\t\t<g>
\t\t\t\t<rect x="243.06" y="234.183" fill="none" stroke="#F0F0F0" stroke-miterlimit="10" width="1.594" height="13.586"/>
\t\t\t</g>
\t\t\t<g>
\t\t\t\t<rect x="247.714" y="234.183" fill="none" stroke="#F0F0F0" stroke-miterlimit="10" width="1.593" height="13.586"/>
\t\t\t</g>
\t\t\t<g>
\t\t\t\t<rect x="252.367" y="234.183" fill="none" stroke="#F0F0F0" stroke-miterlimit="10" width="1.594" height="13.586"/>
\t\t\t</g>
\t\t\t<g>
\t\t\t\t<rect x="257.021" y="234.183" fill="none" stroke="#F0F0F0" stroke-miterlimit="10" width="1.594" height="13.586"/>
\t\t\t</g>
\t\t\t<g>
\t\t\t\t<rect x="261.676" y="234.183" fill="none" stroke="#F0F0F0" stroke-miterlimit="10" width="1.593" height="13.586"/>
\t\t\t</g>
\t\t\t<g>
\t\t\t\t<rect x="266.329" y="234.183" fill="none" stroke="#F0F0F0" stroke-miterlimit="10" width="1.594" height="13.586"/>
\t\t\t</g>
\t\t\t<g>
\t\t\t\t<rect x="270.983" y="234.183" fill="none" stroke="#F0F0F0" stroke-miterlimit="10" width="1.599" height="13.586"/>
\t\t\t</g>
\t\t\t<g>
\t\t\t\t<rect x="275.638" y="234.183" fill="none" stroke="#F0F0F0" stroke-miterlimit="10" width="1.598" height="13.586"/>
\t\t\t</g>
\t\t\t<g>
\t\t\t\t<rect x="280.292" y="234.183" fill="none" stroke="#F0F0F0" stroke-miterlimit="10" width="1.598" height="13.586"/>
\t\t\t</g>
\t\t\t<g>
\t\t\t\t<rect x="284.945" y="234.183" fill="none" stroke="#F0F0F0" stroke-miterlimit="10" width="1.599" height="13.586"/>
\t\t\t</g>
\t\t\t<g>
\t\t\t\t<rect x="289.6" y="234.183" fill="none" stroke="#F0F0F0" stroke-miterlimit="10" width="1.603" height="13.586"/>
\t\t\t</g>
\t\t\t<g>
\t\t\t\t<rect x="294.259" y="234.183" fill="none" stroke="#F0F0F0" stroke-miterlimit="10" width="1.598" height="13.586"/>
\t\t\t</g>
\t\t\t<g>
\t\t\t\t<rect x="298.913" y="234.183" fill="none" stroke="#F0F0F0" stroke-miterlimit="10" width="1.598" height="13.586"/>
\t\t\t</g>
\t\t\t<g>
\t\t\t\t<rect x="303.566" y="234.183" fill="none" stroke="#F0F0F0" stroke-miterlimit="10" width="1.599" height="13.586"/>
\t\t\t</g>
\t\t\t<g>
\t\t\t\t<rect x="308.221" y="234.183" fill="none" stroke="#F0F0F0" stroke-miterlimit="10" width="1.599" height="13.586"/>
\t\t\t</g>
\t\t\t<g>
\t\t\t\t<rect x="312.88" y="234.183" fill="none" stroke="#F0F0F0" stroke-miterlimit="10" width="1.593" height="13.586"/>
\t\t\t</g>
\t\t\t<g>
\t\t\t\t<rect x="317.533" y="234.183" fill="none" stroke="#F0F0F0" stroke-miterlimit="10" width="1.594" height="13.586"/>
\t\t\t</g>
\t\t\t<g>
\t\t\t\t<rect x="322.188" y="234.183" fill="none" stroke="#F0F0F0" stroke-miterlimit="10" width="1.594" height="13.586"/>
\t\t\t</g>
\t\t\t<g>
\t\t\t\t<rect x="326.842" y="234.183" fill="none" stroke="#F0F0F0" stroke-miterlimit="10" width="1.593" height="13.586"/>
\t\t\t</g>
\t\t\t<g>
\t\t\t\t<rect x="331.495" y="234.183" fill="none" stroke="#F0F0F0" stroke-miterlimit="10" width="1.594" height="13.586"/>
\t\t\t</g>
\t\t\t<g>
\t\t\t\t<rect x="336.149" y="234.183" fill="none" stroke="#F0F0F0" stroke-miterlimit="10" width="1.594" height="13.586"/>
\t\t\t</g>
\t\t\t<g>
\t\t\t\t<rect x="340.804" y="234.183" fill="none" stroke="#F0F0F0" stroke-miterlimit="10" width="1.594" height="13.586"/>
\t\t\t</g>
\t\t\t<g>
\t\t\t\t<rect x="345.458" y="234.183" fill="none" stroke="#F0F0F0" stroke-miterlimit="10" width="1.598" height="13.586"/>
\t\t\t</g>
\t\t\t<g>
\t\t\t\t<rect x="350.111" y="234.183" fill="none" stroke="#F0F0F0" stroke-miterlimit="10" width="1.599" height="13.586"/>
\t\t\t</g>
\t\t\t<g>
\t\t\t\t<rect x="354.766" y="234.183" fill="none" stroke="#F0F0F0" stroke-miterlimit="10" width="1.599" height="13.586"/>
\t\t\t</g>
\t\t\t<g>
\t\t\t\t<rect x="359.42" y="234.183" fill="none" stroke="#F0F0F0" stroke-miterlimit="10" width="1.599" height="13.586"/>
\t\t\t</g>
\t\t\t<g>
\t\t\t\t<rect x="364.073" y="234.183" fill="none" stroke="#F0F0F0" stroke-miterlimit="10" width="1.604" height="13.586"/>
\t\t\t</g>
\t\t\t<g>
\t\t\t\t<rect x="368.732" y="234.183" fill="none" stroke="#F0F0F0" stroke-miterlimit="10" width="1.599" height="13.586"/>
\t\t\t</g>
\t\t\t<g>
\t\t\t\t<rect x="373.387" y="234.183" fill="none" stroke="#F0F0F0" stroke-miterlimit="10" width="1.599" height="13.586"/>
\t\t\t</g>
\t\t\t<g>
\t\t\t\t<rect x="378.041" y="234.183" fill="none" stroke="#F0F0F0" stroke-miterlimit="10" width="1.598" height="13.586"/>
\t\t\t</g>
\t\t\t<g>
\t\t\t\t<rect x="382.694" y="234.183" fill="none" stroke="#F0F0F0" stroke-miterlimit="10" width="1.599" height="13.586"/>
\t\t\t</g>
\t\t\t<g>
\t\t\t\t<rect x="387.354" y="234.183" fill="none" stroke="#F0F0F0" stroke-miterlimit="10" width="1.594" height="13.586"/>
\t\t\t</g>
\t\t\t<g>
\t\t\t\t<rect x="392.008" y="234.183" fill="none" stroke="#F0F0F0" stroke-miterlimit="10" width="1.593" height="13.586"/>
\t\t\t</g>
\t\t\t<g>
\t\t\t\t<rect x="396.661" y="234.183" fill="none" stroke="#F0F0F0" stroke-miterlimit="10" width="1.594" height="13.586"/>
\t\t\t</g>
\t\t\t<g>
\t\t\t\t<rect x="401.315" y="234.183" fill="none" stroke="#F0F0F0" stroke-miterlimit="10" width="1.594" height="13.586"/>
\t\t\t</g>
\t\t\t<g>
\t\t\t\t<rect x="405.97" y="234.183" fill="none" stroke="#F0F0F0" stroke-miterlimit="10" width="1.594" height="13.586"/>
\t\t\t</g>
\t\t\t<g>
\t\t\t\t<rect x="410.624" y="234.183" fill="none" stroke="#F0F0F0" stroke-miterlimit="10" width="1.594" height="13.586"/>
\t\t\t</g>
\t\t\t<g>
\t\t\t\t<rect x="415.278" y="234.183" fill="none" stroke="#F0F0F0" stroke-miterlimit="10" width="1.593" height="13.586"/>
\t\t\t</g>
\t\t\t<g>
\t\t\t\t<rect x="419.932" y="234.183" fill="none" stroke="#F0F0F0" stroke-miterlimit="10" width="1.599" height="13.586"/>
\t\t\t</g>
\t\t\t<g>
\t\t\t\t<rect x="424.586" y="234.183" fill="none" stroke="#F0F0F0" stroke-miterlimit="10" width="1.599" height="13.586"/>
\t\t\t</g>
\t\t\t<g>
\t\t\t\t<rect x="429.24" y="234.183" fill="none" stroke="#F0F0F0" stroke-miterlimit="10" width="1.593" height="13.586"/>
\t\t\t</g>
\t\t\t<g>
\t\t\t\t<rect x="433.894" y="234.183" fill="none" stroke="#F0F0F0" stroke-miterlimit="10" width="1.599" height="13.586"/>
\t\t\t</g>
\t\t\t<g>
\t\t\t\t<rect x="438.553" y="234.183" fill="none" stroke="#F0F0F0" stroke-miterlimit="10" width="1.594" height="13.586"/>
\t\t\t</g>
\t\t\t<g>
\t\t\t\t<rect x="443.207" y="234.183" fill="none" stroke="#F0F0F0" stroke-miterlimit="10" width="1.599" height="13.586"/>
\t\t\t</g>
\t\t\t<g>
\t\t\t\t<rect x="447.86" y="234.183" fill="none" stroke="#F0F0F0" stroke-miterlimit="10" width="1.594" height="13.586"/>
\t\t\t</g>
\t\t\t<g>
\t\t\t\t<rect x="452.515" y="234.183" fill="none" stroke="#F0F0F0" stroke-miterlimit="10" width="1.599" height="13.586"/>
\t\t\t</g>
\t\t\t<g>
\t\t\t\t<rect x="457.174" y="234.183" fill="none" stroke="#F0F0F0" stroke-miterlimit="10" width="1.594" height="13.586"/>
\t\t\t</g>
\t\t\t<g>
\t\t\t\t<rect x="461.828" y="234.183" fill="none" stroke="#F0F0F0" stroke-miterlimit="10" width="1.593" height="13.586"/>
\t\t\t</g>
\t\t\t<g>
\t\t\t\t<rect x="466.481" y="234.183" fill="none" stroke="#F0F0F0" stroke-miterlimit="10" width="1.594" height="13.586"/>
\t\t\t</g>
\t\t\t<g>
\t\t\t\t<rect x="471.136" y="234.183" fill="none" stroke="#F0F0F0" stroke-miterlimit="10" width="1.594" height="13.586"/>
\t\t\t</g>
\t\t\t<g>
\t\t\t\t<rect x="475.79" y="234.183" fill="none" stroke="#F0F0F0" stroke-miterlimit="10" width="1.594" height="13.586"/>
\t\t\t</g>
\t\t\t<g>
\t\t\t\t<rect x="480.444" y="234.183" fill="none" stroke="#F0F0F0" stroke-miterlimit="10" width="1.593" height="13.586"/>
\t\t\t</g>
\t\t\t<g>
\t\t\t\t<rect x="485.098" y="234.183" fill="none" stroke="#F0F0F0" stroke-miterlimit="10" width="1.594" height="13.586"/>
\t\t\t</g>
\t\t\t<g>
\t\t\t\t<rect x="489.752" y="234.183" fill="none" stroke="#F0F0F0" stroke-miterlimit="10" width="1.594" height="13.586"/>
\t\t\t</g>
\t\t\t<g>
\t\t\t\t<rect x="494.406" y="234.183" fill="none" stroke="#F0F0F0" stroke-miterlimit="10" width="1.593" height="13.586"/>
\t\t\t</g>
\t\t\t<g>
\t\t\t\t<rect x="499.065" y="234.183" fill="none" stroke="#F0F0F0" stroke-miterlimit="10" width="1.593" height="13.586"/>
\t\t\t</g>
\t\t\t<g>
\t\t\t\t<rect x="503.714" y="234.183" fill="none" stroke="#F0F0F0" stroke-miterlimit="10" width="1.594" height="13.586"/>
\t\t\t</g>
\t\t\t<g>
\t\t\t\t<rect x="508.373" y="234.183" fill="none" stroke="#F0F0F0" stroke-miterlimit="10" width="1.594" height="13.586"/>
\t\t\t</g>
\t\t\t<g>
\t\t\t\t<rect x="513.027" y="234.183" fill="none" stroke="#F0F0F0" stroke-miterlimit="10" width="1.593" height="13.586"/>
\t\t\t</g>
\t\t\t<g>
\t\t\t\t<rect x="517.686" y="234.183" fill="none" stroke="#F0F0F0" stroke-miterlimit="10" width="1.594" height="13.586"/>
\t\t\t</g>
\t\t\t<g>
\t\t\t\t<rect x="522.335" y="234.183" fill="none" stroke="#F0F0F0" stroke-miterlimit="10" width="1.594" height="13.586"/>
\t\t\t</g>
\t\t\t<g>
\t\t\t\t<rect x="526.994" y="234.183" fill="none" stroke="#F0F0F0" stroke-miterlimit="10" width="1.593" height="13.586"/>
\t\t\t</g>
\t\t\t<g>
\t\t\t\t<rect x="531.647" y="234.183" fill="none" stroke="#F0F0F0" stroke-miterlimit="10" width="1.594" height="13.586"/>
\t\t\t</g>
\t\t\t<g>
\t\t\t\t<rect x="536.302" y="234.183" fill="none" stroke="#F0F0F0" stroke-miterlimit="10" width="1.594" height="13.586"/>
\t\t\t</g>
\t\t\t<g>
\t\t\t\t<rect x="540.956" y="234.183" fill="none" stroke="#F0F0F0" stroke-miterlimit="10" width="1.594" height="13.586"/>
\t\t\t</g>
\t\t\t<g>
\t\t\t\t<rect x="545.61" y="234.183" fill="none" stroke="#F0F0F0" stroke-miterlimit="10" width="1.594" height="13.586"/>
\t\t\t</g>
\t\t\t<g>
\t\t\t\t<rect x="550.265" y="234.183" fill="none" stroke="#F0F0F0" stroke-miterlimit="10" width="1.593" height="13.586"/>
\t\t\t</g>
\t\t\t<g>
\t\t\t\t<rect x="554.918" y="234.183" fill="none" stroke="#F0F0F0" stroke-miterlimit="10" width="1.594" height="13.586"/>
\t\t\t</g>
\t\t\t<g>
\t\t\t\t<rect x="559.572" y="234.183" fill="none" stroke="#F0F0F0" stroke-miterlimit="10" width="1.593" height="13.586"/>
\t\t\t</g>
\t\t\t<g>
\t\t\t\t<rect x="564.227" y="234.183" fill="none" stroke="#F0F0F0" stroke-miterlimit="10" width="1.593" height="13.586"/>
\t\t\t</g>
\t\t\t<g>
\t\t\t\t<rect x="568.88" y="234.183" fill="none" stroke="#F0F0F0" stroke-miterlimit="10" width="1.594" height="13.586"/>
\t\t\t</g>
\t\t\t<g>
\t\t\t\t<rect x="573.539" y="234.183" fill="none" stroke="#F0F0F0" stroke-miterlimit="10" width="1.594" height="13.586"/>
\t\t\t</g>
\t\t\t<g>
\t\t\t\t<rect x="578.188" y="234.183" fill="none" stroke="#F0F0F0" stroke-miterlimit="10" width="1.594" height="13.586"/>
\t\t\t</g>
\t\t\t<g>
\t\t\t\t<rect x="582.847" y="234.183" fill="none" stroke="#F0F0F0" stroke-miterlimit="10" width="1.599" height="13.586"/>
\t\t\t</g>
\t\t\t<g>
\t\t\t\t<rect x="587.501" y="234.183" fill="none" stroke="#F0F0F0" stroke-miterlimit="10" width="1.599" height="13.586"/>
\t\t\t</g>
\t\t\t<g>
\t\t\t\t<rect x="592.16" y="234.183" fill="none" stroke="#F0F0F0" stroke-miterlimit="10" width="1.594" height="13.586"/>
\t\t\t</g>
\t\t</g>
\t</g>
\t<g>
\t\t<g>
\t\t\t<rect x="240.693" y="249.957" fill="none" stroke="#F0F0F0" stroke-miterlimit="10" width="355.013" height="14.409"/>
\t\t</g>
\t\t<g>
\t\t\t<g opacity="0.1">
\t\t\t\t<path fill="none" stroke="#F0F0F0" stroke-miterlimit="10" d="M602.148,250.504c0,0.606-0.495,1.098-1.099,1.098H240.747
\t\t\t\t\tc-0.603,0-1.093-0.491-1.093-1.098l0,0c0-0.603,0.49-1.096,1.093-1.096H601.05C601.657,249.408,602.148,249.901,602.148,250.504
\t\t\t\t\tL602.148,250.504z"/>
\t\t\t</g>
\t\t\t<g>
\t\t\t\t<path fill="none" stroke="#F0F0F0" stroke-miterlimit="10" d="M600.507,248.861c0,0.607-0.496,1.096-1.099,1.096H239.104
\t\t\t\t\tc-0.602,0-1.093-0.488-1.093-1.096l0,0c0-0.603,0.491-1.093,1.093-1.093h360.304
\t\t\t\t\tC600.011,247.766,600.507,248.259,600.507,248.861L600.507,248.861z"/>
\t\t\t</g>
\t\t</g>
\t\t<g>
\t\t\t<g opacity="0.1">
\t\t\t\t<path fill="none" stroke="#F0F0F0" stroke-miterlimit="10" d="M602.148,266.011c0,0.603-0.495,1.096-1.099,1.096H240.747
\t\t\t\t\tc-0.603,0-1.093-0.493-1.093-1.096l0,0c0-0.605,0.49-1.096,1.093-1.096H601.05C601.657,264.915,602.148,265.405,602.148,266.011
\t\t\t\t\tL602.148,266.011z"/>
\t\t\t</g>
\t\t\t<g>
\t\t\t\t<path fill="none" stroke="#F0F0F0" stroke-miterlimit="10" d="M600.507,264.366c0,0.607-0.496,1.096-1.099,1.096H239.104
\t\t\t\t\tc-0.602,0-1.093-0.488-1.093-1.096l0,0c0-0.603,0.491-1.091,1.093-1.091h360.304
\t\t\t\t\tC600.011,263.275,600.507,263.764,600.507,264.366L600.507,264.366z"/>
\t\t\t</g>
\t\t</g>
\t</g>
\t<g opacity="0.5">
\t\t<polygon fill="none" stroke="#F0F0F0" stroke-miterlimit="10" points="233.13,237.805 279.009,212.868 324.966,188.082 
\t\t\t370.981,163.398 417.007,138.748 417.396,138.54 417.784,138.748 463.81,163.394 509.825,188.082 555.783,212.868 
\t\t\t601.663,237.805 555.404,213.575 509.233,189.194 463.11,164.715 417.007,140.196 417.784,140.196 371.682,164.715 
\t\t\t325.56,189.194 279.388,213.575 \t\t"/>
\t</g>
</g>
<g>
\t<path fill="#440047" d="M61.157,66.843h2.767v-2.767C52.138,52.35,53.28,43.811,57.729,39.36l-4.27,0.602
\t\tc-4.57,0.602-7.156,5.593-5.051,9.682l1.864,3.668l-3.669-1.863c-4.089-2.105-9.02,0.48-9.682,5.051l-0.602,4.27
\t\tC40.832,56.259,49.371,55.116,61.157,66.843z"/>
\t<polygon fill="#440047" points="44.26,40.804 37.825,40.804 37.825,47.238 44.26,47.238 \t"/>
\t<polygon fill="#440047" points="82.084,76.404 73.425,76.404 73.425,85.063 82.145,85.123 \t"/>
\t<path fill="#440047" d="M68.073,63.775c6.193,4.45,14.433,6.314,21.648,3.969c7.758-0.12,11.906-2.766,16.417-6.734
\t\tc4.209-3.789,6.976-9.382,6.614-14.974h603.095h7.456h5.713c-0.36,5.592,2.405,11.245,6.615,14.974
\t\tc4.51,3.969,8.659,6.614,16.417,6.734c7.216,2.346,15.454,0.481,21.648-3.969c2.646-1.924,5.051-4.27,6.555-7.156
\t\tc1.563-2.886,2.225-6.254,1.503-9.44c-0.722-3.188-2.886-6.074-5.953-7.337c-3.007-1.203-6.855-0.541-8.96,1.924
\t\tc-2.165,2.466-2.346,7.036-0.061,9.382c0.121-1.443,0.902-2.826,2.165-3.669c1.263-0.842,2.826-1.021,4.21-0.601
\t\tc2.766,0.901,4.209,4.209,3.729,7.035c-0.481,2.826-2.406,5.232-4.631,7.156c-4.631,3.969-10.764,6.194-16.838,6.134
\t\tc2.285-0.962,4.45-3.127,5.231-5.412c0.782-2.346,0.181-5.172-1.743-6.735c-1.925-1.563-5.052-1.443-6.615,0.421
\t\tc1.503-0.06,2.887,1.443,3.127,2.947c0.24,1.503-0.541,3.066-1.804,4.028c-1.203,0.902-2.827,1.264-4.33,1.203
\t\ts-3.007-0.541-4.45-1.082c-2.525-0.963-4.991-2.226-6.976-4.09s-3.487-4.329-3.788-7.035s0.722-5.593,2.887-7.276
\t\tc1.503-1.143,3.668-1.624,5.412-0.842c1.743,0.781,3.728,3.607,2.164,4.69c2.406-1.624,3.789-5.954,2.767-8.66
\t\ts-3.729-4.63-6.615-4.991c-2.886-0.36-5.833,0.842-7.938,2.887c-1.744,1.684-2.887,3.909-3.428,6.314h-6.073h-7.457H112.452
\t\tc-0.541-2.346-1.684-4.631-3.427-6.314c-2.105-2.045-5.052-3.188-7.938-2.887s-5.593,2.285-6.614,4.991
\t\tc-1.022,2.706,0.36,7.036,2.766,8.66c-1.563-1.083,0.421-3.909,2.165-4.69c1.744-0.782,3.849-0.301,5.412,0.842
\t\tc2.165,1.623,3.188,4.57,2.887,7.276s-1.805,5.171-3.789,7.035s-4.449,3.127-6.976,4.09c-1.443,0.541-2.946,1.021-4.449,1.082
\t\tc-1.504,0.061-3.128-0.24-4.33-1.203c-1.203-0.901-1.984-2.465-1.804-4.028c0.24-1.504,1.623-3.007,3.127-2.947
\t\tc-1.563-1.864-4.751-1.984-6.615-0.421c-1.924,1.563-2.525,4.39-1.744,6.735c0.782,2.346,2.947,4.45,5.232,5.412
\t\tc-6.074,0.061-12.208-2.165-16.838-6.134c-2.226-1.864-4.149-4.27-4.631-7.156c-0.48-2.826,0.962-6.134,3.729-7.035
\t\tc1.383-0.421,2.946-0.241,4.209,0.601c1.264,0.843,2.045,2.165,2.165,3.669c2.226-2.346,2.105-6.976-0.06-9.382
\t\tc-2.165-2.465-5.954-3.127-9.021-1.924c-3.007,1.203-5.231,4.149-5.953,7.337c-0.722,3.187,0,6.614,1.503,9.44
\t\tC63.021,59.506,65.427,61.911,68.073,63.775z"/>
\t<path fill="#440047" d="M778.027,64.137v2.766h2.766c11.786-11.786,20.266-10.644,24.716-6.193l-0.602-4.27
\t\tc-0.602-4.57-5.593-7.156-9.682-5.052l-3.668,1.864l1.864-3.668c2.104-4.09-0.481-9.021-5.052-9.682l-4.27-0.602
\t\tC788.61,43.811,789.753,52.35,778.027,64.137z"/>
\t<polygon fill="#440047" points="804.065,40.804 797.631,40.804 797.631,47.238 804.065,47.238 \t"/>
\t<polygon fill="#440047" points="768.465,76.404 759.806,76.404 759.746,85.123 768.465,85.063 \t"/>
\t<path fill="#440047" d="M723.364,24.929h-7.096H21.949V466.68v7.097v96.576h694.319h7.096h96.577v-71.5V466.68V24.929H723.364z
\t\t M815.731,498.853v67.291h-92.367h-7.096H26.159v-92.367v-7.097V29.138h690.109h7.096h92.367V466.68V498.853z"/>
\t<path fill="#440047" d="M48.229,517.313c-2.346-2.225-6.976-2.104-9.381,0.061c-2.466,2.165-3.127,5.953-1.925,9.021
\t\tc1.203,3.006,4.149,5.231,7.337,5.953c3.187,0.722,6.614,0,9.441-1.504c2.886-1.563,5.231-3.908,7.155-6.614
\t\tc4.45-6.194,6.314-14.433,3.97-21.648c-0.121-7.758-2.767-11.907-6.735-16.417c-3.789-4.21-9.381-6.976-14.974-6.615v-5.713v-7.457
\t\tV115.792c5.593,0.361,11.245-2.405,14.974-6.614c3.969-4.511,6.614-8.66,6.735-16.417c2.345-7.217,0.48-15.455-3.97-21.648
\t\tc-1.924-2.646-4.27-5.052-7.155-6.615c-2.887-1.563-6.255-2.225-9.441-1.503c-3.188,0.722-6.074,2.886-7.337,5.953
\t\tc-1.202,3.007-0.541,6.855,1.925,9.021c2.465,2.164,7.035,2.345,9.381,0.06c-1.443-0.12-2.826-0.902-3.668-2.165
\t\tc-0.782-1.202-1.022-2.826-0.602-4.209c0.902-2.767,4.209-4.21,7.036-3.729c2.826,0.481,5.231,2.405,7.155,4.631
\t\tc3.97,4.63,6.194,10.764,6.134,16.837c-0.962-2.285-3.127-4.449-5.412-5.231c-2.345-0.781-5.171-0.181-6.734,1.744
\t\tc-1.564,1.924-1.443,5.051,0.421,6.614c-0.061-1.503,1.442-2.886,2.946-3.127c1.503-0.24,3.066,0.542,4.029,1.805
\t\tc0.901,1.202,1.263,2.826,1.202,4.329c-0.06,1.504-0.541,3.007-1.082,4.45c-0.962,2.525-2.226,4.991-4.09,6.976
\t\tc-1.863,1.984-4.329,3.488-7.035,3.789s-5.593-0.722-7.276-2.887c-1.143-1.504-1.624-3.668-0.842-5.412
\t\tc0.781-1.744,3.607-3.729,4.69-2.165c-1.624-2.405-5.953-3.788-8.659-2.767c-2.706,1.022-4.631,3.729-4.991,6.615
\t\tc-0.301,2.887,0.842,5.833,2.886,7.938c1.685,1.744,3.909,2.887,6.314,3.428v350.888v7.457v6.014
\t\tc-2.345,0.541-4.63,1.684-6.314,3.428c-2.044,2.104-3.187,5.052-2.886,7.938c0.301,2.887,2.285,5.593,4.991,6.615
\t\ts7.035-0.361,8.659-2.767c-1.083,1.563-3.909-0.421-4.69-2.164c-0.782-1.744-0.301-3.849,0.842-5.413
\t\tc1.623-2.164,4.57-3.187,7.276-2.886s5.172,1.804,7.035,3.788c1.864,1.984,3.128,4.45,4.09,6.976
\t\tc0.541,1.443,1.022,2.947,1.082,4.45c0.061,1.503-0.24,3.127-1.202,4.33c-0.902,1.202-2.466,1.984-4.029,1.804
\t\tc-1.504-0.24-3.007-1.624-2.946-3.127c-1.864,1.563-1.985,4.751-0.421,6.615c1.563,1.863,4.39,2.525,6.734,1.743
\t\tc2.346-0.781,4.45-2.946,5.412-5.231c0.061,6.073-2.164,12.207-6.134,16.838c-1.864,2.225-4.27,4.149-7.155,4.63
\t\tc-2.827,0.481-6.134-0.962-7.036-3.728c-0.421-1.384-0.24-2.947,0.602-4.21C45.402,518.275,46.785,517.434,48.229,517.313z"/>
\t<path fill="#440047" d="M63.863,531.145v-2.766h-2.766c-11.787,11.786-20.266,10.644-24.716,6.193l0.602,4.27
\t\tc0.602,4.57,5.593,7.156,9.682,5.052l3.668-1.864l-1.864,3.668c-2.104,4.089,0.481,9.021,5.052,9.682l4.27,0.602
\t\tC53.28,551.471,52.138,542.932,63.863,531.145z"/>
\t<polygon fill="#440047" points="37.825,554.478 44.26,554.478 44.26,548.043 37.825,548.043 \t"/>
\t<polygon fill="#440047" points="73.425,518.877 82.084,518.877 82.145,510.157 73.425,510.218 \t"/>
\t<path fill="#440047" d="M780.732,528.438h-2.766v2.767c11.786,11.786,10.644,20.266,6.193,24.715l4.27-0.601
\t\tc4.57-0.602,7.156-5.593,5.052-9.682l-1.864-3.669l3.668,1.864c4.09,2.104,9.021-0.48,9.682-5.051l0.602-4.27
\t\tC801.059,539.022,792.52,540.165,780.732,528.438z"/>
\t<path fill="#440047" d="M801.239,466.319V115.432c2.345-0.541,4.63-1.684,6.313-3.428c2.045-2.104,3.188-5.052,2.887-7.938
\t\tc-0.301-2.887-2.285-5.593-4.991-6.615s-7.036,0.36-8.659,2.767c1.082-1.564,3.908,0.421,4.69,2.164
\t\tc0.782,1.744,0.301,3.849-0.842,5.412c-1.624,2.165-4.57,3.188-7.276,2.887s-5.172-1.804-7.036-3.788s-3.127-4.45-4.089-6.976
\t\tc-0.541-1.443-1.022-2.947-1.082-4.45c-0.061-1.504,0.24-3.127,1.202-4.33c0.902-1.202,2.466-1.984,4.029-1.804
\t\tc1.503,0.24,3.007,1.623,2.946,3.127c1.864-1.563,1.984-4.751,0.421-6.615s-4.39-2.525-6.735-1.743
\t\tc-2.345,0.781-4.449,2.946-5.412,5.231c-0.06-6.074,2.165-12.208,6.134-16.838c1.864-2.225,4.27-4.149,7.156-4.631
\t\tc2.826-0.48,6.134,0.963,7.036,3.729c0.421,1.384,0.24,2.947-0.602,4.21c-0.782,1.202-2.165,2.044-3.668,2.165
\t\tc2.345,2.225,6.976,2.104,9.381-0.061c2.466-2.165,3.127-5.953,1.924-9.021c-1.202-3.007-4.148-5.231-7.336-5.953
\t\ts-6.615,0-9.441,1.503c-2.826,1.504-5.231,3.909-7.156,6.615c-4.449,6.194-6.313,14.433-3.969,21.648
\t\tc0.12,7.758,2.767,11.907,6.735,16.417c3.788,4.21,9.381,6.976,14.974,6.615v350.526v7.457v5.713
\t\tc-5.593-0.36-11.245,2.405-14.974,6.615c-3.969,4.51-6.615,8.659-6.735,16.416c-2.345,7.217-0.48,15.455,3.969,21.649
\t\tc1.925,2.646,4.27,5.051,7.156,6.614s6.254,2.226,9.441,1.504s6.073-2.887,7.336-5.954c1.203-3.006,0.542-6.855-1.924-9.02
\t\tc-2.466-2.165-7.036-2.346-9.381-0.061c1.443,0.12,2.826,0.902,3.668,2.165c0.781,1.202,1.022,2.826,0.602,4.209
\t\tc-0.902,2.767-4.21,4.21-7.036,3.729c-2.826-0.48-5.231-2.405-7.156-4.63c-3.969-4.631-6.193-10.765-6.134-16.838
\t\tc0.963,2.285,3.127,4.45,5.412,5.231c2.346,0.782,5.172,0.181,6.735-1.744c1.563-1.924,1.443-5.051-0.421-6.614
\t\tc0.061,1.503-1.443,2.886-2.946,3.127c-1.504,0.24-3.067-0.541-4.029-1.805c-0.902-1.202-1.263-2.826-1.202-4.329
\t\tc0.06-1.504,0.541-3.007,1.082-4.45c0.962-2.525,2.225-4.991,4.089-6.976s4.33-3.488,7.036-3.788
\t\tc2.706-0.301,5.593,0.721,7.276,2.886c1.143,1.504,1.624,3.669,0.842,5.412c-0.782,1.744-3.608,3.729-4.69,2.165
\t\tc1.623,2.405,5.953,3.789,8.659,2.767s4.631-3.729,4.991-6.615c0.361-2.887-0.842-5.833-2.887-7.938
\t\tc-1.684-1.744-3.908-2.887-6.313-3.428v-6.074V466.319z"/>
\t<polygon fill="#440047" points="797.631,554.478 804.065,554.478 804.065,548.043 797.631,548.043 \t"/>
\t<polygon fill="#440047" points="759.806,518.877 768.465,518.877 768.465,510.218 759.746,510.157 \t"/>
\t<path fill="#440047" d="M773.817,531.506c-6.194-4.45-14.433-6.314-21.648-3.969c-7.758,0.12-11.907,2.766-16.417,6.734
\t\tc-4.209,3.789-6.976,9.381-6.615,14.974h-5.713h-7.456H112.813c0.36-5.592-2.405-11.245-6.615-14.974
\t\tc-4.51-3.969-8.659-6.614-16.416-6.734c-7.217-2.346-15.455-0.481-21.649,3.969c-2.646,1.924-5.051,4.27-6.614,7.156
\t\tc-1.563,2.886-2.226,6.254-1.504,9.44c0.722,3.188,2.887,6.074,5.954,7.337c3.006,1.202,6.854,0.541,9.02-1.925
\t\tc2.165-2.465,2.346-7.035,0.061-9.381c-0.12,1.443-0.902,2.826-2.165,3.668c-1.263,0.843-2.826,1.022-4.209,0.602
\t\tc-2.767-0.901-4.21-4.209-3.729-7.035c0.48-2.827,2.405-5.232,4.63-7.156c4.631-3.969,10.765-6.194,16.838-6.134
\t\tc-2.285,0.962-4.45,3.127-5.231,5.412c-0.782,2.345-0.181,5.172,1.744,6.735c1.924,1.563,5.051,1.442,6.614-0.421
\t\tc-1.503,0.06-2.887-1.443-3.127-2.947c-0.24-1.503,0.541-3.066,1.804-4.029c1.203-0.901,2.827-1.263,4.33-1.202
\t\tc1.504,0.06,3.007,0.541,4.45,1.082c2.525,0.962,4.991,2.226,6.976,4.09c1.984,1.863,3.488,4.329,3.788,7.035
\t\tc0.301,2.706-0.722,5.593-2.886,7.276c-1.504,1.143-3.669,1.624-5.412,0.842c-1.744-0.781-3.729-3.607-2.165-4.69
\t\tc-2.405,1.624-3.789,5.953-2.767,8.659c1.022,2.707,3.729,4.631,6.615,4.991c2.887,0.301,5.833-0.842,7.938-2.886
\t\tc1.744-1.684,2.887-3.909,3.428-6.314h603.455h7.456h6.074c0.541,2.346,1.684,4.631,3.428,6.314
\t\tc2.104,2.044,5.051,3.187,7.938,2.886c2.887-0.3,5.593-2.284,6.615-4.991c1.021-2.706-0.361-7.035-2.767-8.659
\t\tc1.563,1.083-0.421,3.909-2.165,4.69c-1.744,0.782-3.849,0.301-5.412-0.842c-2.165-1.623-3.188-4.57-2.887-7.276
\t\ts1.805-5.171,3.789-7.035s4.45-3.128,6.976-4.09c1.443-0.541,2.946-1.022,4.45-1.082c1.503-0.061,3.127,0.24,4.329,1.202
\t\tc1.203,0.902,1.985,2.466,1.805,4.029c-0.241,1.504-1.624,3.007-3.127,2.947c1.563,1.863,4.75,1.984,6.614,0.421
\t\tc1.925-1.563,2.525-4.391,1.744-6.735c-0.781-2.346-2.946-4.45-5.231-5.412c6.073-0.061,12.207,2.165,16.838,6.134
\t\tc2.225,1.864,4.149,4.27,4.63,7.156c0.481,2.886-0.962,6.134-3.729,7.035c-1.383,0.421-2.946,0.241-4.209-0.602
\t\tc-1.263-0.842-2.045-2.164-2.165-3.668c-2.225,2.346-2.104,6.976,0.061,9.381c2.164,2.405,5.953,3.127,8.96,1.925
\t\tc3.007-1.203,5.231-4.149,5.953-7.337c0.722-3.187,0-6.614-1.503-9.44C778.929,535.835,776.463,533.37,773.817,531.506z"/>
</g>
<g>
\t<path d="M274.878,97.169v0.44c-0.499,0.015-0.877,0.07-1.133,0.165c-0.257,0.096-0.429,0.272-0.517,0.528
\t\tc-0.088,0.257-0.132,0.656-0.132,1.199v10.912c0,0.528,0.044,0.924,0.132,1.188c0.088,0.264,0.26,0.44,0.517,0.528
\t\tc0.256,0.088,0.634,0.147,1.133,0.176v0.44c-0.338-0.029-0.759-0.047-1.265-0.055c-0.506-0.007-1.016-0.011-1.529-0.011
\t\tc-0.572,0-1.108,0.004-1.606,0.011c-0.499,0.008-0.902,0.026-1.21,0.055v-0.44c0.499-0.029,0.876-0.088,1.133-0.176
\t\tc0.256-0.088,0.429-0.264,0.517-0.528c0.088-0.264,0.132-0.66,0.132-1.188V99.501c0-0.542-0.044-0.942-0.132-1.199
\t\tc-0.088-0.256-0.261-0.433-0.517-0.528c-0.257-0.095-0.635-0.15-1.133-0.165v-0.44c0.308,0.015,0.711,0.03,1.21,0.044
\t\tc0.499,0.015,1.034,0.022,1.606,0.022c0.513,0,1.023-0.007,1.529-0.022C274.119,97.199,274.54,97.184,274.878,97.169z"/>
\t<path d="M290.102,99.369v0.44c-0.499,0.015-0.876,0.088-1.133,0.22c-0.257,0.132-0.429,0.345-0.517,0.638
\t\tc-0.088,0.293-0.132,0.712-0.132,1.254v10.934c-0.088,0-0.173,0-0.253,0c-0.081,0-0.165,0-0.253,0l-8.8-12.188v9.526
\t\tc0,0.528,0.047,0.942,0.143,1.243c0.095,0.301,0.286,0.514,0.572,0.638c0.286,0.125,0.715,0.202,1.287,0.231v0.44
\t\tc-0.264-0.029-0.609-0.047-1.034-0.055c-0.426-0.007-0.829-0.011-1.21-0.011c-0.367,0-0.737,0.004-1.111,0.011
\t\tc-0.374,0.008-0.686,0.026-0.935,0.055v-0.44c0.499-0.029,0.876-0.106,1.133-0.231c0.256-0.125,0.429-0.337,0.517-0.638
\t\tc0.088-0.3,0.132-0.715,0.132-1.243v-8.492c0-0.542-0.044-0.942-0.132-1.199c-0.088-0.256-0.261-0.432-0.517-0.528
\t\tc-0.257-0.095-0.635-0.15-1.133-0.165v-0.44c0.25,0.015,0.561,0.029,0.935,0.044s0.744,0.022,1.111,0.022
\t\tc0.366,0,0.726-0.007,1.078-0.022s0.667-0.029,0.946-0.044l7.018,9.768v-7.216c0-0.542-0.048-0.96-0.143-1.254
\t\tc-0.096-0.293-0.286-0.506-0.572-0.638c-0.286-0.132-0.715-0.206-1.287-0.22v-0.44c0.264,0.015,0.612,0.029,1.045,0.044
\t\tc0.432,0.015,0.832,0.022,1.199,0.022c0.381,0,0.759-0.007,1.133-0.022S289.867,99.384,290.102,99.369z"/>
\t<path d="M296.306,99.061c0.733,0,1.291,0.084,1.672,0.253c0.381,0.169,0.718,0.356,1.012,0.561c0.176,0.103,0.319,0.18,0.429,0.231
\t\tc0.11,0.052,0.216,0.077,0.319,0.077c0.234,0,0.396-0.322,0.484-0.968h0.506c-0.015,0.22-0.033,0.484-0.055,0.792
\t\tc-0.022,0.308-0.037,0.719-0.044,1.232c-0.008,0.513-0.011,1.188-0.011,2.024h-0.506c-0.044-0.63-0.198-1.232-0.462-1.804
\t\tc-0.264-0.572-0.649-1.045-1.155-1.419c-0.506-0.374-1.169-0.561-1.991-0.561c-0.778,0-1.419,0.206-1.925,0.616
\t\tc-0.506,0.411-0.759,0.932-0.759,1.562c0,0.558,0.161,1.02,0.484,1.386c0.323,0.367,0.748,0.697,1.276,0.99
\t\tc0.528,0.293,1.1,0.616,1.716,0.968c0.762,0.396,1.448,0.8,2.057,1.21c0.608,0.411,1.089,0.862,1.441,1.353
\t\tc0.352,0.492,0.528,1.075,0.528,1.749c0,0.822-0.206,1.511-0.616,2.068c-0.411,0.557-0.957,0.975-1.639,1.254
\t\tc-0.682,0.278-1.434,0.418-2.255,0.418c-0.778,0-1.394-0.088-1.848-0.264c-0.455-0.176-0.844-0.359-1.166-0.55
\t\tc-0.176-0.117-0.323-0.198-0.44-0.242c-0.118-0.044-0.22-0.066-0.308-0.066c-0.235,0-0.396,0.323-0.484,0.968h-0.506
\t\tc0.029-0.278,0.047-0.612,0.055-1.001c0.007-0.388,0.015-0.891,0.022-1.507s0.011-1.393,0.011-2.332h0.506
\t\tc0.059,0.807,0.224,1.559,0.495,2.255c0.271,0.697,0.689,1.254,1.254,1.672c0.564,0.418,1.316,0.627,2.255,0.627
\t\tc0.44,0,0.869-0.084,1.287-0.253s0.759-0.437,1.023-0.803c0.264-0.366,0.396-0.843,0.396-1.43c0-0.777-0.327-1.404-0.979-1.881
\t\tc-0.653-0.477-1.493-0.979-2.519-1.507c-0.66-0.337-1.269-0.697-1.826-1.078c-0.558-0.381-1.009-0.825-1.353-1.331
\t\tc-0.345-0.506-0.517-1.118-0.517-1.837c0-0.763,0.194-1.397,0.583-1.903c0.388-0.506,0.898-0.887,1.529-1.144
\t\tC294.912,99.189,295.587,99.061,296.306,99.061z"/>
\t<path d="M315.577,99.369c-0.059,0.514-0.099,1.012-0.121,1.496s-0.033,0.851-0.033,1.1c0,0.352,0.007,0.686,0.022,1.001
\t\ts0.029,0.583,0.044,0.803h-0.506c-0.074-0.968-0.209-1.738-0.407-2.31c-0.198-0.572-0.51-0.979-0.935-1.221
\t\tc-0.426-0.242-1.034-0.363-1.826-0.363h-1.408v10.318c0,0.586,0.055,1.026,0.165,1.32s0.326,0.492,0.649,0.594
\t\tc0.323,0.103,0.792,0.169,1.408,0.198v0.44c-0.25-0.015-0.554-0.025-0.913-0.033c-0.359-0.007-0.737-0.014-1.133-0.022
\t\tc-0.396-0.007-0.792-0.011-1.188-0.011c-0.44,0-0.866,0.004-1.276,0.011c-0.411,0.008-0.785,0.015-1.122,0.022
\t\tc-0.338,0.008-0.624,0.019-0.858,0.033v-0.44c0.616-0.029,1.085-0.095,1.408-0.198c0.322-0.102,0.539-0.3,0.649-0.594
\t\ts0.165-0.733,0.165-1.32V99.875h-1.408c-0.792,0-1.4,0.121-1.826,0.363c-0.426,0.242-0.734,0.649-0.924,1.221
\t\tc-0.19,0.572-0.33,1.342-0.418,2.31h-0.506c0.029-0.234,0.047-0.506,0.055-0.814c0.007-0.308,0.011-0.638,0.011-0.99
\t\tc0-0.25-0.011-0.616-0.033-1.1s-0.063-0.982-0.121-1.496c0.616,0.015,1.287,0.029,2.013,0.044c0.726,0.015,1.455,0.022,2.189,0.022
\t\tc0.733,0,1.4,0,2.002,0c0.601,0,1.265,0,1.991,0s1.456-0.007,2.189-0.022S314.976,99.384,315.577,99.369z"/>
\t<path d="M322.859,99.369v0.44c-0.499,0.015-0.877,0.07-1.133,0.165c-0.257,0.096-0.429,0.271-0.517,0.528
\t\tc-0.088,0.257-0.132,0.657-0.132,1.199v8.712c0,0.528,0.044,0.924,0.132,1.188c0.088,0.264,0.26,0.44,0.517,0.528
\t\tc0.256,0.088,0.634,0.147,1.133,0.176v0.44c-0.338-0.029-0.759-0.047-1.265-0.055c-0.506-0.007-1.016-0.011-1.529-0.011
\t\tc-0.572,0-1.108,0.004-1.606,0.011c-0.499,0.008-0.902,0.026-1.21,0.055v-0.44c0.499-0.029,0.876-0.088,1.133-0.176
\t\tc0.256-0.088,0.429-0.264,0.517-0.528c0.088-0.264,0.132-0.66,0.132-1.188v-8.712c0-0.542-0.044-0.942-0.132-1.199
\t\tc-0.088-0.256-0.261-0.432-0.517-0.528c-0.257-0.095-0.635-0.15-1.133-0.165v-0.44c0.308,0.015,0.711,0.029,1.21,0.044
\t\ts1.034,0.022,1.606,0.022c0.513,0,1.023-0.007,1.529-0.022S322.521,99.384,322.859,99.369z"/>
\t<path d="M336.917,99.369c-0.059,0.514-0.099,1.012-0.121,1.496s-0.033,0.851-0.033,1.1c0,0.352,0.007,0.686,0.022,1.001
\t\ts0.029,0.583,0.044,0.803h-0.506c-0.074-0.968-0.209-1.738-0.407-2.31c-0.198-0.572-0.51-0.979-0.935-1.221
\t\tc-0.426-0.242-1.034-0.363-1.826-0.363h-1.408v10.318c0,0.586,0.055,1.026,0.165,1.32s0.326,0.492,0.649,0.594
\t\tc0.323,0.103,0.792,0.169,1.408,0.198v0.44c-0.25-0.015-0.554-0.025-0.913-0.033c-0.359-0.007-0.737-0.014-1.133-0.022
\t\tc-0.396-0.007-0.792-0.011-1.188-0.011c-0.44,0-0.866,0.004-1.276,0.011c-0.411,0.008-0.785,0.015-1.122,0.022
\t\tc-0.338,0.008-0.624,0.019-0.858,0.033v-0.44c0.616-0.029,1.085-0.095,1.408-0.198c0.322-0.102,0.539-0.3,0.649-0.594
\t\ts0.165-0.733,0.165-1.32V99.875h-1.408c-0.792,0-1.4,0.121-1.826,0.363c-0.426,0.242-0.734,0.649-0.924,1.221
\t\tc-0.19,0.572-0.33,1.342-0.418,2.31h-0.506c0.029-0.234,0.047-0.506,0.055-0.814c0.007-0.308,0.011-0.638,0.011-0.99
\t\tc0-0.25-0.011-0.616-0.033-1.1s-0.063-0.982-0.121-1.496c0.616,0.015,1.287,0.029,2.013,0.044c0.726,0.015,1.455,0.022,2.189,0.022
\t\tc0.733,0,1.4,0,2.002,0c0.601,0,1.265,0,1.991,0s1.456-0.007,2.189-0.022S336.315,99.384,336.917,99.369z"/>
\t<path d="M351.591,99.369v0.44c-0.499,0.015-0.876,0.088-1.133,0.22c-0.257,0.132-0.429,0.345-0.517,0.638
\t\tc-0.088,0.293-0.132,0.712-0.132,1.254v4.862c0,0.983-0.066,1.833-0.198,2.552c-0.132,0.719-0.367,1.35-0.704,1.892
\t\tc-0.338,0.543-0.84,0.983-1.507,1.32c-0.667,0.337-1.434,0.506-2.299,0.506c-0.66,0-1.291-0.07-1.892-0.209
\t\tc-0.602-0.139-1.129-0.393-1.584-0.759c-0.425-0.396-0.759-0.814-1.001-1.254s-0.407-0.975-0.495-1.606
\t\tc-0.088-0.63-0.132-1.422-0.132-2.376v-5.148c0-0.542-0.044-0.942-0.132-1.199c-0.088-0.256-0.261-0.432-0.517-0.528
\t\tc-0.257-0.095-0.635-0.15-1.133-0.165v-0.44c0.308,0.015,0.711,0.029,1.21,0.044s1.034,0.022,1.606,0.022
\t\tc0.513,0,1.023-0.007,1.529-0.022s0.927-0.029,1.265-0.044v0.44c-0.499,0.015-0.877,0.07-1.133,0.165
\t\tc-0.257,0.096-0.429,0.271-0.517,0.528c-0.088,0.257-0.132,0.657-0.132,1.199v5.522c0,0.924,0.069,1.771,0.209,2.541
\t\tc0.139,0.77,0.44,1.382,0.902,1.837c0.462,0.455,1.177,0.682,2.145,0.682c1.056,0,1.874-0.228,2.453-0.682
\t\tc0.579-0.455,0.982-1.082,1.21-1.881s0.341-1.698,0.341-2.695v-5.104c0-0.542-0.063-0.96-0.187-1.254
\t\tc-0.125-0.293-0.334-0.506-0.627-0.638c-0.294-0.132-0.69-0.206-1.188-0.22v-0.44c0.264,0.015,0.612,0.029,1.045,0.044
\t\tc0.432,0.015,0.832,0.022,1.199,0.022c0.381,0,0.759-0.007,1.133-0.022S351.356,99.384,351.591,99.369z"/>
\t<path d="M365.274,99.369c-0.059,0.514-0.099,1.012-0.121,1.496s-0.033,0.851-0.033,1.1c0,0.352,0.007,0.686,0.022,1.001
\t\ts0.029,0.583,0.044,0.803h-0.506c-0.074-0.968-0.209-1.738-0.407-2.31c-0.198-0.572-0.51-0.979-0.935-1.221
\t\tc-0.426-0.242-1.034-0.363-1.826-0.363h-1.408v10.318c0,0.586,0.055,1.026,0.165,1.32s0.326,0.492,0.649,0.594
\t\tc0.323,0.103,0.792,0.169,1.408,0.198v0.44c-0.25-0.015-0.554-0.025-0.913-0.033c-0.359-0.007-0.737-0.014-1.133-0.022
\t\tc-0.396-0.007-0.792-0.011-1.188-0.011c-0.44,0-0.866,0.004-1.276,0.011c-0.411,0.008-0.785,0.015-1.122,0.022
\t\tc-0.338,0.008-0.624,0.019-0.858,0.033v-0.44c0.616-0.029,1.085-0.095,1.408-0.198c0.322-0.102,0.539-0.3,0.649-0.594
\t\ts0.165-0.733,0.165-1.32V99.875h-1.408c-0.792,0-1.4,0.121-1.826,0.363c-0.426,0.242-0.734,0.649-0.924,1.221
\t\tc-0.19,0.572-0.33,1.342-0.418,2.31h-0.506c0.029-0.234,0.047-0.506,0.055-0.814c0.007-0.308,0.011-0.638,0.011-0.99
\t\tc0-0.25-0.011-0.616-0.033-1.1s-0.063-0.982-0.121-1.496c0.616,0.015,1.287,0.029,2.013,0.044c0.726,0.015,1.455,0.022,2.189,0.022
\t\tc0.733,0,1.4,0,2.002,0c0.601,0,1.265,0,1.991,0s1.456-0.007,2.189-0.022S364.673,99.384,365.274,99.369z"/>
\t<path d="M377.485,99.369c-0.059,0.47-0.099,0.924-0.121,1.364c-0.022,0.44-0.033,0.778-0.033,1.012
\t\tc0,0.572,0.022,1.027,0.066,1.364h-0.506c-0.103-1.217-0.356-2.06-0.759-2.53c-0.403-0.469-1.06-0.704-1.969-0.704h-1.606
\t\tc-0.499,0-0.877,0.041-1.133,0.121c-0.257,0.081-0.429,0.25-0.517,0.506c-0.088,0.257-0.132,0.657-0.132,1.199v8.712
\t\tc0,0.528,0.044,0.924,0.132,1.188c0.088,0.264,0.26,0.437,0.517,0.517c0.256,0.081,0.634,0.121,1.133,0.121h1.606
\t\tc1.085,0,1.855-0.268,2.31-0.803c0.454-0.535,0.74-1.492,0.858-2.871h0.506c-0.044,0.396-0.066,0.924-0.066,1.584
\t\tc0,0.25,0.011,0.616,0.033,1.1c0.022,0.484,0.063,0.983,0.121,1.496c-0.719-0.029-1.525-0.047-2.42-0.055
\t\tc-0.895-0.007-1.694-0.011-2.398-0.011c-0.426,0-0.99,0-1.694,0s-1.449,0.004-2.233,0.011c-0.785,0.008-1.529,0.026-2.233,0.055
\t\tv-0.44c0.499-0.029,0.876-0.088,1.133-0.176c0.256-0.088,0.429-0.264,0.517-0.528c0.088-0.264,0.132-0.66,0.132-1.188v-8.712
\t\tc0-0.542-0.044-0.942-0.132-1.199c-0.088-0.256-0.261-0.432-0.517-0.528c-0.257-0.095-0.635-0.15-1.133-0.165v-0.44
\t\tc0.704,0.015,1.448,0.029,2.233,0.044c0.784,0.015,1.529,0.022,2.233,0.022s1.268,0,1.694,0c0.645,0,1.375-0.003,2.189-0.011
\t\tS376.839,99.398,377.485,99.369z M374.295,105.793v0.44h-4.18v-0.44H374.295z M374.933,103.549
\t\tc-0.059,0.675-0.085,1.181-0.077,1.518c0.007,0.338,0.011,0.653,0.011,0.946s0.007,0.609,0.022,0.946
\t\tc0.014,0.338,0.051,0.844,0.11,1.518h-0.506c-0.044-0.366-0.118-0.718-0.22-1.056c-0.103-0.337-0.279-0.619-0.528-0.847
\t\tc-0.25-0.228-0.631-0.341-1.144-0.341v-0.44c0.513,0,0.891-0.132,1.133-0.396c0.242-0.264,0.407-0.572,0.495-0.924
\t\ts0.154-0.66,0.198-0.924H374.933z"/>
\t<path d="M392.532,99.061c1.349,0,2.53,0.268,3.542,0.803c1.012,0.536,1.8,1.313,2.365,2.332c0.564,1.02,0.847,2.262,0.847,3.729
\t\tc0,1.422-0.29,2.669-0.869,3.74c-0.58,1.071-1.379,1.903-2.398,2.497c-1.02,0.594-2.189,0.891-3.509,0.891
\t\tc-1.35,0-2.53-0.268-3.542-0.803c-1.012-0.535-1.8-1.316-2.365-2.343c-0.565-1.026-0.847-2.266-0.847-3.718
\t\tc0-1.422,0.29-2.669,0.869-3.74c0.579-1.07,1.378-1.903,2.398-2.497C390.042,99.358,391.212,99.061,392.532,99.061z
\t\t M392.444,99.457c-0.91,0-1.702,0.283-2.376,0.847c-0.675,0.565-1.196,1.346-1.562,2.343c-0.367,0.998-0.55,2.149-0.55,3.454
\t\tc0,1.335,0.209,2.494,0.627,3.476c0.418,0.983,0.979,1.742,1.683,2.277c0.704,0.536,1.481,0.803,2.332,0.803
\t\tc0.909,0,1.701-0.282,2.376-0.847c0.675-0.564,1.195-1.349,1.562-2.354c0.367-1.004,0.55-2.152,0.55-3.443
\t\tc0-1.349-0.209-2.512-0.627-3.487c-0.418-0.975-0.976-1.73-1.672-2.266C394.09,99.725,393.31,99.457,392.444,99.457z"/>
\t<path d="M411.716,99.369c-0.059,0.47-0.099,0.924-0.121,1.364c-0.022,0.44-0.033,0.778-0.033,1.012
\t\tc0,0.572,0.022,1.027,0.066,1.364h-0.506c-0.103-1.217-0.356-2.06-0.759-2.53c-0.403-0.469-1.06-0.704-1.969-0.704h-1.606
\t\tc-0.499,0-0.877,0.041-1.133,0.121c-0.257,0.081-0.429,0.25-0.517,0.506c-0.088,0.257-0.132,0.657-0.132,1.199v8.712
\t\tc0,0.528,0.055,0.924,0.165,1.188s0.326,0.44,0.649,0.528c0.323,0.088,0.792,0.147,1.408,0.176v0.44
\t\tc-0.382-0.029-0.866-0.047-1.452-0.055c-0.587-0.007-1.181-0.011-1.782-0.011c-0.572,0-1.108,0.004-1.606,0.011
\t\tc-0.499,0.008-0.902,0.026-1.21,0.055v-0.44c0.499-0.029,0.876-0.088,1.133-0.176c0.256-0.088,0.429-0.264,0.517-0.528
\t\tc0.088-0.264,0.132-0.66,0.132-1.188v-8.712c0-0.542-0.044-0.942-0.132-1.199c-0.088-0.256-0.261-0.432-0.517-0.528
\t\tc-0.257-0.095-0.635-0.15-1.133-0.165v-0.44c0.704,0.015,1.448,0.029,2.233,0.044c0.784,0.015,1.529,0.022,2.233,0.022
\t\ts1.268,0,1.694,0c0.645,0,1.375-0.003,2.189-0.011S411.071,99.398,411.716,99.369z M408.526,105.793v0.44h-4.18v-0.44H408.526z
\t\t M409.165,103.549c-0.059,0.675-0.085,1.181-0.077,1.518c0.007,0.338,0.011,0.653,0.011,0.946s0.007,0.609,0.022,0.946
\t\tc0.014,0.338,0.051,0.844,0.11,1.518h-0.506c-0.044-0.366-0.118-0.718-0.22-1.056c-0.103-0.337-0.279-0.619-0.528-0.847
\t\tc-0.25-0.228-0.631-0.341-1.144-0.341v-0.44c0.513,0,0.891-0.132,1.133-0.396c0.242-0.264,0.407-0.572,0.495-0.924
\t\ts0.154-0.66,0.198-0.924H409.165z"/>
\t<path d="M423.707,97.059l5.367,13.838c0.221,0.558,0.47,0.928,0.748,1.111s0.535,0.282,0.771,0.297v0.44
\t\tc-0.294-0.029-0.653-0.047-1.078-0.055c-0.426-0.007-0.851-0.011-1.276-0.011c-0.571,0-1.107,0.004-1.605,0.011
\t\tc-0.499,0.008-0.902,0.026-1.21,0.055v-0.44c0.748-0.029,1.225-0.158,1.43-0.385c0.205-0.227,0.161-0.715-0.132-1.463
\t\tl-4.092-10.934l0.352-0.286l-3.828,9.944c-0.308,0.792-0.455,1.412-0.44,1.859c0.015,0.448,0.184,0.767,0.506,0.957
\t\tc0.322,0.191,0.792,0.293,1.408,0.308v0.44c-0.411-0.029-0.84-0.047-1.287-0.055c-0.447-0.007-0.861-0.011-1.243-0.011
\t\tc-0.367,0-0.679,0.004-0.935,0.011c-0.257,0.008-0.495,0.026-0.715,0.055v-0.44c0.293-0.073,0.594-0.246,0.902-0.517
\t\tc0.308-0.271,0.586-0.737,0.836-1.397l5.17-13.332c0.059,0,0.117,0,0.176,0S423.647,97.059,423.707,97.059z M426.391,106.409v0.44
\t\th-6.556l0.22-0.44H426.391z"/>
\t<path d="M435.498,99.369v0.44c-0.748,0.03-1.218,0.154-1.408,0.374s-0.146,0.711,0.132,1.474l3.234,8.822l-0.308,0.154l3.453-8.162
\t\tl0.33,0.44l-4.246,9.944c-0.059,0-0.117,0-0.176,0s-0.117,0-0.176,0l-4.488-11.638c-0.22-0.558-0.47-0.928-0.748-1.111
\t\tc-0.278-0.183-0.535-0.282-0.77-0.297v-0.44c0.293,0.015,0.656,0.029,1.089,0.044s0.854,0.022,1.265,0.022
\t\tc0.572,0,1.107-0.007,1.606-0.022C434.786,99.398,435.19,99.384,435.498,99.369z M449.578,99.369v0.44
\t\tc-0.396,0.088-0.729,0.297-1.001,0.627s-0.532,0.847-0.781,1.551l-3.784,10.868c-0.059,0-0.117,0-0.176,0s-0.117,0-0.176,0
\t\tl-4.708-11.638c-0.235-0.558-0.488-0.928-0.759-1.111c-0.271-0.183-0.525-0.282-0.76-0.297v-0.44
\t\tc0.294,0.015,0.656,0.029,1.09,0.044c0.432,0.015,0.854,0.022,1.265,0.022c0.572,0,1.107-0.007,1.605-0.022
\t\tc0.499-0.015,0.902-0.029,1.21-0.044v0.44c-0.747,0.03-1.217,0.158-1.407,0.385c-0.191,0.228-0.147,0.715,0.132,1.463l3.322,8.734
\t\tl-0.309,0.132l2.53-7.392c0.308-0.88,0.451-1.555,0.429-2.024c-0.021-0.469-0.176-0.799-0.462-0.99
\t\tc-0.286-0.19-0.679-0.293-1.177-0.308v-0.44c0.22,0.015,0.51,0.029,0.869,0.044s0.736,0.022,1.133,0.022
\t\tc0.264,0,0.604-0.007,1.022-0.022S449.402,99.384,449.578,99.369z"/>
\t<path d="M461.105,99.369c-0.059,0.47-0.099,0.924-0.121,1.364c-0.021,0.44-0.033,0.778-0.033,1.012
\t\tc0,0.572,0.022,1.027,0.066,1.364h-0.506c-0.103-1.217-0.355-2.06-0.759-2.53c-0.404-0.469-1.061-0.704-1.97-0.704h-1.605
\t\tc-0.499,0-0.877,0.041-1.133,0.121c-0.257,0.081-0.43,0.25-0.518,0.506c-0.088,0.257-0.132,0.657-0.132,1.199v8.712
\t\tc0,0.528,0.044,0.924,0.132,1.188s0.261,0.437,0.518,0.517c0.256,0.081,0.634,0.121,1.133,0.121h1.605
\t\tc1.086,0,1.855-0.268,2.311-0.803c0.454-0.535,0.74-1.492,0.857-2.871h0.507c-0.045,0.396-0.066,0.924-0.066,1.584
\t\tc0,0.25,0.011,0.616,0.033,1.1c0.021,0.484,0.062,0.983,0.121,1.496c-0.719-0.029-1.526-0.047-2.42-0.055
\t\tc-0.896-0.007-1.694-0.011-2.398-0.011c-0.426,0-0.99,0-1.694,0s-1.448,0.004-2.232,0.011c-0.785,0.008-1.529,0.026-2.233,0.055
\t\tv-0.44c0.499-0.029,0.876-0.088,1.133-0.176c0.257-0.088,0.43-0.264,0.518-0.528s0.132-0.66,0.132-1.188v-8.712
\t\tc0-0.542-0.044-0.942-0.132-1.199c-0.088-0.256-0.261-0.432-0.518-0.528c-0.257-0.095-0.634-0.15-1.133-0.165v-0.44
\t\tc0.704,0.015,1.448,0.029,2.233,0.044c0.784,0.015,1.528,0.022,2.232,0.022s1.269,0,1.694,0c0.646,0,1.375-0.003,2.189-0.011
\t\tC459.73,99.417,460.46,99.398,461.105,99.369z M457.916,105.793v0.44h-4.181v-0.44H457.916z M458.554,103.549
\t\tc-0.059,0.675-0.085,1.181-0.077,1.518c0.007,0.338,0.011,0.653,0.011,0.946s0.008,0.609,0.022,0.946
\t\tc0.015,0.338,0.051,0.844,0.109,1.518h-0.506c-0.044-0.366-0.117-0.718-0.22-1.056c-0.103-0.337-0.278-0.619-0.528-0.847
\t\tc-0.249-0.228-0.631-0.341-1.144-0.341v-0.44c0.513,0,0.891-0.132,1.133-0.396c0.242-0.264,0.407-0.572,0.495-0.924
\t\ts0.154-0.66,0.198-0.924H458.554z"/>
\t<path d="M468.365,99.061c0.733,0,1.291,0.084,1.672,0.253c0.382,0.169,0.719,0.356,1.013,0.561c0.176,0.103,0.318,0.18,0.429,0.231
\t\tc0.11,0.052,0.216,0.077,0.319,0.077c0.234,0,0.396-0.322,0.483-0.968h0.506c-0.015,0.22-0.032,0.484-0.055,0.792
\t\tc-0.022,0.308-0.037,0.719-0.044,1.232c-0.008,0.513-0.011,1.188-0.011,2.024h-0.506c-0.045-0.63-0.198-1.232-0.463-1.804
\t\tc-0.264-0.572-0.648-1.045-1.154-1.419s-1.17-0.561-1.991-0.561c-0.777,0-1.419,0.206-1.925,0.616s-0.759,0.932-0.759,1.562
\t\tc0,0.558,0.161,1.02,0.483,1.386c0.322,0.367,0.748,0.697,1.276,0.99c0.527,0.293,1.1,0.616,1.716,0.968
\t\tc0.763,0.396,1.448,0.8,2.057,1.21c0.608,0.411,1.089,0.862,1.441,1.353c0.352,0.492,0.528,1.075,0.528,1.749
\t\tc0,0.822-0.206,1.511-0.616,2.068c-0.411,0.557-0.957,0.975-1.64,1.254c-0.682,0.278-1.434,0.418-2.255,0.418
\t\tc-0.777,0-1.394-0.088-1.848-0.264c-0.455-0.176-0.844-0.359-1.166-0.55c-0.176-0.117-0.323-0.198-0.44-0.242
\t\ts-0.22-0.066-0.308-0.066c-0.234,0-0.396,0.323-0.484,0.968h-0.506c0.029-0.278,0.048-0.612,0.056-1.001
\t\tc0.007-0.388,0.014-0.891,0.021-1.507s0.011-1.393,0.011-2.332h0.507c0.058,0.807,0.223,1.559,0.494,2.255
\t\tc0.271,0.697,0.689,1.254,1.254,1.672s1.316,0.627,2.256,0.627c0.439,0,0.868-0.084,1.287-0.253
\t\tc0.418-0.168,0.759-0.437,1.022-0.803c0.264-0.366,0.396-0.843,0.396-1.43c0-0.777-0.327-1.404-0.979-1.881
\t\ts-1.492-0.979-2.519-1.507c-0.66-0.337-1.27-0.697-1.826-1.078c-0.558-0.381-1.009-0.825-1.354-1.331s-0.517-1.118-0.517-1.837
\t\tc0-0.763,0.194-1.397,0.583-1.903c0.389-0.506,0.898-0.887,1.529-1.144C466.972,99.189,467.646,99.061,468.365,99.061z"/>
\t<path d="M482.446,99.061c1.349,0,2.529,0.268,3.542,0.803c1.012,0.536,1.8,1.313,2.364,2.332s0.848,2.262,0.848,3.729
\t\tc0,1.422-0.29,2.669-0.869,3.74c-0.58,1.071-1.379,1.903-2.398,2.497c-1.02,0.594-2.188,0.891-3.509,0.891
\t\tc-1.35,0-2.53-0.268-3.542-0.803s-1.801-1.316-2.365-2.343c-0.564-1.026-0.847-2.266-0.847-3.718c0-1.422,0.289-2.669,0.869-3.74
\t\tc0.579-1.07,1.378-1.903,2.397-2.497C479.956,99.358,481.126,99.061,482.446,99.061z M482.357,99.457
\t\tc-0.909,0-1.701,0.283-2.376,0.847c-0.675,0.565-1.195,1.346-1.562,2.343c-0.367,0.998-0.55,2.149-0.55,3.454
\t\tc0,1.335,0.209,2.494,0.627,3.476c0.418,0.983,0.979,1.742,1.683,2.277c0.704,0.536,1.481,0.803,2.332,0.803
\t\tc0.909,0,1.701-0.282,2.376-0.847c0.675-0.564,1.195-1.349,1.563-2.354c0.366-1.004,0.55-2.152,0.55-3.443
\t\tc0-1.349-0.209-2.512-0.627-3.487c-0.418-0.975-0.976-1.73-1.672-2.266C484.004,99.725,483.223,99.457,482.357,99.457z"/>
\t<path d="M507.966,99.369v0.44c-0.499,0.015-0.876,0.07-1.133,0.165c-0.257,0.096-0.429,0.271-0.517,0.528
\t\tc-0.089,0.257-0.133,0.657-0.133,1.199v8.712c0,0.528,0.044,0.924,0.133,1.188c0.088,0.264,0.26,0.44,0.517,0.528
\t\tc0.257,0.088,0.634,0.147,1.133,0.176v0.44c-0.338-0.029-0.759-0.047-1.265-0.055c-0.506-0.007-1.016-0.011-1.529-0.011
\t\tc-0.572,0-1.107,0.004-1.605,0.011c-0.499,0.008-0.902,0.026-1.21,0.055v-0.44c0.498-0.029,0.876-0.088,1.133-0.176
\t\tc0.256-0.088,0.429-0.264,0.517-0.528s0.132-0.66,0.132-1.188v-9.658l-5.302,12.1h-0.352l-5.104-12.078v9.416
\t\tc0,0.528,0.048,0.942,0.144,1.243c0.095,0.301,0.285,0.514,0.571,0.638s0.715,0.202,1.287,0.231v0.44
\t\tc-0.264-0.029-0.608-0.047-1.034-0.055c-0.425-0.007-0.828-0.011-1.21-0.011c-0.366,0-0.736,0.004-1.11,0.011
\t\tc-0.374,0.008-0.687,0.026-0.936,0.055v-0.44c0.499-0.029,0.876-0.106,1.133-0.231s0.43-0.337,0.518-0.638
\t\tc0.088-0.3,0.132-0.715,0.132-1.243v-8.492c0-0.542-0.044-0.942-0.132-1.199c-0.088-0.256-0.261-0.432-0.518-0.528
\t\tc-0.257-0.095-0.634-0.15-1.133-0.165v-0.44c0.249,0.015,0.562,0.029,0.936,0.044s0.744,0.022,1.11,0.022
\t\tc0.367,0,0.7-0.007,1.001-0.022s0.591-0.029,0.869-0.044l4.488,10.89l-0.309,0.352l4.95-11.176c0.161,0,0.33,0,0.506,0
\t\tc0.177,0,0.353,0,0.528,0c0.514,0,1.023-0.007,1.529-0.022S507.628,99.384,507.966,99.369z"/>
\t<path d="M520.352,99.369c-0.059,0.47-0.099,0.924-0.121,1.364c-0.021,0.44-0.033,0.778-0.033,1.012
\t\tc0,0.572,0.022,1.027,0.066,1.364h-0.506c-0.103-1.217-0.355-2.06-0.759-2.53c-0.404-0.469-1.061-0.704-1.97-0.704h-1.605
\t\tc-0.499,0-0.877,0.041-1.133,0.121c-0.257,0.081-0.43,0.25-0.518,0.506c-0.088,0.257-0.132,0.657-0.132,1.199v8.712
\t\tc0,0.528,0.044,0.924,0.132,1.188s0.261,0.437,0.518,0.517c0.256,0.081,0.634,0.121,1.133,0.121h1.605
\t\tc1.086,0,1.855-0.268,2.311-0.803c0.454-0.535,0.74-1.492,0.857-2.871h0.507c-0.045,0.396-0.066,0.924-0.066,1.584
\t\tc0,0.25,0.011,0.616,0.033,1.1c0.021,0.484,0.062,0.983,0.121,1.496c-0.719-0.029-1.526-0.047-2.42-0.055
\t\tc-0.896-0.007-1.694-0.011-2.398-0.011c-0.426,0-0.99,0-1.694,0s-1.448,0.004-2.232,0.011c-0.785,0.008-1.529,0.026-2.233,0.055
\t\tv-0.44c0.499-0.029,0.876-0.088,1.133-0.176c0.257-0.088,0.43-0.264,0.518-0.528s0.132-0.66,0.132-1.188v-8.712
\t\tc0-0.542-0.044-0.942-0.132-1.199c-0.088-0.256-0.261-0.432-0.518-0.528c-0.257-0.095-0.634-0.15-1.133-0.165v-0.44
\t\tc0.704,0.015,1.448,0.029,2.233,0.044c0.784,0.015,1.528,0.022,2.232,0.022s1.269,0,1.694,0c0.646,0,1.375-0.003,2.189-0.011
\t\tC518.977,99.417,519.706,99.398,520.352,99.369z M517.162,105.793v0.44h-4.181v-0.44H517.162z M517.8,103.549
\t\tc-0.059,0.675-0.085,1.181-0.077,1.518c0.007,0.338,0.011,0.653,0.011,0.946s0.008,0.609,0.022,0.946
\t\tc0.015,0.338,0.051,0.844,0.109,1.518h-0.506c-0.044-0.366-0.117-0.718-0.22-1.056c-0.103-0.337-0.278-0.619-0.528-0.847
\t\tc-0.249-0.228-0.631-0.341-1.144-0.341v-0.44c0.513,0,0.891-0.132,1.133-0.396c0.242-0.264,0.407-0.572,0.495-0.924
\t\ts0.154-0.66,0.198-0.924H517.8z"/>
\t<path d="M536.279,99.369v0.44c-0.499,0.015-0.876,0.088-1.133,0.22c-0.257,0.132-0.429,0.345-0.517,0.638
\t\tc-0.089,0.293-0.133,0.712-0.133,1.254v10.934c-0.088,0-0.172,0-0.253,0c-0.08,0-0.165,0-0.253,0l-8.8-12.188v9.526
\t\tc0,0.528,0.048,0.942,0.144,1.243c0.095,0.301,0.285,0.514,0.571,0.638s0.715,0.202,1.287,0.231v0.44
\t\tc-0.264-0.029-0.608-0.047-1.034-0.055c-0.425-0.007-0.828-0.011-1.21-0.011c-0.366,0-0.736,0.004-1.11,0.011
\t\tc-0.374,0.008-0.687,0.026-0.936,0.055v-0.44c0.499-0.029,0.876-0.106,1.133-0.231s0.43-0.337,0.518-0.638
\t\tc0.088-0.3,0.132-0.715,0.132-1.243v-8.492c0-0.542-0.044-0.942-0.132-1.199c-0.088-0.256-0.261-0.432-0.518-0.528
\t\tc-0.257-0.095-0.634-0.15-1.133-0.165v-0.44c0.249,0.015,0.562,0.029,0.936,0.044s0.744,0.022,1.11,0.022
\t\tc0.367,0,0.727-0.007,1.078-0.022c0.353-0.015,0.667-0.029,0.946-0.044l7.018,9.768v-7.216c0-0.542-0.048-0.96-0.143-1.254
\t\tc-0.096-0.293-0.286-0.506-0.572-0.638c-0.286-0.132-0.715-0.206-1.287-0.22v-0.44c0.265,0.015,0.612,0.029,1.045,0.044
\t\ts0.833,0.022,1.199,0.022c0.381,0,0.759-0.007,1.133-0.022S536.045,99.384,536.279,99.369z"/>
\t<path d="M548.379,99.369c-0.059,0.47-0.099,0.924-0.121,1.364c-0.021,0.44-0.033,0.778-0.033,1.012
\t\tc0,0.572,0.022,1.027,0.066,1.364h-0.506c-0.103-1.217-0.355-2.06-0.759-2.53c-0.404-0.469-1.061-0.704-1.97-0.704h-1.605
\t\tc-0.499,0-0.877,0.041-1.133,0.121c-0.257,0.081-0.43,0.25-0.518,0.506c-0.088,0.257-0.132,0.657-0.132,1.199v8.712
\t\tc0,0.528,0.044,0.924,0.132,1.188s0.261,0.437,0.518,0.517c0.256,0.081,0.634,0.121,1.133,0.121h1.605
\t\tc1.086,0,1.855-0.268,2.311-0.803c0.454-0.535,0.74-1.492,0.857-2.871h0.507c-0.045,0.396-0.066,0.924-0.066,1.584
\t\tc0,0.25,0.011,0.616,0.033,1.1c0.021,0.484,0.062,0.983,0.121,1.496c-0.719-0.029-1.526-0.047-2.42-0.055
\t\tc-0.896-0.007-1.694-0.011-2.398-0.011c-0.426,0-0.99,0-1.694,0s-1.448,0.004-2.232,0.011c-0.785,0.008-1.529,0.026-2.233,0.055
\t\tv-0.44c0.499-0.029,0.876-0.088,1.133-0.176c0.257-0.088,0.43-0.264,0.518-0.528s0.132-0.66,0.132-1.188v-8.712
\t\tc0-0.542-0.044-0.942-0.132-1.199c-0.088-0.256-0.261-0.432-0.518-0.528c-0.257-0.095-0.634-0.15-1.133-0.165v-0.44
\t\tc0.704,0.015,1.448,0.029,2.233,0.044c0.784,0.015,1.528,0.022,2.232,0.022s1.269,0,1.694,0c0.646,0,1.375-0.003,2.189-0.011
\t\tC547.004,99.417,547.733,99.398,548.379,99.369z M545.189,105.793v0.44h-4.181v-0.44H545.189z M545.827,103.549
\t\tc-0.059,0.675-0.085,1.181-0.077,1.518c0.007,0.338,0.011,0.653,0.011,0.946s0.008,0.609,0.022,0.946
\t\tc0.015,0.338,0.051,0.844,0.109,1.518h-0.506c-0.044-0.366-0.117-0.718-0.22-1.056c-0.103-0.337-0.278-0.619-0.528-0.847
\t\tc-0.249-0.228-0.631-0.341-1.144-0.341v-0.44c0.513,0,0.891-0.132,1.133-0.396c0.242-0.264,0.407-0.572,0.495-0.924
\t\ts0.154-0.66,0.198-0.924H545.827z"/>
\t<path d="M555.639,99.061c0.733,0,1.291,0.084,1.672,0.253c0.382,0.169,0.719,0.356,1.013,0.561c0.176,0.103,0.318,0.18,0.429,0.231
\t\tc0.11,0.052,0.216,0.077,0.319,0.077c0.234,0,0.396-0.322,0.483-0.968h0.506c-0.015,0.22-0.032,0.484-0.055,0.792
\t\tc-0.022,0.308-0.037,0.719-0.044,1.232c-0.008,0.513-0.011,1.188-0.011,2.024h-0.506c-0.045-0.63-0.198-1.232-0.463-1.804
\t\tc-0.264-0.572-0.648-1.045-1.154-1.419s-1.17-0.561-1.991-0.561c-0.777,0-1.419,0.206-1.925,0.616s-0.759,0.932-0.759,1.562
\t\tc0,0.558,0.161,1.02,0.483,1.386c0.322,0.367,0.748,0.697,1.276,0.99c0.527,0.293,1.1,0.616,1.716,0.968
\t\tc0.763,0.396,1.448,0.8,2.057,1.21c0.608,0.411,1.089,0.862,1.441,1.353c0.352,0.492,0.528,1.075,0.528,1.749
\t\tc0,0.822-0.206,1.511-0.616,2.068c-0.411,0.557-0.957,0.975-1.64,1.254c-0.682,0.278-1.434,0.418-2.255,0.418
\t\tc-0.777,0-1.394-0.088-1.848-0.264c-0.455-0.176-0.844-0.359-1.166-0.55c-0.176-0.117-0.323-0.198-0.44-0.242
\t\ts-0.22-0.066-0.308-0.066c-0.234,0-0.396,0.323-0.484,0.968h-0.506c0.029-0.278,0.048-0.612,0.056-1.001
\t\tc0.007-0.388,0.014-0.891,0.021-1.507s0.011-1.393,0.011-2.332h0.507c0.058,0.807,0.223,1.559,0.494,2.255
\t\tc0.271,0.697,0.689,1.254,1.254,1.672s1.316,0.627,2.256,0.627c0.439,0,0.868-0.084,1.287-0.253
\t\tc0.418-0.168,0.759-0.437,1.022-0.803c0.264-0.366,0.396-0.843,0.396-1.43c0-0.777-0.327-1.404-0.979-1.881
\t\ts-1.492-0.979-2.519-1.507c-0.66-0.337-1.27-0.697-1.826-1.078c-0.558-0.381-1.009-0.825-1.354-1.331s-0.517-1.118-0.517-1.837
\t\tc0-0.763,0.194-1.397,0.583-1.903c0.389-0.506,0.898-0.887,1.529-1.144C554.245,99.189,554.92,99.061,555.639,99.061z"/>
\t<path d="M567.408,99.061c0.733,0,1.291,0.084,1.672,0.253c0.382,0.169,0.719,0.356,1.013,0.561c0.176,0.103,0.318,0.18,0.429,0.231
\t\tc0.11,0.052,0.216,0.077,0.319,0.077c0.234,0,0.396-0.322,0.483-0.968h0.506c-0.015,0.22-0.032,0.484-0.055,0.792
\t\tc-0.022,0.308-0.037,0.719-0.044,1.232c-0.008,0.513-0.011,1.188-0.011,2.024h-0.506c-0.045-0.63-0.198-1.232-0.463-1.804
\t\tc-0.264-0.572-0.648-1.045-1.154-1.419s-1.17-0.561-1.991-0.561c-0.777,0-1.419,0.206-1.925,0.616s-0.759,0.932-0.759,1.562
\t\tc0,0.558,0.161,1.02,0.483,1.386c0.322,0.367,0.748,0.697,1.276,0.99c0.527,0.293,1.1,0.616,1.716,0.968
\t\tc0.763,0.396,1.448,0.8,2.057,1.21c0.608,0.411,1.089,0.862,1.441,1.353c0.352,0.492,0.528,1.075,0.528,1.749
\t\tc0,0.822-0.206,1.511-0.616,2.068c-0.411,0.557-0.957,0.975-1.64,1.254c-0.682,0.278-1.434,0.418-2.255,0.418
\t\tc-0.777,0-1.394-0.088-1.848-0.264c-0.455-0.176-0.844-0.359-1.166-0.55c-0.176-0.117-0.323-0.198-0.44-0.242
\t\ts-0.22-0.066-0.308-0.066c-0.234,0-0.396,0.323-0.484,0.968h-0.506c0.029-0.278,0.048-0.612,0.056-1.001
\t\tc0.007-0.388,0.014-0.891,0.021-1.507s0.011-1.393,0.011-2.332h0.507c0.058,0.807,0.223,1.559,0.494,2.255
\t\tc0.271,0.697,0.689,1.254,1.254,1.672s1.316,0.627,2.256,0.627c0.439,0,0.868-0.084,1.287-0.253
\t\tc0.418-0.168,0.759-0.437,1.022-0.803c0.264-0.366,0.396-0.843,0.396-1.43c0-0.777-0.327-1.404-0.979-1.881
\t\ts-1.492-0.979-2.519-1.507c-0.66-0.337-1.27-0.697-1.826-1.078c-0.558-0.381-1.009-0.825-1.354-1.331s-0.517-1.118-0.517-1.837
\t\tc0-0.763,0.194-1.397,0.583-1.903c0.389-0.506,0.898-0.887,1.529-1.144C566.015,99.189,566.689,99.061,567.408,99.061z"/>
</g>
<g>
\t<path d="M348.568,246.984c-0.048,0.457-0.081,0.897-0.099,1.323s-0.027,0.753-0.027,0.981c0,0.288,0.006,0.558,0.018,0.81
\t\ts0.024,0.474,0.036,0.666h-0.414c-0.084-0.84-0.198-1.503-0.342-1.989c-0.144-0.486-0.396-0.837-0.756-1.053
\t\tc-0.36-0.216-0.918-0.324-1.674-0.324h-0.972v10.242c0,0.48,0.045,0.84,0.135,1.08s0.267,0.402,0.531,0.485
\t\tc0.264,0.085,0.648,0.139,1.152,0.162v0.36c-0.313-0.023-0.708-0.039-1.188-0.045c-0.48-0.006-0.966-0.009-1.458-0.009
\t\tc-0.54,0-1.047,0.003-1.521,0.009c-0.474,0.006-0.855,0.021-1.143,0.045v-0.36c0.504-0.023,0.888-0.077,1.152-0.162
\t\tc0.264-0.083,0.441-0.245,0.531-0.485c0.09-0.24,0.135-0.6,0.135-1.08v-10.242h-0.972c-0.744,0-1.299,0.108-1.665,0.324
\t\tc-0.366,0.216-0.621,0.567-0.765,1.053c-0.144,0.486-0.258,1.149-0.342,1.989h-0.414c0.023-0.192,0.039-0.414,0.045-0.666
\t\tc0.006-0.252,0.009-0.522,0.009-0.81c0-0.228-0.009-0.555-0.027-0.981s-0.051-0.867-0.099-1.323
\t\tc0.504,0.012,1.053,0.024,1.647,0.036c0.594,0.012,1.191,0.018,1.791,0.018s1.146,0,1.638,0c0.492,0,1.035,0,1.629,0
\t\tc0.594,0,1.191-0.006,1.791-0.018C347.53,247.009,348.076,246.997,348.568,246.984z"/>
\t<path d="M352.33,245.634v6.732c0.312-0.816,0.747-1.38,1.305-1.692c0.558-0.312,1.143-0.468,1.755-0.468
\t\tc0.456,0,0.837,0.06,1.143,0.18c0.306,0.12,0.561,0.288,0.765,0.504c0.228,0.24,0.39,0.54,0.486,0.9
\t\tc0.096,0.36,0.144,0.846,0.144,1.458v4.807c0,0.504,0.105,0.846,0.315,1.025c0.209,0.181,0.561,0.271,1.053,0.271v0.378
\t\tc-0.204-0.012-0.516-0.027-0.936-0.045c-0.42-0.019-0.828-0.027-1.224-0.027s-0.783,0.009-1.161,0.027
\t\tc-0.378,0.018-0.664,0.033-0.855,0.045v-0.378c0.432,0,0.738-0.09,0.918-0.271c0.18-0.18,0.27-0.521,0.27-1.025v-5.202
\t\tc0-0.372-0.03-0.714-0.09-1.026s-0.195-0.564-0.405-0.756c-0.21-0.191-0.531-0.288-0.963-0.288c-0.732,0-1.335,0.303-1.809,0.909
\t\tc-0.474,0.606-0.711,1.39-0.711,2.349v4.015c0,0.504,0.09,0.846,0.27,1.025c0.18,0.181,0.486,0.271,0.918,0.271v0.378
\t\tc-0.192-0.012-0.477-0.027-0.855-0.045c-0.378-0.019-0.765-0.027-1.161-0.027s-0.804,0.009-1.224,0.027
\t\tc-0.42,0.018-0.732,0.033-0.936,0.045v-0.378c0.492,0,0.843-0.09,1.053-0.271c0.21-0.18,0.315-0.521,0.315-1.025v-10.26
\t\tc0-0.54-0.096-0.939-0.288-1.197s-0.552-0.387-1.08-0.387v-0.378c0.384,0.036,0.756,0.054,1.116,0.054
\t\tc0.348,0,0.681-0.021,0.999-0.063C351.775,245.782,352.066,245.719,352.33,245.634z"/>
\t<path d="M363.076,250.261v7.794c0,0.504,0.105,0.846,0.315,1.025c0.209,0.181,0.561,0.271,1.053,0.271v0.378
\t\tc-0.204-0.012-0.513-0.027-0.927-0.045c-0.414-0.019-0.831-0.027-1.251-0.027c-0.408,0-0.822,0.009-1.242,0.027
\t\tc-0.42,0.018-0.732,0.033-0.936,0.045v-0.378c0.492,0,0.843-0.09,1.053-0.271c0.209-0.18,0.315-0.521,0.315-1.025v-5.634
\t\tc0-0.541-0.096-0.939-0.288-1.197c-0.192-0.258-0.552-0.387-1.08-0.387v-0.378c0.384,0.036,0.756,0.054,1.116,0.054
\t\tc0.348,0,0.681-0.021,0.999-0.063C362.521,250.408,362.812,250.345,363.076,250.261z M362.14,245.958
\t\tc0.312,0,0.582,0.114,0.81,0.342c0.228,0.229,0.342,0.499,0.342,0.81c0,0.313-0.114,0.583-0.342,0.81
\t\tc-0.228,0.228-0.498,0.342-0.81,0.342c-0.313,0-0.583-0.114-0.81-0.342c-0.228-0.228-0.342-0.498-0.342-0.81
\t\tc0-0.312,0.114-0.582,0.342-0.81C361.558,246.073,361.828,245.958,362.14,245.958z"/>
\t<path d="M368.584,250.207c0.479,0,0.888,0.078,1.224,0.234c0.336,0.156,0.582,0.3,0.738,0.432c0.396,0.324,0.636,0.108,0.72-0.648
\t\th0.414c-0.024,0.336-0.042,0.735-0.054,1.197c-0.012,0.462-0.018,1.083-0.018,1.863h-0.414c-0.072-0.443-0.195-0.875-0.369-1.296
\t\tc-0.174-0.419-0.429-0.765-0.765-1.035c-0.336-0.27-0.774-0.405-1.314-0.405c-0.42,0-0.771,0.117-1.053,0.351
\t\tc-0.282,0.234-0.423,0.579-0.423,1.035c0,0.36,0.108,0.669,0.324,0.927c0.216,0.259,0.498,0.498,0.846,0.72
\t\tc0.348,0.223,0.726,0.466,1.134,0.729c0.684,0.444,1.266,0.895,1.746,1.35c0.48,0.457,0.72,1.057,0.72,1.801
\t\tc0,0.552-0.144,1.014-0.432,1.386s-0.663,0.654-1.125,0.846c-0.462,0.192-0.969,0.288-1.521,0.288
\t\tc-0.264,0-0.513-0.021-0.747-0.063c-0.234-0.043-0.459-0.105-0.675-0.189c-0.12-0.06-0.243-0.132-0.369-0.216
\t\ts-0.249-0.174-0.369-0.271c-0.12-0.096-0.228-0.093-0.324,0.01c-0.096,0.102-0.168,0.303-0.216,0.603h-0.414
\t\tc0.024-0.384,0.042-0.852,0.054-1.404c0.012-0.552,0.018-1.283,0.018-2.195h0.414c0.083,0.672,0.204,1.26,0.36,1.764
\t\tc0.156,0.504,0.402,0.897,0.738,1.179c0.336,0.282,0.804,0.423,1.404,0.423c0.36,0,0.717-0.126,1.071-0.378
\t\ts0.531-0.695,0.531-1.332c0-0.527-0.192-0.96-0.576-1.296c-0.384-0.336-0.87-0.689-1.458-1.062
\t\tc-0.432-0.276-0.837-0.552-1.215-0.828s-0.684-0.588-0.918-0.937c-0.234-0.348-0.351-0.768-0.351-1.26
\t\tc0-0.54,0.123-0.981,0.369-1.323c0.246-0.342,0.573-0.594,0.981-0.756C367.678,250.288,368.116,250.207,368.584,250.207z"/>
\t<path d="M380.877,250.261v7.794c0,0.504,0.105,0.846,0.315,1.025c0.209,0.181,0.561,0.271,1.053,0.271v0.378
\t\tc-0.204-0.012-0.513-0.027-0.927-0.045c-0.414-0.019-0.831-0.027-1.251-0.027c-0.408,0-0.822,0.009-1.242,0.027
\t\tc-0.42,0.018-0.732,0.033-0.936,0.045v-0.378c0.492,0,0.843-0.09,1.053-0.271c0.209-0.18,0.315-0.521,0.315-1.025v-5.634
\t\tc0-0.541-0.096-0.939-0.288-1.197c-0.192-0.258-0.552-0.387-1.08-0.387v-0.378c0.384,0.036,0.756,0.054,1.116,0.054
\t\tc0.348,0,0.681-0.021,0.999-0.063C380.322,250.408,380.614,250.345,380.877,250.261z M379.942,245.958
\t\tc0.312,0,0.582,0.114,0.81,0.342c0.228,0.229,0.342,0.499,0.342,0.81c0,0.313-0.114,0.583-0.342,0.81
\t\tc-0.228,0.228-0.498,0.342-0.81,0.342c-0.313,0-0.583-0.114-0.81-0.342c-0.228-0.228-0.342-0.498-0.342-0.81
\t\tc0-0.312,0.114-0.582,0.342-0.81C379.359,246.073,379.629,245.958,379.942,245.958z"/>
\t<path d="M386.386,250.207c0.479,0,0.888,0.078,1.224,0.234c0.336,0.156,0.582,0.3,0.738,0.432c0.396,0.324,0.636,0.108,0.72-0.648
\t\th0.414c-0.024,0.336-0.042,0.735-0.054,1.197c-0.012,0.462-0.018,1.083-0.018,1.863h-0.414c-0.072-0.443-0.195-0.875-0.369-1.296
\t\tc-0.174-0.419-0.429-0.765-0.765-1.035c-0.336-0.27-0.774-0.405-1.314-0.405c-0.42,0-0.771,0.117-1.053,0.351
\t\tc-0.282,0.234-0.423,0.579-0.423,1.035c0,0.36,0.108,0.669,0.324,0.927c0.216,0.259,0.498,0.498,0.846,0.72
\t\tc0.348,0.223,0.726,0.466,1.134,0.729c0.684,0.444,1.266,0.895,1.746,1.35c0.48,0.457,0.72,1.057,0.72,1.801
\t\tc0,0.552-0.144,1.014-0.432,1.386s-0.663,0.654-1.125,0.846c-0.462,0.192-0.969,0.288-1.521,0.288
\t\tc-0.264,0-0.513-0.021-0.747-0.063c-0.234-0.043-0.459-0.105-0.675-0.189c-0.12-0.06-0.243-0.132-0.369-0.216
\t\ts-0.249-0.174-0.369-0.271c-0.12-0.096-0.228-0.093-0.324,0.01c-0.096,0.102-0.168,0.303-0.216,0.603h-0.414
\t\tc0.024-0.384,0.042-0.852,0.054-1.404c0.012-0.552,0.018-1.283,0.018-2.195h0.414c0.083,0.672,0.204,1.26,0.36,1.764
\t\tc0.156,0.504,0.402,0.897,0.738,1.179c0.336,0.282,0.804,0.423,1.404,0.423c0.36,0,0.717-0.126,1.071-0.378
\t\ts0.531-0.695,0.531-1.332c0-0.527-0.192-0.96-0.576-1.296c-0.384-0.336-0.87-0.689-1.458-1.062
\t\tc-0.432-0.276-0.837-0.552-1.215-0.828s-0.684-0.588-0.918-0.937c-0.234-0.348-0.351-0.768-0.351-1.26
\t\tc0-0.54,0.123-0.981,0.369-1.323c0.246-0.342,0.573-0.594,0.981-0.756C385.479,250.288,385.917,250.207,386.386,250.207z"/>
\t<path d="M398.355,247.471v3.006h2.664v0.36h-2.664v6.966c0,0.564,0.102,0.96,0.306,1.188c0.204,0.229,0.486,0.342,0.846,0.342
\t\tc0.36,0,0.672-0.146,0.936-0.44c0.264-0.294,0.492-0.789,0.684-1.485l0.36,0.09c-0.12,0.696-0.357,1.284-0.711,1.765
\t\tc-0.354,0.479-0.897,0.72-1.629,0.72c-0.408,0-0.744-0.051-1.008-0.153c-0.264-0.102-0.498-0.249-0.702-0.44
\t\tc-0.264-0.276-0.447-0.606-0.549-0.99c-0.102-0.384-0.153-0.894-0.153-1.53v-6.03h-1.728v-0.36h1.728v-2.754
\t\tc0.3-0.012,0.588-0.036,0.864-0.072C397.875,247.615,398.127,247.555,398.355,247.471z"/>
\t<path d="M406.239,250.207c0.768,0,1.458,0.168,2.07,0.504s1.101,0.864,1.467,1.584c0.366,0.72,0.549,1.656,0.549,2.808
\t\ts-0.183,2.085-0.549,2.799c-0.366,0.714-0.855,1.239-1.467,1.575s-1.302,0.504-2.07,0.504c-0.756,0-1.443-0.168-2.061-0.504
\t\tc-0.618-0.336-1.11-0.861-1.476-1.575c-0.366-0.714-0.549-1.646-0.549-2.799s0.183-2.088,0.549-2.808
\t\tc0.366-0.72,0.858-1.248,1.476-1.584C404.796,250.375,405.483,250.207,406.239,250.207z M406.239,250.566
\t\tc-0.684,0-1.245,0.354-1.683,1.062c-0.438,0.708-0.657,1.866-0.657,3.474s0.219,2.763,0.657,3.465s0.999,1.053,1.683,1.053
\t\ts1.245-0.351,1.683-1.053c0.438-0.702,0.657-1.856,0.657-3.465s-0.219-2.766-0.657-3.474
\t\tC407.484,250.92,406.923,250.566,406.239,250.566z"/>
\t<path d="M420.675,250.207c0.384,0,0.756,0.048,1.116,0.144c0.359,0.096,0.684,0.24,0.972,0.432c0.276,0.18,0.486,0.387,0.63,0.621
\t\tc0.145,0.234,0.217,0.489,0.217,0.765c0,0.325-0.09,0.574-0.271,0.747c-0.181,0.175-0.408,0.262-0.685,0.262
\t\tc-0.264,0-0.494-0.078-0.692-0.234c-0.198-0.155-0.297-0.378-0.297-0.666c0-0.276,0.075-0.498,0.225-0.666
\t\tc0.149-0.168,0.327-0.276,0.531-0.324c-0.12-0.216-0.333-0.396-0.639-0.54c-0.306-0.144-0.633-0.216-0.981-0.216
\t\tc-0.3,0-0.6,0.072-0.9,0.216c-0.3,0.144-0.576,0.381-0.828,0.711s-0.456,0.78-0.612,1.35c-0.156,0.57-0.234,1.281-0.234,2.133
\t\tc0,1.057,0.12,1.896,0.36,2.521c0.24,0.624,0.555,1.07,0.945,1.341c0.39,0.27,0.819,0.404,1.287,0.404s0.942-0.138,1.421-0.413
\t\tc0.48-0.276,0.877-0.757,1.188-1.44l0.343,0.126c-0.121,0.384-0.318,0.771-0.595,1.161s-0.636,0.711-1.08,0.963
\t\tc-0.444,0.252-0.99,0.378-1.638,0.378c-0.744,0-1.416-0.192-2.016-0.576s-1.077-0.933-1.431-1.646s-0.531-1.563-0.531-2.547
\t\tc0-0.984,0.177-1.852,0.531-2.602c0.354-0.75,0.849-1.338,1.485-1.764C419.133,250.42,419.859,250.207,420.675,250.207z"/>
\t<path d="M429.297,250.207c1.056,0,1.881,0.321,2.475,0.963c0.594,0.642,0.892,1.641,0.892,2.998h-6.354l-0.018-0.343h4.645
\t\tc0.023-0.588-0.025-1.13-0.145-1.629c-0.12-0.498-0.309-0.896-0.566-1.197c-0.259-0.3-0.592-0.45-1-0.45
\t\tc-0.552,0-1.041,0.276-1.467,0.828c-0.426,0.552-0.682,1.428-0.766,2.628l0.055,0.071c-0.024,0.181-0.042,0.378-0.055,0.595
\t\tc-0.012,0.216-0.018,0.432-0.018,0.647c0,0.816,0.133,1.519,0.396,2.106s0.609,1.035,1.035,1.341s0.866,0.459,1.322,0.459
\t\tc0.541,0,1.038-0.132,1.494-0.396c0.456-0.264,0.84-0.731,1.152-1.403l0.359,0.144c-0.132,0.396-0.348,0.78-0.647,1.152
\t\ts-0.679,0.675-1.134,0.909c-0.457,0.233-0.984,0.351-1.584,0.351c-0.864,0-1.605-0.198-2.223-0.594
\t\tc-0.619-0.396-1.093-0.945-1.422-1.647c-0.331-0.702-0.496-1.509-0.496-2.421c0-1.056,0.168-1.965,0.504-2.728
\t\tc0.336-0.761,0.811-1.349,1.422-1.763C427.768,250.414,428.48,250.207,429.297,250.207z"/>
\t<path d="M439.629,250.207c0.324,0,0.609,0.066,0.855,0.198c0.245,0.132,0.438,0.303,0.575,0.513s0.207,0.447,0.207,0.711
\t\tc0,0.3-0.093,0.564-0.278,0.792c-0.187,0.228-0.436,0.342-0.748,0.342c-0.252,0-0.474-0.081-0.666-0.243
\t\tc-0.191-0.162-0.287-0.393-0.287-0.693c0-0.228,0.063-0.423,0.188-0.585c0.126-0.162,0.267-0.291,0.423-0.387
\t\tc-0.084-0.12-0.21-0.18-0.377-0.18c-0.505,0-0.943,0.189-1.314,0.567c-0.373,0.378-0.664,0.831-0.873,1.359
\t\tc-0.211,0.528-0.315,1.014-0.315,1.458v3.816c0,0.588,0.171,0.98,0.513,1.179c0.343,0.198,0.808,0.297,1.396,0.297v0.378
\t\tc-0.276-0.012-0.666-0.027-1.17-0.045c-0.504-0.019-1.038-0.027-1.603-0.027c-0.408,0-0.813,0.009-1.215,0.027
\t\tc-0.402,0.018-0.705,0.033-0.908,0.045v-0.378c0.491,0,0.842-0.09,1.053-0.271c0.209-0.18,0.314-0.521,0.314-1.025v-5.634
\t\tc0-0.541-0.096-0.939-0.287-1.197c-0.193-0.258-0.553-0.387-1.08-0.387v-0.378c0.383,0.036,0.756,0.054,1.115,0.054
\t\tc0.348,0,0.682-0.021,1-0.063c0.316-0.042,0.608-0.105,0.872-0.189v2.25c0.132-0.348,0.321-0.699,0.567-1.053
\t\ts0.543-0.651,0.891-0.891C438.824,250.327,439.209,250.207,439.629,250.207z"/>
\t<path d="M444.938,247.471v3.006h2.664v0.36h-2.664v6.966c0,0.564,0.103,0.96,0.306,1.188c0.204,0.229,0.486,0.342,0.846,0.342
\t\tc0.361,0,0.673-0.146,0.937-0.44c0.265-0.294,0.492-0.789,0.685-1.485l0.359,0.09c-0.119,0.696-0.357,1.284-0.711,1.765
\t\tc-0.354,0.479-0.896,0.72-1.629,0.72c-0.408,0-0.744-0.051-1.008-0.153c-0.264-0.102-0.498-0.249-0.702-0.44
\t\tc-0.265-0.276-0.447-0.606-0.549-0.99c-0.103-0.384-0.153-0.894-0.153-1.53v-6.03h-1.729v-0.36h1.729v-2.754
\t\tc0.3-0.012,0.588-0.036,0.864-0.072C444.459,247.615,444.711,247.555,444.938,247.471z"/>
\t<path d="M451.4,250.261v7.794c0,0.504,0.105,0.846,0.315,1.025c0.21,0.181,0.561,0.271,1.053,0.271v0.378
\t\tc-0.204-0.012-0.513-0.027-0.927-0.045c-0.414-0.019-0.831-0.027-1.251-0.027c-0.408,0-0.822,0.009-1.242,0.027
\t\tc-0.421,0.018-0.731,0.033-0.937,0.045v-0.378c0.492,0,0.844-0.09,1.054-0.271c0.21-0.18,0.315-0.521,0.315-1.025v-5.634
\t\tc0-0.541-0.097-0.939-0.289-1.197c-0.191-0.258-0.552-0.387-1.08-0.387v-0.378c0.385,0.036,0.756,0.054,1.117,0.054
\t\tc0.348,0,0.68-0.021,0.998-0.063S451.137,250.345,451.4,250.261z M450.465,245.958c0.312,0,0.582,0.114,0.81,0.342
\t\tc0.228,0.229,0.343,0.499,0.343,0.81c0,0.313-0.115,0.583-0.343,0.81c-0.228,0.228-0.498,0.342-0.81,0.342
\t\tc-0.313,0-0.582-0.114-0.811-0.342c-0.228-0.228-0.342-0.498-0.342-0.81c0-0.312,0.114-0.582,0.342-0.81
\t\tC449.883,246.073,450.152,245.958,450.465,245.958z"/>
\t<path d="M458.133,245.652c0.336,0,0.633,0.045,0.891,0.135s0.482,0.207,0.676,0.351c0.18,0.144,0.317,0.312,0.414,0.504
\t\tc0.096,0.192,0.143,0.396,0.143,0.612c0,0.276-0.09,0.504-0.27,0.684s-0.408,0.27-0.684,0.27c-0.264,0-0.495-0.087-0.693-0.261
\t\tc-0.197-0.174-0.297-0.411-0.297-0.711c0-0.252,0.068-0.456,0.207-0.612c0.138-0.156,0.32-0.258,0.549-0.306
\t\tc-0.035-0.108-0.129-0.201-0.279-0.279s-0.351-0.117-0.603-0.117c-0.276,0-0.513,0.061-0.711,0.18
\t\tc-0.198,0.12-0.351,0.282-0.459,0.486c-0.144,0.252-0.233,0.627-0.271,1.125c-0.035,0.498-0.054,1.263-0.054,2.295v0.468h1.927
\t\tv0.36h-1.927v7.039c0,0.588,0.171,0.98,0.513,1.179s0.808,0.297,1.396,0.297v0.378c-0.276-0.012-0.666-0.027-1.17-0.045
\t\tc-0.504-0.019-1.038-0.027-1.603-0.027c-0.408,0-0.813,0.009-1.215,0.027c-0.402,0.018-0.705,0.033-0.908,0.045v-0.378
\t\tc0.491,0,0.842-0.09,1.053-0.271c0.209-0.18,0.314-0.521,0.314-1.025v-7.218h-1.439v-0.36h1.439c0-1.068,0.069-1.887,0.207-2.457
\t\tc0.139-0.57,0.375-1.059,0.711-1.467c0.24-0.276,0.547-0.495,0.918-0.657C457.28,245.733,457.688,245.652,458.133,245.652z"/>
\t<path d="M468.032,250.458v0.36c-0.228,0.024-0.438,0.126-0.63,0.306c-0.192,0.18-0.372,0.498-0.54,0.954l-2.952,7.74h-0.307
\t\tl-3.312-7.974c-0.228-0.468-0.453-0.753-0.675-0.855c-0.223-0.102-0.405-0.153-0.549-0.153v-0.378
\t\tc0.264,0.036,0.539,0.063,0.828,0.081c0.287,0.018,0.6,0.027,0.936,0.027c0.372,0,0.766-0.012,1.18-0.036
\t\tc0.414-0.023,0.801-0.047,1.16-0.072v0.378c-0.3,0-0.566,0.021-0.801,0.063c-0.234,0.042-0.381,0.168-0.441,0.378
\t\tc-0.06,0.21,0.012,0.567,0.217,1.071l2.214,5.49l-0.108,0.036l2.053-5.364c0.227-0.6,0.252-1.02,0.071-1.26
\t\tc-0.18-0.24-0.588-0.384-1.224-0.432v-0.36c0.312,0.012,0.572,0.024,0.783,0.036c0.209,0.012,0.453,0.018,0.729,0.018
\t\ts0.525-0.006,0.747-0.018C467.633,250.483,467.84,250.471,468.032,250.458z M463.91,259.818l-0.611,1.566
\t\tc-0.229,0.588-0.486,1.008-0.775,1.26c-0.191,0.18-0.42,0.303-0.684,0.369c-0.264,0.065-0.504,0.099-0.72,0.099
\t\tc-0.228,0-0.435-0.042-0.621-0.126s-0.333-0.207-0.44-0.369c-0.108-0.162-0.162-0.356-0.162-0.585c0-0.288,0.084-0.519,0.252-0.692
\t\tc0.168-0.175,0.401-0.262,0.701-0.262c0.252,0,0.469,0.072,0.648,0.217c0.18,0.144,0.27,0.354,0.27,0.63
\t\tc0,0.204-0.047,0.371-0.144,0.504c-0.097,0.132-0.21,0.233-0.342,0.306c0.024,0.012,0.045,0.019,0.063,0.019
\t\tc0.018,0,0.032,0,0.045,0c0.324,0,0.611-0.111,0.863-0.333c0.252-0.223,0.475-0.579,0.666-1.071l0.648-1.674L463.91,259.818z"/>
\t<path d="M475.412,247.471v3.006h2.664v0.36h-2.664v6.966c0,0.564,0.103,0.96,0.307,1.188c0.203,0.229,0.486,0.342,0.846,0.342
\t\tc0.36,0,0.672-0.146,0.936-0.44c0.265-0.294,0.492-0.789,0.685-1.485l0.36,0.09c-0.12,0.696-0.357,1.284-0.711,1.765
\t\tc-0.354,0.479-0.897,0.72-1.629,0.72c-0.408,0-0.744-0.051-1.008-0.153c-0.265-0.102-0.498-0.249-0.703-0.44
\t\tc-0.264-0.276-0.447-0.606-0.549-0.99s-0.152-0.894-0.152-1.53v-6.03h-1.729v-0.36h1.729v-2.754
\t\tc0.299-0.012,0.588-0.036,0.863-0.072C474.933,247.615,475.185,247.555,475.412,247.471z"/>
\t<path d="M481.713,245.634v6.732c0.311-0.816,0.746-1.38,1.305-1.692c0.558-0.312,1.143-0.468,1.755-0.468
\t\tc0.456,0,0.837,0.06,1.143,0.18s0.562,0.288,0.765,0.504c0.229,0.24,0.391,0.54,0.486,0.9s0.145,0.846,0.145,1.458v4.807
\t\tc0,0.504,0.104,0.846,0.314,1.025c0.21,0.181,0.561,0.271,1.053,0.271v0.378c-0.203-0.012-0.516-0.027-0.936-0.045
\t\tc-0.42-0.019-0.828-0.027-1.225-0.027c-0.396,0-0.782,0.009-1.16,0.027c-0.378,0.018-0.664,0.033-0.855,0.045v-0.378
\t\tc0.432,0,0.738-0.09,0.918-0.271c0.18-0.18,0.271-0.521,0.271-1.025v-5.202c0-0.372-0.03-0.714-0.091-1.026
\t\tc-0.06-0.312-0.194-0.564-0.404-0.756c-0.21-0.191-0.531-0.288-0.963-0.288c-0.732,0-1.336,0.303-1.809,0.909
\t\tc-0.475,0.606-0.711,1.39-0.711,2.349v4.015c0,0.504,0.09,0.846,0.27,1.025c0.18,0.181,0.486,0.271,0.918,0.271v0.378
\t\tc-0.192-0.012-0.477-0.027-0.855-0.045c-0.377-0.019-0.765-0.027-1.16-0.027c-0.396,0-0.805,0.009-1.225,0.027
\t\tc-0.42,0.018-0.732,0.033-0.936,0.045v-0.378c0.492,0,0.843-0.09,1.053-0.271c0.21-0.18,0.314-0.521,0.314-1.025v-10.26
\t\tc0-0.54-0.096-0.939-0.287-1.197c-0.192-0.258-0.553-0.387-1.08-0.387v-0.378c0.384,0.036,0.756,0.054,1.115,0.054
\t\tc0.348,0,0.682-0.021,1-0.063C481.157,245.782,481.448,245.719,481.713,245.634z"/>
\t<path d="M491.883,259.854c-0.781,0-1.348-0.195-1.701-0.585c-0.354-0.39-0.531-0.909-0.531-1.557c0-0.504,0.123-0.921,0.369-1.251
\t\ts0.561-0.601,0.945-0.811c0.383-0.21,0.788-0.39,1.215-0.54c0.426-0.149,0.83-0.297,1.215-0.44
\t\tc0.384-0.145,0.699-0.307,0.945-0.486c0.245-0.18,0.368-0.408,0.368-0.684v-1.117c0-0.504-0.075-0.888-0.225-1.152
\t\tc-0.15-0.264-0.351-0.441-0.603-0.531c-0.252-0.09-0.535-0.135-0.847-0.135c-0.3,0-0.621,0.042-0.963,0.126
\t\tc-0.343,0.084-0.615,0.258-0.819,0.522c0.229,0.048,0.426,0.168,0.594,0.36s0.252,0.438,0.252,0.738s-0.096,0.537-0.287,0.711
\t\tc-0.192,0.174-0.438,0.261-0.738,0.261c-0.348,0-0.604-0.11-0.766-0.333c-0.162-0.222-0.242-0.471-0.242-0.747
\t\tc0-0.312,0.078-0.564,0.234-0.756c0.155-0.192,0.354-0.372,0.594-0.54c0.275-0.192,0.633-0.357,1.07-0.495
\t\tc0.438-0.138,0.934-0.207,1.485-0.207c0.491,0,0.911,0.057,1.26,0.171c0.349,0.114,0.636,0.279,0.864,0.495
\t\tc0.313,0.288,0.516,0.639,0.611,1.053c0.097,0.414,0.145,0.91,0.145,1.485v5.004c0,0.288,0.042,0.502,0.126,0.64
\t\ts0.228,0.207,0.433,0.207c0.168,0,0.314-0.036,0.44-0.108s0.249-0.162,0.369-0.27l0.198,0.306
\t\tc-0.252,0.192-0.492,0.351-0.721,0.477c-0.228,0.127-0.533,0.189-0.918,0.189c-0.623,0-1.037-0.162-1.242-0.486
\t\tc-0.203-0.323-0.306-0.695-0.306-1.115c-0.384,0.636-0.819,1.062-1.305,1.277S492.41,259.854,491.883,259.854z M492.674,259.135
\t\tc0.36,0,0.715-0.105,1.063-0.315s0.672-0.561,0.972-1.053v-3.528c-0.18,0.265-0.459,0.475-0.837,0.63
\t\tc-0.378,0.156-0.768,0.327-1.17,0.514c-0.402,0.186-0.747,0.438-1.035,0.756c-0.287,0.318-0.432,0.771-0.432,1.358
\t\tc0,0.553,0.138,0.964,0.414,1.233C491.924,259,492.266,259.135,492.674,259.135z"/>
\t<path d="M501.332,247.471v3.006h2.664v0.36h-2.664v6.966c0,0.564,0.102,0.96,0.307,1.188c0.203,0.229,0.485,0.342,0.846,0.342
\t\tc0.359,0,0.672-0.146,0.936-0.44s0.492-0.789,0.684-1.485l0.36,0.09c-0.12,0.696-0.356,1.284-0.711,1.765
\t\tc-0.354,0.479-0.897,0.72-1.629,0.72c-0.408,0-0.744-0.051-1.008-0.153c-0.265-0.102-0.498-0.249-0.702-0.44
\t\tc-0.264-0.276-0.447-0.606-0.549-0.99s-0.153-0.894-0.153-1.53v-6.03h-1.728v-0.36h1.728v-2.754c0.3-0.012,0.588-0.036,0.864-0.072
\t\tC500.852,247.615,501.104,247.555,501.332,247.471z"/>
\t<path d="M254.564,332.035v6.731c0.312-0.815,0.747-1.38,1.305-1.691c0.558-0.313,1.143-0.469,1.755-0.469
\t\tc0.456,0,0.837,0.061,1.143,0.181c0.306,0.12,0.561,0.288,0.765,0.504c0.228,0.24,0.39,0.54,0.486,0.899
\t\tc0.096,0.36,0.144,0.847,0.144,1.458v4.807c0,0.504,0.105,0.846,0.315,1.025c0.209,0.181,0.561,0.271,1.053,0.271v0.378
\t\tc-0.204-0.012-0.516-0.027-0.936-0.045c-0.42-0.019-0.828-0.027-1.224-0.027s-0.783,0.009-1.161,0.027
\t\tc-0.378,0.018-0.664,0.033-0.855,0.045v-0.378c0.432,0,0.738-0.09,0.918-0.271c0.18-0.18,0.27-0.521,0.27-1.025v-5.202
\t\tc0-0.372-0.03-0.714-0.09-1.026c-0.06-0.312-0.195-0.563-0.405-0.756c-0.21-0.191-0.531-0.288-0.963-0.288
\t\tc-0.732,0-1.335,0.304-1.809,0.909c-0.474,0.606-0.711,1.39-0.711,2.349v4.015c0,0.504,0.09,0.846,0.27,1.025
\t\tc0.18,0.181,0.486,0.271,0.918,0.271v0.378c-0.192-0.012-0.477-0.027-0.855-0.045c-0.378-0.019-0.765-0.027-1.161-0.027
\t\ts-0.804,0.009-1.224,0.027c-0.42,0.018-0.732,0.033-0.936,0.045v-0.378c0.492,0,0.843-0.09,1.053-0.271
\t\tc0.21-0.18,0.315-0.521,0.315-1.025v-10.26c0-0.54-0.096-0.939-0.288-1.197s-0.552-0.387-1.08-0.387v-0.379
\t\tc0.384,0.036,0.756,0.055,1.116,0.055c0.348,0,0.681-0.021,0.999-0.063C254.009,332.182,254.3,332.119,254.564,332.035z"/>
\t<path d="M264.734,346.255c-0.78,0-1.347-0.195-1.701-0.585s-0.531-0.909-0.531-1.557c0-0.504,0.123-0.921,0.369-1.251
\t\ts0.561-0.601,0.945-0.811c0.384-0.21,0.789-0.39,1.215-0.54c0.426-0.149,0.831-0.297,1.215-0.44
\t\tc0.384-0.145,0.699-0.307,0.945-0.486c0.246-0.18,0.369-0.408,0.369-0.684v-1.116c0-0.504-0.075-0.888-0.225-1.152
\t\tc-0.15-0.264-0.351-0.44-0.603-0.531c-0.252-0.09-0.534-0.135-0.846-0.135c-0.3,0-0.621,0.042-0.963,0.126
\t\tc-0.342,0.084-0.615,0.259-0.819,0.522c0.228,0.048,0.426,0.168,0.594,0.359c0.168,0.192,0.252,0.438,0.252,0.738
\t\ts-0.096,0.537-0.288,0.711s-0.438,0.261-0.738,0.261c-0.348,0-0.603-0.11-0.765-0.333c-0.162-0.222-0.243-0.471-0.243-0.747
\t\tc0-0.312,0.078-0.563,0.234-0.756c0.156-0.191,0.354-0.371,0.594-0.54c0.276-0.191,0.633-0.356,1.071-0.495
\t\tc0.438-0.138,0.933-0.207,1.485-0.207c0.492,0,0.912,0.058,1.26,0.172s0.636,0.278,0.864,0.494c0.312,0.288,0.516,0.64,0.612,1.054
\t\ts0.144,0.909,0.144,1.484v5.004c0,0.288,0.042,0.502,0.126,0.64s0.228,0.207,0.432,0.207c0.168,0,0.315-0.036,0.441-0.108
\t\ts0.249-0.162,0.369-0.27l0.198,0.306c-0.252,0.192-0.492,0.351-0.72,0.477c-0.228,0.127-0.534,0.189-0.918,0.189
\t\tc-0.624,0-1.038-0.162-1.242-0.486c-0.204-0.323-0.306-0.695-0.306-1.115c-0.384,0.636-0.819,1.062-1.305,1.277
\t\tS265.262,346.255,264.734,346.255z M265.526,345.535c0.36,0,0.714-0.105,1.062-0.315c0.348-0.21,0.672-0.561,0.972-1.053v-3.528
\t\tc-0.18,0.265-0.459,0.475-0.837,0.63c-0.378,0.156-0.768,0.327-1.17,0.514c-0.402,0.186-0.747,0.438-1.035,0.756
\t\tc-0.288,0.318-0.432,0.771-0.432,1.358c0,0.553,0.138,0.964,0.414,1.233C264.776,345.4,265.118,345.535,265.526,345.535z"/>
\t<path d="M274.508,336.606c0.479,0,0.888,0.079,1.224,0.234c0.336,0.156,0.582,0.3,0.738,0.432c0.396,0.324,0.636,0.108,0.72-0.647
\t\th0.414c-0.024,0.336-0.042,0.735-0.054,1.197c-0.012,0.462-0.018,1.083-0.018,1.862h-0.414c-0.072-0.443-0.195-0.875-0.369-1.296
\t\tc-0.174-0.42-0.429-0.765-0.765-1.035c-0.336-0.27-0.774-0.404-1.314-0.404c-0.42,0-0.771,0.116-1.053,0.351
\t\tc-0.282,0.234-0.423,0.579-0.423,1.035c0,0.36,0.108,0.669,0.324,0.927c0.216,0.259,0.498,0.498,0.846,0.72
\t\tc0.348,0.223,0.726,0.466,1.134,0.729c0.684,0.444,1.266,0.895,1.746,1.35c0.48,0.457,0.72,1.057,0.72,1.801
\t\tc0,0.552-0.144,1.014-0.432,1.386s-0.663,0.654-1.125,0.846c-0.462,0.192-0.969,0.288-1.521,0.288
\t\tc-0.264,0-0.513-0.021-0.747-0.063c-0.234-0.043-0.459-0.105-0.675-0.189c-0.12-0.06-0.243-0.132-0.369-0.216
\t\ts-0.249-0.174-0.369-0.271c-0.12-0.096-0.228-0.093-0.324,0.01c-0.096,0.102-0.168,0.303-0.216,0.603h-0.414
\t\tc0.024-0.384,0.042-0.852,0.054-1.404c0.012-0.552,0.018-1.283,0.018-2.195h0.414c0.083,0.672,0.204,1.26,0.36,1.764
\t\tc0.156,0.504,0.402,0.897,0.738,1.179c0.336,0.282,0.804,0.423,1.404,0.423c0.36,0,0.717-0.126,1.071-0.378
\t\ts0.531-0.695,0.531-1.332c0-0.527-0.192-0.96-0.576-1.296c-0.384-0.336-0.87-0.689-1.458-1.062
\t\tc-0.432-0.276-0.837-0.552-1.215-0.828s-0.684-0.588-0.918-0.937c-0.234-0.348-0.351-0.768-0.351-1.26
\t\tc0-0.54,0.123-0.98,0.369-1.323c0.246-0.342,0.573-0.594,0.981-0.756C273.602,336.688,274.04,336.606,274.508,336.606z"/>
\t<path d="M286.675,336.661v11.052c0,0.516,0.153,0.861,0.459,1.035c0.306,0.174,0.729,0.261,1.269,0.261v0.378
\t\tc-0.264-0.012-0.63-0.026-1.098-0.045c-0.468-0.018-0.966-0.027-1.494-0.027c-0.372,0-0.741,0.01-1.107,0.027
\t\tc-0.366,0.019-0.646,0.033-0.837,0.045v-0.378c0.432,0,0.738-0.078,0.918-0.233c0.18-0.156,0.27-0.45,0.27-0.883v-9.071
\t\tc0-0.54-0.096-0.939-0.288-1.197c-0.192-0.258-0.552-0.387-1.08-0.387v-0.378c0.384,0.035,0.756,0.054,1.116,0.054
\t\tc0.348,0,0.681-0.021,0.999-0.063C286.12,336.808,286.412,336.745,286.675,336.661z M289.556,336.606
\t\tc0.948,0,1.743,0.382,2.385,1.144s0.963,1.869,0.963,3.321c0,0.863-0.141,1.704-0.423,2.52c-0.282,0.816-0.723,1.485-1.323,2.007
\t\tc-0.601,0.522-1.393,0.783-2.376,0.783c-0.564,0-1.053-0.132-1.467-0.396c-0.414-0.265-0.693-0.582-0.837-0.954l0.18-0.252
\t\tc0.167,0.336,0.411,0.618,0.729,0.846c0.317,0.228,0.717,0.342,1.197,0.342c0.672,0,1.194-0.21,1.566-0.63s0.633-0.981,0.783-1.683
\t\tc0.15-0.702,0.225-1.479,0.225-2.331c0-0.96-0.079-1.746-0.234-2.358s-0.39-1.067-0.702-1.368c-0.313-0.3-0.696-0.45-1.152-0.45
\t\tc-0.588,0-1.122,0.243-1.602,0.729s-0.786,1.221-0.918,2.205l-0.216-0.252c0.132-1.02,0.495-1.812,1.089-2.376
\t\tC288.017,336.889,288.728,336.606,289.556,336.606z"/>
\t<path d="M299.87,336.606c0.324,0,0.609,0.066,0.855,0.198c0.246,0.133,0.438,0.304,0.576,0.514c0.138,0.21,0.207,0.446,0.207,0.711
\t\tc0,0.3-0.093,0.563-0.279,0.792c-0.186,0.228-0.435,0.342-0.747,0.342c-0.252,0-0.474-0.081-0.666-0.243s-0.288-0.393-0.288-0.693
\t\tc0-0.228,0.063-0.423,0.189-0.585s0.267-0.29,0.423-0.387c-0.084-0.12-0.21-0.18-0.378-0.18c-0.504,0-0.942,0.188-1.314,0.566
\t\tc-0.373,0.378-0.664,0.831-0.873,1.359c-0.21,0.528-0.315,1.014-0.315,1.458v3.816c0,0.588,0.171,0.98,0.513,1.179
\t\tc0.342,0.198,0.807,0.297,1.395,0.297v0.378c-0.276-0.012-0.666-0.027-1.17-0.045c-0.504-0.019-1.038-0.027-1.602-0.027
\t\tc-0.408,0-0.813,0.009-1.215,0.027c-0.402,0.018-0.705,0.033-0.909,0.045v-0.378c0.492,0,0.843-0.09,1.053-0.271
\t\tc0.209-0.18,0.315-0.521,0.315-1.025v-5.634c0-0.54-0.096-0.939-0.288-1.197c-0.192-0.258-0.552-0.387-1.08-0.387v-0.378
\t\tc0.384,0.035,0.756,0.054,1.116,0.054c0.348,0,0.681-0.021,0.999-0.063c0.317-0.042,0.609-0.104,0.873-0.188v2.25
\t\tc0.132-0.348,0.321-0.699,0.567-1.054c0.246-0.354,0.542-0.65,0.891-0.891S299.449,336.606,299.87,336.606z"/>
\t<path d="M306.332,336.606c0.768,0,1.458,0.169,2.07,0.505s1.101,0.863,1.467,1.584c0.366,0.72,0.549,1.655,0.549,2.808
\t\ts-0.183,2.085-0.549,2.799c-0.366,0.714-0.855,1.239-1.467,1.575s-1.302,0.504-2.07,0.504c-0.756,0-1.443-0.168-2.061-0.504
\t\tc-0.618-0.336-1.11-0.861-1.476-1.575c-0.366-0.714-0.549-1.646-0.549-2.799s0.183-2.088,0.549-2.808
\t\tc0.366-0.721,0.858-1.248,1.476-1.584C304.889,336.775,305.576,336.606,306.332,336.606z M306.332,336.967
\t\tc-0.684,0-1.245,0.354-1.683,1.063s-0.657,1.865-0.657,3.474s0.219,2.763,0.657,3.465s0.999,1.053,1.683,1.053
\t\ts1.245-0.351,1.683-1.053c0.438-0.702,0.657-1.856,0.657-3.465s-0.219-2.766-0.657-3.474
\t\tC307.577,337.321,307.016,336.967,306.332,336.967z"/>
\t<path d="M319.886,336.859v0.359c-0.228,0.024-0.438,0.126-0.63,0.307c-0.192,0.18-0.372,0.498-0.54,0.953l-2.952,7.74
\t\tc-0.048,0-0.096,0-0.144,0s-0.102,0-0.162,0l-3.312-7.974c-0.192-0.468-0.381-0.753-0.567-0.855
\t\tc-0.186-0.102-0.345-0.152-0.477-0.152v-0.378c0.24,0.012,0.489,0.026,0.747,0.045c0.258,0.018,0.537,0.026,0.837,0.026
\t\tc0.372,0,0.765-0.006,1.179-0.018s0.801-0.03,1.161-0.054v0.378c-0.3,0-0.567,0.021-0.801,0.063s-0.384,0.159-0.45,0.352
\t\tc-0.066,0.191-0.009,0.521,0.171,0.989l2.25,5.563l-0.108,0.09l2.052-5.382c0.228-0.601,0.255-1.021,0.081-1.26
\t\tc-0.174-0.24-0.579-0.385-1.215-0.433v-0.359c0.312,0.012,0.573,0.023,0.783,0.035c0.21,0.013,0.453,0.019,0.729,0.019
\t\ts0.525-0.006,0.747-0.019C319.487,336.883,319.693,336.871,319.886,336.859z"/>
\t<path d="M324.674,336.606c1.056,0,1.881,0.321,2.475,0.964c0.594,0.642,0.891,1.641,0.891,2.997h-6.354l-0.018-0.343h4.644
\t\tc0.024-0.588-0.024-1.131-0.144-1.629c-0.12-0.498-0.309-0.896-0.567-1.197c-0.258-0.3-0.591-0.449-0.999-0.449
\t\tc-0.552,0-1.042,0.275-1.467,0.828c-0.426,0.552-0.681,1.428-0.765,2.628l0.054,0.071c-0.024,0.181-0.042,0.378-0.054,0.595
\t\tc-0.012,0.216-0.018,0.432-0.018,0.647c0,0.816,0.132,1.519,0.396,2.106c0.264,0.588,0.609,1.035,1.035,1.341
\t\ts0.867,0.459,1.323,0.459c0.54,0,1.038-0.132,1.494-0.396c0.456-0.264,0.84-0.731,1.152-1.403l0.36,0.144
\t\tc-0.132,0.396-0.348,0.78-0.648,1.152c-0.3,0.372-0.678,0.675-1.134,0.909c-0.456,0.233-0.984,0.351-1.584,0.351
\t\tc-0.864,0-1.605-0.198-2.223-0.594c-0.618-0.396-1.092-0.945-1.422-1.647c-0.331-0.702-0.495-1.509-0.495-2.421
\t\tc0-1.056,0.168-1.965,0.504-2.727s0.81-1.351,1.422-1.765C323.144,336.813,323.857,336.606,324.674,336.606z"/>
\t<path d="M335.456,336.606c0.456,0,0.837,0.061,1.143,0.181c0.306,0.12,0.561,0.288,0.765,0.504c0.228,0.24,0.39,0.54,0.486,0.899
\t\tc0.096,0.36,0.144,0.847,0.144,1.458v4.807c0,0.504,0.105,0.846,0.315,1.025c0.21,0.181,0.561,0.271,1.053,0.271v0.378
\t\tc-0.204-0.012-0.516-0.027-0.936-0.045c-0.42-0.019-0.828-0.027-1.224-0.027s-0.783,0.009-1.161,0.027
\t\tc-0.378,0.018-0.663,0.033-0.855,0.045v-0.378c0.432,0,0.738-0.09,0.918-0.271c0.18-0.18,0.27-0.521,0.27-1.025v-5.202
\t\tc0-0.372-0.03-0.714-0.09-1.026c-0.06-0.312-0.195-0.563-0.405-0.756c-0.21-0.191-0.531-0.288-0.963-0.288
\t\tc-0.756,0-1.365,0.315-1.827,0.945c-0.462,0.63-0.693,1.407-0.693,2.331v3.996c0,0.504,0.09,0.846,0.27,1.025
\t\tc0.18,0.181,0.486,0.271,0.918,0.271v0.378c-0.192-0.012-0.477-0.027-0.855-0.045c-0.378-0.019-0.765-0.027-1.161-0.027
\t\ts-0.804,0.009-1.224,0.027c-0.42,0.018-0.732,0.033-0.936,0.045v-0.378c0.492,0,0.843-0.09,1.053-0.271
\t\tc0.209-0.18,0.315-0.521,0.315-1.025v-5.634c0-0.54-0.096-0.939-0.288-1.197c-0.192-0.258-0.552-0.387-1.08-0.387v-0.378
\t\tc0.384,0.035,0.756,0.054,1.116,0.054c0.348,0,0.681-0.021,0.999-0.063c0.317-0.042,0.609-0.104,0.873-0.188v2.124
\t\tc0.312-0.816,0.747-1.383,1.305-1.701S334.844,336.606,335.456,336.606z"/>
\t<path d="M347.173,332.035v13.158c-0.156,0.132-0.315,0.267-0.477,0.404c-0.162,0.139-0.321,0.273-0.477,0.405
\t\tc-0.156,0.132-0.318,0.27-0.486,0.414l-0.306-0.126c0.048-0.168,0.081-0.339,0.099-0.513s0.027-0.352,0.027-0.531v-11.052
\t\tc0-0.54-0.096-0.939-0.288-1.197s-0.552-0.387-1.08-0.387v-0.379c0.384,0.036,0.756,0.055,1.116,0.055
\t\tc0.348,0,0.681-0.021,0.999-0.063S346.909,332.119,347.173,332.035z M349.981,336.606c0.984,0,1.8,0.405,2.448,1.216
\t\tc0.648,0.81,0.972,2.037,0.972,3.681c0,1.08-0.183,1.983-0.549,2.709c-0.366,0.727-0.855,1.269-1.467,1.629s-1.278,0.54-1.998,0.54
\t\tc-0.6,0-1.179-0.171-1.737-0.513s-0.975-0.849-1.251-1.521l0.36,0.198c0.3,0.48,0.672,0.837,1.116,1.071
\t\tc0.444,0.233,0.9,0.351,1.368,0.351c0.804,0,1.407-0.362,1.809-1.089c0.402-0.726,0.603-1.851,0.603-3.375
\t\tc0-0.972-0.078-1.782-0.234-2.43c-0.156-0.648-0.396-1.131-0.72-1.449c-0.324-0.318-0.738-0.478-1.242-0.478
\t\tc-0.552,0-1.065,0.25-1.539,0.747c-0.474,0.498-0.765,1.228-0.873,2.188l-0.216-0.252c0.132-1.02,0.489-1.812,1.071-2.376
\t\tS349.177,336.606,349.981,336.606z"/>
\t<path d="M359.143,336.606c1.056,0,1.881,0.321,2.475,0.964c0.594,0.642,0.891,1.641,0.891,2.997h-6.354l-0.018-0.343h4.644
\t\tc0.024-0.588-0.024-1.131-0.144-1.629c-0.12-0.498-0.309-0.896-0.567-1.197c-0.258-0.3-0.591-0.449-0.999-0.449
\t\tc-0.552,0-1.042,0.275-1.467,0.828c-0.426,0.552-0.681,1.428-0.765,2.628l0.054,0.071c-0.024,0.181-0.042,0.378-0.054,0.595
\t\tc-0.012,0.216-0.018,0.432-0.018,0.647c0,0.816,0.132,1.519,0.396,2.106c0.264,0.588,0.609,1.035,1.035,1.341
\t\ts0.867,0.459,1.323,0.459c0.54,0,1.038-0.132,1.494-0.396c0.456-0.264,0.84-0.731,1.152-1.403l0.36,0.144
\t\tc-0.132,0.396-0.348,0.78-0.648,1.152c-0.3,0.372-0.678,0.675-1.134,0.909c-0.456,0.233-0.984,0.351-1.584,0.351
\t\tc-0.864,0-1.605-0.198-2.223-0.594c-0.618-0.396-1.092-0.945-1.422-1.647c-0.331-0.702-0.495-1.509-0.495-2.421
\t\tc0-1.056,0.168-1.965,0.504-2.727s0.81-1.351,1.422-1.765C357.613,336.813,358.327,336.606,359.143,336.606z"/>
\t<path d="M372.067,336.859v0.359c-0.228,0.024-0.438,0.126-0.63,0.307c-0.192,0.18-0.372,0.498-0.54,0.953l-2.952,7.74h-0.306
\t\tl-3.312-7.974c-0.228-0.468-0.453-0.753-0.675-0.855c-0.222-0.102-0.405-0.152-0.549-0.152v-0.378
\t\tc0.264,0.035,0.54,0.063,0.828,0.08c0.288,0.019,0.6,0.027,0.936,0.027c0.372,0,0.765-0.012,1.179-0.036
\t\tc0.414-0.023,0.801-0.048,1.161-0.071v0.378c-0.3,0-0.567,0.021-0.801,0.063s-0.381,0.168-0.441,0.378s0.012,0.567,0.216,1.071
\t\tl2.214,5.49l-0.108,0.036l2.052-5.364c0.228-0.601,0.252-1.021,0.072-1.26c-0.18-0.24-0.588-0.385-1.224-0.433v-0.359
\t\tc0.312,0.012,0.573,0.023,0.783,0.035c0.21,0.013,0.453,0.019,0.729,0.019s0.525-0.006,0.747-0.019
\t\tC371.668,336.883,371.875,336.871,372.067,336.859z M367.945,346.219l-0.612,1.566c-0.229,0.588-0.486,1.008-0.774,1.26
\t\tc-0.192,0.18-0.42,0.303-0.684,0.369c-0.264,0.065-0.504,0.099-0.72,0.099c-0.228,0-0.435-0.042-0.621-0.126
\t\ts-0.333-0.207-0.441-0.369s-0.162-0.356-0.162-0.585c0-0.288,0.084-0.519,0.252-0.692c0.168-0.175,0.402-0.262,0.702-0.262
\t\tc0.252,0,0.468,0.072,0.648,0.217c0.18,0.144,0.27,0.354,0.27,0.63c0,0.204-0.048,0.371-0.144,0.504
\t\tc-0.096,0.132-0.21,0.233-0.342,0.306c0.024,0.012,0.045,0.019,0.063,0.019s0.033,0,0.045,0c0.324,0,0.612-0.111,0.864-0.333
\t\tc0.252-0.223,0.474-0.579,0.666-1.071l0.648-1.674L367.945,346.219z"/>
\t<path d="M377.018,336.606c0.768,0,1.458,0.169,2.07,0.505s1.101,0.863,1.467,1.584c0.366,0.72,0.549,1.655,0.549,2.808
\t\ts-0.183,2.085-0.549,2.799c-0.366,0.714-0.855,1.239-1.467,1.575s-1.302,0.504-2.07,0.504c-0.756,0-1.443-0.168-2.061-0.504
\t\tc-0.618-0.336-1.11-0.861-1.476-1.575c-0.366-0.714-0.549-1.646-0.549-2.799s0.183-2.088,0.549-2.808
\t\tc0.366-0.721,0.858-1.248,1.476-1.584C375.574,336.775,376.261,336.606,377.018,336.606z M377.018,336.967
\t\tc-0.684,0-1.245,0.354-1.683,1.063s-0.657,1.865-0.657,3.474s0.219,2.763,0.657,3.465s0.999,1.053,1.683,1.053
\t\ts1.245-0.351,1.683-1.053c0.438-0.702,0.657-1.856,0.657-3.465s-0.219-2.766-0.657-3.474
\t\tC378.262,337.321,377.702,336.967,377.018,336.967z"/>
\t<path d="M388.519,336.606c0.456,0,0.837,0.061,1.143,0.181c0.306,0.12,0.561,0.288,0.765,0.504c0.228,0.24,0.39,0.54,0.486,0.899
\t\tc0.096,0.36,0.144,0.847,0.144,1.458v4.807c0,0.504,0.105,0.846,0.315,1.025c0.21,0.181,0.561,0.271,1.053,0.271v0.378
\t\tc-0.204-0.012-0.516-0.027-0.936-0.045c-0.42-0.019-0.828-0.027-1.224-0.027s-0.783,0.009-1.161,0.027
\t\tc-0.378,0.018-0.663,0.033-0.855,0.045v-0.378c0.432,0,0.738-0.09,0.918-0.271c0.18-0.18,0.27-0.521,0.27-1.025v-5.202
\t\tc0-0.372-0.03-0.714-0.09-1.026c-0.06-0.312-0.195-0.563-0.405-0.756c-0.21-0.191-0.531-0.288-0.963-0.288
\t\tc-0.756,0-1.365,0.315-1.827,0.945c-0.462,0.63-0.693,1.407-0.693,2.331v3.996c0,0.504,0.09,0.846,0.27,1.025
\t\tc0.18,0.181,0.486,0.271,0.918,0.271v0.378c-0.192-0.012-0.477-0.027-0.855-0.045c-0.378-0.019-0.765-0.027-1.161-0.027
\t\ts-0.804,0.009-1.224,0.027c-0.42,0.018-0.732,0.033-0.936,0.045v-0.378c0.492,0,0.843-0.09,1.053-0.271
\t\tc0.209-0.18,0.315-0.521,0.315-1.025v-5.634c0-0.54-0.096-0.939-0.288-1.197c-0.192-0.258-0.552-0.387-1.08-0.387v-0.378
\t\tc0.384,0.035,0.756,0.054,1.116,0.054c0.348,0,0.681-0.021,0.999-0.063c0.317-0.042,0.609-0.104,0.873-0.188v2.124
\t\tc0.312-0.816,0.747-1.383,1.305-1.701S387.907,336.606,388.519,336.606z"/>
\t<path d="M397.627,336.606c0.528,0,1.023,0.126,1.485,0.379c0.462,0.252,0.771,0.684,0.927,1.296l-0.234,0.18
\t\tc-0.168-0.504-0.429-0.87-0.783-1.098c-0.354-0.229-0.771-0.343-1.251-0.343c-0.708,0-1.299,0.366-1.773,1.099
\t\tc-0.475,0.731-0.705,1.86-0.693,3.384c0,0.972,0.093,1.779,0.279,2.421c0.186,0.643,0.461,1.122,0.828,1.44
\t\tc0.366,0.317,0.813,0.477,1.341,0.477c0.504,0,0.96-0.216,1.368-0.647c0.408-0.433,0.654-1.068,0.738-1.908l0.216,0.252
\t\tc-0.096,0.912-0.387,1.614-0.873,2.105c-0.486,0.492-1.143,0.738-1.971,0.738c-0.744,0-1.395-0.18-1.953-0.54
\t\ts-0.984-0.9-1.278-1.62c-0.294-0.72-0.441-1.626-0.441-2.718s0.189-2.001,0.567-2.727c0.378-0.727,0.875-1.27,1.494-1.63
\t\tC396.238,336.787,396.907,336.606,397.627,336.606z M401.407,332.017v12.15c0,0.54,0.099,0.939,0.297,1.197
\t\tc0.198,0.258,0.555,0.387,1.071,0.387v0.378c-0.372-0.036-0.744-0.054-1.116-0.054c-0.348,0-0.681,0.018-0.999,0.054
\t\ts-0.609,0.102-0.873,0.198v-12.15c0-0.54-0.096-0.938-0.288-1.197c-0.192-0.258-0.552-0.387-1.08-0.387v-0.378
\t\tc0.384,0.036,0.756,0.054,1.116,0.054c0.348,0,0.681-0.021,0.999-0.063S401.143,332.101,401.407,332.017z"/>
\t<path d="M413.755,336.606c0.324,0,0.609,0.066,0.855,0.198c0.246,0.133,0.438,0.304,0.576,0.514
\t\tc0.138,0.21,0.207,0.446,0.207,0.711c0,0.3-0.093,0.563-0.279,0.792c-0.186,0.228-0.435,0.342-0.747,0.342
\t\tc-0.252,0-0.474-0.081-0.666-0.243s-0.288-0.393-0.288-0.693c0-0.228,0.063-0.423,0.189-0.585s0.267-0.29,0.423-0.387
\t\tc-0.084-0.12-0.21-0.18-0.378-0.18c-0.504,0-0.942,0.188-1.314,0.566c-0.373,0.378-0.664,0.831-0.873,1.359
\t\tc-0.21,0.528-0.315,1.014-0.315,1.458v3.816c0,0.588,0.171,0.98,0.513,1.179c0.342,0.198,0.807,0.297,1.395,0.297v0.378
\t\tc-0.276-0.012-0.666-0.027-1.17-0.045c-0.504-0.019-1.038-0.027-1.602-0.027c-0.408,0-0.813,0.009-1.215,0.027
\t\tc-0.402,0.018-0.705,0.033-0.909,0.045v-0.378c0.492,0,0.843-0.09,1.053-0.271c0.209-0.18,0.315-0.521,0.315-1.025v-5.634
\t\tc0-0.54-0.096-0.939-0.288-1.197c-0.192-0.258-0.552-0.387-1.08-0.387v-0.378c0.384,0.035,0.756,0.054,1.116,0.054
\t\tc0.348,0,0.681-0.021,0.999-0.063c0.317-0.042,0.609-0.104,0.873-0.188v2.25c0.132-0.348,0.321-0.699,0.567-1.054
\t\tc0.246-0.354,0.542-0.65,0.891-0.891S413.334,336.606,413.755,336.606z"/>
\t<path d="M420.181,336.606c1.056,0,1.881,0.321,2.476,0.964c0.594,0.642,0.891,1.641,0.891,2.997h-6.354l-0.018-0.343h4.644
\t\tc0.024-0.588-0.024-1.131-0.144-1.629c-0.12-0.498-0.309-0.896-0.567-1.197c-0.258-0.3-0.591-0.449-0.999-0.449
\t\tc-0.552,0-1.042,0.275-1.467,0.828c-0.426,0.552-0.681,1.428-0.765,2.628l0.054,0.071c-0.024,0.181-0.042,0.378-0.054,0.595
\t\tc-0.012,0.216-0.018,0.432-0.018,0.647c0,0.816,0.132,1.519,0.396,2.106c0.264,0.588,0.609,1.035,1.035,1.341
\t\ts0.867,0.459,1.323,0.459c0.54,0,1.038-0.132,1.494-0.396c0.456-0.264,0.84-0.731,1.152-1.403l0.36,0.144
\t\tc-0.133,0.396-0.349,0.78-0.648,1.152c-0.301,0.372-0.678,0.675-1.134,0.909c-0.456,0.233-0.984,0.351-1.584,0.351
\t\tc-0.864,0-1.605-0.198-2.223-0.594c-0.618-0.396-1.092-0.945-1.422-1.647c-0.331-0.702-0.495-1.509-0.495-2.421
\t\tc0-1.056,0.168-1.965,0.504-2.727s0.81-1.351,1.422-1.765C418.651,336.813,419.364,336.606,420.181,336.606z"/>
\t<path d="M427.326,346.255c-0.779,0-1.347-0.195-1.701-0.585c-0.354-0.39-0.53-0.909-0.53-1.557c0-0.504,0.122-0.921,0.368-1.251
\t\ts0.562-0.601,0.945-0.811c0.385-0.21,0.789-0.39,1.215-0.54c0.426-0.149,0.832-0.297,1.215-0.44
\t\tc0.385-0.145,0.699-0.307,0.945-0.486s0.369-0.408,0.369-0.684v-1.116c0-0.504-0.074-0.888-0.225-1.152
\t\tc-0.15-0.264-0.352-0.44-0.604-0.531c-0.252-0.09-0.533-0.135-0.846-0.135c-0.3,0-0.621,0.042-0.963,0.126
\t\ts-0.615,0.259-0.818,0.522c0.227,0.048,0.426,0.168,0.594,0.359c0.168,0.192,0.252,0.438,0.252,0.738s-0.097,0.537-0.289,0.711
\t\tc-0.191,0.174-0.438,0.261-0.737,0.261c-0.349,0-0.603-0.11-0.765-0.333c-0.162-0.222-0.243-0.471-0.243-0.747
\t\tc0-0.312,0.077-0.563,0.233-0.756c0.156-0.191,0.354-0.371,0.594-0.54c0.276-0.191,0.633-0.356,1.072-0.495
\t\tc0.438-0.138,0.932-0.207,1.484-0.207c0.492,0,0.912,0.058,1.26,0.172s0.637,0.278,0.864,0.494c0.312,0.288,0.516,0.64,0.612,1.054
\t\tc0.096,0.414,0.144,0.909,0.144,1.484v5.004c0,0.288,0.042,0.502,0.126,0.64s0.229,0.207,0.432,0.207
\t\tc0.168,0,0.315-0.036,0.441-0.108s0.249-0.162,0.369-0.27l0.197,0.306c-0.252,0.192-0.492,0.351-0.719,0.477
\t\tc-0.229,0.127-0.535,0.189-0.918,0.189c-0.625,0-1.039-0.162-1.242-0.486c-0.205-0.323-0.307-0.695-0.307-1.115
\t\tc-0.384,0.636-0.818,1.062-1.305,1.277S427.854,346.255,427.326,346.255z M428.119,345.535c0.359,0,0.713-0.105,1.062-0.315
\t\tc0.348-0.21,0.672-0.561,0.972-1.053v-3.528c-0.18,0.265-0.459,0.475-0.837,0.63c-0.378,0.156-0.769,0.327-1.17,0.514
\t\tc-0.402,0.186-0.747,0.438-1.035,0.756s-0.432,0.771-0.432,1.358c0,0.553,0.138,0.964,0.414,1.233
\t\tC427.369,345.4,427.711,345.535,428.119,345.535z"/>
\t<path d="M437.101,336.606c0.479,0,0.888,0.079,1.224,0.234c0.336,0.156,0.582,0.3,0.738,0.432c0.396,0.324,0.636,0.108,0.72-0.647
\t\th0.414c-0.024,0.336-0.042,0.735-0.054,1.197s-0.018,1.083-0.018,1.862h-0.414c-0.072-0.443-0.195-0.875-0.369-1.296
\t\tc-0.174-0.42-0.43-0.765-0.766-1.035c-0.336-0.27-0.773-0.404-1.314-0.404c-0.42,0-0.771,0.116-1.053,0.351
\t\tc-0.281,0.234-0.422,0.579-0.422,1.035c0,0.36,0.107,0.669,0.323,0.927c0.216,0.259,0.498,0.498,0.847,0.72
\t\tc0.348,0.223,0.725,0.466,1.133,0.729c0.685,0.444,1.267,0.895,1.746,1.35c0.48,0.457,0.721,1.057,0.721,1.801
\t\tc0,0.552-0.145,1.014-0.432,1.386c-0.289,0.372-0.664,0.654-1.125,0.846c-0.463,0.192-0.97,0.288-1.521,0.288
\t\tc-0.264,0-0.514-0.021-0.747-0.063c-0.233-0.043-0.459-0.105-0.675-0.189c-0.12-0.06-0.243-0.132-0.369-0.216
\t\ts-0.249-0.174-0.369-0.271c-0.12-0.096-0.229-0.093-0.324,0.01c-0.096,0.102-0.168,0.303-0.216,0.603h-0.414
\t\tc0.024-0.384,0.042-0.852,0.054-1.404c0.012-0.552,0.019-1.283,0.019-2.195h0.414c0.083,0.672,0.204,1.26,0.36,1.764
\t\tc0.155,0.504,0.401,0.897,0.737,1.179c0.336,0.282,0.804,0.423,1.404,0.423c0.36,0,0.717-0.126,1.071-0.378
\t\tc0.354-0.252,0.531-0.695,0.531-1.332c0-0.527-0.193-0.96-0.576-1.296c-0.385-0.336-0.871-0.689-1.459-1.062
\t\tc-0.432-0.276-0.836-0.552-1.215-0.828c-0.377-0.276-0.684-0.588-0.918-0.937c-0.234-0.348-0.351-0.768-0.351-1.26
\t\tc0-0.54,0.123-0.98,0.369-1.323c0.245-0.342,0.573-0.594,0.981-0.756C436.194,336.688,436.633,336.606,437.101,336.606z"/>
\t<path d="M446.334,336.606c0.768,0,1.459,0.169,2.07,0.505s1.102,0.863,1.467,1.584c0.366,0.72,0.549,1.655,0.549,2.808
\t\ts-0.183,2.085-0.549,2.799c-0.365,0.714-0.855,1.239-1.467,1.575s-1.303,0.504-2.07,0.504c-0.756,0-1.443-0.168-2.061-0.504
\t\tc-0.618-0.336-1.11-0.861-1.477-1.575c-0.365-0.714-0.549-1.646-0.549-2.799s0.184-2.088,0.549-2.808
\t\tc0.366-0.721,0.858-1.248,1.477-1.584C444.891,336.775,445.578,336.606,446.334,336.606z M446.334,336.967
\t\tc-0.684,0-1.244,0.354-1.683,1.063s-0.657,1.865-0.657,3.474s0.219,2.763,0.657,3.465s0.999,1.053,1.683,1.053
\t\tc0.685,0,1.245-0.351,1.684-1.053c0.438-0.702,0.656-1.856,0.656-3.465s-0.219-2.766-0.656-3.474
\t\tC447.579,337.321,447.019,336.967,446.334,336.967z"/>
\t<path d="M457.836,336.606c0.456,0,0.838,0.061,1.143,0.181c0.307,0.12,0.562,0.288,0.766,0.504c0.229,0.24,0.39,0.54,0.486,0.899
\t\tc0.096,0.36,0.144,0.847,0.144,1.458v4.807c0,0.504,0.104,0.846,0.315,1.025c0.209,0.181,0.561,0.271,1.053,0.271v0.378
\t\tc-0.204-0.012-0.516-0.027-0.936-0.045c-0.421-0.019-0.828-0.027-1.225-0.027s-0.783,0.009-1.161,0.027
\t\tc-0.378,0.018-0.663,0.033-0.854,0.045v-0.378c0.432,0,0.738-0.09,0.918-0.271c0.18-0.18,0.27-0.521,0.27-1.025v-5.202
\t\tc0-0.372-0.029-0.714-0.09-1.026c-0.061-0.312-0.195-0.563-0.404-0.756c-0.211-0.191-0.531-0.288-0.964-0.288
\t\tc-0.756,0-1.364,0.315-1.827,0.945c-0.462,0.63-0.693,1.407-0.693,2.331v3.996c0,0.504,0.091,0.846,0.271,1.025
\t\tc0.181,0.181,0.486,0.271,0.919,0.271v0.378c-0.193-0.012-0.478-0.027-0.855-0.045c-0.379-0.019-0.766-0.027-1.161-0.027
\t\ts-0.804,0.009-1.224,0.027c-0.421,0.018-0.732,0.033-0.937,0.045v-0.378c0.491,0,0.843-0.09,1.054-0.271
\t\tc0.209-0.18,0.314-0.521,0.314-1.025v-5.634c0-0.54-0.096-0.939-0.288-1.197s-0.552-0.387-1.08-0.387v-0.378
\t\tc0.384,0.035,0.756,0.054,1.116,0.054c0.348,0,0.681-0.021,0.999-0.063c0.317-0.042,0.608-0.104,0.872-0.188v2.124
\t\tc0.313-0.816,0.748-1.383,1.306-1.701S457.225,336.606,457.836,336.606z"/>
\t<path d="M464.945,346.255c-0.779,0-1.347-0.195-1.701-0.585c-0.354-0.39-0.53-0.909-0.53-1.557c0-0.504,0.122-0.921,0.368-1.251
\t\ts0.562-0.601,0.945-0.811c0.385-0.21,0.789-0.39,1.215-0.54c0.426-0.149,0.832-0.297,1.215-0.44
\t\tc0.385-0.145,0.699-0.307,0.945-0.486s0.369-0.408,0.369-0.684v-1.116c0-0.504-0.074-0.888-0.225-1.152
\t\tc-0.15-0.264-0.352-0.44-0.604-0.531c-0.252-0.09-0.533-0.135-0.846-0.135c-0.3,0-0.621,0.042-0.963,0.126
\t\ts-0.615,0.259-0.818,0.522c0.227,0.048,0.426,0.168,0.594,0.359c0.168,0.192,0.252,0.438,0.252,0.738s-0.097,0.537-0.289,0.711
\t\tc-0.191,0.174-0.438,0.261-0.737,0.261c-0.349,0-0.603-0.11-0.765-0.333c-0.162-0.222-0.243-0.471-0.243-0.747
\t\tc0-0.312,0.077-0.563,0.233-0.756c0.156-0.191,0.354-0.371,0.594-0.54c0.276-0.191,0.633-0.356,1.072-0.495
\t\tc0.438-0.138,0.932-0.207,1.484-0.207c0.492,0,0.912,0.058,1.26,0.172s0.637,0.278,0.864,0.494c0.312,0.288,0.516,0.64,0.612,1.054
\t\tc0.096,0.414,0.144,0.909,0.144,1.484v5.004c0,0.288,0.042,0.502,0.126,0.64s0.229,0.207,0.432,0.207
\t\tc0.168,0,0.315-0.036,0.441-0.108s0.249-0.162,0.369-0.27l0.197,0.306c-0.252,0.192-0.492,0.351-0.719,0.477
\t\tc-0.229,0.127-0.535,0.189-0.918,0.189c-0.625,0-1.039-0.162-1.242-0.486c-0.205-0.323-0.307-0.695-0.307-1.115
\t\tc-0.384,0.636-0.818,1.062-1.305,1.277S465.474,346.255,464.945,346.255z M465.738,345.535c0.359,0,0.713-0.105,1.062-0.315
\t\tc0.348-0.21,0.672-0.561,0.972-1.053v-3.528c-0.18,0.265-0.459,0.475-0.837,0.63c-0.378,0.156-0.769,0.327-1.17,0.514
\t\tc-0.402,0.186-0.747,0.438-1.035,0.756s-0.432,0.771-0.432,1.358c0,0.553,0.138,0.964,0.414,1.233
\t\tC464.988,345.4,465.33,345.535,465.738,345.535z"/>
\t<path d="M474.035,332.035v13.158c-0.156,0.132-0.314,0.267-0.477,0.404c-0.162,0.139-0.321,0.273-0.477,0.405
\t\tc-0.156,0.132-0.318,0.27-0.486,0.414l-0.307-0.126c0.049-0.168,0.082-0.339,0.1-0.513s0.027-0.352,0.027-0.531v-11.052
\t\tc0-0.54-0.097-0.939-0.289-1.197c-0.191-0.258-0.551-0.387-1.08-0.387v-0.379c0.385,0.036,0.757,0.055,1.117,0.055
\t\tc0.348,0,0.681-0.021,0.998-0.063C473.48,332.182,473.771,332.119,474.035,332.035z M476.844,336.606
\t\tc0.984,0,1.8,0.405,2.447,1.216c0.648,0.81,0.973,2.037,0.973,3.681c0,1.08-0.184,1.983-0.549,2.709
\t\tc-0.366,0.727-0.855,1.269-1.467,1.629c-0.612,0.36-1.278,0.54-1.998,0.54c-0.601,0-1.18-0.171-1.737-0.513
\t\ts-0.976-0.849-1.251-1.521l0.359,0.198c0.301,0.48,0.672,0.837,1.117,1.071c0.443,0.233,0.899,0.351,1.367,0.351
\t\tc0.804,0,1.406-0.362,1.809-1.089c0.402-0.726,0.604-1.851,0.604-3.375c0-0.972-0.078-1.782-0.234-2.43
\t\tc-0.156-0.648-0.396-1.131-0.72-1.449c-0.323-0.318-0.737-0.478-1.241-0.478c-0.553,0-1.066,0.25-1.539,0.747
\t\tc-0.475,0.498-0.766,1.228-0.873,2.188l-0.217-0.252c0.133-1.02,0.489-1.812,1.071-2.376S476.039,336.606,476.844,336.606z"/>
\t<path d="M484.313,332.017v12.438c0,0.504,0.105,0.846,0.316,1.025c0.209,0.181,0.561,0.271,1.053,0.271v0.378
\t\tc-0.205-0.012-0.514-0.027-0.928-0.045c-0.414-0.019-0.83-0.027-1.25-0.027c-0.408,0-0.822,0.009-1.242,0.027
\t\tc-0.421,0.018-0.732,0.033-0.937,0.045v-0.378c0.491,0,0.843-0.09,1.054-0.271c0.209-0.18,0.314-0.521,0.314-1.025v-10.278
\t\tc0-0.54-0.096-0.938-0.288-1.197c-0.192-0.258-0.552-0.387-1.08-0.387v-0.378c0.384,0.036,0.756,0.054,1.116,0.054
\t\tc0.348,0,0.681-0.021,0.999-0.063C483.758,332.164,484.049,332.101,484.313,332.017z"/>
\t<path d="M490.793,336.606c1.057,0,1.881,0.321,2.476,0.964c0.594,0.642,0.892,1.641,0.892,2.997h-6.354l-0.019-0.343h4.645
\t\tc0.023-0.588-0.024-1.131-0.145-1.629c-0.119-0.498-0.309-0.896-0.566-1.197c-0.258-0.3-0.592-0.449-0.999-0.449
\t\tc-0.552,0-1.042,0.275-1.468,0.828c-0.426,0.552-0.681,1.428-0.765,2.628l0.054,0.071c-0.023,0.181-0.041,0.378-0.054,0.595
\t\tc-0.012,0.216-0.018,0.432-0.018,0.647c0,0.816,0.132,1.519,0.396,2.106s0.609,1.035,1.035,1.341s0.867,0.459,1.323,0.459
\t\tc0.54,0,1.038-0.132,1.493-0.396c0.457-0.264,0.84-0.731,1.152-1.403l0.36,0.144c-0.132,0.396-0.349,0.78-0.647,1.152
\t\tc-0.301,0.372-0.679,0.675-1.135,0.909c-0.456,0.233-0.984,0.351-1.584,0.351c-0.863,0-1.605-0.198-2.223-0.594
\t\tc-0.618-0.396-1.092-0.945-1.422-1.647s-0.495-1.509-0.495-2.421c0-1.056,0.168-1.965,0.504-2.727s0.81-1.351,1.422-1.765
\t\tS489.977,336.606,490.793,336.606z"/>
\t<path d="M504.42,336.606c0.527,0,1.022,0.126,1.484,0.379c0.462,0.252,0.771,0.684,0.928,1.296l-0.234,0.18
\t\tc-0.168-0.504-0.43-0.87-0.783-1.098c-0.354-0.229-0.771-0.343-1.251-0.343c-0.708,0-1.3,0.366-1.772,1.099
\t\tc-0.475,0.731-0.705,1.86-0.693,3.384c0,0.972,0.092,1.779,0.279,2.421c0.186,0.643,0.461,1.122,0.827,1.44
\t\tc0.366,0.317,0.813,0.477,1.341,0.477c0.504,0,0.961-0.216,1.368-0.647c0.408-0.433,0.654-1.068,0.738-1.908l0.216,0.252
\t\tc-0.096,0.912-0.387,1.614-0.873,2.105c-0.486,0.492-1.143,0.738-1.971,0.738c-0.744,0-1.395-0.18-1.953-0.54
\t\ts-0.984-0.9-1.277-1.62c-0.295-0.72-0.441-1.626-0.441-2.718s0.189-2.001,0.566-2.727c0.379-0.727,0.876-1.27,1.494-1.63
\t\tC503.03,336.787,503.699,336.606,504.42,336.606z M508.199,332.017v12.15c0,0.54,0.1,0.939,0.297,1.197
\t\tc0.198,0.258,0.555,0.387,1.071,0.387v0.378c-0.372-0.036-0.744-0.054-1.116-0.054c-0.348,0-0.682,0.018-0.999,0.054
\t\ts-0.608,0.102-0.873,0.198v-12.15c0-0.54-0.097-0.938-0.288-1.197c-0.191-0.258-0.552-0.387-1.08-0.387v-0.378
\t\tc0.385,0.036,0.756,0.054,1.116,0.054c0.349,0,0.681-0.021,0.999-0.063S507.936,332.101,508.199,332.017z"/>
\t<path d="M514.877,336.606c0.768,0,1.459,0.169,2.07,0.505s1.102,0.863,1.467,1.584c0.366,0.72,0.549,1.655,0.549,2.808
\t\ts-0.183,2.085-0.549,2.799c-0.365,0.714-0.855,1.239-1.467,1.575s-1.303,0.504-2.07,0.504c-0.756,0-1.443-0.168-2.061-0.504
\t\tc-0.618-0.336-1.11-0.861-1.477-1.575c-0.365-0.714-0.549-1.646-0.549-2.799s0.184-2.088,0.549-2.808
\t\tc0.366-0.721,0.858-1.248,1.477-1.584C513.434,336.775,514.121,336.606,514.877,336.606z M514.877,336.967
\t\tc-0.684,0-1.244,0.354-1.683,1.063s-0.657,1.865-0.657,3.474s0.219,2.763,0.657,3.465s0.999,1.053,1.683,1.053
\t\tc0.685,0,1.245-0.351,1.684-1.053c0.438-0.702,0.656-1.856,0.656-3.465s-0.219-2.766-0.656-3.474
\t\tC516.122,337.321,515.562,336.967,514.877,336.967z"/>
\t<path d="M528.503,336.661v7.523c0,0.54,0.099,0.939,0.298,1.197c0.197,0.258,0.555,0.387,1.07,0.387v0.378
\t\tc-0.372-0.035-0.744-0.054-1.116-0.054c-0.349,0-0.681,0.019-0.999,0.054c-0.318,0.036-0.609,0.103-0.873,0.198v-2.124
\t\tc-0.3,0.78-0.723,1.335-1.269,1.665s-1.106,0.495-1.683,0.495c-0.421,0-0.787-0.061-1.099-0.18
\t\tc-0.312-0.12-0.569-0.288-0.774-0.504c-0.228-0.24-0.384-0.552-0.467-0.937c-0.085-0.384-0.127-0.857-0.127-1.422v-4.518
\t\tc0-0.54-0.096-0.939-0.287-1.197c-0.193-0.258-0.553-0.387-1.08-0.387v-0.378c0.383,0.035,0.756,0.054,1.115,0.054
\t\tc0.348,0,0.682-0.021,0.999-0.063c0.317-0.042,0.608-0.104,0.873-0.188v7.074c0,0.372,0.024,0.714,0.072,1.025
\t\tc0.048,0.313,0.168,0.564,0.36,0.756c0.191,0.192,0.504,0.288,0.936,0.288c0.708,0,1.289-0.314,1.746-0.944
\t\tc0.455-0.631,0.684-1.401,0.684-2.313v-3.726c0-0.54-0.096-0.939-0.287-1.197c-0.193-0.258-0.553-0.387-1.08-0.387v-0.378
\t\tc0.383,0.035,0.756,0.054,1.115,0.054c0.348,0,0.682-0.021,0.999-0.063C527.947,336.808,528.238,336.745,528.503,336.661z"/>
\t<path d="M533.327,332.035v13.158c-0.156,0.132-0.315,0.267-0.478,0.404c-0.162,0.139-0.32,0.273-0.477,0.405
\t\ts-0.318,0.27-0.486,0.414l-0.306-0.126c0.048-0.168,0.081-0.339,0.099-0.513c0.019-0.174,0.027-0.352,0.027-0.531v-11.052
\t\tc0-0.54-0.096-0.939-0.288-1.197s-0.552-0.387-1.08-0.387v-0.379c0.384,0.036,0.757,0.055,1.116,0.055
\t\tc0.348,0,0.682-0.021,0.999-0.063S533.063,332.119,533.327,332.035z M536.135,336.606c0.984,0,1.801,0.405,2.448,1.216
\t\tc0.647,0.81,0.972,2.037,0.972,3.681c0,1.08-0.183,1.983-0.549,2.709c-0.365,0.727-0.855,1.269-1.467,1.629s-1.277,0.54-1.998,0.54
\t\tc-0.6,0-1.179-0.171-1.736-0.513c-0.559-0.342-0.976-0.849-1.252-1.521l0.36,0.198c0.3,0.48,0.672,0.837,1.116,1.071
\t\tc0.443,0.233,0.9,0.351,1.367,0.351c0.805,0,1.407-0.362,1.81-1.089c0.401-0.726,0.603-1.851,0.603-3.375
\t\tc0-0.972-0.078-1.782-0.233-2.43c-0.156-0.648-0.396-1.131-0.72-1.449s-0.738-0.478-1.242-0.478c-0.553,0-1.065,0.25-1.539,0.747
\t\tc-0.475,0.498-0.766,1.228-0.873,2.188l-0.217-0.252c0.133-1.02,0.49-1.812,1.072-2.376S535.331,336.606,536.135,336.606z"/>
\t<path d="M543.82,333.871v3.006h2.664v0.36h-2.664v6.966c0,0.564,0.103,0.96,0.307,1.188c0.203,0.229,0.486,0.342,0.846,0.342
\t\tc0.36,0,0.672-0.146,0.936-0.44c0.265-0.294,0.492-0.789,0.685-1.485l0.36,0.09c-0.12,0.696-0.357,1.284-0.711,1.765
\t\tc-0.354,0.479-0.897,0.72-1.629,0.72c-0.408,0-0.744-0.051-1.008-0.153c-0.265-0.102-0.498-0.249-0.703-0.44
\t\tc-0.264-0.276-0.447-0.606-0.549-0.99s-0.152-0.894-0.152-1.53v-6.029h-1.729v-0.36h1.729v-2.754
\t\tc0.299-0.012,0.588-0.036,0.863-0.072C543.341,334.015,543.593,333.955,543.82,333.871z"/>
\t<path d="M554.135,333.871v3.006h2.664v0.36h-2.664v6.966c0,0.564,0.102,0.96,0.306,1.188c0.204,0.229,0.485,0.342,0.846,0.342
\t\ts0.673-0.146,0.937-0.44s0.492-0.789,0.684-1.485l0.359,0.09c-0.119,0.696-0.356,1.284-0.711,1.765
\t\tc-0.354,0.479-0.896,0.72-1.629,0.72c-0.408,0-0.744-0.051-1.008-0.153c-0.264-0.102-0.498-0.249-0.701-0.44
\t\tc-0.265-0.276-0.447-0.606-0.549-0.99c-0.103-0.384-0.154-0.894-0.154-1.53v-6.029h-1.728v-0.36h1.728v-2.754
\t\tc0.301-0.012,0.588-0.036,0.865-0.072C553.654,334.015,553.906,333.955,554.135,333.871z"/>
\t<path d="M562.019,336.606c0.768,0,1.458,0.169,2.069,0.505c0.612,0.336,1.102,0.863,1.468,1.584
\t\tc0.366,0.72,0.549,1.655,0.549,2.808s-0.183,2.085-0.549,2.799s-0.855,1.239-1.468,1.575c-0.611,0.336-1.302,0.504-2.069,0.504
\t\tc-0.757,0-1.443-0.168-2.062-0.504c-0.617-0.336-1.109-0.861-1.476-1.575s-0.549-1.646-0.549-2.799s0.183-2.088,0.549-2.808
\t\tc0.366-0.721,0.858-1.248,1.476-1.584C560.575,336.775,561.262,336.606,562.019,336.606z M562.019,336.967
\t\tc-0.685,0-1.245,0.354-1.683,1.063c-0.438,0.708-0.658,1.865-0.658,3.474s0.22,2.763,0.658,3.465
\t\tc0.438,0.702,0.998,1.053,1.683,1.053s1.245-0.351,1.683-1.053s0.657-1.856,0.657-3.465s-0.22-2.766-0.657-3.474
\t\tS562.703,336.967,562.019,336.967z"/>
\t<path d="M574.492,332.035v13.158c-0.156,0.132-0.314,0.267-0.477,0.404c-0.162,0.139-0.322,0.273-0.478,0.405
\t\tc-0.156,0.132-0.317,0.27-0.485,0.414l-0.307-0.126c0.048-0.168,0.081-0.339,0.1-0.513c0.018-0.174,0.026-0.352,0.026-0.531
\t\tv-11.052c0-0.54-0.097-0.939-0.288-1.197s-0.552-0.387-1.08-0.387v-0.379c0.385,0.036,0.756,0.055,1.116,0.055
\t\tc0.349,0,0.681-0.021,0.999-0.063S574.229,332.119,574.492,332.035z M577.301,336.606c0.983,0,1.799,0.405,2.447,1.216
\t\tc0.648,0.81,0.973,2.037,0.973,3.681c0,1.08-0.184,1.983-0.55,2.709c-0.366,0.727-0.854,1.269-1.467,1.629s-1.278,0.54-1.998,0.54
\t\tc-0.601,0-1.179-0.171-1.737-0.513c-0.558-0.342-0.975-0.849-1.25-1.521l0.359,0.198c0.3,0.48,0.672,0.837,1.116,1.071
\t\tc0.444,0.233,0.899,0.351,1.368,0.351c0.803,0,1.406-0.362,1.809-1.089c0.402-0.726,0.604-1.851,0.604-3.375
\t\tc0-0.972-0.078-1.782-0.234-2.43c-0.156-0.648-0.396-1.131-0.721-1.449c-0.323-0.318-0.737-0.478-1.241-0.478
\t\tc-0.552,0-1.065,0.25-1.539,0.747c-0.474,0.498-0.765,1.228-0.873,2.188l-0.216-0.252c0.132-1.02,0.488-1.812,1.07-2.376
\t\tS576.496,336.606,577.301,336.606z"/>
\t<path d="M586.462,336.606c1.056,0,1.881,0.321,2.476,0.964c0.594,0.642,0.891,1.641,0.891,2.997h-6.354l-0.019-0.343h4.644
\t\tc0.024-0.588-0.023-1.131-0.144-1.629s-0.31-0.896-0.567-1.197c-0.258-0.3-0.591-0.449-0.998-0.449
\t\tc-0.553,0-1.042,0.275-1.468,0.828c-0.427,0.552-0.681,1.428-0.765,2.628l0.054,0.071c-0.024,0.181-0.042,0.378-0.054,0.595
\t\tc-0.012,0.216-0.018,0.432-0.018,0.647c0,0.816,0.131,1.519,0.396,2.106c0.264,0.588,0.608,1.035,1.034,1.341
\t\ts0.867,0.459,1.324,0.459c0.539,0,1.037-0.132,1.493-0.396c0.456-0.264,0.84-0.731,1.152-1.403l0.36,0.144
\t\tc-0.133,0.396-0.349,0.78-0.648,1.152c-0.301,0.372-0.678,0.675-1.135,0.909c-0.455,0.233-0.983,0.351-1.584,0.351
\t\tc-0.863,0-1.604-0.198-2.223-0.594c-0.617-0.396-1.092-0.945-1.422-1.647s-0.494-1.509-0.494-2.421
\t\tc0-1.056,0.168-1.965,0.504-2.727s0.81-1.351,1.422-1.765C584.932,336.813,585.646,336.606,586.462,336.606z"/>
\t<path fill="#E11D74" d="M374.886,409.344c-0.7,0.117-1.33,0.222-1.89,0.314c0.093-0.77,0.187-1.527,0.28-2.274
\t\tc0.07-0.653,0.134-1.307,0.192-1.96c0.058-0.653,0.11-1.213,0.157-1.681h-3.045l-1.4,5.04c-0.514,0.07-1.12,0.146-1.82,0.228
\t\tc-0.7,0.082-1.354,0.158-1.96,0.228c-0.747,0.094-1.482,0.188-2.205,0.28c0.933-3.103,1.855-6.16,2.765-9.17
\t\tc0.373-1.26,0.764-2.578,1.173-3.955c0.408-1.377,0.816-2.747,1.225-4.112c0.408-1.365,0.799-2.678,1.172-3.938
\t\tc0.373-1.26,0.7-2.415,0.98-3.465c1.586-0.117,3.203-0.257,4.848-0.42c1.645-0.163,3.249-0.386,4.813-0.665
\t\tc-0.07,0.747-0.146,1.674-0.228,2.782c-0.082,1.108-0.158,2.315-0.228,3.622c-0.07,1.308-0.14,2.672-0.21,4.096
\t\tc-0.07,1.423-0.14,2.823-0.21,4.199c-0.117,3.221-0.245,6.639-0.385,10.255c-0.7,0.117-1.4,0.211-2.1,0.28
\t\tC376.227,409.122,375.585,409.227,374.886,409.344z M371.421,400.243c0.093,0,0.262-0.005,0.507-0.018
\t\tc0.245-0.011,0.501-0.022,0.77-0.034c0.268-0.012,0.519-0.023,0.752-0.035c0.233-0.012,0.396-0.029,0.49-0.053
\t\tc0-0.14,0.018-0.379,0.053-0.718c0.035-0.338,0.07-0.723,0.105-1.154s0.076-0.904,0.123-1.418c0.046-0.513,0.093-1.015,0.14-1.505
\t\tc0.093-1.19,0.21-2.473,0.35-3.85l0.21-3.046l-0.525,0.035l-0.805,3.011L371.421,400.243z"/>
\t<path fill="#E11D74" d="M380.521,409.868l2.66-24.85c1.096-0.047,2.129-0.157,3.097-0.333c0.968-0.175,1.931-0.297,2.888-0.367
\t\tc0,0.023-0.035,0.263-0.105,0.718c-0.07,0.455-0.164,1.05-0.28,1.785c-0.117,0.734-0.245,1.563-0.385,2.484
\t\tc-0.14,0.923-0.345,2.188-0.613,3.798s-0.625,3.839-1.067,6.685h0.525c0.607-2.146,1.108-3.902,1.505-5.267
\t\tc0.396-1.365,0.717-2.474,0.962-3.325s0.478-1.674,0.7-2.468c0.222-0.793,0.432-1.528,0.63-2.205
\t\tc0.198-0.677,0.355-1.248,0.472-1.715h0.315c0.42,0,0.845-0.023,1.277-0.07c0.432-0.046,0.84-0.099,1.225-0.157
\t\tc0.385-0.058,0.787-0.134,1.208-0.228c-0.047,0.21-0.1,0.642-0.158,1.295c-0.059,0.653-0.134,1.441-0.228,2.362
\t\tc-0.093,0.922-0.187,1.938-0.28,3.045c-0.094,1.108-0.187,2.194-0.28,3.255c-0.093,1.063-0.181,2.071-0.262,3.028
\t\tc-0.082,0.957-0.158,1.773-0.228,2.449h0.63l3.325-15.05c0.793-0.116,1.575-0.222,2.345-0.314c0.677-0.094,1.376-0.187,2.1-0.28
\t\tc0.723-0.093,1.365-0.198,1.925-0.315c-0.35,0.98-0.7,2.071-1.05,3.272c-0.35,1.202-0.712,2.468-1.085,3.798
\t\tc-0.374,1.33-0.729,2.672-1.067,4.025c-0.339,1.354-0.671,2.66-0.998,3.92c-0.747,2.986-1.47,6.055-2.17,9.205l-7.91,1.33
\t\tl1.54-13.511h-0.42l-3.465,12.705L380.521,409.868z"/>
\t<path fill="#E11D74" d="M400.4,409.764l4.585-24.885l11.06-0.945c-0.163,0.514-0.303,1.038-0.42,1.575
\t\tc-0.117,0.443-0.228,0.904-0.333,1.382c-0.105,0.479-0.192,0.905-0.262,1.278c-0.234,0.023-0.566,0.041-0.998,0.052
\t\tc-0.432,0.013-0.893,0.023-1.383,0.035c-0.49,0.012-0.986,0.018-1.487,0.018c-0.502,0-0.939,0-1.313,0l-1.225,6.265
\t\tc0.723-0.046,1.4-0.081,2.03-0.104c0.513-0.023,1.038-0.035,1.575-0.035c0.537,0,0.933-0.012,1.19-0.035
\t\tc-0.093,0.653-0.181,1.295-0.262,1.925c-0.082,0.631-0.181,1.272-0.297,1.926c-0.234,0.023-0.613,0.041-1.138,0.052
\t\tc-0.525,0.013-1.056,0.03-1.593,0.053c-0.607,0.023-1.272,0.047-1.995,0.07l-1.19,6.439l5.11-0.805l-0.35,3.99L400.4,409.764z"/>
\t<path fill="#E11D74" d="M426.808,404.636c-0.502,1.063-1.179,1.978-2.03,2.748c-0.852,0.77-1.854,1.364-3.01,1.785
\t\tc-1.155,0.42-2.397,0.63-3.728,0.63c-0.934,0-1.867-0.251-2.8-0.753c-0.934-0.501-1.832-1.301-2.695-2.397l2.59-5.075
\t\tc0.163,0.304,0.408,0.612,0.735,0.928c0.327,0.315,0.694,0.59,1.103,0.822c0.408,0.233,0.816,0.427,1.225,0.578
\t\tc0.408,0.151,0.775,0.228,1.103,0.228c0.28,0,0.571-0.047,0.875-0.141c0.303-0.093,0.583-0.232,0.84-0.42
\t\tc0.256-0.187,0.46-0.402,0.612-0.647s0.228-0.53,0.228-0.857c0-0.56-0.181-1.079-0.542-1.558c-0.362-0.478-0.811-0.933-1.347-1.365
\t\tc-0.537-0.431-1.12-0.868-1.75-1.313c-0.63-0.442-1.208-0.909-1.732-1.399c-0.525-0.49-0.975-1.038-1.348-1.646
\t\tc-0.374-0.606-0.56-1.282-0.56-2.029c0-1.213,0.361-2.346,1.085-3.396c0.723-1.05,1.627-1.954,2.712-2.712
\t\tc1.085-0.759,2.269-1.359,3.553-1.803c1.283-0.443,2.497-0.665,3.639-0.665c0.631,0,1.226,0.076,1.785,0.228
\t\tc0.561,0.151,1.045,0.396,1.453,0.734c0.408,0.339,0.734,0.765,0.98,1.278c0.244,0.513,0.367,1.154,0.367,1.925
\t\tc0,0.327-0.029,0.653-0.088,0.979c-0.059,0.327-0.111,0.678-0.157,1.05l-5.075,1.471c0.047-0.187,0.088-0.373,0.123-0.561
\t\tc0.035-0.187,0.053-0.373,0.053-0.56c0-0.7-0.123-1.208-0.368-1.522c-0.245-0.315-0.565-0.473-0.962-0.473
\t\tc-0.188,0-0.415,0.07-0.684,0.21c-0.268,0.14-0.537,0.321-0.805,0.543c-0.269,0.222-0.496,0.484-0.682,0.787
\t\tc-0.187,0.304-0.28,0.63-0.28,0.979c0,0.561,0.158,1.027,0.473,1.4c0.315,0.374,0.706,0.724,1.172,1.05
\t\tc0.466,0.327,0.974,0.66,1.522,0.998c0.548,0.338,1.056,0.775,1.522,1.313s0.857,1.207,1.173,2.013
\t\tc0.314,0.805,0.473,1.825,0.473,3.063C427.561,402.391,427.309,403.574,426.808,404.636z"/>
\t<path fill="#E11D74" d="M446.092,396.201c-0.408,1.691-0.95,3.243-1.627,4.655c-0.677,1.411-1.465,2.672-2.362,3.779
\t\tc-0.898,1.108-1.837,2.054-2.817,2.835c-0.98,0.782-1.973,1.377-2.975,1.785c-1.004,0.408-1.961,0.612-2.871,0.612
\t\tc-0.77,0-1.469-0.146-2.1-0.438c-0.63-0.291-1.178-0.723-1.645-1.295c-0.467-0.571-0.835-1.307-1.104-2.205
\t\tc-0.268-0.897-0.402-1.978-0.402-3.237c0-1.307,0.164-2.683,0.49-4.13c0.326-1.446,0.771-2.882,1.33-4.305
\t\tc0.561-1.423,1.248-2.776,2.065-4.061c0.816-1.282,1.715-2.42,2.694-3.412c0.98-0.991,2.03-1.779,3.15-2.362s2.287-0.875,3.5-0.875
\t\tc1.051,0,1.919,0.216,2.607,0.647c0.688,0.432,1.225,0.992,1.61,1.68c0.385,0.688,0.659,1.465,0.822,2.327
\t\tc0.163,0.864,0.245,1.728,0.245,2.591C446.705,392.707,446.5,394.51,446.092,396.201z M433.545,402.589
\t\tc0.094,0.279,0.222,0.524,0.385,0.734s0.367,0.392,0.613,0.543c0.244,0.151,0.507,0.228,0.787,0.228c1.05,0,1.982-0.361,2.8-1.085
\t\tc0.816-0.724,1.493-1.61,2.03-2.66c0.536-1.05,0.95-2.17,1.242-3.36c0.291-1.189,0.438-2.274,0.438-3.255
\t\tc0-0.28-0.018-0.583-0.053-0.91c-0.035-0.326-0.104-0.63-0.209-0.91c-0.105-0.279-0.27-0.519-0.49-0.717
\t\tc-0.223-0.198-0.52-0.298-0.893-0.298c-0.748,0-1.436,0.188-2.065,0.561s-1.196,0.869-1.697,1.487
\t\tc-0.502,0.618-0.939,1.318-1.313,2.1c-0.374,0.782-0.694,1.575-0.962,2.38c-0.27,0.806-0.462,1.593-0.578,2.362
\t\tc-0.117,0.771-0.176,1.436-0.176,1.995C433.404,402.041,433.451,402.309,433.545,402.589z"/>
\t<path fill="#E11D74" d="M443.311,409.519c1.026-3.103,2.006-6.148,2.939-9.135c0.396-1.261,0.805-2.578,1.225-3.955
\t\ts0.84-2.741,1.26-4.095s0.811-2.66,1.173-3.921c0.362-1.26,0.683-2.426,0.962-3.5c1.283,0,2.497-0.069,3.641-0.21
\t\tc1.143-0.14,2.38-0.268,3.71-0.385c-0.069,0.514-0.146,1.091-0.228,1.732c-0.082,0.643-0.175,1.313-0.279,2.013
\t\tc-0.105,0.7-0.217,1.424-0.333,2.17c-0.117,0.747-0.257,1.704-0.42,2.87c-0.163,1.167-0.386,2.625-0.665,4.375h0.455
\t\tc0.676-1.773,1.236-3.249,1.68-4.428c0.443-1.178,0.805-2.141,1.085-2.888c0.28-0.746,0.565-1.475,0.858-2.188
\t\tc0.291-0.711,0.57-1.388,0.84-2.029c0.268-0.642,0.519-1.208,0.752-1.698c0.583-0.046,1.131-0.104,1.645-0.175
\t\tc0.514-0.069,1.027-0.146,1.541-0.228c0.512-0.081,1.031-0.163,1.557-0.245c0.525-0.081,1.104-0.18,1.732-0.297
\t\tc-0.232,1.354-0.455,2.742-0.664,4.165c-0.211,1.423-0.397,2.841-0.561,4.252c-0.164,1.412-0.32,2.807-0.473,4.183
\t\tc-0.152,1.377-0.285,2.684-0.402,3.92c-0.279,2.894-0.525,5.718-0.734,8.471l-6.091,1.085c0-0.117,0.018-0.432,0.053-0.945
\t\tc0.035-0.513,0.081-1.143,0.14-1.89s0.141-1.575,0.245-2.485s0.21-1.82,0.315-2.73c0.105-0.909,0.215-1.79,0.332-2.643
\t\tc0.117-0.851,0.233-1.58,0.35-2.188h-0.314c-0.327,0.98-0.688,2.013-1.085,3.098c-0.397,1.085-0.8,2.188-1.208,3.308
\t\ts-0.811,2.217-1.207,3.29s-0.747,2.101-1.05,3.08c-0.817,0.023-1.517,0.053-2.101,0.088c-0.583,0.035-1.26,0.076-2.029,0.122
\t\tl1.119-12.985h-0.524c-0.047,0.304-0.163,0.812-0.351,1.523c-0.187,0.712-0.402,1.528-0.646,2.449
\t\tc-0.246,0.922-0.508,1.879-0.788,2.87c-0.28,0.992-0.542,1.914-0.787,2.766s-0.45,1.552-0.612,2.1
\t\tc-0.164,0.549-0.258,0.857-0.28,0.928L443.311,409.519z"/>
\t<path fill="#E11D74" d="M466.305,409.764l4.585-24.885l11.06-0.945c-0.163,0.514-0.303,1.038-0.42,1.575
\t\tc-0.117,0.443-0.228,0.904-0.332,1.382c-0.105,0.479-0.193,0.905-0.263,1.278c-0.233,0.023-0.566,0.041-0.997,0.052
\t\tc-0.433,0.013-0.893,0.023-1.383,0.035s-0.986,0.018-1.488,0.018s-0.939,0-1.313,0l-1.225,6.265c0.723-0.046,1.4-0.081,2.03-0.104
\t\tc0.513-0.023,1.038-0.035,1.575-0.035c0.536,0,0.933-0.012,1.189-0.035c-0.094,0.653-0.181,1.295-0.262,1.925
\t\tc-0.082,0.631-0.182,1.272-0.298,1.926c-0.233,0.023-0.612,0.041-1.138,0.052c-0.525,0.013-1.057,0.03-1.593,0.053
\t\tc-0.606,0.023-1.272,0.047-1.995,0.07l-1.189,6.439l5.109-0.805l-0.35,3.99L466.305,409.764z"/>
\t<path d="M395.436,504.197v-0.221c0.293-0.029,0.542-0.115,0.748-0.258c0.205-0.144,0.363-0.388,0.473-0.731
\t\tc0.11-0.345,0.165-0.829,0.165-1.452v-6.028c0-0.271-0.02-0.471-0.061-0.6c-0.04-0.128-0.119-0.216-0.236-0.264
\t\ts-0.293-0.075-0.528-0.083v-0.22c0.183,0.008,0.423,0.015,0.72,0.022c0.297,0.007,0.611,0.011,0.941,0.011
\t\tc0.33,0,0.65-0.004,0.962-0.011c0.312-0.008,0.57-0.015,0.775-0.022v0.22c-0.228,0.008-0.401,0.035-0.522,0.083
\t\ts-0.202,0.136-0.242,0.264c-0.04,0.129-0.061,0.328-0.061,0.6v4.026c0,0.301,0,0.625,0,0.974c0,0.348-0.013,0.688-0.039,1.017
\t\tc-0.025,0.33-0.075,0.624-0.148,0.881c-0.146,0.506-0.464,0.931-0.951,1.275C396.945,504.024,396.279,504.197,395.436,504.197z"/>
\t<path d="M402.586,496.288c0.645,0,1.151,0.19,1.518,0.572c0.367,0.381,0.55,1.004,0.55,1.869h-3.619l-0.022-0.209h2.255
\t\tc0.007-0.358-0.015-0.69-0.066-0.995s-0.132-0.548-0.242-0.731s-0.257-0.275-0.44-0.275c-0.257,0-0.475,0.162-0.655,0.484
\t\tc-0.18,0.323-0.288,0.858-0.324,1.606l0.033,0.065c-0.007,0.088-0.013,0.18-0.016,0.275c-0.004,0.095-0.006,0.194-0.006,0.297
\t\tc0,0.506,0.072,0.917,0.215,1.231c0.143,0.315,0.324,0.545,0.544,0.688c0.22,0.144,0.443,0.215,0.671,0.215
\t\tc0.22,0,0.46-0.055,0.72-0.165s0.504-0.348,0.732-0.715l0.198,0.066c-0.088,0.278-0.228,0.55-0.418,0.813
\t\tc-0.191,0.264-0.433,0.48-0.726,0.649c-0.293,0.168-0.646,0.253-1.056,0.253c-0.499,0-0.939-0.106-1.32-0.319
\t\ts-0.68-0.535-0.896-0.968s-0.324-0.982-0.324-1.65c0-0.682,0.115-1.25,0.346-1.705c0.231-0.454,0.55-0.793,0.957-1.018
\t\tC401.602,496.399,402.065,496.288,402.586,496.288z"/>
\t<path d="M409.318,496.288c0.308,0,0.559,0.038,0.753,0.115s0.347,0.182,0.457,0.313c0.117,0.14,0.204,0.319,0.259,0.539
\t\tc0.055,0.22,0.083,0.517,0.083,0.891v2.959c0,0.309,0.053,0.518,0.159,0.627c0.106,0.11,0.288,0.165,0.544,0.165v0.231
\t\tc-0.139-0.007-0.348-0.017-0.627-0.027s-0.554-0.017-0.825-0.017c-0.279,0-0.552,0.006-0.82,0.017s-0.471,0.021-0.61,0.027v-0.231
\t\tc0.22,0,0.374-0.055,0.462-0.165c0.088-0.109,0.132-0.318,0.132-0.627v-3.421c0-0.183-0.019-0.343-0.055-0.479
\t\ts-0.103-0.241-0.198-0.318s-0.238-0.116-0.429-0.116c-0.308,0-0.566,0.121-0.775,0.363s-0.314,0.547-0.314,0.913v3.058
\t\tc0,0.309,0.046,0.518,0.138,0.627c0.092,0.11,0.244,0.165,0.457,0.165v0.231c-0.132-0.007-0.325-0.017-0.578-0.027
\t\ts-0.515-0.017-0.786-0.017c-0.279,0-0.565,0.006-0.858,0.017s-0.513,0.021-0.66,0.027v-0.231c0.256,0,0.438-0.055,0.544-0.165
\t\tc0.106-0.109,0.159-0.318,0.159-0.627v-3.465c0-0.33-0.049-0.573-0.148-0.731c-0.099-0.157-0.284-0.236-0.555-0.236v-0.23
\t\tc0.234,0.021,0.462,0.032,0.682,0.032c0.308,0,0.596-0.013,0.864-0.038c0.268-0.025,0.515-0.064,0.742-0.115v0.989
\t\tc0.184-0.374,0.433-0.638,0.748-0.792C408.577,496.365,408.929,496.288,409.318,496.288z"/>
\t<path d="M414.158,496.288c0.308,0,0.578,0.038,0.809,0.115s0.398,0.152,0.5,0.226c0.249,0.169,0.403,0.059,0.462-0.33h0.231
\t\tc-0.015,0.205-0.025,0.457-0.033,0.754s-0.011,0.691-0.011,1.183h-0.231c-0.037-0.271-0.106-0.539-0.209-0.804
\t\tc-0.103-0.264-0.251-0.479-0.445-0.648c-0.194-0.169-0.446-0.253-0.753-0.253c-0.213,0-0.393,0.061-0.539,0.181
\t\tc-0.147,0.121-0.22,0.296-0.22,0.523c0,0.22,0.066,0.412,0.198,0.577c0.132,0.165,0.302,0.322,0.511,0.473s0.427,0.311,0.655,0.479
\t\tc0.366,0.271,0.676,0.545,0.929,0.819c0.253,0.275,0.379,0.633,0.379,1.073c0,0.33-0.095,0.617-0.286,0.863
\t\tc-0.191,0.246-0.442,0.435-0.753,0.566c-0.312,0.132-0.659,0.198-1.04,0.198c-0.22,0-0.417-0.022-0.589-0.066
\t\ts-0.321-0.099-0.445-0.165c-0.095-0.044-0.185-0.086-0.27-0.126c-0.084-0.041-0.163-0.075-0.236-0.104
\t\tc-0.074-0.022-0.138,0.003-0.193,0.076c-0.055,0.074-0.097,0.177-0.126,0.309h-0.231c0.015-0.234,0.026-0.521,0.033-0.858
\t\tc0.007-0.337,0.011-0.784,0.011-1.342h0.231c0.044,0.396,0.125,0.746,0.242,1.051c0.117,0.304,0.277,0.543,0.479,0.715
\t\ts0.457,0.259,0.765,0.259c0.19,0,0.367-0.061,0.528-0.182s0.242-0.321,0.242-0.6c0-0.322-0.104-0.591-0.313-0.803
\t\tc-0.209-0.213-0.475-0.444-0.798-0.693c-0.234-0.19-0.457-0.38-0.666-0.566c-0.209-0.188-0.379-0.393-0.511-0.616
\t\ts-0.198-0.479-0.198-0.765c0-0.33,0.088-0.608,0.264-0.836c0.176-0.227,0.409-0.397,0.699-0.512
\t\tC413.518,496.345,413.828,496.288,414.158,496.288z"/>
\t<path d="M423.134,494.341v0.22c-0.235,0.008-0.413,0.035-0.534,0.083c-0.12,0.048-0.199,0.136-0.236,0.264
\t\tc-0.036,0.129-0.055,0.328-0.055,0.6v5.456c0,0.264,0.021,0.462,0.061,0.594c0.041,0.132,0.119,0.221,0.236,0.265
\t\ts0.293,0.073,0.528,0.088v0.22c-0.198-0.015-0.447-0.023-0.748-0.027s-0.605-0.006-0.913-0.006c-0.352,0-0.684,0.002-0.995,0.006
\t\tc-0.312,0.004-0.56,0.013-0.743,0.027v-0.22c0.234-0.015,0.411-0.044,0.528-0.088s0.196-0.133,0.237-0.265
\t\tc0.04-0.132,0.061-0.33,0.061-0.594v-5.456c0-0.271-0.021-0.471-0.061-0.6c-0.041-0.128-0.121-0.216-0.242-0.264
\t\ts-0.295-0.075-0.522-0.083v-0.22c0.183,0.008,0.431,0.015,0.743,0.022c0.312,0.007,0.643,0.011,0.995,0.011
\t\tc0.308,0,0.612-0.004,0.913-0.011C422.687,494.355,422.936,494.349,423.134,494.341z M426.939,494.341v0.209
\t\tc-0.234,0.052-0.485,0.154-0.753,0.308c-0.269,0.154-0.548,0.404-0.842,0.748l-2.067,2.465l0.638-0.913l2.618,4.081
\t\tc0.102,0.168,0.213,0.304,0.33,0.406c0.117,0.104,0.271,0.191,0.461,0.265v0.22c-0.256-0.015-0.542-0.023-0.857-0.027
\t\ts-0.602-0.006-0.857-0.006c-0.154,0-0.342,0.002-0.562,0.006s-0.499,0.013-0.836,0.027v-0.22c0.3-0.015,0.481-0.057,0.544-0.127
\t\tc0.063-0.069,0.035-0.192-0.082-0.368l-1.596-2.585c-0.102-0.169-0.193-0.295-0.274-0.38c-0.081-0.084-0.161-0.143-0.241-0.176
\t\tc-0.082-0.033-0.188-0.053-0.32-0.061v-0.231c0.323-0.007,0.611-0.095,0.864-0.264s0.53-0.425,0.831-0.77l0.648-0.781
\t\tc0.278-0.33,0.441-0.614,0.489-0.853s0-0.424-0.144-0.556c-0.143-0.132-0.36-0.201-0.654-0.209v-0.209
\t\tc0.271,0.008,0.53,0.015,0.775,0.022c0.246,0.007,0.53,0.011,0.854,0.011C426.331,494.374,426.676,494.363,426.939,494.341z"/>
\t<path d="M430.141,496.288c0.535,0,1.004,0.099,1.408,0.297c0.402,0.198,0.719,0.517,0.945,0.957
\t\tc0.228,0.439,0.342,1.022,0.342,1.749c0,0.726-0.114,1.308-0.342,1.743c-0.227,0.437-0.543,0.754-0.945,0.952
\t\tc-0.404,0.197-0.873,0.297-1.408,0.297c-0.521,0-0.984-0.1-1.392-0.297c-0.407-0.198-0.726-0.516-0.957-0.952
\t\tc-0.231-0.436-0.347-1.018-0.347-1.743c0-0.727,0.115-1.31,0.347-1.749c0.231-0.44,0.55-0.759,0.957-0.957
\t\tS429.619,496.288,430.141,496.288z M430.141,496.508c-0.293,0-0.539,0.219-0.736,0.654c-0.199,0.437-0.298,1.146-0.298,2.129
\t\ts0.099,1.69,0.298,2.123c0.197,0.433,0.443,0.648,0.736,0.648c0.301,0,0.548-0.216,0.742-0.648s0.292-1.141,0.292-2.123
\t\ts-0.098-1.692-0.292-2.129C430.688,496.727,430.441,496.508,430.141,496.508z"/>
\t<path d="M436.355,496.288c0.535,0,1.004,0.099,1.408,0.297c0.402,0.198,0.719,0.517,0.945,0.957
\t\tc0.228,0.439,0.342,1.022,0.342,1.749c0,0.726-0.114,1.308-0.342,1.743c-0.227,0.437-0.543,0.754-0.945,0.952
\t\tc-0.404,0.197-0.873,0.297-1.408,0.297c-0.521,0-0.984-0.1-1.392-0.297c-0.407-0.198-0.726-0.516-0.957-0.952
\t\tc-0.231-0.436-0.347-1.018-0.347-1.743c0-0.727,0.115-1.31,0.347-1.749c0.231-0.44,0.55-0.759,0.957-0.957
\t\tS435.834,496.288,436.355,496.288z M436.355,496.508c-0.293,0-0.539,0.219-0.736,0.654c-0.199,0.437-0.298,1.146-0.298,2.129
\t\ts0.099,1.69,0.298,2.123c0.197,0.433,0.443,0.648,0.736,0.648c0.301,0,0.548-0.216,0.742-0.648s0.292-1.141,0.292-2.123
\t\ts-0.098-1.692-0.292-2.129C436.903,496.727,436.656,496.508,436.355,496.508z"/>
\t<path d="M442.02,496.321v4.784c0,0.309,0.054,0.518,0.16,0.627c0.106,0.11,0.287,0.165,0.545,0.165v0.231
\t\tc-0.133-0.007-0.338-0.017-0.616-0.027s-0.562-0.017-0.847-0.017c-0.287,0-0.576,0.006-0.869,0.017
\t\tc-0.294,0.011-0.514,0.021-0.66,0.027v-0.231c0.256,0,0.438-0.055,0.545-0.165c0.105-0.109,0.158-0.318,0.158-0.627v-3.465
\t\tc0-0.33-0.049-0.573-0.148-0.731c-0.098-0.157-0.283-0.236-0.555-0.236v-0.23c0.234,0.021,0.462,0.032,0.682,0.032
\t\tc0.309,0,0.596-0.013,0.863-0.038S441.793,496.372,442.02,496.321z M441.162,493.604c0.309,0,0.552,0.075,0.731,0.226
\t\ts0.27,0.357,0.27,0.622c0,0.264-0.09,0.471-0.27,0.621s-0.423,0.226-0.731,0.226c-0.308,0-0.552-0.075-0.731-0.226
\t\ts-0.27-0.357-0.27-0.621c0-0.265,0.09-0.472,0.27-0.622S440.854,493.604,441.162,493.604z"/>
\t<path d="M442.68,504.197v-0.198c0.359-0.008,0.646-0.148,0.858-0.424c0.212-0.274,0.319-0.691,0.319-1.248v-4.687
\t\tc0-0.33-0.052-0.573-0.154-0.731c-0.104-0.157-0.286-0.236-0.55-0.236v-0.23c0.234,0.021,0.462,0.032,0.682,0.032
\t\tc0.301,0,0.587-0.013,0.858-0.038s0.521-0.064,0.748-0.115v4.773c0,0.609-0.061,1.111-0.182,1.507
\t\tc-0.121,0.396-0.324,0.729-0.611,1.001c-0.183,0.177-0.434,0.319-0.753,0.43C443.576,504.142,443.172,504.197,442.68,504.197z
\t\t M444.528,493.604c0.308,0,0.552,0.075,0.731,0.226s0.27,0.357,0.27,0.622c0,0.264-0.09,0.471-0.27,0.621s-0.424,0.226-0.731,0.226
\t\ts-0.552-0.075-0.731-0.226s-0.27-0.357-0.27-0.621c0-0.265,0.09-0.472,0.27-0.622S444.221,493.604,444.528,493.604z"/>
\t<path d="M309.418,507.54v0.22c-0.25,0.008-0.438,0.035-0.566,0.083c-0.128,0.048-0.215,0.136-0.259,0.264
\t\tc-0.044,0.129-0.066,0.328-0.066,0.6v5.456c0,0.264,0.022,0.462,0.066,0.594s0.13,0.221,0.259,0.265
\t\tc0.128,0.044,0.317,0.073,0.566,0.088v0.22c-0.169-0.015-0.379-0.023-0.633-0.027c-0.253-0.004-0.508-0.006-0.764-0.006
\t\tc-0.286,0-0.554,0.002-0.803,0.006s-0.451,0.013-0.605,0.027v-0.22c0.25-0.015,0.438-0.044,0.566-0.088s0.215-0.133,0.259-0.265
\t\ts0.066-0.33,0.066-0.594v-5.456c0-0.271-0.022-0.471-0.066-0.6c-0.044-0.128-0.13-0.216-0.259-0.264s-0.317-0.075-0.566-0.083
\t\tv-0.22c0.154,0.008,0.355,0.015,0.605,0.022c0.25,0.007,0.517,0.011,0.803,0.011c0.256,0,0.511-0.004,0.764-0.011
\t\tC309.039,507.555,309.249,507.548,309.418,507.54z"/>
\t<path d="M313.895,509.509c0.278,0,0.511,0.037,0.698,0.11s0.343,0.176,0.468,0.308c0.139,0.147,0.238,0.33,0.297,0.551
\t\tc0.059,0.22,0.088,0.517,0.088,0.891v2.937c0,0.309,0.064,0.518,0.192,0.627c0.128,0.11,0.343,0.165,0.644,0.165v0.231
\t\tc-0.125-0.007-0.315-0.017-0.572-0.027c-0.257-0.011-0.506-0.017-0.748-0.017s-0.479,0.006-0.709,0.017s-0.405,0.021-0.522,0.027
\t\tv-0.231c0.264,0,0.451-0.055,0.561-0.165c0.11-0.109,0.165-0.318,0.165-0.627v-3.179c0-0.228-0.019-0.437-0.055-0.627
\t\ts-0.119-0.345-0.248-0.462s-0.324-0.176-0.588-0.176c-0.462,0-0.834,0.192-1.117,0.577s-0.423,0.86-0.423,1.425v2.441
\t\tc0,0.309,0.055,0.518,0.165,0.627c0.11,0.11,0.297,0.165,0.561,0.165v0.231c-0.118-0.007-0.292-0.017-0.522-0.027
\t\ts-0.468-0.017-0.709-0.017c-0.242,0-0.492,0.006-0.748,0.017c-0.257,0.011-0.447,0.021-0.572,0.027v-0.231
\t\tc0.301,0,0.515-0.055,0.644-0.165c0.128-0.109,0.192-0.318,0.192-0.627v-3.442c0-0.33-0.059-0.574-0.176-0.731
\t\tc-0.117-0.158-0.337-0.236-0.66-0.236v-0.231c0.235,0.022,0.462,0.033,0.682,0.033c0.212,0,0.416-0.013,0.61-0.039
\t\tc0.194-0.025,0.372-0.063,0.534-0.115v1.298c0.19-0.498,0.457-0.845,0.797-1.039C313.164,509.606,313.521,509.509,313.895,509.509z
\t\t"/>
\t<path d="M318.735,509.509c0.293,0,0.542,0.048,0.748,0.144c0.205,0.095,0.355,0.184,0.451,0.264
\t\tc0.242,0.198,0.389,0.066,0.44-0.396h0.253c-0.015,0.205-0.026,0.449-0.033,0.731s-0.011,0.661-0.011,1.138h-0.253
\t\tc-0.044-0.271-0.119-0.535-0.225-0.792c-0.106-0.256-0.263-0.467-0.468-0.632c-0.206-0.165-0.473-0.248-0.803-0.248
\t\tc-0.257,0-0.472,0.072-0.644,0.215c-0.172,0.143-0.259,0.354-0.259,0.633c0,0.22,0.066,0.408,0.198,0.566
\t\tc0.132,0.157,0.304,0.304,0.517,0.439c0.212,0.136,0.443,0.284,0.693,0.446c0.418,0.271,0.773,0.546,1.067,0.824
\t\tc0.293,0.279,0.44,0.646,0.44,1.101c0,0.337-0.088,0.619-0.264,0.847c-0.176,0.228-0.405,0.399-0.688,0.517
\t\tc-0.283,0.118-0.592,0.177-0.93,0.177c-0.162,0-0.313-0.014-0.457-0.039s-0.28-0.064-0.413-0.115
\t\tc-0.073-0.036-0.148-0.081-0.226-0.132c-0.077-0.052-0.152-0.106-0.225-0.165c-0.074-0.059-0.14-0.057-0.198,0.006
\t\tc-0.059,0.062-0.103,0.185-0.132,0.368h-0.253c0.014-0.234,0.025-0.521,0.033-0.858c0.007-0.337,0.011-0.784,0.011-1.342h0.253
\t\tc0.051,0.411,0.125,0.771,0.22,1.078s0.246,0.548,0.451,0.721c0.205,0.172,0.491,0.258,0.858,0.258c0.22,0,0.438-0.076,0.655-0.23
\t\tc0.216-0.154,0.324-0.426,0.324-0.814c0-0.322-0.117-0.586-0.352-0.792c-0.235-0.205-0.532-0.421-0.891-0.648
\t\tc-0.264-0.169-0.512-0.338-0.743-0.506c-0.231-0.169-0.418-0.359-0.561-0.572s-0.214-0.47-0.214-0.771
\t\tc0-0.33,0.075-0.599,0.226-0.809c0.15-0.209,0.35-0.362,0.599-0.462C318.181,509.559,318.449,509.509,318.735,509.509z"/>
\t<path d="M323.453,507.837v1.837h1.628v0.221h-1.628v4.257c0,0.345,0.063,0.587,0.187,0.726c0.125,0.14,0.297,0.209,0.517,0.209
\t\tc0.22,0,0.411-0.09,0.572-0.27c0.161-0.18,0.3-0.481,0.418-0.907l0.22,0.055c-0.073,0.426-0.218,0.785-0.435,1.078
\t\ts-0.548,0.44-0.995,0.44c-0.25,0-0.455-0.031-0.616-0.094c-0.162-0.063-0.304-0.152-0.429-0.27
\t\tc-0.161-0.169-0.273-0.37-0.335-0.605c-0.062-0.234-0.093-0.546-0.093-0.935v-3.685h-1.056v-0.221h1.056v-1.683
\t\tc0.183-0.007,0.359-0.022,0.528-0.044C323.16,507.925,323.314,507.889,323.453,507.837z"/>
\t<path d="M327.402,509.542v4.763c0,0.309,0.064,0.518,0.192,0.627c0.128,0.11,0.343,0.165,0.644,0.165v0.231
\t\tc-0.125-0.007-0.313-0.017-0.566-0.027s-0.508-0.017-0.765-0.017c-0.25,0-0.502,0.006-0.759,0.017
\t\tc-0.257,0.011-0.447,0.021-0.572,0.027v-0.231c0.301,0,0.515-0.055,0.644-0.165c0.128-0.109,0.192-0.318,0.192-0.627v-3.442
\t\tc0-0.33-0.059-0.574-0.176-0.731c-0.117-0.158-0.337-0.236-0.66-0.236v-0.231c0.235,0.022,0.462,0.033,0.682,0.033
\t\tc0.212,0,0.416-0.013,0.61-0.039C327.063,509.632,327.241,509.594,327.402,509.542z M326.831,506.913
\t\tc0.19,0,0.355,0.069,0.495,0.209s0.209,0.305,0.209,0.495s-0.07,0.355-0.209,0.495s-0.304,0.209-0.495,0.209
\t\tc-0.191,0-0.356-0.069-0.495-0.209c-0.14-0.14-0.209-0.305-0.209-0.495s0.069-0.355,0.209-0.495
\t\tC326.475,506.982,326.64,506.913,326.831,506.913z"/>
\t<path d="M330.57,507.837v1.837h1.628v0.221h-1.628v4.257c0,0.345,0.063,0.587,0.187,0.726c0.125,0.14,0.297,0.209,0.517,0.209
\t\tc0.22,0,0.411-0.09,0.572-0.27c0.161-0.18,0.3-0.481,0.418-0.907l0.22,0.055c-0.073,0.426-0.218,0.785-0.435,1.078
\t\ts-0.548,0.44-0.995,0.44c-0.25,0-0.455-0.031-0.616-0.094c-0.162-0.063-0.304-0.152-0.429-0.27
\t\tc-0.161-0.169-0.273-0.37-0.335-0.605c-0.062-0.234-0.093-0.546-0.093-0.935v-3.685h-1.056v-0.221h1.056v-1.683
\t\tc0.183-0.007,0.359-0.022,0.528-0.044C330.277,507.925,330.431,507.889,330.57,507.837z"/>
\t<path d="M337.687,509.542v4.598c0,0.33,0.061,0.574,0.182,0.732c0.121,0.157,0.339,0.236,0.654,0.236v0.23
\t\tc-0.228-0.021-0.455-0.033-0.682-0.033c-0.212,0-0.416,0.012-0.61,0.033c-0.194,0.022-0.372,0.063-0.533,0.121v-1.298
\t\tc-0.184,0.477-0.442,0.815-0.776,1.018c-0.333,0.201-0.676,0.303-1.028,0.303c-0.257,0-0.48-0.037-0.671-0.11
\t\tc-0.191-0.073-0.349-0.176-0.473-0.308c-0.139-0.146-0.235-0.338-0.286-0.572s-0.077-0.524-0.077-0.869v-2.761
\t\tc0-0.33-0.059-0.574-0.176-0.731c-0.117-0.158-0.337-0.236-0.66-0.236v-0.231c0.235,0.022,0.462,0.033,0.682,0.033
\t\tc0.213,0,0.416-0.013,0.61-0.039c0.194-0.025,0.373-0.063,0.534-0.115v4.323c0,0.228,0.015,0.437,0.044,0.627
\t\ts0.103,0.345,0.22,0.462c0.117,0.117,0.308,0.176,0.572,0.176c0.433,0,0.788-0.192,1.067-0.577s0.418-0.856,0.418-1.414v-2.276
\t\tc0-0.33-0.059-0.574-0.176-0.731c-0.117-0.158-0.337-0.236-0.66-0.236v-0.231c0.235,0.022,0.462,0.033,0.682,0.033
\t\tc0.213,0,0.416-0.013,0.61-0.039C337.348,509.632,337.525,509.594,337.687,509.542z"/>
\t<path d="M340.855,507.837v1.837h1.628v0.221h-1.628v4.257c0,0.345,0.063,0.587,0.187,0.726c0.125,0.14,0.297,0.209,0.517,0.209
\t\tc0.22,0,0.411-0.09,0.572-0.27c0.161-0.18,0.3-0.481,0.418-0.907l0.22,0.055c-0.073,0.426-0.218,0.785-0.435,1.078
\t\ts-0.548,0.44-0.995,0.44c-0.25,0-0.455-0.031-0.616-0.094c-0.162-0.063-0.304-0.152-0.429-0.27
\t\tc-0.161-0.169-0.273-0.37-0.335-0.605c-0.062-0.234-0.093-0.546-0.093-0.935v-3.685h-1.056v-0.221h1.056v-1.683
\t\tc0.183-0.007,0.359-0.022,0.528-0.044C340.562,507.925,340.716,507.889,340.855,507.837z"/>
\t<path d="M345.651,509.509c0.645,0,1.149,0.196,1.512,0.589c0.363,0.393,0.544,1.003,0.544,1.831h-3.883l-0.011-0.209h2.838
\t\tc0.015-0.358-0.015-0.69-0.088-0.995s-0.188-0.548-0.346-0.731s-0.361-0.275-0.61-0.275c-0.337,0-0.637,0.169-0.897,0.507
\t\tc-0.26,0.337-0.416,0.872-0.467,1.605l0.033,0.044c-0.015,0.11-0.026,0.231-0.033,0.363s-0.011,0.264-0.011,0.396
\t\tc0,0.499,0.081,0.928,0.242,1.287s0.372,0.633,0.632,0.819c0.26,0.188,0.53,0.281,0.809,0.281c0.33,0,0.634-0.081,0.913-0.242
\t\ts0.513-0.447,0.704-0.858l0.22,0.088c-0.081,0.242-0.212,0.478-0.396,0.704c-0.183,0.228-0.414,0.413-0.693,0.556
\t\ts-0.601,0.215-0.968,0.215c-0.528,0-0.981-0.121-1.358-0.363c-0.378-0.242-0.667-0.577-0.869-1.007
\t\tc-0.202-0.429-0.303-0.922-0.303-1.479c0-0.645,0.103-1.2,0.308-1.666c0.205-0.466,0.495-0.825,0.869-1.078
\t\tS345.152,509.509,345.651,509.509z"/>
\t<path d="M353.978,509.509c0.469,0,0.891,0.104,1.265,0.309s0.673,0.527,0.896,0.968s0.336,1.012,0.336,1.716
\t\ts-0.112,1.274-0.336,1.711s-0.522,0.757-0.896,0.962s-0.795,0.309-1.265,0.309c-0.462,0-0.882-0.104-1.26-0.309
\t\tc-0.377-0.205-0.678-0.525-0.902-0.962s-0.335-1.007-0.335-1.711s0.112-1.275,0.335-1.716s0.524-0.763,0.902-0.968
\t\tC353.096,509.612,353.516,509.509,353.978,509.509z M353.978,509.729c-0.418,0-0.761,0.216-1.029,0.648s-0.401,1.141-0.401,2.123
\t\tc0,0.983,0.134,1.688,0.401,2.117c0.268,0.43,0.611,0.644,1.029,0.644s0.761-0.214,1.028-0.644
\t\tc0.268-0.429,0.401-1.134,0.401-2.117c0-0.982-0.134-1.69-0.401-2.123S354.396,509.729,353.978,509.729z"/>
\t<path d="M360.028,506.727c0.205,0,0.387,0.027,0.544,0.082s0.295,0.127,0.413,0.215c0.11,0.088,0.194,0.19,0.253,0.308
\t\tc0.059,0.117,0.088,0.242,0.088,0.374c0,0.169-0.055,0.308-0.165,0.418s-0.25,0.165-0.418,0.165c-0.161,0-0.302-0.053-0.423-0.159
\t\ts-0.182-0.251-0.182-0.435c0-0.154,0.042-0.279,0.126-0.374c0.084-0.096,0.196-0.158,0.335-0.188
\t\tc-0.022-0.065-0.079-0.122-0.17-0.17s-0.214-0.071-0.369-0.071c-0.168,0-0.313,0.036-0.435,0.109
\t\tc-0.121,0.073-0.214,0.173-0.28,0.297c-0.088,0.154-0.143,0.384-0.165,0.688c-0.022,0.305-0.033,0.772-0.033,1.402v0.286h1.177
\t\tv0.221h-1.177v4.301c0,0.359,0.105,0.6,0.314,0.721s0.493,0.181,0.852,0.181v0.231c-0.168-0.007-0.407-0.017-0.715-0.027
\t\ts-0.634-0.017-0.979-0.017c-0.25,0-0.497,0.006-0.742,0.017c-0.246,0.011-0.431,0.021-0.556,0.027v-0.231
\t\tc0.3,0,0.515-0.055,0.644-0.165c0.128-0.109,0.192-0.318,0.192-0.627v-4.41h-0.88v-0.221h0.88c0-0.652,0.042-1.152,0.126-1.501
\t\ts0.229-0.647,0.435-0.896c0.146-0.169,0.333-0.303,0.561-0.401C359.507,506.775,359.756,506.727,360.028,506.727z"/>
\t<path d="M366.562,507.485l2.684,6.919c0.11,0.278,0.235,0.464,0.374,0.556s0.268,0.141,0.385,0.148v0.22
\t\tc-0.147-0.015-0.327-0.023-0.539-0.027c-0.213-0.004-0.425-0.006-0.638-0.006c-0.286,0-0.554,0.002-0.803,0.006
\t\tc-0.25,0.004-0.451,0.013-0.605,0.027v-0.22c0.374-0.015,0.612-0.079,0.715-0.192c0.103-0.114,0.081-0.357-0.066-0.731
\t\tl-2.046-5.468l0.176-0.143l-1.914,4.972c-0.154,0.396-0.227,0.706-0.22,0.93c0.007,0.224,0.091,0.384,0.253,0.479
\t\tc0.161,0.096,0.396,0.146,0.704,0.154v0.22c-0.206-0.015-0.42-0.023-0.644-0.027c-0.224-0.004-0.431-0.006-0.622-0.006
\t\tc-0.184,0-0.339,0.002-0.468,0.006c-0.128,0.004-0.247,0.013-0.357,0.027v-0.22c0.147-0.037,0.297-0.123,0.451-0.259
\t\ts0.293-0.368,0.418-0.698l2.585-6.666c0.029,0,0.059,0,0.088,0S366.532,507.485,366.562,507.485z M367.904,512.16v0.22h-3.278
\t\tl0.11-0.22H367.904z"/>
\t<path d="M372.336,509.663v0.231c-0.183,0-0.345,0.013-0.484,0.038c-0.14,0.025-0.231,0.101-0.275,0.226s-0.011,0.345,0.099,0.66
\t\tl1.144,3.277l-0.077-0.011l1.617-4.466l0.22,0.088l-2.013,5.676c-0.029,0-0.059,0-0.088,0s-0.063,0-0.099,0l-1.804-4.873
\t\tc-0.118-0.293-0.233-0.469-0.347-0.527s-0.211-0.088-0.292-0.088v-0.231c0.146,0.022,0.299,0.038,0.457,0.05
\t\tc0.158,0.011,0.328,0.017,0.511,0.017c0.228,0,0.468-0.008,0.721-0.022S372.117,509.678,372.336,509.663z M378.375,509.663v0.22
\t\tc-0.139,0.015-0.27,0.075-0.391,0.182s-0.229,0.303-0.324,0.589l-1.595,4.729c-0.029,0-0.059,0-0.088,0s-0.063,0-0.099,0
\t\tl-1.771-4.499l0.253-1.265c0.044,0,0.086,0,0.126,0c0.041,0,0.083,0,0.126,0l1.716,4.543l-0.088,0.066l1.089-3.312
\t\tc0.125-0.366,0.134-0.623,0.027-0.77s-0.354-0.235-0.743-0.265v-0.22c0.132,0.008,0.246,0.013,0.341,0.017s0.188,0.008,0.28,0.011
\t\tc0.092,0.004,0.193,0.006,0.303,0.006c0.168,0,0.321-0.004,0.457-0.011C378.131,509.678,378.258,509.671,378.375,509.663z"/>
\t<path d="M381.302,509.509c0.645,0,1.149,0.196,1.512,0.589c0.363,0.393,0.544,1.003,0.544,1.831h-3.883l-0.011-0.209h2.838
\t\tc0.015-0.358-0.015-0.69-0.088-0.995s-0.188-0.548-0.346-0.731s-0.361-0.275-0.61-0.275c-0.337,0-0.637,0.169-0.897,0.507
\t\tc-0.26,0.337-0.416,0.872-0.467,1.605l0.033,0.044c-0.015,0.11-0.026,0.231-0.033,0.363s-0.011,0.264-0.011,0.396
\t\tc0,0.499,0.081,0.928,0.242,1.287s0.372,0.633,0.632,0.819c0.26,0.188,0.53,0.281,0.809,0.281c0.33,0,0.634-0.081,0.913-0.242
\t\ts0.513-0.447,0.704-0.858l0.22,0.088c-0.081,0.242-0.212,0.478-0.396,0.704c-0.183,0.228-0.414,0.413-0.693,0.556
\t\ts-0.601,0.215-0.968,0.215c-0.528,0-0.981-0.121-1.358-0.363c-0.378-0.242-0.667-0.577-0.869-1.007
\t\tc-0.202-0.429-0.303-0.922-0.303-1.479c0-0.645,0.103-1.2,0.308-1.666c0.205-0.466,0.495-0.825,0.869-1.078
\t\tS380.803,509.509,381.302,509.509z"/>
\t<path d="M386.164,509.509c0.293,0,0.542,0.048,0.748,0.144c0.205,0.095,0.355,0.184,0.451,0.264
\t\tc0.242,0.198,0.389,0.066,0.44-0.396h0.253c-0.015,0.205-0.026,0.449-0.033,0.731s-0.011,0.661-0.011,1.138h-0.253
\t\tc-0.044-0.271-0.119-0.535-0.225-0.792c-0.106-0.256-0.263-0.467-0.468-0.632c-0.206-0.165-0.473-0.248-0.803-0.248
\t\tc-0.257,0-0.472,0.072-0.644,0.215c-0.172,0.143-0.259,0.354-0.259,0.633c0,0.22,0.066,0.408,0.198,0.566
\t\tc0.132,0.157,0.304,0.304,0.517,0.439c0.212,0.136,0.443,0.284,0.693,0.446c0.418,0.271,0.773,0.546,1.067,0.824
\t\tc0.293,0.279,0.44,0.646,0.44,1.101c0,0.337-0.088,0.619-0.264,0.847c-0.176,0.228-0.405,0.399-0.688,0.517
\t\tc-0.283,0.118-0.592,0.177-0.93,0.177c-0.162,0-0.313-0.014-0.457-0.039s-0.28-0.064-0.413-0.115
\t\tc-0.073-0.036-0.148-0.081-0.226-0.132c-0.077-0.052-0.152-0.106-0.225-0.165c-0.074-0.059-0.14-0.057-0.198,0.006
\t\tc-0.059,0.062-0.103,0.185-0.132,0.368h-0.253c0.014-0.234,0.025-0.521,0.033-0.858c0.007-0.337,0.011-0.784,0.011-1.342h0.253
\t\tc0.051,0.411,0.125,0.771,0.22,1.078s0.246,0.548,0.451,0.721c0.205,0.172,0.491,0.258,0.858,0.258c0.22,0,0.438-0.076,0.655-0.23
\t\tc0.216-0.154,0.324-0.426,0.324-0.814c0-0.322-0.117-0.586-0.352-0.792c-0.235-0.205-0.532-0.421-0.891-0.648
\t\tc-0.264-0.169-0.512-0.338-0.743-0.506c-0.231-0.169-0.418-0.359-0.561-0.572s-0.214-0.47-0.214-0.771
\t\tc0-0.33,0.075-0.599,0.226-0.809c0.15-0.209,0.35-0.362,0.599-0.462C385.61,509.559,385.877,509.509,386.164,509.509z"/>
\t<path d="M391.807,509.509c0.469,0,0.891,0.104,1.265,0.309s0.673,0.527,0.896,0.968s0.336,1.012,0.336,1.716
\t\ts-0.112,1.274-0.336,1.711s-0.522,0.757-0.896,0.962s-0.795,0.309-1.265,0.309c-0.462,0-0.882-0.104-1.26-0.309
\t\tc-0.377-0.205-0.678-0.525-0.902-0.962s-0.335-1.007-0.335-1.711s0.112-1.275,0.335-1.716s0.524-0.763,0.902-0.968
\t\tC390.925,509.612,391.344,509.509,391.807,509.509z M391.807,509.729c-0.418,0-0.761,0.216-1.029,0.648s-0.401,1.141-0.401,2.123
\t\tc0,0.983,0.134,1.688,0.401,2.117c0.268,0.43,0.611,0.644,1.029,0.644s0.761-0.214,1.028-0.644
\t\tc0.268-0.429,0.401-1.134,0.401-2.117c0-0.982-0.134-1.69-0.401-2.123S392.225,509.729,391.807,509.729z"/>
\t<path d="M398.77,509.509c0.256,0,0.48,0.037,0.671,0.11c0.19,0.073,0.348,0.176,0.473,0.308c0.14,0.147,0.235,0.338,0.286,0.572
\t\tc0.051,0.235,0.077,0.524,0.077,0.869v2.937c0,0.309,0.064,0.518,0.192,0.627c0.128,0.11,0.343,0.165,0.644,0.165v0.231
\t\tc-0.125-0.007-0.315-0.017-0.572-0.027c-0.257-0.011-0.506-0.017-0.748-0.017c-0.242,0-0.479,0.006-0.709,0.017
\t\ts-0.405,0.021-0.522,0.027v-0.231c0.264,0,0.451-0.055,0.561-0.165c0.11-0.109,0.165-0.318,0.165-0.627v-3.179
\t\tc0-0.228-0.015-0.437-0.044-0.627s-0.103-0.345-0.22-0.462c-0.118-0.117-0.308-0.176-0.572-0.176c-0.286,0-0.541,0.088-0.764,0.264
\t\tc-0.224,0.176-0.4,0.416-0.528,0.721s-0.192,0.644-0.192,1.018v2.441c0,0.309,0.055,0.518,0.165,0.627
\t\tc0.11,0.11,0.297,0.165,0.561,0.165v0.231c-0.118-0.007-0.292-0.017-0.522-0.027s-0.468-0.017-0.709-0.017
\t\tc-0.242,0-0.492,0.006-0.748,0.017c-0.257,0.011-0.447,0.021-0.572,0.027v-0.231c0.301,0,0.515-0.055,0.644-0.165
\t\tc0.128-0.109,0.192-0.318,0.192-0.627v-3.442c0-0.33-0.059-0.574-0.176-0.731c-0.117-0.158-0.337-0.236-0.66-0.236v-0.231
\t\tc0.235,0.022,0.462,0.033,0.682,0.033c0.212,0,0.416-0.013,0.61-0.039c0.194-0.025,0.372-0.063,0.534-0.115v1.298
\t\tc0.19-0.477,0.451-0.817,0.781-1.022C398.076,509.612,398.417,509.509,398.77,509.509z M402.08,509.509
\t\tc0.257,0,0.48,0.037,0.671,0.11c0.19,0.073,0.348,0.176,0.473,0.308c0.14,0.147,0.235,0.338,0.286,0.572
\t\tc0.051,0.235,0.077,0.524,0.077,0.869v2.937c0,0.309,0.064,0.518,0.192,0.627c0.128,0.11,0.343,0.165,0.644,0.165v0.231
\t\tc-0.125-0.007-0.315-0.017-0.572-0.027c-0.256-0.011-0.506-0.017-0.748-0.017c-0.242,0-0.479,0.006-0.709,0.017
\t\tc-0.231,0.011-0.405,0.021-0.523,0.027v-0.231c0.264,0,0.451-0.055,0.561-0.165c0.11-0.109,0.165-0.318,0.165-0.627v-3.179
\t\tc0-0.228-0.015-0.437-0.044-0.627s-0.103-0.345-0.22-0.462c-0.118-0.117-0.308-0.176-0.572-0.176c-0.425,0-0.779,0.19-1.062,0.572
\t\tc-0.282,0.381-0.423,0.854-0.423,1.419l-0.033-1.023c0.198-0.506,0.466-0.853,0.803-1.039
\t\tC401.383,509.603,401.729,509.509,402.08,509.509z"/>
\t<path d="M407.58,509.509c0.645,0,1.149,0.196,1.512,0.589c0.363,0.393,0.544,1.003,0.544,1.831h-3.883l-0.011-0.209h2.838
\t\tc0.015-0.358-0.015-0.69-0.088-0.995s-0.188-0.548-0.346-0.731s-0.361-0.275-0.61-0.275c-0.337,0-0.637,0.169-0.897,0.507
\t\tc-0.26,0.337-0.416,0.872-0.467,1.605l0.033,0.044c-0.015,0.11-0.026,0.231-0.033,0.363s-0.011,0.264-0.011,0.396
\t\tc0,0.499,0.081,0.928,0.242,1.287s0.372,0.633,0.632,0.819c0.26,0.188,0.53,0.281,0.809,0.281c0.33,0,0.634-0.081,0.913-0.242
\t\ts0.513-0.447,0.704-0.858l0.22,0.088c-0.081,0.242-0.212,0.478-0.396,0.704c-0.183,0.228-0.414,0.413-0.693,0.556
\t\ts-0.601,0.215-0.968,0.215c-0.528,0-0.981-0.121-1.358-0.363c-0.378-0.242-0.667-0.577-0.869-1.007
\t\tc-0.202-0.429-0.303-0.922-0.303-1.479c0-0.645,0.103-1.2,0.308-1.666c0.205-0.466,0.495-0.825,0.869-1.078
\t\tS407.081,509.509,407.58,509.509z"/>
\t<path d="M414.169,509.509c0.278,0,0.511,0.037,0.698,0.11s0.343,0.176,0.468,0.308c0.139,0.147,0.238,0.33,0.297,0.551
\t\tc0.059,0.22,0.088,0.517,0.088,0.891v2.937c0,0.309,0.064,0.518,0.192,0.627c0.128,0.11,0.343,0.165,0.644,0.165v0.231
\t\tc-0.125-0.007-0.315-0.017-0.572-0.027c-0.257-0.011-0.506-0.017-0.748-0.017s-0.479,0.006-0.709,0.017s-0.405,0.021-0.522,0.027
\t\tv-0.231c0.264,0,0.451-0.055,0.561-0.165c0.11-0.109,0.165-0.318,0.165-0.627v-3.179c0-0.228-0.019-0.437-0.055-0.627
\t\ts-0.119-0.345-0.248-0.462s-0.324-0.176-0.588-0.176c-0.462,0-0.834,0.192-1.117,0.577s-0.423,0.86-0.423,1.425v2.441
\t\tc0,0.309,0.055,0.518,0.165,0.627c0.11,0.11,0.297,0.165,0.561,0.165v0.231c-0.118-0.007-0.292-0.017-0.522-0.027
\t\ts-0.468-0.017-0.709-0.017c-0.242,0-0.492,0.006-0.748,0.017c-0.257,0.011-0.447,0.021-0.572,0.027v-0.231
\t\tc0.301,0,0.515-0.055,0.644-0.165c0.128-0.109,0.192-0.318,0.192-0.627v-3.442c0-0.33-0.059-0.574-0.176-0.731
\t\tc-0.117-0.158-0.337-0.236-0.66-0.236v-0.231c0.235,0.022,0.462,0.033,0.682,0.033c0.212,0,0.416-0.013,0.61-0.039
\t\tc0.194-0.025,0.372-0.063,0.534-0.115v1.298c0.19-0.498,0.457-0.845,0.797-1.039C413.438,509.606,413.795,509.509,414.169,509.509z
\t\t"/>
\t<path d="M419.713,509.509c0.645,0,1.149,0.196,1.512,0.589c0.363,0.393,0.544,1.003,0.544,1.831h-3.883l-0.011-0.209h2.838
\t\tc0.015-0.358-0.015-0.69-0.088-0.995s-0.188-0.548-0.346-0.731s-0.361-0.275-0.61-0.275c-0.337,0-0.637,0.169-0.897,0.507
\t\tc-0.26,0.337-0.416,0.872-0.467,1.605l0.033,0.044c-0.015,0.11-0.026,0.231-0.033,0.363s-0.011,0.264-0.011,0.396
\t\tc0,0.499,0.081,0.928,0.242,1.287s0.372,0.633,0.632,0.819c0.26,0.188,0.53,0.281,0.809,0.281c0.33,0,0.634-0.081,0.913-0.242
\t\ts0.513-0.447,0.704-0.858l0.22,0.088c-0.081,0.242-0.212,0.478-0.396,0.704c-0.183,0.228-0.414,0.413-0.693,0.556
\t\ts-0.601,0.215-0.968,0.215c-0.528,0-0.981-0.121-1.358-0.363c-0.378-0.242-0.667-0.577-0.869-1.007
\t\tc-0.202-0.429-0.303-0.922-0.303-1.479c0-0.645,0.103-1.2,0.308-1.666c0.205-0.466,0.495-0.825,0.869-1.078
\t\tS419.214,509.509,419.713,509.509z"/>
\t<path d="M424.574,509.509c0.293,0,0.543,0.048,0.748,0.144c0.205,0.095,0.355,0.184,0.451,0.264
\t\tc0.242,0.198,0.389,0.066,0.439-0.396h0.254c-0.016,0.205-0.025,0.449-0.033,0.731s-0.011,0.661-0.011,1.138h-0.253
\t\tc-0.045-0.271-0.119-0.535-0.226-0.792c-0.106-0.256-0.263-0.467-0.468-0.632s-0.473-0.248-0.803-0.248
\t\tc-0.257,0-0.472,0.072-0.644,0.215c-0.173,0.143-0.259,0.354-0.259,0.633c0,0.22,0.066,0.408,0.198,0.566
\t\tc0.132,0.157,0.304,0.304,0.517,0.439s0.443,0.284,0.693,0.446c0.418,0.271,0.773,0.546,1.066,0.824
\t\tc0.294,0.279,0.44,0.646,0.44,1.101c0,0.337-0.088,0.619-0.264,0.847c-0.177,0.228-0.405,0.399-0.688,0.517
\t\tc-0.282,0.118-0.593,0.177-0.93,0.177c-0.161,0-0.313-0.014-0.456-0.039c-0.144-0.025-0.281-0.064-0.413-0.115
\t\tc-0.073-0.036-0.148-0.081-0.226-0.132c-0.076-0.052-0.152-0.106-0.225-0.165c-0.074-0.059-0.141-0.057-0.199,0.006
\t\tc-0.059,0.062-0.102,0.185-0.131,0.368h-0.254c0.015-0.234,0.025-0.521,0.033-0.858c0.008-0.337,0.012-0.784,0.012-1.342h0.252
\t\tc0.052,0.411,0.125,0.771,0.221,1.078c0.095,0.308,0.245,0.548,0.451,0.721c0.205,0.172,0.49,0.258,0.857,0.258
\t\tc0.221,0,0.438-0.076,0.654-0.23s0.324-0.426,0.324-0.814c0-0.322-0.117-0.586-0.352-0.792c-0.234-0.205-0.531-0.421-0.891-0.648
\t\tc-0.264-0.169-0.512-0.338-0.742-0.506c-0.231-0.169-0.418-0.359-0.562-0.572s-0.214-0.47-0.214-0.771
\t\tc0-0.33,0.074-0.599,0.225-0.809c0.15-0.209,0.351-0.362,0.6-0.462C424.021,509.559,424.289,509.509,424.574,509.509z"/>
\t<path d="M429.492,509.509c0.293,0,0.542,0.048,0.748,0.144c0.205,0.095,0.355,0.184,0.45,0.264c0.242,0.198,0.389,0.066,0.44-0.396
\t\th0.253c-0.015,0.205-0.026,0.449-0.033,0.731s-0.011,0.661-0.011,1.138h-0.254c-0.043-0.271-0.119-0.535-0.225-0.792
\t\tc-0.106-0.256-0.263-0.467-0.468-0.632c-0.206-0.165-0.474-0.248-0.803-0.248c-0.257,0-0.472,0.072-0.644,0.215
\t\ts-0.259,0.354-0.259,0.633c0,0.22,0.065,0.408,0.198,0.566c0.132,0.157,0.305,0.304,0.518,0.439
\t\tc0.212,0.136,0.443,0.284,0.692,0.446c0.418,0.271,0.773,0.546,1.067,0.824c0.293,0.279,0.439,0.646,0.439,1.101
\t\tc0,0.337-0.088,0.619-0.264,0.847s-0.405,0.399-0.688,0.517c-0.283,0.118-0.592,0.177-0.93,0.177c-0.162,0-0.313-0.014-0.457-0.039
\t\tc-0.143-0.025-0.279-0.064-0.412-0.115c-0.073-0.036-0.148-0.081-0.226-0.132c-0.077-0.052-0.151-0.106-0.226-0.165
\t\tc-0.073-0.059-0.139-0.057-0.197,0.006c-0.059,0.062-0.104,0.185-0.133,0.368h-0.252c0.014-0.234,0.025-0.521,0.032-0.858
\t\tc0.007-0.337,0.011-0.784,0.011-1.342h0.254c0.051,0.411,0.124,0.771,0.219,1.078c0.096,0.308,0.246,0.548,0.451,0.721
\t\tc0.205,0.172,0.492,0.258,0.858,0.258c0.22,0,0.438-0.076,0.655-0.23c0.215-0.154,0.324-0.426,0.324-0.814
\t\tc0-0.322-0.117-0.586-0.353-0.792c-0.235-0.205-0.532-0.421-0.891-0.648c-0.265-0.169-0.512-0.338-0.743-0.506
\t\tc-0.23-0.169-0.418-0.359-0.561-0.572s-0.215-0.47-0.215-0.771c0-0.33,0.076-0.599,0.227-0.809c0.149-0.209,0.35-0.362,0.599-0.462
\t\tC428.938,509.559,429.205,509.509,429.492,509.509z"/>
\t<path d="M438.127,507.54v0.22c-0.25,0.008-0.439,0.035-0.566,0.083c-0.129,0.048-0.215,0.136-0.26,0.264
\t\tc-0.043,0.129-0.065,0.328-0.065,0.6v5.456c0,0.264,0.022,0.462,0.065,0.594c0.045,0.132,0.131,0.219,0.26,0.259
\t\tc0.127,0.04,0.316,0.061,0.566,0.061h0.803c0.462,0,0.813-0.066,1.051-0.198s0.41-0.348,0.517-0.648s0.185-0.704,0.236-1.21h0.253
\t\tc-0.021,0.227-0.033,0.527-0.033,0.901c0,0.14,0.006,0.34,0.017,0.6c0.011,0.261,0.031,0.53,0.061,0.809
\t\tc-0.374-0.015-0.796-0.023-1.265-0.027c-0.47-0.004-0.888-0.006-1.254-0.006c-0.221,0-0.512,0-0.875,0s-0.75,0.002-1.16,0.006
\t\tc-0.411,0.004-0.796,0.013-1.155,0.027v-0.22c0.249-0.015,0.438-0.044,0.566-0.088s0.215-0.133,0.259-0.265s0.066-0.33,0.066-0.594
\t\tv-5.456c0-0.271-0.022-0.471-0.066-0.6c-0.044-0.128-0.131-0.216-0.259-0.264s-0.317-0.075-0.566-0.083v-0.22
\t\tc0.154,0.008,0.355,0.015,0.604,0.022c0.25,0.007,0.518,0.011,0.804,0.011c0.257,0,0.511-0.004,0.765-0.011
\t\tC437.747,507.555,437.957,507.548,438.127,507.54z"/>
\t<path d="M444.32,509.509c0.645,0,1.148,0.196,1.512,0.589s0.545,1.003,0.545,1.831h-3.883l-0.012-0.209h2.838
\t\tc0.016-0.358-0.015-0.69-0.088-0.995s-0.188-0.548-0.346-0.731c-0.158-0.184-0.361-0.275-0.611-0.275
\t\tc-0.337,0-0.637,0.169-0.896,0.507c-0.26,0.337-0.416,0.872-0.467,1.605l0.033,0.044c-0.016,0.11-0.026,0.231-0.033,0.363
\t\tc-0.008,0.132-0.012,0.264-0.012,0.396c0,0.499,0.081,0.928,0.242,1.287s0.372,0.633,0.633,0.819
\t\tc0.26,0.188,0.529,0.281,0.809,0.281c0.33,0,0.634-0.081,0.912-0.242c0.279-0.161,0.514-0.447,0.705-0.858l0.219,0.088
\t\tc-0.08,0.242-0.212,0.478-0.396,0.704c-0.183,0.228-0.414,0.413-0.692,0.556c-0.279,0.143-0.602,0.215-0.969,0.215
\t\tc-0.527,0-0.98-0.121-1.357-0.363c-0.379-0.242-0.668-0.577-0.869-1.007c-0.202-0.429-0.303-0.922-0.303-1.479
\t\tc0-0.645,0.102-1.2,0.308-1.666c0.205-0.466,0.495-0.825,0.869-1.078S443.82,509.509,444.32,509.509z"/>
\t<path d="M448.687,515.405c-0.477,0-0.823-0.119-1.04-0.357c-0.216-0.238-0.324-0.556-0.324-0.952c0-0.308,0.075-0.563,0.226-0.764
\t\tc0.15-0.202,0.343-0.367,0.577-0.495c0.235-0.129,0.482-0.238,0.743-0.33s0.508-0.182,0.742-0.27
\t\tc0.235-0.088,0.427-0.188,0.577-0.297c0.15-0.11,0.227-0.25,0.227-0.418v-0.683c0-0.308-0.047-0.542-0.139-0.704
\t\tc-0.092-0.161-0.214-0.27-0.367-0.324c-0.154-0.055-0.327-0.082-0.518-0.082c-0.184,0-0.379,0.025-0.588,0.076
\t\tc-0.209,0.052-0.377,0.158-0.501,0.319c0.14,0.029,0.261,0.103,0.362,0.22c0.104,0.118,0.154,0.269,0.154,0.451
\t\tc0,0.184-0.059,0.328-0.176,0.435s-0.268,0.159-0.451,0.159c-0.213,0-0.368-0.067-0.467-0.203c-0.1-0.136-0.148-0.288-0.148-0.456
\t\tc0-0.191,0.047-0.345,0.143-0.462s0.216-0.228,0.363-0.33c0.168-0.117,0.387-0.219,0.654-0.303s0.57-0.127,0.907-0.127
\t\tc0.301,0,0.558,0.035,0.771,0.104c0.212,0.07,0.389,0.171,0.527,0.303c0.191,0.176,0.315,0.391,0.374,0.644
\t\ts0.088,0.556,0.088,0.907v3.059c0,0.176,0.026,0.306,0.077,0.391c0.051,0.084,0.139,0.126,0.264,0.126
\t\tc0.104,0,0.193-0.021,0.27-0.065c0.077-0.045,0.152-0.1,0.227-0.165l0.12,0.187c-0.153,0.117-0.301,0.215-0.44,0.292
\t\tc-0.139,0.076-0.326,0.115-0.561,0.115c-0.381,0-0.635-0.1-0.759-0.297c-0.125-0.198-0.187-0.426-0.187-0.683
\t\tc-0.235,0.389-0.501,0.649-0.798,0.781C449.318,515.339,449.01,515.405,448.687,515.405z M449.17,514.965
\t\tc0.221,0,0.438-0.063,0.649-0.192c0.213-0.128,0.411-0.343,0.595-0.644v-2.156c-0.11,0.162-0.281,0.29-0.512,0.386
\t\tc-0.231,0.095-0.47,0.199-0.715,0.313c-0.246,0.113-0.457,0.268-0.633,0.462s-0.264,0.471-0.264,0.83
\t\tc0,0.338,0.084,0.589,0.252,0.754C448.713,514.883,448.922,514.965,449.17,514.965z"/>
\t<path d="M455.386,509.509c0.322,0,0.625,0.077,0.907,0.231s0.471,0.418,0.566,0.792l-0.143,0.11
\t\tc-0.104-0.309-0.263-0.532-0.479-0.672c-0.217-0.139-0.472-0.209-0.765-0.209c-0.433,0-0.794,0.225-1.083,0.672
\t\tc-0.291,0.447-0.432,1.137-0.424,2.067c0,0.594,0.057,1.088,0.17,1.479c0.113,0.393,0.283,0.686,0.506,0.88
\t\tc0.225,0.194,0.497,0.292,0.82,0.292c0.308,0,0.586-0.132,0.836-0.396c0.249-0.264,0.399-0.652,0.451-1.166l0.131,0.154
\t\tc-0.059,0.558-0.236,0.986-0.533,1.287s-0.698,0.451-1.204,0.451c-0.454,0-0.853-0.11-1.193-0.33
\t\tc-0.341-0.221-0.602-0.55-0.781-0.99s-0.27-0.993-0.27-1.661c0-0.667,0.116-1.223,0.347-1.666c0.23-0.444,0.535-0.775,0.913-0.996
\t\tC454.537,509.619,454.945,509.509,455.386,509.509z M457.695,506.704v7.425c0,0.33,0.061,0.574,0.182,0.731
\t\tc0.121,0.158,0.34,0.236,0.654,0.236v0.231c-0.227-0.022-0.455-0.033-0.682-0.033c-0.213,0-0.416,0.011-0.61,0.033
\t\ts-0.372,0.063-0.534,0.121v-7.425c0-0.33-0.059-0.574-0.176-0.731c-0.117-0.158-0.337-0.237-0.66-0.237v-0.23
\t\tc0.234,0.021,0.463,0.033,0.683,0.033c0.212,0,0.416-0.013,0.61-0.039C457.356,506.794,457.534,506.756,457.695,506.704z"/>
\t<path d="M464.768,507.54v0.22c-0.249,0.008-0.438,0.035-0.566,0.083c-0.128,0.048-0.215,0.136-0.258,0.264
\t\tc-0.045,0.129-0.066,0.328-0.066,0.6v5.456c0,0.264,0.021,0.462,0.066,0.594c0.043,0.132,0.13,0.221,0.258,0.265
\t\tc0.129,0.044,0.317,0.073,0.566,0.088v0.22c-0.168-0.015-0.379-0.023-0.633-0.027c-0.252-0.004-0.508-0.006-0.764-0.006
\t\tc-0.286,0-0.554,0.002-0.803,0.006c-0.25,0.004-0.451,0.013-0.605,0.027v-0.22c0.25-0.015,0.438-0.044,0.566-0.088
\t\ts0.215-0.133,0.259-0.265s0.065-0.33,0.065-0.594v-5.456c0-0.271-0.021-0.471-0.065-0.6c-0.044-0.128-0.13-0.216-0.259-0.264
\t\ts-0.316-0.075-0.566-0.083v-0.22c0.154,0.008,0.355,0.015,0.605,0.022c0.249,0.007,0.517,0.011,0.803,0.011
\t\tc0.256,0,0.512-0.004,0.764-0.011C464.389,507.555,464.6,507.548,464.768,507.54z"/>
\t<path d="M469.245,509.509c0.278,0,0.511,0.037,0.698,0.11s0.343,0.176,0.468,0.308c0.14,0.147,0.238,0.33,0.297,0.551
\t\tc0.059,0.22,0.088,0.517,0.088,0.891v2.937c0,0.309,0.063,0.518,0.192,0.627c0.129,0.11,0.343,0.165,0.644,0.165v0.231
\t\tc-0.124-0.007-0.315-0.017-0.571-0.027c-0.258-0.011-0.506-0.017-0.748-0.017s-0.479,0.006-0.71,0.017s-0.405,0.021-0.522,0.027
\t\tv-0.231c0.264,0,0.451-0.055,0.561-0.165c0.11-0.109,0.166-0.318,0.166-0.627v-3.179c0-0.228-0.02-0.437-0.056-0.627
\t\ts-0.119-0.345-0.247-0.462c-0.129-0.117-0.324-0.176-0.589-0.176c-0.462,0-0.835,0.192-1.116,0.577
\t\tc-0.283,0.385-0.424,0.86-0.424,1.425v2.441c0,0.309,0.055,0.518,0.165,0.627c0.11,0.11,0.297,0.165,0.562,0.165v0.231
\t\tc-0.118-0.007-0.292-0.017-0.523-0.027c-0.23-0.011-0.467-0.017-0.709-0.017s-0.492,0.006-0.748,0.017
\t\tc-0.257,0.011-0.447,0.021-0.572,0.027v-0.231c0.301,0,0.516-0.055,0.644-0.165c0.128-0.109,0.192-0.318,0.192-0.627v-3.442
\t\tc0-0.33-0.059-0.574-0.176-0.731c-0.117-0.158-0.338-0.236-0.66-0.236v-0.231c0.234,0.022,0.462,0.033,0.682,0.033
\t\tc0.213,0,0.416-0.013,0.611-0.039c0.193-0.025,0.371-0.063,0.533-0.115v1.298c0.19-0.498,0.457-0.845,0.797-1.039
\t\tC468.514,509.606,468.871,509.509,469.245,509.509z"/>
\t<path d="M474.085,509.509c0.293,0,0.542,0.048,0.748,0.144c0.205,0.095,0.355,0.184,0.45,0.264c0.242,0.198,0.389,0.066,0.44-0.396
\t\th0.253c-0.015,0.205-0.025,0.449-0.033,0.731c-0.007,0.282-0.01,0.661-0.01,1.138h-0.254c-0.044-0.271-0.119-0.535-0.225-0.792
\t\tc-0.107-0.256-0.264-0.467-0.469-0.632s-0.473-0.248-0.803-0.248c-0.256,0-0.471,0.072-0.643,0.215
\t\tc-0.173,0.143-0.26,0.354-0.26,0.633c0,0.22,0.066,0.408,0.199,0.566c0.131,0.157,0.304,0.304,0.517,0.439
\t\tc0.212,0.136,0.443,0.284,0.692,0.446c0.418,0.271,0.773,0.546,1.067,0.824c0.294,0.279,0.44,0.646,0.44,1.101
\t\tc0,0.337-0.088,0.619-0.264,0.847c-0.177,0.228-0.406,0.399-0.688,0.517c-0.283,0.118-0.593,0.177-0.93,0.177
\t\tc-0.162,0-0.314-0.014-0.457-0.039s-0.28-0.064-0.412-0.115c-0.074-0.036-0.148-0.081-0.227-0.132
\t\tc-0.076-0.052-0.151-0.106-0.225-0.165c-0.074-0.059-0.14-0.057-0.198,0.006c-0.059,0.062-0.103,0.185-0.132,0.368h-0.253
\t\tc0.015-0.234,0.025-0.521,0.032-0.858c0.008-0.337,0.012-0.784,0.012-1.342h0.253c0.052,0.411,0.124,0.771,0.22,1.078
\t\ts0.246,0.548,0.451,0.721c0.205,0.172,0.491,0.258,0.857,0.258c0.221,0,0.438-0.076,0.655-0.23
\t\tc0.216-0.154,0.324-0.426,0.324-0.814c0-0.322-0.117-0.586-0.353-0.792c-0.234-0.205-0.531-0.421-0.891-0.648
\t\tc-0.264-0.169-0.512-0.338-0.742-0.506c-0.23-0.169-0.418-0.359-0.561-0.572c-0.144-0.213-0.215-0.47-0.215-0.771
\t\tc0-0.33,0.075-0.599,0.226-0.809c0.149-0.209,0.351-0.362,0.599-0.462C473.531,509.559,473.799,509.509,474.085,509.509z"/>
\t<path d="M478.804,507.837v1.837h1.628v0.221h-1.628v4.257c0,0.345,0.063,0.587,0.187,0.726c0.125,0.14,0.298,0.209,0.518,0.209
\t\tc0.221,0,0.41-0.09,0.572-0.27c0.161-0.18,0.301-0.481,0.418-0.907l0.22,0.055c-0.073,0.426-0.218,0.785-0.435,1.078
\t\ts-0.549,0.44-0.995,0.44c-0.249,0-0.455-0.031-0.616-0.094c-0.162-0.063-0.305-0.152-0.43-0.27c-0.16-0.169-0.272-0.37-0.335-0.605
\t\tc-0.062-0.234-0.093-0.546-0.093-0.935v-3.685h-1.057v-0.221h1.057v-1.683c0.183-0.007,0.358-0.022,0.527-0.044
\t\tC478.51,507.925,478.664,507.889,478.804,507.837z"/>
\t<path d="M484.348,509.509c0.198,0,0.373,0.041,0.523,0.121c0.149,0.081,0.268,0.186,0.352,0.313
\t\tc0.084,0.129,0.127,0.273,0.127,0.435c0,0.184-0.058,0.345-0.172,0.484c-0.113,0.14-0.266,0.209-0.456,0.209
\t\tc-0.153,0-0.29-0.05-0.407-0.148c-0.117-0.1-0.176-0.24-0.176-0.424c0-0.139,0.039-0.259,0.115-0.357
\t\tc0.078-0.099,0.164-0.178,0.259-0.236c-0.052-0.073-0.128-0.11-0.231-0.11c-0.307,0-0.575,0.115-0.803,0.347
\t\tc-0.227,0.231-0.404,0.508-0.533,0.83c-0.129,0.323-0.192,0.62-0.192,0.892v2.332c0,0.359,0.104,0.6,0.313,0.721
\t\ts0.493,0.181,0.853,0.181v0.231c-0.169-0.007-0.407-0.017-0.715-0.027s-0.635-0.017-0.979-0.017c-0.249,0-0.496,0.006-0.742,0.017
\t\ts-0.431,0.021-0.556,0.027v-0.231c0.301,0,0.515-0.055,0.644-0.165c0.129-0.109,0.192-0.318,0.192-0.627v-3.442
\t\tc0-0.33-0.059-0.574-0.176-0.731c-0.117-0.158-0.337-0.236-0.66-0.236v-0.231c0.235,0.022,0.462,0.033,0.683,0.033
\t\tc0.212,0,0.416-0.013,0.609-0.039c0.195-0.025,0.373-0.063,0.534-0.115v1.375c0.081-0.213,0.196-0.427,0.347-0.644
\t\tc0.15-0.216,0.332-0.397,0.545-0.544C483.856,509.582,484.092,509.509,484.348,509.509z"/>
\t<path d="M490.815,509.542v4.598c0,0.33,0.061,0.574,0.182,0.732c0.121,0.157,0.339,0.236,0.654,0.236v0.23
\t\tc-0.228-0.021-0.454-0.033-0.683-0.033c-0.212,0-0.416,0.012-0.609,0.033c-0.195,0.022-0.373,0.063-0.533,0.121v-1.298
\t\tc-0.184,0.477-0.442,0.815-0.776,1.018c-0.333,0.201-0.677,0.303-1.028,0.303c-0.257,0-0.48-0.037-0.671-0.11
\t\ts-0.349-0.176-0.474-0.308c-0.139-0.146-0.234-0.338-0.285-0.572c-0.052-0.234-0.077-0.524-0.077-0.869v-2.761
\t\tc0-0.33-0.06-0.574-0.177-0.731c-0.117-0.158-0.337-0.236-0.66-0.236v-0.231c0.235,0.022,0.463,0.033,0.683,0.033
\t\tc0.213,0,0.416-0.013,0.61-0.039c0.194-0.025,0.373-0.063,0.533-0.115v4.323c0,0.228,0.016,0.437,0.045,0.627
\t\ts0.102,0.345,0.22,0.462c0.117,0.117,0.308,0.176,0.571,0.176c0.434,0,0.789-0.192,1.067-0.577s0.419-0.856,0.419-1.414v-2.276
\t\tc0-0.33-0.06-0.574-0.177-0.731c-0.117-0.158-0.337-0.236-0.66-0.236v-0.231c0.235,0.022,0.462,0.033,0.683,0.033
\t\tc0.213,0,0.416-0.013,0.609-0.039C490.477,509.632,490.654,509.594,490.815,509.542z"/>
\t<path d="M494.963,509.509c0.234,0,0.461,0.029,0.682,0.088c0.221,0.06,0.418,0.147,0.594,0.265
\t\tc0.169,0.109,0.297,0.236,0.385,0.379c0.088,0.144,0.133,0.299,0.133,0.468c0,0.198-0.055,0.351-0.166,0.457
\t\tc-0.109,0.106-0.248,0.159-0.418,0.159c-0.16,0-0.302-0.048-0.423-0.144c-0.121-0.095-0.182-0.23-0.182-0.406
\t\tc0-0.169,0.046-0.305,0.138-0.407s0.199-0.169,0.324-0.198c-0.073-0.132-0.203-0.242-0.391-0.33
\t\tc-0.187-0.088-0.387-0.132-0.6-0.132c-0.184,0-0.366,0.044-0.549,0.132c-0.184,0.088-0.353,0.233-0.507,0.435
\t\tc-0.153,0.202-0.278,0.477-0.374,0.825s-0.143,0.783-0.143,1.304c0,0.646,0.072,1.158,0.22,1.54
\t\tc0.146,0.381,0.339,0.654,0.577,0.819s0.501,0.247,0.787,0.247c0.285,0,0.575-0.084,0.869-0.253
\t\tc0.293-0.168,0.535-0.462,0.726-0.88l0.209,0.077c-0.073,0.234-0.194,0.472-0.362,0.71c-0.17,0.238-0.39,0.434-0.66,0.588
\t\tc-0.271,0.154-0.605,0.231-1.002,0.231c-0.454,0-0.865-0.118-1.231-0.353s-0.658-0.57-0.874-1.006
\t\tc-0.217-0.437-0.325-0.955-0.325-1.557s0.108-1.132,0.325-1.59c0.216-0.458,0.518-0.817,0.906-1.078
\t\tC494.02,509.64,494.464,509.509,494.963,509.509z"/>
\t<path d="M499.329,507.837v1.837h1.628v0.221h-1.628v4.257c0,0.345,0.063,0.587,0.187,0.726c0.125,0.14,0.298,0.209,0.518,0.209
\t\tc0.221,0,0.41-0.09,0.572-0.27c0.161-0.18,0.301-0.481,0.418-0.907l0.22,0.055c-0.073,0.426-0.218,0.785-0.435,1.078
\t\ts-0.549,0.44-0.995,0.44c-0.249,0-0.455-0.031-0.616-0.094c-0.162-0.063-0.305-0.152-0.43-0.27c-0.16-0.169-0.272-0.37-0.335-0.605
\t\tc-0.062-0.234-0.093-0.546-0.093-0.935v-3.685h-1.057v-0.221h1.057v-1.683c0.183-0.007,0.358-0.022,0.527-0.044
\t\tC499.035,507.925,499.189,507.889,499.329,507.837z"/>
\t<path d="M504.147,509.509c0.47,0,0.891,0.104,1.265,0.309s0.673,0.527,0.896,0.968s0.336,1.012,0.336,1.716
\t\ts-0.112,1.274-0.336,1.711s-0.522,0.757-0.896,0.962s-0.795,0.309-1.265,0.309c-0.462,0-0.882-0.104-1.26-0.309
\t\ts-0.679-0.525-0.901-0.962c-0.225-0.437-0.336-1.007-0.336-1.711s0.111-1.275,0.336-1.716c0.223-0.44,0.523-0.763,0.901-0.968
\t\tS503.686,509.509,504.147,509.509z M504.147,509.729c-0.418,0-0.761,0.216-1.028,0.648s-0.402,1.141-0.402,2.123
\t\tc0,0.983,0.135,1.688,0.402,2.117c0.268,0.43,0.61,0.644,1.028,0.644s0.761-0.214,1.028-0.644c0.268-0.429,0.401-1.134,0.401-2.117
\t\tc0-0.982-0.134-1.69-0.401-2.123S504.565,509.729,504.147,509.729z"/>
\t<path d="M510.901,509.509c0.198,0,0.372,0.041,0.522,0.121c0.15,0.081,0.268,0.186,0.352,0.313
\t\tc0.084,0.129,0.127,0.273,0.127,0.435c0,0.184-0.057,0.345-0.171,0.484s-0.266,0.209-0.456,0.209c-0.154,0-0.29-0.05-0.407-0.148
\t\tc-0.117-0.1-0.177-0.24-0.177-0.424c0-0.139,0.039-0.259,0.116-0.357s0.163-0.178,0.259-0.236c-0.052-0.073-0.129-0.11-0.231-0.11
\t\tc-0.308,0-0.575,0.115-0.803,0.347s-0.405,0.508-0.534,0.83c-0.128,0.323-0.191,0.62-0.191,0.892v2.332
\t\tc0,0.359,0.104,0.6,0.313,0.721s0.494,0.181,0.854,0.181v0.231c-0.17-0.007-0.408-0.017-0.715-0.027
\t\tc-0.309-0.011-0.635-0.017-0.979-0.017c-0.249,0-0.497,0.006-0.743,0.017c-0.245,0.011-0.43,0.021-0.555,0.027v-0.231
\t\tc0.301,0,0.515-0.055,0.643-0.165c0.129-0.109,0.193-0.318,0.193-0.627v-3.442c0-0.33-0.059-0.574-0.176-0.731
\t\tc-0.117-0.158-0.338-0.236-0.66-0.236v-0.231c0.234,0.022,0.461,0.033,0.682,0.033c0.213,0,0.416-0.013,0.61-0.039
\t\tc0.194-0.025,0.372-0.063,0.534-0.115v1.375c0.08-0.213,0.195-0.427,0.346-0.644c0.15-0.216,0.332-0.397,0.545-0.544
\t\tC510.41,509.582,510.645,509.509,510.901,509.509z"/>
\t<path d="M516.357,515.405c-0.477,0-0.824-0.119-1.04-0.357s-0.324-0.556-0.324-0.952c0-0.308,0.075-0.563,0.226-0.764
\t\tc0.15-0.202,0.342-0.367,0.577-0.495c0.235-0.129,0.481-0.238,0.743-0.33c0.26-0.092,0.508-0.182,0.742-0.27
\t\ts0.427-0.188,0.577-0.297c0.15-0.11,0.226-0.25,0.226-0.418v-0.683c0-0.308-0.046-0.542-0.138-0.704
\t\tc-0.092-0.161-0.214-0.27-0.368-0.324s-0.326-0.082-0.518-0.082c-0.184,0-0.379,0.025-0.588,0.076
\t\tc-0.209,0.052-0.376,0.158-0.5,0.319c0.139,0.029,0.26,0.103,0.362,0.22c0.103,0.118,0.153,0.269,0.153,0.451
\t\tc0,0.184-0.059,0.328-0.176,0.435s-0.268,0.159-0.451,0.159c-0.212,0-0.367-0.067-0.467-0.203s-0.148-0.288-0.148-0.456
\t\tc0-0.191,0.047-0.345,0.143-0.462s0.217-0.228,0.363-0.33c0.168-0.117,0.387-0.219,0.654-0.303s0.57-0.127,0.908-0.127
\t\tc0.301,0,0.557,0.035,0.77,0.104c0.213,0.07,0.389,0.171,0.527,0.303c0.191,0.176,0.316,0.391,0.375,0.644s0.088,0.556,0.088,0.907
\t\tv3.059c0,0.176,0.025,0.306,0.076,0.391c0.052,0.084,0.14,0.126,0.265,0.126c0.103,0,0.192-0.021,0.27-0.065
\t\tc0.077-0.045,0.151-0.1,0.226-0.165l0.121,0.187c-0.154,0.117-0.301,0.215-0.44,0.292c-0.14,0.076-0.326,0.115-0.562,0.115
\t\tc-0.381,0-0.634-0.1-0.758-0.297c-0.125-0.198-0.188-0.426-0.188-0.683c-0.234,0.389-0.5,0.649-0.797,0.781
\t\tC516.989,515.339,516.68,515.405,516.357,515.405z M516.841,514.965c0.22,0,0.437-0.063,0.649-0.192
\t\tc0.213-0.128,0.41-0.343,0.594-0.644v-2.156c-0.109,0.162-0.281,0.29-0.512,0.386c-0.23,0.095-0.469,0.199-0.715,0.313
\t\tc-0.246,0.113-0.457,0.268-0.633,0.462s-0.264,0.471-0.264,0.83c0,0.338,0.084,0.589,0.253,0.754S516.592,514.965,516.841,514.965z
\t\t"/>
\t<path d="M524.057,509.509c0.279,0,0.512,0.037,0.699,0.11c0.187,0.073,0.342,0.176,0.467,0.308c0.14,0.147,0.238,0.33,0.297,0.551
\t\tc0.059,0.22,0.088,0.517,0.088,0.891v2.937c0,0.309,0.064,0.518,0.193,0.627c0.128,0.11,0.342,0.165,0.643,0.165v0.231
\t\tc-0.124-0.007-0.314-0.017-0.571-0.027s-0.506-0.017-0.748-0.017s-0.479,0.006-0.71,0.017c-0.23,0.011-0.404,0.021-0.521,0.027
\t\tv-0.231c0.264,0,0.45-0.055,0.561-0.165c0.109-0.109,0.165-0.318,0.165-0.627v-3.179c0-0.228-0.019-0.437-0.056-0.627
\t\tc-0.036-0.19-0.119-0.345-0.247-0.462s-0.324-0.176-0.589-0.176c-0.461,0-0.834,0.192-1.116,0.577s-0.423,0.86-0.423,1.425v2.441
\t\tc0,0.309,0.055,0.518,0.164,0.627c0.11,0.11,0.297,0.165,0.562,0.165v0.231c-0.118-0.007-0.292-0.017-0.522-0.027
\t\ts-0.468-0.017-0.709-0.017c-0.242,0-0.492,0.006-0.748,0.017c-0.258,0.011-0.447,0.021-0.572,0.027v-0.231
\t\tc0.301,0,0.515-0.055,0.643-0.165c0.129-0.109,0.193-0.318,0.193-0.627v-3.442c0-0.33-0.059-0.574-0.176-0.731
\t\tc-0.117-0.158-0.338-0.236-0.66-0.236v-0.231c0.234,0.022,0.461,0.033,0.682,0.033c0.213,0,0.416-0.013,0.61-0.039
\t\tc0.194-0.025,0.372-0.063,0.534-0.115v1.298c0.189-0.498,0.456-0.845,0.797-1.039C523.326,509.606,523.684,509.509,524.057,509.509
\t\tz"/>
\t<path d="M529.623,509.509c0.322,0,0.625,0.077,0.907,0.231s0.472,0.418,0.566,0.792l-0.143,0.11
\t\tc-0.103-0.309-0.263-0.532-0.479-0.672c-0.217-0.139-0.472-0.209-0.765-0.209c-0.433,0-0.794,0.225-1.084,0.672
\t\tc-0.289,0.447-0.431,1.137-0.423,2.067c0,0.594,0.057,1.088,0.171,1.479c0.113,0.393,0.281,0.686,0.506,0.88
\t\tc0.223,0.194,0.496,0.292,0.818,0.292c0.309,0,0.588-0.132,0.836-0.396c0.25-0.264,0.4-0.652,0.451-1.166l0.133,0.154
\t\tc-0.059,0.558-0.236,0.986-0.533,1.287s-0.699,0.451-1.205,0.451c-0.455,0-0.853-0.11-1.193-0.33
\t\tc-0.342-0.221-0.602-0.55-0.781-0.99s-0.27-0.993-0.27-1.661c0-0.667,0.115-1.223,0.347-1.666c0.231-0.444,0.535-0.775,0.913-0.996
\t\tC528.773,509.619,529.184,509.509,529.623,509.509z M531.934,506.704v7.425c0,0.33,0.06,0.574,0.181,0.731
\t\tc0.121,0.158,0.339,0.236,0.655,0.236v0.231c-0.229-0.022-0.455-0.033-0.683-0.033c-0.213,0-0.417,0.011-0.61,0.033
\t\tc-0.194,0.022-0.373,0.063-0.533,0.121v-7.425c0-0.33-0.059-0.574-0.176-0.731c-0.118-0.158-0.338-0.237-0.66-0.237v-0.23
\t\tc0.234,0.021,0.461,0.033,0.682,0.033c0.213,0,0.416-0.013,0.61-0.039C531.594,506.794,531.771,506.756,531.934,506.704z"/>
\t<path d="M383.842,520.586c0.484,0,0.882,0.072,1.193,0.215s0.588,0.31,0.831,0.501c0.146,0.109,0.258,0.122,0.335,0.038
\t\ts0.13-0.284,0.16-0.6h0.253c-0.015,0.271-0.026,0.602-0.033,0.99s-0.011,0.901-0.011,1.54h-0.253
\t\tc-0.052-0.315-0.099-0.566-0.143-0.754c-0.044-0.187-0.097-0.347-0.16-0.479s-0.145-0.264-0.248-0.396
\t\tc-0.227-0.301-0.513-0.519-0.858-0.654s-0.708-0.204-1.089-0.204c-0.359,0-0.684,0.09-0.974,0.27s-0.539,0.438-0.748,0.775
\t\tc-0.209,0.338-0.37,0.744-0.484,1.222c-0.114,0.477-0.17,1.012-0.17,1.605c0,0.616,0.062,1.16,0.187,1.634
\t\tc0.125,0.473,0.299,0.871,0.522,1.193s0.487,0.566,0.792,0.731c0.304,0.165,0.632,0.247,0.984,0.247
\t\tc0.33,0,0.675-0.065,1.034-0.197c0.359-0.133,0.642-0.345,0.847-0.639c0.162-0.212,0.273-0.443,0.336-0.692
\t\tc0.062-0.249,0.119-0.598,0.17-1.045h0.253c0,0.667,0.003,1.204,0.011,1.611s0.019,0.75,0.033,1.028h-0.253
\t\tc-0.029-0.315-0.079-0.514-0.148-0.594c-0.07-0.081-0.186-0.07-0.347,0.033c-0.271,0.19-0.559,0.357-0.863,0.5
\t\tc-0.305,0.143-0.695,0.215-1.172,0.215c-0.697,0-1.307-0.158-1.832-0.474c-0.524-0.314-0.931-0.77-1.221-1.363
\t\tc-0.29-0.595-0.435-1.31-0.435-2.146c0-0.821,0.15-1.54,0.451-2.156c0.3-0.615,0.713-1.096,1.237-1.44
\t\tS383.16,520.586,383.842,520.586z"/>
\t<path d="M390.343,522.709c0.469,0,0.891,0.104,1.265,0.309s0.673,0.527,0.896,0.968s0.336,1.012,0.336,1.716
\t\ts-0.112,1.274-0.336,1.711s-0.522,0.757-0.896,0.962s-0.795,0.309-1.265,0.309c-0.462,0-0.882-0.104-1.26-0.309
\t\tc-0.377-0.205-0.678-0.525-0.902-0.962s-0.335-1.007-0.335-1.711s0.112-1.275,0.335-1.716s0.524-0.763,0.902-0.968
\t\tC389.461,522.813,389.881,522.709,390.343,522.709z M390.343,522.93c-0.418,0-0.761,0.216-1.029,0.648s-0.401,1.141-0.401,2.123
\t\tc0,0.983,0.134,1.688,0.401,2.117c0.268,0.43,0.611,0.644,1.029,0.644s0.761-0.214,1.028-0.644
\t\tc0.268-0.429,0.401-1.134,0.401-2.117c0-0.982-0.134-1.69-0.401-2.123S390.761,522.93,390.343,522.93z"/>
\t<path d="M398.67,522.742v4.598c0,0.33,0.061,0.574,0.182,0.732c0.121,0.157,0.339,0.236,0.654,0.236v0.23
\t\tc-0.228-0.021-0.455-0.033-0.682-0.033c-0.212,0-0.416,0.012-0.61,0.033c-0.194,0.022-0.372,0.063-0.533,0.121v-1.298
\t\tc-0.184,0.477-0.442,0.815-0.776,1.018c-0.333,0.201-0.676,0.303-1.028,0.303c-0.257,0-0.48-0.037-0.671-0.11
\t\tc-0.191-0.073-0.349-0.176-0.473-0.308c-0.139-0.146-0.235-0.338-0.286-0.572s-0.077-0.524-0.077-0.869v-2.761
\t\tc0-0.33-0.059-0.574-0.176-0.731c-0.117-0.158-0.337-0.236-0.66-0.236v-0.231c0.235,0.022,0.462,0.033,0.682,0.033
\t\tc0.213,0,0.416-0.013,0.61-0.039c0.194-0.025,0.373-0.063,0.534-0.115v4.323c0,0.228,0.015,0.437,0.044,0.627
\t\ts0.103,0.345,0.22,0.462c0.117,0.117,0.308,0.176,0.572,0.176c0.433,0,0.788-0.192,1.067-0.577s0.418-0.856,0.418-1.414v-2.276
\t\tc0-0.33-0.059-0.574-0.176-0.731c-0.117-0.158-0.337-0.236-0.66-0.236v-0.231c0.235,0.022,0.462,0.033,0.682,0.033
\t\tc0.213,0,0.416-0.013,0.61-0.039C398.331,522.832,398.508,522.794,398.67,522.742z"/>
\t<path d="M403.488,522.709c0.198,0,0.372,0.041,0.522,0.121c0.15,0.081,0.268,0.186,0.352,0.313
\t\tc0.084,0.129,0.127,0.273,0.127,0.435c0,0.184-0.057,0.345-0.171,0.484s-0.266,0.209-0.457,0.209c-0.154,0-0.29-0.05-0.407-0.148
\t\tc-0.117-0.1-0.176-0.24-0.176-0.424c0-0.139,0.039-0.259,0.116-0.357s0.163-0.178,0.258-0.236c-0.051-0.073-0.128-0.11-0.231-0.11
\t\tc-0.308,0-0.576,0.115-0.803,0.347c-0.228,0.231-0.405,0.508-0.534,0.83c-0.128,0.323-0.192,0.62-0.192,0.892v2.332
\t\tc0,0.359,0.104,0.6,0.313,0.721s0.493,0.181,0.853,0.181v0.231c-0.169-0.007-0.407-0.017-0.715-0.027
\t\tc-0.308-0.011-0.635-0.017-0.979-0.017c-0.25,0-0.497,0.006-0.743,0.017s-0.431,0.021-0.556,0.027v-0.231
\t\tc0.301,0,0.515-0.055,0.644-0.165c0.128-0.109,0.192-0.318,0.192-0.627v-3.442c0-0.33-0.059-0.574-0.176-0.731
\t\tc-0.117-0.158-0.337-0.236-0.66-0.236v-0.231c0.235,0.022,0.462,0.033,0.682,0.033c0.212,0,0.416-0.013,0.61-0.039
\t\tc0.194-0.025,0.372-0.063,0.534-0.115v1.375c0.081-0.213,0.196-0.427,0.347-0.644c0.15-0.216,0.332-0.397,0.544-0.544
\t\tC402.997,522.782,403.231,522.709,403.488,522.709z"/>
\t<path d="M406.931,522.709c0.293,0,0.542,0.048,0.748,0.144c0.205,0.095,0.355,0.184,0.451,0.264
\t\tc0.242,0.198,0.389,0.066,0.44-0.396h0.253c-0.015,0.205-0.026,0.449-0.033,0.731s-0.011,0.661-0.011,1.138h-0.253
\t\tc-0.044-0.271-0.119-0.535-0.225-0.792c-0.106-0.256-0.263-0.467-0.468-0.632c-0.206-0.165-0.473-0.248-0.803-0.248
\t\tc-0.257,0-0.472,0.072-0.644,0.215c-0.172,0.143-0.259,0.354-0.259,0.633c0,0.22,0.066,0.408,0.198,0.566
\t\tc0.132,0.157,0.304,0.304,0.517,0.439c0.212,0.136,0.443,0.284,0.693,0.446c0.418,0.271,0.773,0.546,1.067,0.824
\t\tc0.293,0.279,0.44,0.646,0.44,1.101c0,0.337-0.088,0.619-0.264,0.847c-0.176,0.228-0.405,0.399-0.688,0.517
\t\tc-0.283,0.118-0.592,0.177-0.93,0.177c-0.162,0-0.313-0.014-0.457-0.039s-0.28-0.064-0.413-0.115
\t\tc-0.073-0.036-0.148-0.081-0.226-0.132c-0.077-0.052-0.152-0.106-0.225-0.165c-0.074-0.059-0.14-0.057-0.198,0.006
\t\tc-0.059,0.062-0.103,0.185-0.132,0.368h-0.253c0.014-0.234,0.025-0.521,0.033-0.858c0.007-0.337,0.011-0.784,0.011-1.342h0.253
\t\tc0.051,0.411,0.125,0.771,0.22,1.078s0.246,0.548,0.451,0.721c0.205,0.172,0.491,0.258,0.858,0.258c0.22,0,0.438-0.076,0.655-0.23
\t\tc0.216-0.154,0.324-0.426,0.324-0.814c0-0.322-0.117-0.586-0.352-0.792c-0.235-0.205-0.532-0.421-0.891-0.648
\t\tc-0.264-0.169-0.512-0.338-0.743-0.506c-0.231-0.169-0.418-0.359-0.561-0.572s-0.214-0.47-0.214-0.771
\t\tc0-0.33,0.075-0.599,0.226-0.809c0.15-0.209,0.35-0.362,0.599-0.462C406.377,522.759,406.645,522.709,406.931,522.709z"/>
\t<path d="M412.552,522.709c0.645,0,1.149,0.196,1.512,0.589c0.363,0.393,0.544,1.003,0.544,1.831h-3.883l-0.011-0.209h2.838
\t\tc0.015-0.358-0.015-0.69-0.088-0.995s-0.188-0.548-0.346-0.731s-0.361-0.275-0.61-0.275c-0.337,0-0.637,0.169-0.897,0.507
\t\tc-0.26,0.337-0.416,0.872-0.467,1.605l0.033,0.044c-0.015,0.11-0.026,0.231-0.033,0.363s-0.011,0.264-0.011,0.396
\t\tc0,0.499,0.081,0.928,0.242,1.287s0.372,0.633,0.632,0.819c0.26,0.188,0.53,0.281,0.809,0.281c0.33,0,0.634-0.081,0.913-0.242
\t\ts0.513-0.447,0.704-0.858l0.22,0.088c-0.081,0.242-0.212,0.478-0.396,0.704c-0.183,0.228-0.414,0.413-0.693,0.556
\t\ts-0.601,0.215-0.968,0.215c-0.528,0-0.981-0.121-1.358-0.363c-0.378-0.242-0.667-0.577-0.869-1.007
\t\tc-0.202-0.429-0.303-0.922-0.303-1.479c0-0.645,0.103-1.2,0.308-1.666c0.205-0.466,0.495-0.825,0.869-1.078
\t\tS412.053,522.709,412.552,522.709z"/>
\t<path d="M421.33,520.74c1.313,0,2.288,0.328,2.926,0.984c0.638,0.656,0.957,1.597,0.957,2.821c0,0.8-0.156,1.498-0.468,2.096
\t\ts-0.765,1.062-1.358,1.392s-1.313,0.495-2.156,0.495c-0.162,0-0.4-0.006-0.715-0.017s-0.59-0.017-0.825-0.017
\t\tc-0.257,0-0.51,0.002-0.759,0.006s-0.451,0.013-0.605,0.027v-0.22c0.25-0.015,0.438-0.044,0.566-0.088s0.215-0.133,0.259-0.265
\t\ts0.066-0.33,0.066-0.594v-5.456c0-0.271-0.022-0.471-0.066-0.6c-0.044-0.128-0.13-0.216-0.259-0.264s-0.317-0.075-0.566-0.083
\t\tv-0.22c0.154,0.008,0.355,0.017,0.605,0.027c0.25,0.012,0.495,0.013,0.737,0.006c0.256-0.008,0.546-0.015,0.869-0.022
\t\tC420.86,520.744,421.125,520.74,421.33,520.74z M421.055,520.938c-0.337,0-0.557,0.063-0.66,0.187
\t\tc-0.103,0.125-0.154,0.378-0.154,0.76v5.5c0,0.381,0.053,0.634,0.16,0.759c0.106,0.125,0.328,0.187,0.666,0.187
\t\tc0.799,0,1.418-0.145,1.859-0.435c0.44-0.289,0.748-0.713,0.924-1.271c0.177-0.557,0.265-1.235,0.265-2.035
\t\tc0-1.231-0.227-2.148-0.678-2.75C422.985,521.239,422.191,520.938,421.055,520.938z"/>
\t<path d="M428.81,522.709c0.646,0,1.149,0.196,1.513,0.589s0.544,1.003,0.544,1.831h-3.883l-0.011-0.209h2.838
\t\tc0.015-0.358-0.016-0.69-0.088-0.995c-0.074-0.305-0.189-0.548-0.347-0.731s-0.361-0.275-0.61-0.275
\t\tc-0.338,0-0.637,0.169-0.896,0.507c-0.261,0.337-0.416,0.872-0.468,1.605l0.033,0.044c-0.015,0.11-0.026,0.231-0.033,0.363
\t\ts-0.011,0.264-0.011,0.396c0,0.499,0.08,0.928,0.242,1.287c0.16,0.359,0.371,0.633,0.632,0.819c0.261,0.188,0.53,0.281,0.809,0.281
\t\tc0.33,0,0.634-0.081,0.913-0.242s0.514-0.447,0.704-0.858l0.22,0.088c-0.08,0.242-0.213,0.478-0.396,0.704
\t\tc-0.183,0.228-0.414,0.413-0.692,0.556s-0.601,0.215-0.968,0.215c-0.528,0-0.98-0.121-1.358-0.363s-0.667-0.577-0.869-1.007
\t\tc-0.202-0.429-0.303-0.922-0.303-1.479c0-0.645,0.103-1.2,0.308-1.666s0.496-0.825,0.869-1.078
\t\tC427.875,522.836,428.311,522.709,428.81,522.709z"/>
\t<path d="M433.672,522.709c0.293,0,0.542,0.048,0.748,0.144c0.205,0.095,0.355,0.184,0.45,0.264c0.242,0.198,0.389,0.066,0.44-0.396
\t\th0.253c-0.015,0.205-0.026,0.449-0.033,0.731s-0.011,0.661-0.011,1.138h-0.254c-0.043-0.271-0.119-0.535-0.225-0.792
\t\tc-0.106-0.256-0.263-0.467-0.468-0.632c-0.206-0.165-0.474-0.248-0.803-0.248c-0.257,0-0.472,0.072-0.644,0.215
\t\ts-0.259,0.354-0.259,0.633c0,0.22,0.065,0.408,0.198,0.566c0.132,0.157,0.305,0.304,0.518,0.439
\t\tc0.212,0.136,0.443,0.284,0.692,0.446c0.418,0.271,0.773,0.546,1.067,0.824c0.293,0.279,0.439,0.646,0.439,1.101
\t\tc0,0.337-0.088,0.619-0.264,0.847s-0.405,0.399-0.688,0.517c-0.283,0.118-0.592,0.177-0.93,0.177c-0.162,0-0.313-0.014-0.457-0.039
\t\tc-0.143-0.025-0.279-0.064-0.412-0.115c-0.073-0.036-0.148-0.081-0.226-0.132c-0.077-0.052-0.151-0.106-0.226-0.165
\t\tc-0.073-0.059-0.139-0.057-0.197,0.006c-0.059,0.062-0.104,0.185-0.133,0.368H432c0.014-0.234,0.025-0.521,0.032-0.858
\t\tc0.007-0.337,0.011-0.784,0.011-1.342h0.254c0.051,0.411,0.124,0.771,0.219,1.078c0.096,0.308,0.246,0.548,0.451,0.721
\t\tc0.205,0.172,0.492,0.258,0.858,0.258c0.22,0,0.438-0.076,0.655-0.23c0.215-0.154,0.324-0.426,0.324-0.814
\t\tc0-0.322-0.117-0.586-0.353-0.792c-0.235-0.205-0.532-0.421-0.891-0.648c-0.265-0.169-0.512-0.338-0.743-0.506
\t\tc-0.23-0.169-0.418-0.359-0.561-0.572s-0.215-0.47-0.215-0.771c0-0.33,0.076-0.599,0.227-0.809c0.149-0.209,0.35-0.362,0.599-0.462
\t\tC433.117,522.759,433.385,522.709,433.672,522.709z"/>
\t<path d="M438.445,522.742v4.763c0,0.309,0.064,0.518,0.192,0.627c0.128,0.11,0.343,0.165,0.644,0.165v0.231
\t\tc-0.125-0.007-0.313-0.017-0.566-0.027s-0.508-0.017-0.765-0.017c-0.249,0-0.503,0.006-0.759,0.017
\t\tc-0.257,0.011-0.447,0.021-0.572,0.027v-0.231c0.301,0,0.516-0.055,0.644-0.165c0.128-0.109,0.192-0.318,0.192-0.627v-3.442
\t\tc0-0.33-0.059-0.574-0.176-0.731c-0.117-0.158-0.338-0.236-0.66-0.236v-0.231c0.234,0.022,0.462,0.033,0.682,0.033
\t\tc0.213,0,0.416-0.013,0.611-0.039C438.105,522.832,438.283,522.794,438.445,522.742z M437.873,520.113
\t\tc0.191,0,0.355,0.069,0.495,0.209s0.209,0.305,0.209,0.495s-0.069,0.355-0.209,0.495s-0.304,0.209-0.495,0.209
\t\tc-0.19,0-0.355-0.069-0.494-0.209c-0.141-0.14-0.209-0.305-0.209-0.495s0.068-0.355,0.209-0.495
\t\tC437.518,520.183,437.683,520.113,437.873,520.113z"/>
\t<path d="M441.866,530.597c-0.389,0-0.755-0.041-1.1-0.121c-0.345-0.081-0.622-0.208-0.831-0.38s-0.313-0.391-0.313-0.654
\t\tc0-0.257,0.102-0.477,0.308-0.66c0.205-0.184,0.483-0.322,0.836-0.418l0.065,0.176c-0.161,0.052-0.289,0.154-0.385,0.309
\t\tc-0.096,0.153-0.143,0.326-0.143,0.517c0,0.337,0.155,0.596,0.467,0.775c0.313,0.18,0.717,0.27,1.216,0.27
\t\tc0.33,0,0.653-0.042,0.968-0.126c0.315-0.085,0.576-0.226,0.781-0.424s0.309-0.458,0.309-0.781c0-0.242-0.087-0.441-0.259-0.6
\t\tc-0.173-0.157-0.512-0.236-1.018-0.236h-0.813c-0.235,0-0.463-0.019-0.683-0.055c-0.22-0.037-0.399-0.114-0.539-0.231
\t\ts-0.209-0.297-0.209-0.539c0-0.249,0.103-0.489,0.308-0.72c0.205-0.231,0.584-0.468,1.133-0.71l0.133,0.099
\t\tc-0.25,0.133-0.466,0.267-0.649,0.402c-0.183,0.136-0.274,0.295-0.274,0.479c0,0.234,0.176,0.352,0.527,0.352h1.375
\t\tc0.455,0,0.856,0.101,1.205,0.303c0.348,0.201,0.521,0.533,0.521,0.995c0,0.345-0.109,0.667-0.329,0.968s-0.548,0.545-0.985,0.731
\t\tC443.053,530.503,442.512,530.597,441.866,530.597z M441.922,526.186c-0.543,0-1-0.14-1.37-0.418
\t\tc-0.37-0.279-0.556-0.719-0.556-1.32c0-0.608,0.186-1.051,0.556-1.325c0.37-0.275,0.827-0.413,1.37-0.413
\t\tc0.542,0,0.998,0.138,1.369,0.413c0.37,0.274,0.556,0.717,0.556,1.325c0,0.602-0.186,1.041-0.556,1.32
\t\tC442.92,526.046,442.464,526.186,441.922,526.186z M441.922,525.987c0.278,0,0.496-0.11,0.654-0.33
\t\tc0.157-0.22,0.236-0.623,0.236-1.21s-0.079-0.99-0.236-1.21c-0.158-0.22-0.376-0.33-0.654-0.33c-0.271,0-0.488,0.11-0.649,0.33
\t\ts-0.242,0.623-0.242,1.21s0.081,0.99,0.242,1.21S441.65,525.987,441.922,525.987z M443.506,523.622l-0.198-0.077
\t\tc0.103-0.249,0.278-0.469,0.528-0.659c0.248-0.191,0.51-0.286,0.78-0.286c0.19,0,0.345,0.053,0.462,0.159s0.176,0.266,0.176,0.479
\t\tc0,0.228-0.061,0.387-0.182,0.479c-0.12,0.092-0.243,0.138-0.367,0.138c-0.111,0-0.213-0.039-0.309-0.115
\t\tc-0.096-0.077-0.15-0.2-0.165-0.369c-0.015-0.168,0.036-0.385,0.153-0.648l0.144,0.032c-0.308,0.118-0.528,0.24-0.659,0.369
\t\tC443.736,523.25,443.615,523.417,443.506,523.622z"/>
\t<path d="M449.347,522.709c0.278,0,0.511,0.037,0.698,0.11s0.343,0.176,0.468,0.308c0.14,0.147,0.238,0.33,0.297,0.551
\t\tc0.059,0.22,0.088,0.517,0.088,0.891v2.937c0,0.309,0.063,0.518,0.192,0.627c0.129,0.11,0.343,0.165,0.644,0.165v0.231
\t\tc-0.124-0.007-0.315-0.017-0.571-0.027c-0.258-0.011-0.506-0.017-0.748-0.017s-0.479,0.006-0.71,0.017s-0.405,0.021-0.522,0.027
\t\tv-0.231c0.264,0,0.451-0.055,0.561-0.165c0.11-0.109,0.166-0.318,0.166-0.627v-3.179c0-0.228-0.02-0.437-0.056-0.627
\t\ts-0.119-0.345-0.247-0.462c-0.129-0.117-0.324-0.176-0.589-0.176c-0.462,0-0.835,0.192-1.116,0.577
\t\tc-0.283,0.385-0.424,0.86-0.424,1.425v2.441c0,0.309,0.055,0.518,0.165,0.627c0.11,0.11,0.297,0.165,0.562,0.165v0.231
\t\tc-0.118-0.007-0.292-0.017-0.523-0.027c-0.23-0.011-0.467-0.017-0.709-0.017s-0.492,0.006-0.748,0.017
\t\tc-0.257,0.011-0.447,0.021-0.572,0.027v-0.231c0.301,0,0.516-0.055,0.644-0.165c0.128-0.109,0.192-0.318,0.192-0.627v-3.442
\t\tc0-0.33-0.059-0.574-0.176-0.731c-0.117-0.158-0.338-0.236-0.66-0.236v-0.231c0.234,0.022,0.462,0.033,0.682,0.033
\t\tc0.213,0,0.416-0.013,0.611-0.039c0.193-0.025,0.371-0.063,0.533-0.115v1.298c0.19-0.498,0.457-0.845,0.797-1.039
\t\tC448.615,522.807,448.973,522.709,449.347,522.709z"/>
\t<path d="M454.891,522.709c0.645,0,1.149,0.196,1.512,0.589c0.363,0.393,0.545,1.003,0.545,1.831h-3.883l-0.012-0.209h2.839
\t\tc0.015-0.358-0.015-0.69-0.089-0.995c-0.072-0.305-0.188-0.548-0.346-0.731s-0.361-0.275-0.61-0.275
\t\tc-0.337,0-0.637,0.169-0.897,0.507c-0.26,0.337-0.416,0.872-0.467,1.605l0.033,0.044c-0.016,0.11-0.025,0.231-0.033,0.363
\t\ts-0.011,0.264-0.011,0.396c0,0.499,0.081,0.928,0.241,1.287c0.162,0.359,0.373,0.633,0.633,0.819
\t\tc0.26,0.188,0.529,0.281,0.809,0.281c0.33,0,0.635-0.081,0.913-0.242s0.513-0.447,0.704-0.858l0.22,0.088
\t\tc-0.081,0.242-0.212,0.478-0.396,0.704c-0.184,0.228-0.414,0.413-0.693,0.556s-0.602,0.215-0.968,0.215
\t\tc-0.528,0-0.981-0.121-1.358-0.363c-0.378-0.242-0.668-0.577-0.869-1.007c-0.201-0.429-0.303-0.922-0.303-1.479
\t\tc0-0.645,0.103-1.2,0.309-1.666c0.205-0.466,0.494-0.825,0.869-1.078C453.955,522.836,454.392,522.709,454.891,522.709z"/>
\t<path d="M461.205,522.709c0.197,0,0.371,0.041,0.521,0.121c0.15,0.081,0.268,0.186,0.352,0.313
\t\tc0.085,0.129,0.127,0.273,0.127,0.435c0,0.184-0.057,0.345-0.17,0.484c-0.114,0.14-0.266,0.209-0.457,0.209
\t\tc-0.154,0-0.289-0.05-0.406-0.148c-0.117-0.1-0.177-0.24-0.177-0.424c0-0.139,0.038-0.259,0.116-0.357
\t\tc0.076-0.099,0.162-0.178,0.258-0.236c-0.051-0.073-0.129-0.11-0.23-0.11c-0.309,0-0.576,0.115-0.803,0.347
\t\tc-0.229,0.231-0.406,0.508-0.534,0.83c-0.128,0.323-0.192,0.62-0.192,0.892v2.332c0,0.359,0.104,0.6,0.313,0.721
\t\ts0.493,0.181,0.853,0.181v0.231c-0.169-0.007-0.407-0.017-0.715-0.027c-0.309-0.011-0.635-0.017-0.979-0.017
\t\tc-0.25,0-0.498,0.006-0.743,0.017s-0.431,0.021-0.556,0.027v-0.231c0.301,0,0.516-0.055,0.644-0.165
\t\tc0.128-0.109,0.192-0.318,0.192-0.627v-3.442c0-0.33-0.059-0.574-0.176-0.731c-0.117-0.158-0.338-0.236-0.66-0.236v-0.231
\t\tc0.234,0.022,0.462,0.033,0.682,0.033c0.213,0,0.416-0.013,0.611-0.039c0.193-0.025,0.371-0.063,0.533-0.115v1.375
\t\tc0.08-0.213,0.196-0.427,0.347-0.644c0.149-0.216,0.331-0.397,0.544-0.544C460.713,522.782,460.947,522.709,461.205,522.709z"/>
</g>
<g>
\t<path fill-rule="evenodd" clip-rule="evenodd" d="M428.648,438.911c0.125-0.196,0.205-0.482,0.383-0.57
\t\tc0.58-0.285,1.197-0.498,1.799-0.739c-0.043,0.578,0.041,1.202-0.148,1.728c-1.326,3.672-2.707,7.323-4.164,11.234
\t\tc0.742-0.229,1.193-0.418,1.662-0.502c1.043-0.187,1.576,0.391,1.092,1.323c-0.963,1.854-2.031,3.655-3.125,5.436
\t\tc-0.904,1.469-1.908,2.875-2.961,4.448c0.824-0.195,1.445-0.38,2.082-0.484c1.322-0.219,2.023,0.316,1.811,1.628
\t\tc-0.229,1.399-0.727,2.76-1.166,4.118c-0.533,1.646-1.129,3.272-1.66,4.799c-0.338-0.329-0.496-1.271-0.145-2.14
\t\tc0.508-1.258,1.098-2.484,1.559-3.758c0.326-0.902,0.566-1.849,0.715-2.796c0.17-1.075-0.232-1.481-1.297-1.243
\t\tc-1.371,0.306-2.506,0.808-3.167,2.361c-0.991,2.326-0.945,4.658-0.838,7.061c0.111,2.485,0.1,4.978-0.792,7.345
\t\tc-0.279,0.741-0.743,1.461-1.28,2.047c-0.899,0.979-2.004,0.59-3.16-0.761c-2.058-2.403-2.795-5.277-2.01-8.388
\t\tc0.78-3.091,1.704-6.146,2.579-9.213c0.067-0.233,0.245-0.435,0.372-0.651c0.102,0.035,0.203,0.069,0.304,0.104
\t\tc-0.27,0.933-0.564,1.858-0.805,2.799c-0.72,2.811-1.56,5.601-2.082,8.448c-0.414,2.26,0.326,4.403,1.703,6.242
\t\tc0.421,0.563,1.075,1.042,1.726,1.315c0.692,0.292,1.244-0.168,1.583-0.833c0.784-1.534,1.396-3.098,1.346-4.877
\t\tc-0.056-2.013-0.013-4.028-0.013-6.043c-0.094-0.018-0.188-0.036-0.282-0.054c-0.795,2.298-1.59,4.597-2.385,6.895
\t\tc-0.119-0.024-0.239-0.05-0.358-0.075c0.344-2.309,0.688-4.617,1.051-7.053c-1.36,0.392-1.743,2.146-3.354,1.684
\t\tc0.673-0.784,1.278-1.489,1.912-2.228c0.115,0.038,0.329,0.107,0.543,0.178c0.048-0.211,0.118-0.42,0.138-0.633
\t\tc0.022-0.237-0.094-0.533,0.009-0.712c1.607-2.785,0.794-5.879,1.22-8.837c-1.361,0.648-1.023,2.206-2.051,3.079
\t\tc0.069-0.454,0.139-0.909,0.208-1.363c-0.063-0.066-0.126-0.133-0.189-0.198c-2.864,3.466-5.729,6.932-8.786,10.631
\t\tc0.062-0.576-0.019-1.086,0.177-1.444c1.915-3.507,3.834-7.013,5.841-10.468c1.11-1.911,2.343-3.758,3.615-5.566
\t\tc0.632-0.899,0.647-1.749,0.337-2.715c-2.408,1.233-2.67,1.184-4.04-0.978c-0.832,4.35-2.419,8.303-2.706,12.635
\t\tc-0.698,0.231-0.769,0.21-0.591-0.639c0.798-3.795,1.637-7.58,2.449-11.372c0.17-0.796,0.286-1.603,0.439-2.402
\t\tc0.065-0.341,0.163-0.675,0.245-1.013c0.104,0.007,0.206,0.013,0.31,0.02c0.16,0.767,0.253,1.555,0.497,2.294
\t\tc0.271,0.824,0.495,1.814,1.765,1.557c0.891-0.182,1.59-1.118,1.567-2.154c-0.033-1.476,0.035-2.847,0.798-4.259
\t\tc0.472-0.873,0.091-2.2,0.119-3.323c0.079-3.217,0.14-6.435,0.29-9.647c0.022-0.476,0.388-1.091,0.784-1.335
\t\tc0.236-0.146,0.904,0.223,1.268,0.503c1.251,0.967,1.903,2.332,1.79,3.865c-0.164,2.183-0.51,4.357-0.877,6.519
\t\tc-0.437,2.577-0.978,5.137-1.537,8.023c1.037-1.358,1.853-2.515,2.759-3.597c1.295-1.547,2.637-3.055,3.998-4.542
\t\tc0.295-0.323,0.758-0.492,1.145-0.732c-0.184,0.284-0.334,0.596-0.553,0.849c-1.389,1.611-2.891,3.134-4.162,4.831
\t\tc-1.484,1.985-3.479,3.702-3.916,6.321c-0.256,1.535-0.346,3.097-0.511,4.646c0.12,0.031,0.239,0.062,0.358,0.093
\t\tc1.717-1.353,3.458-2.675,5.134-4.078c0.496-0.416,0.939-1.006,1.172-1.607c0.861-2.235,1.623-4.509,2.424-6.769
\t\tc0.582-1.644,1.166-3.287,1.768-4.986C429.73,438.461,429.189,438.686,428.648,438.911z M419.698,448.508
\t\tc0.105,0.021,0.211,0.044,0.316,0.065c0.067-0.16,0.162-0.315,0.197-0.483c0.452-2.112,0.935-4.22,1.33-6.343
\t\tc0.451-2.431,0.92-4.866,1.186-7.32c0.148-1.359,0.104-2.829-0.98-3.932c-0.327-0.333-0.783-0.538-1.18-0.802
\t\tc-0.184,0.444-0.509,0.882-0.529,1.333c-0.163,3.74-0.275,7.482-0.385,11.225c-0.008,0.296,0.014,0.682,0.188,0.878
\t\tc0.807,0.911,0.462,1.865,0.244,2.853C419.901,446.812,419.823,447.665,419.698,448.508z M425.518,452.379
\t\tc-0.111-0.068-0.223-0.137-0.334-0.206c-1.271,0.993-2.496,2.055-3.829,2.958c-1.258,0.852-1.583,1.92-1.325,3.348
\t\tc0.258,1.426,0.388,2.875,0.574,4.313c0.154,0.038,0.308,0.077,0.461,0.115C423.73,459.915,423.855,455.811,425.518,452.379z
\t\t M418.386,452.11c-2.89,4.863-5.629,9.474-8.369,14.084c2.306-2.366,4.468-4.794,6.542-7.293c0.669-0.806,1.317-1.741,1.602-2.725
\t\tC418.499,455.01,419.285,453.808,418.386,452.11z M423.814,459.004c1.873-2.603,3.996-5.104,5.131-8.381
\t\tc-1.342,0.025-2.379,0.255-2.787,1.377C425.316,454.313,424.59,456.667,423.814,459.004z M420.339,466.13
\t\tc-1.883,0.774-0.943,2.468-1.259,3.793C419.5,468.658,419.919,467.395,420.339,466.13z M419.253,464.095
\t\tc0.124,0.01,0.247,0.021,0.37,0.03c0.988-1.019,0.065-2.185-0.056-3.287C419.463,461.923,419.358,463.009,419.253,464.095z"/>
\t<path fill-rule="evenodd" clip-rule="evenodd" d="M432.533,478.893c-0.021-0.411-0.043-0.823-0.072-1.413
\t\tc0.467,0.233,0.758,0.377,1.201,0.599c-0.291,0.35-0.531,0.636-0.77,0.922C432.773,478.964,432.654,478.928,432.533,478.893z"/>
\t<path fill-rule="evenodd" clip-rule="evenodd" fill="#FFFFFF" d="M428.648,438.911c0.541-0.226,1.082-0.45,1.736-0.722
\t\tc-0.602,1.699-1.186,3.343-1.768,4.986c-0.801,2.26-1.563,4.533-2.424,6.769c-0.232,0.602-0.676,1.191-1.172,1.607
\t\tc-1.676,1.403-3.417,2.726-5.134,4.078c-0.12-0.031-0.239-0.062-0.358-0.093c0.165-1.549,0.255-3.11,0.511-4.646
\t\tc0.437-2.619,2.431-4.336,3.916-6.321c1.271-1.697,2.773-3.22,4.162-4.831c0.219-0.253,0.369-0.564,0.553-0.849L428.648,438.911z"
\t\t/>
\t<path fill-rule="evenodd" clip-rule="evenodd" fill="#FFFFFF" d="M419.698,448.508c0.125-0.843,0.204-1.696,0.387-2.526
\t\tc0.218-0.987,0.563-1.941-0.244-2.853c-0.174-0.196-0.196-0.582-0.188-0.878c0.11-3.742,0.222-7.484,0.385-11.225
\t\tc0.02-0.451,0.345-0.889,0.529-1.333c0.397,0.264,0.853,0.469,1.18,0.802c1.084,1.103,1.129,2.572,0.98,3.932
\t\tc-0.266,2.454-0.734,4.89-1.186,7.32c-0.395,2.123-0.878,4.23-1.33,6.343c-0.036,0.168-0.13,0.323-0.197,0.483
\t\tC419.909,448.552,419.803,448.529,419.698,448.508z"/>
\t<path fill-rule="evenodd" clip-rule="evenodd" fill="#FFFFFF" d="M425.518,452.379c-1.662,3.432-1.787,7.536-4.452,10.528
\t\tc-0.154-0.038-0.308-0.077-0.461-0.115c-0.186-1.438-0.316-2.888-0.574-4.313c-0.258-1.428,0.066-2.496,1.325-3.348
\t\tc1.333-0.903,2.558-1.965,3.829-2.958C425.295,452.242,425.406,452.311,425.518,452.379z"/>
\t<path fill-rule="evenodd" clip-rule="evenodd" fill="#FFFFFF" d="M418.386,452.11c0.898,1.697,0.112,2.899-0.225,4.066
\t\tc-0.285,0.983-0.933,1.919-1.602,2.725c-2.075,2.499-4.236,4.927-6.542,7.293C412.757,461.584,415.497,456.974,418.386,452.11z"/>
\t<path fill-rule="evenodd" clip-rule="evenodd" fill="#FFFFFF" d="M423.814,459.004c0.775-2.337,1.502-4.691,2.344-7.004
\t\tc0.408-1.122,1.445-1.352,2.787-1.377C427.811,453.899,425.688,456.401,423.814,459.004z"/>
\t<path fill-rule="evenodd" clip-rule="evenodd" fill="#FFFFFF" d="M420.339,466.13c-0.419,1.265-0.839,2.528-1.259,3.793
\t\tC419.396,468.598,418.456,466.904,420.339,466.13z"/>
\t<path fill-rule="evenodd" clip-rule="evenodd" fill="#FFFFFF" d="M419.253,464.095c0.105-1.086,0.209-2.172,0.314-3.257
\t\tc0.122,1.103,1.044,2.269,0.056,3.287C419.5,464.115,419.377,464.104,419.253,464.095z"/>
</g>
<line fill="none" stroke="#000000" stroke-miterlimit="10" x1="365.778" y1="479" x2="476.111" y2="479"/>
<g>
\t<polygon fill="#431347" points="420.6,179.691 420.121,179.691 390.92,179.691 390.92,225.989 420.121,225.989 420.6,225.989 
\t\t449.803,225.989 449.803,179.691 \t"/>
\t<path fill="#440047" d="M388.302,190.672c2.429-0.729,1.904-0.617,3.59-0.531v1.935
\t\tC391.892,192.076,388.788,190.906,388.302,190.672z"/>
\t<rect x="417.42" y="185.498" fill="#FBECEC" width="7.109" height="9.399"/>
\t<rect x="399.613" y="185.498" fill="#FBECEC" width="7.108" height="9.399"/>
\t<path fill="#E11D74" d="M417.218,185.294v9.807h7.513v-9.807H417.218z M424.325,189.995h-3.149v-4.294h3.149V189.995z
\t\t M420.771,185.701v4.294h-3.149v-4.294H420.771z M417.622,190.401h3.149v4.294h-3.149V190.401z M421.176,194.695v-4.294h3.149
\t\tv4.294H421.176z"/>
\t<rect x="434.605" y="185.498" fill="#FBECEC" width="7.109" height="9.399"/>
\t<path fill="#E11D74" d="M434.402,185.294v9.807h7.513v-9.807H434.402z M441.51,189.995h-3.147v-4.294h3.147V189.995z
\t\t M437.958,185.701v4.294h-3.15v-4.294H437.958z M434.808,190.401h3.15v4.294h-3.15V190.401z M438.362,194.695v-4.294h3.147v4.294
\t\tH438.362z"/>
\t<path fill="#E11D74" d="M399.41,185.294v9.807h7.514v-9.807H399.41z M406.52,189.995h-3.149v-4.294h3.149V189.995z
\t\t M402.964,185.701v4.294h-3.148v-4.294H402.964z M399.815,194.695v-4.294h3.148v4.294H399.815z M403.37,194.695v-4.294h3.149v4.294
\t\tH403.37z"/>
\t<path fill="#779683" d="M411.571,223.501"/>
\t<path fill="#779683" d="M436.966,223.501"/>
\t<path fill="#779683" d="M450.752,223.501"/>
\t<line fill="#9FCEB3" x1="415.882" y1="224.917" x2="420.6" y2="224.917"/>
\t<g>
\t\t<polygon fill="#E11D74" points="451.889,227.061 451.889,224.917 420.6,224.917 389.312,224.917 389.312,227.061 383.458,227.061 
\t\t\t383.458,229.205 420.6,229.205 457.741,229.205 457.741,227.061 \t\t"/>
\t\t<g>
\t\t\t<rect x="383.458" y="228.624" fill="#E6E8E8" width="37.142" height="0.581"/>
\t\t\t<rect x="420.6" y="226.48" fill="#E6E8E8" width="31.289" height="0.581"/>
\t\t\t<rect x="420.6" y="228.624" fill="#E6E8E8" width="37.142" height="0.581"/>
\t\t\t<rect x="389.313" y="226.48" fill="#E6E8E8" width="31.287" height="0.581"/>
\t\t</g>
\t</g>
\t<g>
\t\t<g opacity="0.1">
\t\t\t<polygon points="420.441,154.73 379.783,177.342 384.244,177.342 420.441,158.025 456.64,177.342 461.103,177.342 \t\t\t"/>
\t\t</g>
\t\t<g>
\t\t\t<polygon fill="#E11D74" points="420.441,151.788 379.783,174.399 384.244,174.399 420.441,155.082 456.64,174.399 
\t\t\t\t461.103,174.399 \t\t\t"/>
\t\t</g>
\t</g>
\t<polygon fill="#440047" points="420.441,155.082 385.576,173.688 420.441,173.688 455.306,173.688 \t"/>
\t<path fill="#779683" d="M397.783,223.501"/>
\t<g>
\t\t<g opacity="0.1">
\t\t\t<rect x="388.94" y="181.16" width="6.252" height="42.183"/>
\t\t</g>
\t\t<g>
\t\t\t<rect x="390.94" y="180.16" fill="#FBECEC" width="6.252" height="42.183"/>
\t\t</g>
\t</g>
\t<g>
\t\t<rect x="391.414" y="182.511" fill="#EAD6D6" width="0.162" height="39.66"/>
\t</g>
\t<g>
\t\t<rect x="391.931" y="182.511" fill="#EAD6D6" width="0.161" height="39.66"/>
\t</g>
\t<g>
\t\t<rect x="392.446" y="182.511" fill="#EAD6D6" width="0.162" height="39.66"/>
\t</g>
\t<g>
\t\t<rect x="392.963" y="182.511" fill="#EAD6D6" width="0.161" height="39.66"/>
\t</g>
\t<g>
\t\t<rect x="393.479" y="182.511" fill="#EAD6D6" width="0.162" height="39.66"/>
\t</g>
\t<g>
\t\t<rect x="393.994" y="182.511" fill="#EAD6D6" width="0.162" height="39.66"/>
\t</g>
\t<g>
\t\t<rect x="394.512" y="182.511" fill="#EAD6D6" width="0.16" height="39.66"/>
\t</g>
\t<g>
\t\t<rect x="395.027" y="182.511" fill="#EAD6D6" width="0.161" height="39.66"/>
\t</g>
\t<g>
\t\t<rect x="395.544" y="182.511" fill="#EAD6D6" width="0.16" height="39.66"/>
\t</g>
\t<g>
\t\t<rect x="396.06" y="182.511" fill="#EAD6D6" width="0.162" height="39.66"/>
\t</g>
\t<g>
\t\t<rect x="396.575" y="182.511" fill="#EAD6D6" width="0.162" height="39.66"/>
\t</g>
\t<path fill="#E11D74" d="M397.518,222.171h-3.369h-3.37c-0.772,0-1.399,0.692-1.399,1.544v1.202h4.77h4.769v-1.202
\t\tC398.917,222.863,398.291,222.171,397.518,222.171z"/>
\t<g>
\t\t<g opacity="0.1">
\t\t\t<path d="M389.379,181.691v1.201c0,0.852,0.627,1.545,1.399,1.545h3.37h3.369c0.773,0,1.399-0.693,1.399-1.545v-1.201h-4.769
\t\t\t\tH389.379z"/>
\t\t</g>
\t\t<g>
\t\t\t<path fill="#E11D74" d="M389.379,179.691v1.201c0,0.852,0.627,1.545,1.399,1.545h3.37h3.369c0.773,0,1.399-0.693,1.399-1.545
\t\t\t\tv-1.201h-4.769H389.379z"/>
\t\t</g>
\t</g>
\t<g>
\t\t<g opacity="0.05">
\t\t\t<path d="M397.518,219.503c0,0.369-0.299,0.668-0.669,0.668h-5.653c-0.369,0-0.668-0.299-0.668-0.668l0,0
\t\t\t\tc0-0.369,0.299-0.668,0.668-0.668h5.653C397.219,218.834,397.518,219.134,397.518,219.503L397.518,219.503z"/>
\t\t</g>
\t\t<g>
\t\t\t<path fill="#E11D74" d="M397.518,221.503c0,0.369-0.299,0.668-0.669,0.668h-5.653c-0.369,0-0.668-0.299-0.668-0.668l0,0
\t\t\t\tc0-0.369,0.299-0.668,0.668-0.668h5.653C397.219,220.834,397.518,221.134,397.518,221.503L397.518,221.503z"/>
\t\t</g>
\t</g>
\t<g>
\t\t<g opacity="0.1">
\t\t\t<rect x="402.708" y="180.989" width="6.252" height="42.182"/>
\t\t</g>
\t\t<g>
\t\t\t<rect x="404.708" y="179.989" fill="#FBECEC" width="6.252" height="42.182"/>
\t\t</g>
\t</g>
\t<g>
\t\t<rect x="405.201" y="182.511" fill="#EAD6D6" width="0.162" height="39.66"/>
\t</g>
\t<g>
\t\t<rect x="405.718" y="182.511" fill="#EAD6D6" width="0.161" height="39.66"/>
\t</g>
\t<g>
\t\t<rect x="406.234" y="182.511" fill="#EAD6D6" width="0.161" height="39.66"/>
\t</g>
\t<g>
\t\t<rect x="406.751" y="182.511" fill="#EAD6D6" width="0.16" height="39.66"/>
\t</g>
\t<g>
\t\t<rect x="407.267" y="182.511" fill="#EAD6D6" width="0.161" height="39.66"/>
\t</g>
\t<g>
\t\t<rect x="407.783" y="182.511" fill="#EAD6D6" width="0.16" height="39.66"/>
\t</g>
\t<g>
\t\t<rect x="408.299" y="182.511" fill="#EAD6D6" width="0.162" height="39.66"/>
\t</g>
\t<g>
\t\t<rect x="408.814" y="182.511" fill="#EAD6D6" width="0.162" height="39.66"/>
\t</g>
\t<g>
\t\t<rect x="409.331" y="182.511" fill="#EAD6D6" width="0.161" height="39.66"/>
\t</g>
\t<g>
\t\t<rect x="409.847" y="182.511" fill="#EAD6D6" width="0.162" height="39.66"/>
\t</g>
\t<g>
\t\t<rect x="410.363" y="182.511" fill="#EAD6D6" width="0.161" height="39.66"/>
\t</g>
\t<path fill="#E11D74" d="M411.306,222.171h-3.37h-3.368c-0.774,0-1.4,0.692-1.4,1.544v1.202h4.769h4.769v-1.202
\t\tC412.704,222.863,412.078,222.171,411.306,222.171z"/>
\t<g>
\t\t<g opacity="0.1">
\t\t\t<path d="M403.168,181.691v1.201c0,0.852,0.625,1.545,1.399,1.545h3.368h3.37c0.772,0,1.398-0.693,1.398-1.545v-1.201h-4.769
\t\t\t\tH403.168z"/>
\t\t</g>
\t\t<g>
\t\t\t<path fill="#E11D74" d="M403.168,179.691v1.201c0,0.852,0.625,1.545,1.399,1.545h3.368h3.37c0.772,0,1.398-0.693,1.398-1.545
\t\t\t\tv-1.201h-4.769H403.168z"/>
\t\t</g>
\t</g>
\t<g>
\t\t<g opacity="0.05">
\t\t\t<path d="M411.431,219.503c0,0.369-0.299,0.668-0.668,0.668h-5.653c-0.369,0-0.669-0.299-0.669-0.668l0,0
\t\t\t\tc0-0.369,0.3-0.668,0.669-0.668h5.653C411.132,218.834,411.431,219.134,411.431,219.503L411.431,219.503z"/>
\t\t</g>
\t\t<g>
\t\t\t<path fill="#E11D74" d="M411.431,221.503c0,0.369-0.299,0.668-0.668,0.668h-5.653c-0.369,0-0.669-0.299-0.669-0.668l0,0
\t\t\t\tc0-0.369,0.3-0.668,0.669-0.668h5.653C411.132,220.834,411.431,221.134,411.431,221.503L411.431,221.503z"/>
\t\t</g>
\t</g>
\t<g>
\t\t<g opacity="0.1">
\t\t\t<rect x="428.103" y="180.989" width="6.253" height="42.182"/>
\t\t</g>
\t\t<g>
\t\t\t<rect x="430.103" y="179.989" fill="#FBECEC" width="6.253" height="42.182"/>
\t\t</g>
\t</g>
\t<g>
\t\t<rect x="430.597" y="182.511" fill="#EAD6D6" width="0.163" height="39.66"/>
\t</g>
\t<g>
\t\t<rect x="431.113" y="182.511" fill="#EAD6D6" width="0.163" height="39.66"/>
\t</g>
\t<g>
\t\t<rect x="431.63" y="182.511" fill="#EAD6D6" width="0.161" height="39.66"/>
\t</g>
\t<g>
\t\t<rect x="432.146" y="182.511" fill="#EAD6D6" width="0.161" height="39.66"/>
\t</g>
\t<g>
\t\t<rect x="432.661" y="182.511" fill="#EAD6D6" width="0.161" height="39.66"/>
\t</g>
\t<g>
\t\t<rect x="433.178" y="182.511" fill="#EAD6D6" width="0.161" height="39.66"/>
\t</g>
\t<g>
\t\t<rect x="433.692" y="182.511" fill="#EAD6D6" width="0.163" height="39.66"/>
\t</g>
\t<g>
\t\t<rect x="434.21" y="182.511" fill="#EAD6D6" width="0.162" height="39.66"/>
\t</g>
\t<g>
\t\t<rect x="434.727" y="182.511" fill="#EAD6D6" width="0.163" height="39.66"/>
\t</g>
\t<g>
\t\t<rect x="435.243" y="182.511" fill="#EAD6D6" width="0.161" height="39.66"/>
\t</g>
\t<g>
\t\t<rect x="435.76" y="182.511" fill="#EAD6D6" width="0.158" height="39.66"/>
\t</g>
\t<path fill="#E11D74" d="M436.7,222.171h-3.368h-3.369c-0.772,0-1.401,0.692-1.401,1.544v1.202h4.771h4.77v-1.202
\t\tC438.102,222.863,437.474,222.171,436.7,222.171z"/>
\t<g>
\t\t<g opacity="0.1">
\t\t\t<path d="M428.562,181.691v1.201c0,0.852,0.629,1.545,1.401,1.545h3.369h3.368c0.773,0,1.401-0.693,1.401-1.545v-1.201h-4.77
\t\t\t\tH428.562z"/>
\t\t</g>
\t\t<g>
\t\t\t<path fill="#E11D74" d="M428.562,179.691v1.201c0,0.852,0.629,1.545,1.401,1.545h3.369h3.368c0.773,0,1.401-0.693,1.401-1.545
\t\t\t\tv-1.201h-4.77H428.562z"/>
\t\t</g>
\t</g>
\t<g>
\t\t<g opacity="0.05">
\t\t\t<path d="M436.724,219.503c0,0.369-0.298,0.668-0.666,0.668h-5.656c-0.368,0-0.666-0.299-0.666-0.668l0,0
\t\t\t\tc0-0.369,0.298-0.668,0.666-0.668h5.656C436.426,218.834,436.724,219.134,436.724,219.503L436.724,219.503z"/>
\t\t</g>
\t\t<g>
\t\t\t<path fill="#E11D74" d="M436.724,221.503c0,0.369-0.298,0.668-0.666,0.668h-5.656c-0.368,0-0.666-0.299-0.666-0.668l0,0
\t\t\t\tc0-0.369,0.298-0.668,0.666-0.668h5.656C436.426,220.834,436.724,221.134,436.724,221.503L436.724,221.503z"/>
\t\t</g>
\t</g>
\t<g>
\t\t<g opacity="0.1">
\t\t\t<rect x="441.892" y="180.989" width="6.251" height="42.182"/>
\t\t</g>
\t\t<g>
\t\t\t<rect x="443.892" y="179.989" fill="#FBECEC" width="6.251" height="42.182"/>
\t\t</g>
\t</g>
\t<g>
\t\t<rect x="444.385" y="182.511" fill="#EAD6D6" width="0.161" height="39.66"/>
\t</g>
\t<g>
\t\t<rect x="444.899" y="182.511" fill="#EAD6D6" width="0.163" height="39.66"/>
\t</g>
\t<g>
\t\t<rect x="445.416" y="182.511" fill="#EAD6D6" width="0.163" height="39.66"/>
\t</g>
\t<g>
\t\t<rect x="445.934" y="182.511" fill="#EAD6D6" width="0.162" height="39.66"/>
\t</g>
\t<g>
\t\t<rect x="446.45" y="182.511" fill="#EAD6D6" width="0.16" height="39.66"/>
\t</g>
\t<g>
\t\t<rect x="446.967" y="182.511" fill="#EAD6D6" width="0.161" height="39.66"/>
\t</g>
\t<g>
\t\t<rect x="447.481" y="182.511" fill="#EAD6D6" width="0.16" height="39.66"/>
\t</g>
\t<g>
\t\t<rect x="447.998" y="182.511" fill="#EAD6D6" width="0.161" height="39.66"/>
\t</g>
\t<g>
\t\t<rect x="448.513" y="182.511" fill="#EAD6D6" width="0.163" height="39.66"/>
\t</g>
\t<g>
\t\t<rect x="449.029" y="182.511" fill="#EAD6D6" width="0.163" height="39.66"/>
\t</g>
\t<g>
\t\t<rect x="449.546" y="182.511" fill="#EAD6D6" width="0.163" height="39.66"/>
\t</g>
\t<path fill="#E11D74" d="M450.486,222.171h-3.366h-3.37c-0.773,0-1.399,0.692-1.399,1.544v1.202h4.77h4.769v-1.202
\t\tC451.889,222.863,451.262,222.171,450.486,222.171z"/>
\t<g>
\t\t<g opacity="0.1">
\t\t\t<path d="M447.12,181.691h-4.77v1.201c0,0.852,0.626,1.545,1.399,1.545h3.37h3.366c0.775,0,1.402-0.693,1.402-1.545v-1.201H447.12
\t\t\t\tz"/>
\t\t</g>
\t\t<g>
\t\t\t<path fill="#E11D74" d="M447.12,179.691h-4.77v1.201c0,0.852,0.626,1.545,1.399,1.545h3.37h3.366
\t\t\t\tc0.775,0,1.402-0.693,1.402-1.545v-1.201H447.12z"/>
\t\t</g>
\t</g>
\t<g>
\t\t<g opacity="0.05">
\t\t\t<path d="M450.513,219.503c0,0.369-0.301,0.668-0.669,0.668h-5.652c-0.372,0-0.67-0.299-0.67-0.668l0,0
\t\t\t\tc0-0.369,0.298-0.668,0.67-0.668h5.652C450.212,218.834,450.513,219.134,450.513,219.503L450.513,219.503z"/>
\t\t</g>
\t\t<g>
\t\t\t<path fill="#E11D74" d="M450.513,221.503c0,0.369-0.301,0.668-0.669,0.668h-5.652c-0.372,0-0.67-0.299-0.67-0.668l0,0
\t\t\t\tc0-0.369,0.298-0.668,0.67-0.668h5.652C450.212,220.834,450.513,221.134,450.513,221.503L450.513,221.503z"/>
\t\t</g>
\t</g>
\t<g>
\t\t<rect x="420.288" y="143.748" fill="#E6E8E8" width="0.322" height="8.217"/>
\t</g>
\t<path fill="#E11D74" d="M420.59,148.008c0,0,0.577,0.328,1.649-0.226c0.445-0.23,0.962-0.466,1.437-0.523
\t\tc0.549-0.065,1.254-0.076,2.172,0.671c0.772,0.628,1.962,0.078,1.962,0.078v-0.778v-3.35c0,0-1.045,0.633-1.962-0.079
\t\tc-0.934-0.727-1.623-0.737-2.172-0.67c-0.475,0.057-0.991,0.293-1.437,0.523c-1.072,0.553-1.649,0.226-1.649,0.226v3.157"/>
\t<circle fill="#E6E8E8" cx="420.448" cy="143.466" r="0.363"/>
\t<g>
\t\t<g>
\t\t\t<g opacity="0.15">
\t\t\t\t<polygon points="423.459,175.264 423.459,175.264 388.097,175.102 388.097,177.931 423.089,177.931 423.089,178.094 
\t\t\t\t\t457.954,178.094 457.954,175.264 \t\t\t\t"/>
\t\t\t</g>
\t\t\t<g>
\t\t\t\t<polygon fill="#FBECEC" points="421.097,173.688 421.097,173.688 385.734,173.526 385.734,176.356 420.727,176.356 
\t\t\t\t\t420.727,176.519 455.592,176.519 455.592,173.688 \t\t\t\t"/>
\t\t\t</g>
\t\t</g>
\t\t<g>
\t\t\t<g>
\t\t\t\t<rect x="386.193" y="173.688" fill="#D8D8D8" width="0.313" height="2.667"/>
\t\t\t</g>
\t\t\t<g>
\t\t\t\t<rect x="387.108" y="173.688" fill="#D8D8D8" width="0.313" height="2.667"/>
\t\t\t</g>
\t\t\t<g>
\t\t\t\t<rect x="388.022" y="173.688" fill="#D8D8D8" width="0.313" height="2.667"/>
\t\t\t</g>
\t\t\t<g>
\t\t\t\t<rect x="388.937" y="173.688" fill="#D8D8D8" width="0.313" height="2.667"/>
\t\t\t</g>
\t\t\t<g>
\t\t\t\t<rect x="389.851" y="173.688" fill="#D8D8D8" width="0.314" height="2.667"/>
\t\t\t</g>
\t\t\t<g>
\t\t\t\t<rect x="390.766" y="173.688" fill="#D8D8D8" width="0.313" height="2.667"/>
\t\t\t</g>
\t\t\t<g>
\t\t\t\t<rect x="391.68" y="173.688" fill="#D8D8D8" width="0.313" height="2.667"/>
\t\t\t</g>
\t\t\t<g>
\t\t\t\t<rect x="392.594" y="173.688" fill="#D8D8D8" width="0.313" height="2.667"/>
\t\t\t</g>
\t\t\t<g>
\t\t\t\t<rect x="393.509" y="173.688" fill="#D8D8D8" width="0.313" height="2.667"/>
\t\t\t</g>
\t\t\t<g>
\t\t\t\t<rect x="394.423" y="173.688" fill="#D8D8D8" width="0.313" height="2.667"/>
\t\t\t</g>
\t\t\t<g>
\t\t\t\t<rect x="395.338" y="173.688" fill="#D8D8D8" width="0.313" height="2.667"/>
\t\t\t</g>
\t\t\t<g>
\t\t\t\t<rect x="396.252" y="173.688" fill="#D8D8D8" width="0.313" height="2.667"/>
\t\t\t</g>
\t\t\t<g>
\t\t\t\t<rect x="397.166" y="173.688" fill="#D8D8D8" width="0.313" height="2.667"/>
\t\t\t</g>
\t\t\t<g>
\t\t\t\t<rect x="398.08" y="173.688" fill="#D8D8D8" width="0.314" height="2.667"/>
\t\t\t</g>
\t\t\t<g>
\t\t\t\t<rect x="398.995" y="173.688" fill="#D8D8D8" width="0.313" height="2.667"/>
\t\t\t</g>
\t\t\t<g>
\t\t\t\t<rect x="399.91" y="173.688" fill="#D8D8D8" width="0.313" height="2.667"/>
\t\t\t</g>
\t\t\t<g>
\t\t\t\t<rect x="400.823" y="173.688" fill="#D8D8D8" width="0.313" height="2.667"/>
\t\t\t</g>
\t\t\t<g>
\t\t\t\t<rect x="401.738" y="173.688" fill="#D8D8D8" width="0.313" height="2.667"/>
\t\t\t</g>
\t\t\t<g>
\t\t\t\t<rect x="402.652" y="173.688" fill="#D8D8D8" width="0.314" height="2.667"/>
\t\t\t</g>
\t\t\t<g>
\t\t\t\t<rect x="403.567" y="173.688" fill="#D8D8D8" width="0.313" height="2.667"/>
\t\t\t</g>
\t\t\t<g>
\t\t\t\t<rect x="404.48" y="173.688" fill="#D8D8D8" width="0.314" height="2.667"/>
\t\t\t</g>
\t\t\t<g>
\t\t\t\t<rect x="405.396" y="173.688" fill="#D8D8D8" width="0.313" height="2.667"/>
\t\t\t</g>
\t\t\t<g>
\t\t\t\t<rect x="406.31" y="173.688" fill="#D8D8D8" width="0.314" height="2.667"/>
\t\t\t</g>
\t\t\t<g>
\t\t\t\t<rect x="407.225" y="173.688" fill="#D8D8D8" width="0.313" height="2.667"/>
\t\t\t</g>
\t\t\t<g>
\t\t\t\t<rect x="408.14" y="173.688" fill="#D8D8D8" width="0.313" height="2.667"/>
\t\t\t</g>
\t\t\t<g>
\t\t\t\t<rect x="409.053" y="173.688" fill="#D8D8D8" width="0.313" height="2.667"/>
\t\t\t</g>
\t\t\t<g>
\t\t\t\t<rect x="409.968" y="173.688" fill="#D8D8D8" width="0.313" height="2.667"/>
\t\t\t</g>
\t\t\t<g>
\t\t\t\t<rect x="410.882" y="173.688" fill="#D8D8D8" width="0.314" height="2.667"/>
\t\t\t</g>
\t\t\t<g>
\t\t\t\t<rect x="411.797" y="173.688" fill="#D8D8D8" width="0.313" height="2.667"/>
\t\t\t</g>
\t\t\t<g>
\t\t\t\t<rect x="412.71" y="173.688" fill="#D8D8D8" width="0.314" height="2.667"/>
\t\t\t</g>
\t\t\t<g>
\t\t\t\t<rect x="413.625" y="173.688" fill="#D8D8D8" width="0.313" height="2.667"/>
\t\t\t</g>
\t\t\t<g>
\t\t\t\t<rect x="414.54" y="173.688" fill="#D8D8D8" width="0.313" height="2.667"/>
\t\t\t</g>
\t\t\t<g>
\t\t\t\t<rect x="415.454" y="173.688" fill="#D8D8D8" width="0.313" height="2.667"/>
\t\t\t</g>
\t\t\t<g>
\t\t\t\t<rect x="416.369" y="173.688" fill="#D8D8D8" width="0.313" height="2.667"/>
\t\t\t</g>
\t\t\t<g>
\t\t\t\t<rect x="417.282" y="173.688" fill="#D8D8D8" width="0.313" height="2.667"/>
\t\t\t</g>
\t\t\t<g>
\t\t\t\t<rect x="418.197" y="173.688" fill="#D8D8D8" width="0.313" height="2.667"/>
\t\t\t</g>
\t\t\t<g>
\t\t\t\t<rect x="419.111" y="173.688" fill="#D8D8D8" width="0.314" height="2.667"/>
\t\t\t</g>
\t\t\t<g>
\t\t\t\t<rect x="420.026" y="173.688" fill="#D8D8D8" width="0.313" height="2.667"/>
\t\t\t</g>
\t\t\t<g>
\t\t\t\t<rect x="420.941" y="173.688" fill="#D8D8D8" width="0.313" height="2.667"/>
\t\t\t</g>
\t\t\t<g>
\t\t\t\t<rect x="421.854" y="173.688" fill="#D8D8D8" width="0.313" height="2.667"/>
\t\t\t</g>
\t\t\t<g>
\t\t\t\t<rect x="422.77" y="173.688" fill="#D8D8D8" width="0.313" height="2.667"/>
\t\t\t</g>
\t\t\t<g>
\t\t\t\t<rect x="423.684" y="173.688" fill="#D8D8D8" width="0.313" height="2.667"/>
\t\t\t</g>
\t\t\t<g>
\t\t\t\t<rect x="424.6" y="173.688" fill="#D8D8D8" width="0.312" height="2.667"/>
\t\t\t</g>
\t\t\t<g>
\t\t\t\t<rect x="425.512" y="173.688" fill="#D8D8D8" width="0.314" height="2.667"/>
\t\t\t</g>
\t\t\t<g>
\t\t\t\t<rect x="426.427" y="173.688" fill="#D8D8D8" width="0.314" height="2.667"/>
\t\t\t</g>
\t\t\t<g>
\t\t\t\t<rect x="427.342" y="173.688" fill="#D8D8D8" width="0.313" height="2.667"/>
\t\t\t</g>
\t\t\t<g>
\t\t\t\t<rect x="428.255" y="173.688" fill="#D8D8D8" width="0.313" height="2.667"/>
\t\t\t</g>
\t\t\t<g>
\t\t\t\t<rect x="429.172" y="173.688" fill="#D8D8D8" width="0.312" height="2.667"/>
\t\t\t</g>
\t\t\t<g>
\t\t\t\t<rect x="430.084" y="173.688" fill="#D8D8D8" width="0.313" height="2.667"/>
\t\t\t</g>
\t\t\t<g>
\t\t\t\t<rect x="430.999" y="173.688" fill="#D8D8D8" width="0.314" height="2.667"/>
\t\t\t</g>
\t\t\t<g>
\t\t\t\t<rect x="431.914" y="173.688" fill="#D8D8D8" width="0.313" height="2.667"/>
\t\t\t</g>
\t\t\t<g>
\t\t\t\t<rect x="432.827" y="173.688" fill="#D8D8D8" width="0.313" height="2.667"/>
\t\t\t</g>
\t\t\t<g>
\t\t\t\t<rect x="433.741" y="173.688" fill="#D8D8D8" width="0.314" height="2.667"/>
\t\t\t</g>
\t\t\t<g>
\t\t\t\t<rect x="434.656" y="173.688" fill="#D8D8D8" width="0.313" height="2.667"/>
\t\t\t</g>
\t\t\t<g>
\t\t\t\t<rect x="435.571" y="173.688" fill="#D8D8D8" width="0.314" height="2.667"/>
\t\t\t</g>
\t\t\t<g>
\t\t\t\t<rect x="436.486" y="173.688" fill="#D8D8D8" width="0.312" height="2.667"/>
\t\t\t</g>
\t\t\t<g>
\t\t\t\t<rect x="437.398" y="173.688" fill="#D8D8D8" width="0.314" height="2.667"/>
\t\t\t</g>
\t\t\t<g>
\t\t\t\t<rect x="438.313" y="173.688" fill="#D8D8D8" width="0.314" height="2.667"/>
\t\t\t</g>
\t\t\t<g>
\t\t\t\t<rect x="439.229" y="173.688" fill="#D8D8D8" width="0.313" height="2.667"/>
\t\t\t</g>
\t\t\t<g>
\t\t\t\t<rect x="440.142" y="173.688" fill="#D8D8D8" width="0.316" height="2.667"/>
\t\t\t</g>
\t\t\t<g>
\t\t\t\t<rect x="441.059" y="173.688" fill="#D8D8D8" width="0.312" height="2.667"/>
\t\t\t</g>
\t\t\t<g>
\t\t\t\t<rect x="441.971" y="173.688" fill="#D8D8D8" width="0.314" height="2.667"/>
\t\t\t</g>
\t\t\t<g>
\t\t\t\t<rect x="442.886" y="173.688" fill="#D8D8D8" width="0.314" height="2.667"/>
\t\t\t</g>
\t\t\t<g>
\t\t\t\t<rect x="443.801" y="173.688" fill="#D8D8D8" width="0.313" height="2.667"/>
\t\t\t</g>
\t\t\t<g>
\t\t\t\t<rect x="444.713" y="173.688" fill="#D8D8D8" width="0.314" height="2.667"/>
\t\t\t</g>
\t\t\t<g>
\t\t\t\t<rect x="445.631" y="173.688" fill="#D8D8D8" width="0.312" height="2.667"/>
\t\t\t</g>
\t\t\t<g>
\t\t\t\t<rect x="446.543" y="173.688" fill="#D8D8D8" width="0.313" height="2.667"/>
\t\t\t</g>
\t\t\t<g>
\t\t\t\t<rect x="447.458" y="173.688" fill="#D8D8D8" width="0.314" height="2.667"/>
\t\t\t</g>
\t\t\t<g>
\t\t\t\t<rect x="448.373" y="173.688" fill="#D8D8D8" width="0.312" height="2.667"/>
\t\t\t</g>
\t\t\t<g>
\t\t\t\t<rect x="449.285" y="173.688" fill="#D8D8D8" width="0.314" height="2.667"/>
\t\t\t</g>
\t\t\t<g>
\t\t\t\t<rect x="450.203" y="173.688" fill="#D8D8D8" width="0.312" height="2.667"/>
\t\t\t</g>
\t\t\t<g>
\t\t\t\t<rect x="451.115" y="173.688" fill="#D8D8D8" width="0.313" height="2.667"/>
\t\t\t</g>
\t\t\t<g>
\t\t\t\t<rect x="452.03" y="173.688" fill="#D8D8D8" width="0.314" height="2.667"/>
\t\t\t</g>
\t\t\t<g>
\t\t\t\t<rect x="452.945" y="173.688" fill="#D8D8D8" width="0.312" height="2.667"/>
\t\t\t</g>
\t\t\t<g>
\t\t\t\t<rect x="453.857" y="173.688" fill="#D8D8D8" width="0.314" height="2.667"/>
\t\t\t</g>
\t\t\t<g>
\t\t\t\t<rect x="454.775" y="173.688" fill="#D8D8D8" width="0.312" height="2.667"/>
\t\t\t</g>
\t\t</g>
\t</g>
\t<g>
\t\t<g>
\t\t\t<rect x="385.729" y="176.787" fill="#431047" width="69.742" height="2.831"/>
\t\t</g>
\t\t<g>
\t\t\t<g opacity="0.1">
\t\t\t\t<path d="M458.414,178.571c0,0.119-0.098,0.215-0.215,0.215h-70.782c-0.119,0-0.215-0.096-0.215-0.215l0,0
\t\t\t\t\tc0-0.119,0.096-0.215,0.215-0.215h70.782C458.316,178.356,458.414,178.453,458.414,178.571L458.414,178.571z"/>
\t\t\t</g>
\t\t\t<g>
\t\t\t\t<path fill="#431047" d="M456.414,176.571c0,0.119-0.098,0.215-0.215,0.215h-70.782c-0.119,0-0.215-0.096-0.215-0.215l0,0
\t\t\t\t\tc0-0.119,0.096-0.215,0.215-0.215h70.782C456.316,176.356,456.414,176.453,456.414,176.571L456.414,176.571z"/>
\t\t\t</g>
\t\t</g>
\t\t<g>
\t\t\t<g opacity="0.1">
\t\t\t\t<path d="M458.414,181.618c0,0.119-0.098,0.215-0.215,0.215h-70.782c-0.119,0-0.215-0.097-0.215-0.215l0,0
\t\t\t\t\tc0-0.119,0.096-0.215,0.215-0.215h70.782C458.316,181.402,458.414,181.499,458.414,181.618L458.414,181.618z"/>
\t\t\t</g>
\t\t\t<g>
\t\t\t\t<path fill="#431047" d="M456.414,179.618c0,0.119-0.098,0.215-0.215,0.215h-70.782c-0.119,0-0.215-0.097-0.215-0.215l0,0
\t\t\t\t\tc0-0.119,0.096-0.215,0.215-0.215h70.782C456.316,179.402,456.414,179.499,456.414,179.618L456.414,179.618z"/>
\t\t\t</g>
\t\t</g>
\t</g>
\t
\t\t<rect x="416.524" y="211.474" fill="#E3E5E4" stroke="#F9F9F9" stroke-width="0.5" stroke-miterlimit="10" width="3.423" height="4.909"/>
\t
\t\t<rect x="421.215" y="211.474" fill="#E3E5E4" stroke="#F9F9F9" stroke-width="0.5" stroke-miterlimit="10" width="3.422" height="4.909"/>
\t<g>
\t\t<g>
\t\t\t<g>
\t\t\t\t<g opacity="0.15">
\t\t\t\t\t<path d="M423.102,198.749c0.054,0,0.106,0.002,0.158,0.003c0.053-0.001,0.105-0.003,0.158-0.003H423.102z"/>
\t\t\t\t\t<path fill="none" stroke="#000000" stroke-width="0.5" stroke-miterlimit="10" d="M423.102,198.749
\t\t\t\t\t\tc0.054,0,0.106,0.002,0.158,0.003c0.053-0.001,0.105-0.003,0.158-0.003H423.102z"/>
\t\t\t\t</g>
\t\t\t\t<g>
\t\t\t\t\t<path fill="#87CCA3" d="M420.63,198.749c0.054,0,0.106,0.002,0.158,0.003c0.053-0.001,0.105-0.003,0.158-0.003H420.63z"/>
\t\t\t\t\t<path fill="none" stroke="#F9F9F9" stroke-width="0.5" stroke-miterlimit="10" d="M420.63,198.749
\t\t\t\t\t\tc0.054,0,0.106,0.002,0.158,0.003c0.053-0.001,0.105-0.003,0.158-0.003H420.63z"/>
\t\t\t\t</g>
\t\t\t</g>
\t\t\t<g opacity="0.15">
\t\t\t\t<path fill="#797979" d="M421.188,198.752c-3.023,0.077-5.449,2.34-5.449,5.122v20.922h5.162h5.736v-20.922
\t\t\t\t\tC426.637,201.092,424.211,198.83,421.188,198.752z"/>
\t\t\t\t<path fill="#797979" stroke="#000000" stroke-width="0.5" stroke-miterlimit="10" d="M421.188,198.752
\t\t\t\t\tc-3.023,0.077-5.449,2.34-5.449,5.122v20.922h5.162h5.736v-20.922C426.637,201.092,424.211,198.83,421.188,198.752z"/>
\t\t\t</g>
\t\t</g>
\t\t<g id="XMLID_3_">
\t\t\t<g>
\t\t\t\t<path fill="#EAD6D6" d="M426.687,203.682v21.245h-11.543v-21.245c0-2.971,2.532-5.362,5.764-5.442l0.008-0.003l0.008,0.003
\t\t\t\t\tC424.154,198.32,426.687,200.711,426.687,203.682z M426.041,224.28v-20.598c0-0.037-0.004-0.074-0.004-0.111h-5.123h-5.123
\t\t\t\t\tc0,0.037-0.003,0.074-0.003,0.111v20.598H426.041z M421.746,202.925h4.232c-0.172-0.997-0.674-1.891-1.408-2.582
\t\t\t\t\tL421.746,202.925z M421.237,202.515l2.827-2.587c-0.783-0.571-1.758-0.94-2.827-1.028V202.515z M420.592,202.365V198.9
\t\t\t\t\tc-0.854,0.069-1.644,0.318-2.328,0.705L420.592,202.365z M420.221,202.925l-2.502-2.964c-0.979,0.728-1.661,1.771-1.865,2.964
\t\t\t\t\tH420.221z"/>
\t\t\t\t<path fill="#EAD6D6" d="M426.041,203.682v20.598h-10.252v-20.598c0-0.037,0.003-0.074,0.003-0.111h5.123h5.123
\t\t\t\t\tC426.037,203.607,426.041,203.645,426.041,203.682z"/>
\t\t\t\t<path fill="#EAD6D6" d="M425.978,202.925h-4.232l2.824-2.582C425.304,201.034,425.806,201.928,425.978,202.925z"/>
\t\t\t\t<path fill="#EAD6D6" d="M424.064,199.928l-2.827,2.587V198.9C422.307,198.988,423.281,199.357,424.064,199.928z"/>
\t\t\t\t<path fill="#EAD6D6" d="M420.592,198.9v3.464l-2.328-2.759C418.948,199.218,419.738,198.97,420.592,198.9z"/>
\t\t\t\t<path fill="#EAD6D6" d="M417.718,199.96l2.502,2.964h-4.368C416.058,201.731,416.739,200.688,417.718,199.96z"/>
\t\t\t</g>
\t\t\t<g>
\t\t\t</g>
\t\t</g>
\t\t<g>
\t\t\t<g opacity="0.15">
\t\t\t\t<path d="M421.922,199.237h-0.008h-0.008c-3.232,0.083-5.765,2.474-5.765,5.444v21.244h11.544v-21.244
\t\t\t\t\tC427.686,201.71,425.155,199.319,421.922,199.237z M422.236,199.9c1.072,0.087,2.045,0.456,2.83,1.028l-2.83,2.587V199.9z
\t\t\t\t\t M425.569,201.343c0.733,0.691,1.236,1.584,1.406,2.582h-4.23L425.569,201.343z M421.592,199.9v3.464l-2.329-2.758
\t\t\t\t\tC419.946,200.218,420.737,199.969,421.592,199.9z M418.718,200.961l2.502,2.963h-4.368
\t\t\t\t\tC417.057,202.731,417.737,201.688,418.718,200.961z M427.041,225.28h-10.253v-20.6c0-0.037,0.002-0.073,0.002-0.11h5.124l0,0
\t\t\t\t\tl0,0h5.124c0,0.037,0.003,0.073,0.003,0.11V225.28z"/>
\t\t\t</g>
\t\t\t<g>
\t\t\t\t<path fill="#F9F9F9" d="M420.922,198.237h-0.008h-0.008c-3.232,0.083-5.765,2.474-5.765,5.444v21.244h11.544v-21.244
\t\t\t\t\tC426.686,200.71,424.155,198.319,420.922,198.237z M421.236,198.9c1.072,0.087,2.045,0.456,2.83,1.028l-2.83,2.587V198.9z
\t\t\t\t\t M424.569,200.343c0.733,0.691,1.236,1.584,1.406,2.582h-4.23L424.569,200.343z M420.592,198.9v3.464l-2.329-2.758
\t\t\t\t\tC418.946,199.218,419.737,198.969,420.592,198.9z M417.718,199.961l2.502,2.963h-4.368
\t\t\t\t\tC416.057,201.731,416.737,200.688,417.718,199.961z M426.041,224.28h-10.253v-20.6c0-0.037,0.002-0.073,0.002-0.11h5.124l0,0
\t\t\t\t\tl0,0h5.124c0,0.037,0.003,0.073,0.003,0.11V224.28z"/>
\t\t\t</g>
\t\t</g>
\t\t<g>
\t\t\t<g>
\t\t\t\t<g opacity="0.05">
\t\t\t\t\t<rect x="417.839" y="205.627" width="3.422" height="4.907"/>
\t\t\t\t</g>
\t\t\t\t<g>
\t\t\t\t\t<rect x="416.839" y="204.627" fill="#FBECEC" width="3.422" height="4.907"/>
\t\t\t\t</g>
\t\t\t</g>
\t\t\t<g>
\t\t\t\t<g opacity="0.05">
\t\t\t\t\t<rect x="422.528" y="205.627" width="3.423" height="4.907"/>
\t\t\t\t</g>
\t\t\t\t<g>
\t\t\t\t\t<rect x="421.528" y="204.627" fill="#FBECEC" width="3.423" height="4.907"/>
\t\t\t\t</g>
\t\t\t</g>
\t\t\t<g>
\t\t\t\t<g opacity="0.05">
\t\t\t\t\t<rect x="417.839" y="218.694" width="3.422" height="4.907"/>
\t\t\t\t</g>
\t\t\t\t<g>
\t\t\t\t\t<rect x="416.839" y="217.694" fill="#FBECEC" width="3.422" height="4.907"/>
\t\t\t\t</g>
\t\t\t</g>
\t\t\t<g>
\t\t\t\t<g opacity="0.05">
\t\t\t\t\t<rect x="422.528" y="218.694" width="3.423" height="4.907"/>
\t\t\t\t</g>
\t\t\t\t<g>
\t\t\t\t\t<rect x="421.528" y="217.694" fill="#FBECEC" width="3.423" height="4.907"/>
\t\t\t\t</g>
\t\t\t</g>
\t\t\t<g>
\t\t\t\t<g opacity="0.05">
\t\t\t\t\t<rect x="417.839" y="212.314" width="3.422" height="4.907"/>
\t\t\t\t</g>
\t\t\t\t<g>
\t\t\t\t\t<rect x="416.839" y="211.314" fill="#FBECEC" width="3.422" height="4.907"/>
\t\t\t\t</g>
\t\t\t</g>
\t\t\t<g>
\t\t\t\t<g opacity="0.05">
\t\t\t\t\t<rect x="422.528" y="212.314" width="3.423" height="4.907"/>
\t\t\t\t</g>
\t\t\t\t<g>
\t\t\t\t\t<rect x="421.528" y="211.314" fill="#FBECEC" width="3.423" height="4.907"/>
\t\t\t\t</g>
\t\t\t</g>
\t\t</g>
\t</g>
\t<g opacity="0.5">
\t\t<polygon fill="#8D9899" points="384.244,174.399 393.236,169.463 402.265,164.595 420.324,154.862 420.441,154.799 
\t\t\t420.559,154.862 438.619,164.594 447.647,169.462 456.64,174.399 447.532,169.678 438.462,164.887 420.324,155.302 
\t\t\t420.559,155.302 402.421,164.887 393.351,169.677 \t\t"/>
\t</g>
\t<path fill="#440047" d="M452.859,190.776c0,0-1.662-0.378-3.59-0.53v1.934C449.27,192.18,452.372,191.01,452.859,190.776z"/>
\t<g>
\t\t<g>
\t\t\t<g opacity="0.15">
\t\t\t\t<path d="M448.142,211.611c8.521-0.365,16.169-3.458,21.957-8.204c-3.743,0.108-7.25-0.45-10.379-1.491
\t\t\t\t\tc0.257-2.386,0.384-4.77,0.382-7.142c-4.352,2.797-10.141,4.36-16.511,3.616C444.183,202.798,445.695,207.309,448.142,211.611z"
\t\t\t\t\t/>
\t\t\t</g>
\t\t\t<g>
\t\t\t\t<path fill="#E0B11E" d="M447.142,208.611c8.521-0.365,16.169-3.458,21.957-8.204c-3.743,0.108-7.25-0.45-10.379-1.491
\t\t\t\t\tc0.257-2.386,0.384-4.77,0.382-7.142c-4.352,2.797-10.141,4.36-16.511,3.616C443.183,199.798,444.695,204.309,447.142,208.611z"
\t\t\t\t\t/>
\t\t\t</g>
\t\t</g>
\t\t<g>
\t\t\t<g opacity="0.15">
\t\t\t\t<path d="M396.385,211.399c-8.79-0.308-16.607-3.564-22.368-8.504c3.314,0.119,6.442-0.299,9.28-1.123
\t\t\t\t\tc-0.166-2.39-0.207-4.775-0.125-7.145c4.334,3.097,10.315,4.94,16.995,4.263C399.698,203.042,398.443,207.292,396.385,211.399z"
\t\t\t\t\t/>
\t\t\t</g>
\t\t\t<g>
\t\t\t\t<path fill="#E0B11E" d="M395.385,208.399c-8.79-0.308-16.607-3.564-22.368-8.504c3.314,0.119,6.442-0.299,9.28-1.123
\t\t\t\t\tc-0.166-2.39-0.207-4.775-0.125-7.145c4.334,3.097,10.315,4.94,16.995,4.263C398.698,200.042,397.443,204.292,395.385,208.399z"
\t\t\t\t\t/>
\t\t\t</g>
\t\t</g>
\t\t<path fill="#E0B11E" stroke="#440047" stroke-width="0.5" stroke-miterlimit="10" d="M453.178,203.934
\t\t\tc-20.365,8.567-44.446,8.829-64.876-0.317c0-3.688,0-9.234,0-12.922c21.294,8.844,42.97,9.048,64.558,0.204
\t\t\tC452.961,195.707,453.178,200.246,453.178,203.934z"/>
\t\t<g>
\t\t\t<path fill="none" d="M387.969,198.024c0,0,30.305,15.951,65.246,0.43"/>
\t\t\t<path fill="#431347" d="M394.376,197.042l-0.035,0.09c-0.098-0.034-0.175-0.051-0.232-0.05c-0.057,0-0.104,0.024-0.139,0.071
\t\t\t\ts-0.074,0.126-0.117,0.238l-0.861,2.241c-0.042,0.108-0.064,0.193-0.069,0.253s0.014,0.109,0.056,0.146
\t\t\t\tc0.041,0.037,0.109,0.077,0.203,0.12l-0.035,0.09c-0.079-0.037-0.18-0.08-0.303-0.129c-0.123-0.049-0.248-0.098-0.374-0.146
\t\t\t\tc-0.148-0.057-0.285-0.108-0.41-0.155c-0.125-0.047-0.229-0.082-0.309-0.106l0.035-0.09c0.099,0.031,0.175,0.046,0.23,0.047
\t\t\t\ts0.102-0.023,0.139-0.071c0.037-0.048,0.077-0.126,0.119-0.235l0.861-2.241c0.042-0.111,0.066-0.197,0.069-0.256
\t\t\t\tc0.004-0.059-0.015-0.108-0.058-0.146c-0.042-0.039-0.109-0.078-0.202-0.116l0.035-0.09c0.077,0.033,0.178,0.075,0.302,0.126
\t\t\t\tc0.124,0.051,0.26,0.105,0.407,0.162c0.126,0.049,0.252,0.095,0.377,0.14C394.19,196.977,394.293,197.013,394.376,197.042z"/>
\t\t\t<path fill="#431347" d="M396.054,198.541c0.128,0.044,0.227,0.096,0.297,0.156c0.07,0.06,0.119,0.125,0.146,0.196
\t\t\t\tc0.029,0.075,0.039,0.162,0.03,0.261c-0.009,0.099-0.04,0.227-0.093,0.382l-0.424,1.231c-0.044,0.128-0.052,0.223-0.023,0.284
\t\t\t\tc0.028,0.061,0.096,0.11,0.203,0.146l-0.033,0.096c-0.057-0.023-0.143-0.057-0.257-0.101c-0.114-0.044-0.228-0.086-0.341-0.125
\t\t\t\tc-0.116-0.04-0.23-0.077-0.343-0.111s-0.199-0.059-0.258-0.076l0.033-0.096c0.091,0.031,0.163,0.031,0.216-0.002
\t\t\t\tc0.052-0.033,0.101-0.114,0.145-0.242l0.49-1.423c0.026-0.076,0.042-0.146,0.046-0.207c0.004-0.062-0.008-0.115-0.037-0.161
\t\t\t\tc-0.029-0.046-0.083-0.082-0.162-0.109c-0.128-0.044-0.253-0.031-0.375,0.04s-0.209,0.182-0.261,0.335l-0.438,1.272
\t\t\t\tc-0.044,0.128-0.055,0.222-0.033,0.28c0.022,0.059,0.078,0.104,0.167,0.134l-0.033,0.096c-0.054-0.022-0.133-0.054-0.236-0.094
\t\t\t\tc-0.104-0.041-0.212-0.081-0.325-0.12c-0.116-0.04-0.235-0.079-0.359-0.116c-0.124-0.038-0.216-0.065-0.278-0.083l0.033-0.096
\t\t\t\tc0.106,0.037,0.19,0.04,0.25,0.009c0.06-0.031,0.112-0.11,0.156-0.238l0.496-1.442c0.047-0.137,0.062-0.246,0.043-0.326
\t\t\t\tc-0.019-0.08-0.084-0.139-0.197-0.178l0.033-0.096c0.094,0.043,0.188,0.08,0.279,0.111c0.128,0.044,0.25,0.08,0.365,0.108
\t\t\t\tc0.115,0.027,0.223,0.047,0.325,0.058l-0.142,0.412c0.13-0.129,0.271-0.204,0.425-0.223
\t\t\t\tC395.735,198.467,395.893,198.485,396.054,198.541z"/>
\t\t\t<path fill="#431347" d="M398.118,199.203c0.129,0.04,0.238,0.09,0.325,0.152c0.087,0.062,0.148,0.115,0.182,0.159
\t\t\t\tc0.083,0.103,0.162,0.077,0.237-0.08l0.097,0.03c-0.033,0.084-0.07,0.189-0.111,0.313s-0.093,0.29-0.156,0.496l-0.097-0.03
\t\t\t\tc0.019-0.119,0.024-0.24,0.015-0.365c-0.01-0.124-0.044-0.234-0.104-0.33c-0.06-0.096-0.155-0.164-0.285-0.204
\t\t\t\tc-0.089-0.027-0.173-0.025-0.25,0.007c-0.078,0.032-0.13,0.096-0.16,0.192c-0.028,0.092-0.025,0.182,0.009,0.269
\t\t\t\tc0.034,0.086,0.086,0.174,0.154,0.265c0.069,0.09,0.14,0.185,0.214,0.285c0.119,0.162,0.215,0.316,0.286,0.464
\t\t\t\tc0.071,0.148,0.079,0.315,0.022,0.5c-0.042,0.139-0.12,0.248-0.231,0.327c-0.112,0.079-0.242,0.126-0.39,0.142
\t\t\t\tc-0.148,0.016-0.302-0.001-0.463-0.05c-0.092-0.028-0.172-0.063-0.239-0.103c-0.067-0.041-0.122-0.083-0.166-0.127
\t\t\t\tc-0.035-0.031-0.067-0.06-0.097-0.088c-0.03-0.027-0.059-0.052-0.086-0.074c-0.028-0.019-0.058-0.016-0.091,0.008
\t\t\t\ts-0.063,0.062-0.093,0.113l-0.097-0.03c0.036-0.097,0.078-0.216,0.124-0.357c0.046-0.141,0.105-0.329,0.177-0.563l0.097,0.029
\t\t\t\tc-0.032,0.172-0.043,0.33-0.033,0.473c0.01,0.144,0.047,0.264,0.109,0.363c0.063,0.098,0.159,0.167,0.289,0.207
\t\t\t\tc0.081,0.025,0.162,0.022,0.246-0.008s0.143-0.104,0.179-0.221c0.042-0.136,0.032-0.262-0.029-0.378
\t\t\t\tc-0.061-0.116-0.143-0.248-0.247-0.394c-0.074-0.11-0.144-0.218-0.208-0.324c-0.064-0.106-0.109-0.214-0.136-0.325
\t\t\t\tc-0.027-0.111-0.021-0.227,0.015-0.347c0.042-0.139,0.115-0.245,0.218-0.318c0.104-0.073,0.223-0.115,0.36-0.125
\t\t\t\tC397.841,199.145,397.979,199.16,398.118,199.203z"/>
\t\t\t<path fill="#431347" d="M400.518,199.142l-0.211,0.765l0.55,0.152l-0.026,0.093l-0.55-0.152l-0.522,1.885
\t\t\t\tc-0.026,0.093-0.027,0.164-0.004,0.212s0.07,0.082,0.138,0.101c0.056,0.016,0.118,0.01,0.185-0.017
\t\t\t\tc0.068-0.026,0.138-0.085,0.209-0.175l0.068,0.064c-0.089,0.126-0.196,0.22-0.322,0.282c-0.126,0.063-0.276,0.069-0.451,0.021
\t\t\t\tc-0.103-0.028-0.189-0.066-0.259-0.114c-0.071-0.048-0.125-0.106-0.163-0.173c-0.05-0.087-0.071-0.185-0.065-0.292
\t\t\t\tc0.006-0.107,0.032-0.241,0.077-0.403l0.437-1.577l-0.392-0.108l0.026-0.093l0.392,0.108l0.17-0.616
\t\t\t\tc0.131,0.036,0.254,0.058,0.371,0.065C400.293,199.177,400.407,199.168,400.518,199.142z"/>
\t\t\t<path fill="#431347" d="M402.023,200.302l-0.52,2.041c-0.033,0.131-0.033,0.226,0,0.285c0.034,0.059,0.105,0.102,0.214,0.129
\t\t\t\tl-0.025,0.099c-0.056-0.018-0.142-0.043-0.26-0.079s-0.237-0.068-0.359-0.099c-0.122-0.031-0.246-0.06-0.373-0.087
\t\t\t\tc-0.126-0.027-0.221-0.047-0.284-0.06l0.025-0.099c0.109,0.028,0.193,0.024,0.25-0.011c0.057-0.036,0.103-0.119,0.136-0.25
\t\t\t\tl0.376-1.478c0.036-0.141,0.042-0.25,0.016-0.328c-0.025-0.078-0.095-0.132-0.211-0.161l0.025-0.099
\t\t\t\tc0.098,0.035,0.193,0.064,0.287,0.088c0.131,0.034,0.256,0.06,0.373,0.078C401.812,200.289,401.921,200.299,402.023,200.302z
\t\t\t\t M401.953,199.05c0.131,0.034,0.227,0.092,0.288,0.176c0.06,0.083,0.076,0.182,0.047,0.294c-0.029,0.112-0.089,0.191-0.183,0.235
\t\t\t\tc-0.093,0.045-0.205,0.05-0.336,0.017c-0.131-0.034-0.227-0.092-0.287-0.176c-0.061-0.083-0.076-0.182-0.047-0.294
\t\t\t\tc0.028-0.112,0.089-0.191,0.182-0.236C401.709,199.022,401.821,199.017,401.953,199.05z"/>
\t\t\t<path fill="#431347" d="M403.705,199.942l-0.18,0.773l0.557,0.129l-0.022,0.094l-0.557-0.129l-0.442,1.905
\t\t\t\tc-0.022,0.095-0.021,0.166,0.005,0.213c0.025,0.047,0.072,0.079,0.142,0.095c0.057,0.013,0.118,0.005,0.185-0.024
\t\t\t\ts0.134-0.091,0.202-0.184l0.07,0.061c-0.083,0.129-0.187,0.228-0.31,0.295c-0.124,0.067-0.273,0.081-0.449,0.04
\t\t\t\tc-0.104-0.024-0.192-0.059-0.264-0.104s-0.129-0.1-0.17-0.166c-0.053-0.085-0.079-0.182-0.077-0.289
\t\t\t\tc0.002-0.107,0.022-0.243,0.06-0.406l0.371-1.594l-0.396-0.092l0.022-0.094l0.396,0.092l0.145-0.623
\t\t\t\tc0.132,0.031,0.256,0.047,0.374,0.05S403.595,199.973,403.705,199.942z"/>
\t\t\t<path fill="#431347" d="M406.614,201.313l-0.402,1.988c-0.029,0.142-0.028,0.252,0.003,0.329c0.03,0.077,0.103,0.127,0.216,0.15
\t\t\t\tl-0.02,0.1c-0.096-0.029-0.193-0.054-0.291-0.074c-0.133-0.027-0.257-0.047-0.373-0.061c-0.116-0.014-0.227-0.018-0.333-0.013
\t\t\t\tl0.086-0.427c-0.108,0.143-0.236,0.234-0.384,0.275c-0.148,0.041-0.305,0.044-0.469,0.011c-0.133-0.027-0.237-0.066-0.312-0.117
\t\t\t\tc-0.075-0.052-0.131-0.109-0.167-0.172c-0.042-0.071-0.064-0.158-0.066-0.26c-0.002-0.103,0.012-0.231,0.043-0.386l0.242-1.2
\t\t\t\tc0.029-0.143,0.029-0.252,0-0.329c-0.029-0.077-0.102-0.127-0.219-0.15l0.021-0.1c0.099,0.03,0.196,0.054,0.291,0.074
\t\t\t\tc0.133,0.027,0.258,0.046,0.376,0.059s0.228,0.017,0.33,0.015l-0.397,1.964c-0.016,0.079-0.023,0.15-0.021,0.213
\t\t\t\tc0.002,0.063,0.02,0.115,0.055,0.157c0.034,0.042,0.091,0.07,0.17,0.086c0.126,0.026,0.246-0.005,0.357-0.093
\t\t\t\tc0.111-0.087,0.183-0.209,0.214-0.364l0.251-1.243c0.029-0.143,0.029-0.252,0-0.329c-0.029-0.077-0.102-0.127-0.219-0.15
\t\t\t\tl0.02-0.1c0.1,0.03,0.197,0.054,0.292,0.074c0.133,0.027,0.258,0.046,0.376,0.059
\t\t\t\tC406.401,201.311,406.511,201.315,406.614,201.313z"/>
\t\t\t<path fill="#431347" d="M408.256,200.847l-0.135,0.782l0.563,0.097l-0.017,0.096l-0.563-0.097l-0.332,1.928
\t\t\t\tc-0.017,0.095-0.011,0.166,0.018,0.211c0.028,0.046,0.077,0.075,0.147,0.087c0.057,0.01,0.118-0.002,0.183-0.035
\t\t\t\tc0.064-0.033,0.128-0.098,0.191-0.195l0.074,0.057c-0.076,0.134-0.173,0.239-0.292,0.313c-0.12,0.074-0.268,0.096-0.446,0.066
\t\t\t\tc-0.105-0.018-0.195-0.048-0.27-0.088c-0.075-0.041-0.135-0.093-0.18-0.156c-0.058-0.082-0.089-0.176-0.094-0.284
\t\t\t\tc-0.004-0.107,0.008-0.243,0.036-0.409l0.277-1.612l-0.401-0.069l0.016-0.095l0.401,0.069l0.108-0.63
\t\t\t\tc0.133,0.023,0.259,0.032,0.376,0.028C408.036,200.905,408.148,200.884,408.256,200.847z"/>
\t\t\t<path fill="#431347" d="M410.109,201.871c0.281,0.041,0.489,0.156,0.625,0.345c0.136,0.189,0.176,0.473,0.122,0.849l-1.576-0.229
\t\t\t\tl0.004-0.092l0.982,0.143c0.026-0.156,0.037-0.302,0.034-0.438c-0.003-0.136-0.023-0.247-0.059-0.333
\t\t\t\tc-0.037-0.087-0.095-0.136-0.174-0.148c-0.112-0.016-0.217,0.041-0.316,0.169c-0.099,0.129-0.179,0.355-0.243,0.679l0.01,0.031
\t\t\t\tc-0.009,0.038-0.017,0.078-0.024,0.119c-0.008,0.042-0.015,0.084-0.021,0.129c-0.032,0.22-0.026,0.404,0.016,0.55
\t\t\t\tc0.042,0.146,0.107,0.257,0.194,0.333s0.18,0.122,0.279,0.136c0.096,0.014,0.204,0.005,0.324-0.026
\t\t\t\tc0.12-0.032,0.241-0.12,0.364-0.265l0.082,0.041c-0.056,0.116-0.134,0.225-0.234,0.328c-0.1,0.103-0.219,0.182-0.357,0.237
\t\t\t\tc-0.139,0.055-0.297,0.069-0.476,0.043c-0.217-0.032-0.402-0.106-0.555-0.222c-0.152-0.117-0.262-0.276-0.329-0.479
\t\t\t\ts-0.079-0.448-0.037-0.739c0.043-0.297,0.129-0.538,0.259-0.721c0.129-0.184,0.29-0.311,0.481-0.383
\t\t\t\tS409.882,201.837,410.109,201.871z"/>
\t\t\t<path fill="#431347" d="M413.718,202.304c0.234,0.022,0.436,0.086,0.604,0.19c0.168,0.104,0.293,0.257,0.374,0.459
\t\t\t\tc0.081,0.203,0.105,0.463,0.075,0.781s-0.105,0.568-0.224,0.749c-0.118,0.182-0.27,0.307-0.455,0.376
\t\t\t\tc-0.185,0.07-0.395,0.093-0.629,0.07c-0.228-0.022-0.427-0.085-0.597-0.189s-0.296-0.257-0.379-0.458
\t\t\t\tc-0.083-0.201-0.108-0.46-0.078-0.778c0.031-0.318,0.106-0.568,0.226-0.751c0.12-0.183,0.273-0.309,0.46-0.378
\t\t\t\tS413.49,202.282,413.718,202.304z M413.709,202.4c-0.128-0.013-0.246,0.073-0.351,0.255c-0.105,0.183-0.179,0.489-0.221,0.919
\t\t\t\tc-0.042,0.431-0.028,0.745,0.04,0.943c0.068,0.198,0.167,0.303,0.295,0.315c0.132,0.013,0.25-0.071,0.353-0.253
\t\t\t\tc0.104-0.181,0.176-0.487,0.218-0.917c0.042-0.43,0.029-0.745-0.037-0.944C413.94,202.519,413.841,202.413,413.709,202.4z"/>
\t\t\t<path fill="#431347" d="M416.599,201.315c0.112,0.008,0.209,0.029,0.292,0.063c0.082,0.035,0.153,0.079,0.214,0.131
\t\t\t\tc0.042,0.039,0.078,0.09,0.105,0.155s0.039,0.138,0.034,0.218c-0.006,0.087-0.042,0.162-0.11,0.226s-0.153,0.091-0.256,0.084
\t\t\t\tc-0.106-0.007-0.192-0.044-0.257-0.11s-0.094-0.152-0.086-0.258c0.005-0.083,0.035-0.156,0.087-0.217s0.126-0.102,0.222-0.121
\t\t\t\tc-0.008-0.02-0.026-0.037-0.055-0.052c-0.028-0.015-0.066-0.024-0.114-0.027c-0.064-0.004-0.119,0.003-0.162,0.023
\t\t\t\tc-0.043,0.02-0.079,0.048-0.107,0.084c-0.039,0.046-0.068,0.114-0.087,0.205c-0.019,0.091-0.035,0.231-0.048,0.421l-0.03,0.439
\t\t\t\tl0.517,0.035l-0.006,0.097l-0.517-0.035l-0.13,1.898c-0.011,0.158,0.022,0.266,0.099,0.325c0.077,0.059,0.186,0.093,0.328,0.102
\t\t\t\tl-0.007,0.102c-0.083-0.009-0.201-0.021-0.352-0.036s-0.31-0.028-0.478-0.04c-0.123-0.008-0.243-0.014-0.36-0.017
\t\t\t\ts-0.208-0.005-0.269-0.006l0.007-0.101c0.112,0.007,0.194-0.011,0.244-0.056c0.05-0.045,0.08-0.135,0.089-0.271l0.133-1.946
\t\t\t\tl-0.328-0.022l0.006-0.097l0.329,0.022c0.02-0.287,0.057-0.503,0.112-0.65c0.055-0.146,0.143-0.271,0.263-0.373
\t\t\t\tc0.075-0.063,0.173-0.113,0.294-0.152C416.336,201.32,416.464,201.306,416.599,201.315z"/>
\t\t\t<path fill="#431347" d="M419.634,201.788l1.104,3.058c0.042,0.117,0.091,0.198,0.147,0.243c0.055,0.045,0.105,0.069,0.151,0.074
\t\t\t\tl-0.002,0.097c-0.097-0.009-0.21-0.015-0.341-0.02c-0.131-0.005-0.262-0.009-0.395-0.012c-0.148-0.004-0.289-0.006-0.421-0.008
\t\t\t\ts-0.237,0-0.315,0.004l0.002-0.097c0.165-0.002,0.269-0.028,0.314-0.077s0.037-0.156-0.024-0.323l-0.769-2.27l0.09-0.124
\t\t\t\tl-0.781,1.899c-0.096,0.237-0.145,0.416-0.146,0.539c-0.001,0.123,0.036,0.207,0.113,0.252c0.076,0.045,0.179,0.073,0.308,0.082
\t\t\t\tl-0.002,0.097c-0.106-0.009-0.208-0.016-0.304-0.02s-0.195-0.007-0.295-0.009c-0.071-0.002-0.138-0.002-0.201-0.002
\t\t\t\ts-0.119,0.003-0.167,0.008l0.002-0.097c0.068-0.011,0.139-0.053,0.211-0.125c0.072-0.073,0.145-0.197,0.217-0.373l1.171-2.805
\t\t\t\tc0.052,0.004,0.107,0.007,0.167,0.009C419.527,201.791,419.583,201.79,419.634,201.788z M419.924,203.838l-0.002,0.097
\t\t\t\tl-1.442-0.034l0.051-0.096L419.924,203.838z"/>
\t\t\t<path fill="#431347" d="M422.355,202.736l0.002,0.102c-0.07,0.001-0.133,0.007-0.186,0.016s-0.088,0.033-0.105,0.072
\t\t\t\tc-0.017,0.039-0.01,0.102,0.021,0.188l0.49,1.427l-0.061,0.069l0.525-1.895l0.098,0.042l-0.688,2.489
\t\t\t\tc-0.051-0.002-0.104-0.003-0.154-0.001c-0.052,0.001-0.105,0.004-0.16,0.008l-0.811-2.123c-0.048-0.128-0.099-0.205-0.152-0.229
\t\t\t\tc-0.054-0.025-0.099-0.037-0.134-0.036l-0.002-0.102c0.104,0.008,0.211,0.013,0.322,0.015s0.233,0.002,0.366-0.001
\t\t\t\tc0.1-0.002,0.206-0.007,0.316-0.017C422.154,202.753,422.259,202.744,422.355,202.736z M424.881,202.681l0.002,0.097
\t\t\t\tc-0.061,0.018-0.117,0.056-0.17,0.115c-0.055,0.059-0.104,0.162-0.148,0.308l-0.602,2.013c-0.051-0.002-0.104-0.002-0.156-0.001
\t\t\t\tc-0.053,0.001-0.105,0.004-0.158,0.008l-0.723-1.848l0.117-0.656c0.058,0.002,0.117,0.001,0.176-0.001
\t\t\t\tc0.061-0.003,0.119-0.005,0.178-0.006l0.705,1.941l-0.055-0.052l0.303-1.033c0.078-0.267,0.082-0.458,0.014-0.577
\t\t\t\tc-0.068-0.118-0.184-0.182-0.346-0.191l-0.002-0.097c0.051,0.002,0.107,0.003,0.166,0.003c0.061,0,0.121,0,0.18,0.001
\t\t\t\tc0.061,0,0.111,0,0.152-0.001c0.064-0.001,0.133-0.004,0.201-0.009C424.783,202.689,424.84,202.685,424.881,202.681z"/>
\t\t\t<path fill="#431347" d="M426.213,202.515c0.284-0.018,0.512,0.052,0.683,0.21c0.172,0.157,0.27,0.426,0.293,0.806l-1.59,0.099
\t\t\t\tl-0.015-0.091l0.99-0.062c-0.007-0.158-0.025-0.303-0.057-0.436c-0.03-0.132-0.073-0.237-0.126-0.315
\t\t\t\tc-0.054-0.077-0.12-0.114-0.201-0.108c-0.112,0.007-0.204,0.084-0.274,0.23c-0.069,0.146-0.103,0.385-0.098,0.714l0.016,0.028
\t\t\t\tc-0.001,0.039-0.001,0.079,0,0.121s0.003,0.085,0.006,0.131c0.015,0.222,0.057,0.4,0.128,0.535s0.158,0.23,0.258,0.287
\t\t\t\tc0.101,0.057,0.201,0.082,0.301,0.076c0.097-0.006,0.201-0.037,0.313-0.092c0.111-0.055,0.212-0.167,0.302-0.334l0.089,0.024
\t\t\t\tc-0.031,0.125-0.085,0.248-0.162,0.369c-0.076,0.121-0.177,0.223-0.301,0.305s-0.276,0.128-0.457,0.14
\t\t\t\tc-0.219,0.014-0.415-0.021-0.588-0.104c-0.174-0.083-0.313-0.216-0.421-0.4c-0.106-0.184-0.169-0.423-0.188-0.716
\t\t\t\tc-0.019-0.3,0.017-0.552,0.105-0.758s0.22-0.364,0.393-0.473S425.984,202.529,426.213,202.515z"/>
\t\t\t<path fill="#431347" d="M428.383,202.345c0.135-0.013,0.255-0.007,0.359,0.017c0.104,0.024,0.181,0.051,0.229,0.079
\t\t\t\tc0.116,0.063,0.179,0.009,0.188-0.164l0.102-0.01c0.002,0.091,0.008,0.201,0.017,0.332s0.023,0.303,0.043,0.519l-0.101,0.01
\t\t\t\tc-0.027-0.118-0.069-0.232-0.125-0.343c-0.056-0.112-0.13-0.2-0.222-0.266s-0.205-0.093-0.34-0.08
\t\t\t\tc-0.094,0.009-0.17,0.042-0.229,0.102c-0.059,0.059-0.084,0.139-0.074,0.238c0.009,0.097,0.046,0.178,0.11,0.245
\t\t\t\ts0.146,0.129,0.243,0.186c0.098,0.058,0.2,0.119,0.307,0.183c0.172,0.104,0.318,0.211,0.44,0.321
\t\t\t\tc0.122,0.11,0.192,0.262,0.211,0.455c0.013,0.145-0.017,0.274-0.091,0.39c-0.073,0.116-0.176,0.208-0.307,0.279
\t\t\t\ts-0.28,0.114-0.447,0.129c-0.097,0.009-0.184,0.007-0.261-0.005s-0.145-0.03-0.201-0.054c-0.044-0.016-0.085-0.03-0.124-0.044
\t\t\t\tc-0.038-0.014-0.074-0.026-0.107-0.036c-0.033-0.007-0.061,0.007-0.081,0.042c-0.021,0.035-0.036,0.081-0.043,0.14l-0.102,0.01
\t\t\t\tc-0.003-0.104-0.01-0.229-0.021-0.377s-0.027-0.344-0.05-0.588l0.101-0.01c0.036,0.172,0.085,0.322,0.149,0.451
\t\t\t\tc0.063,0.128,0.144,0.226,0.239,0.293c0.095,0.067,0.21,0.095,0.345,0.082c0.084-0.008,0.158-0.042,0.225-0.101
\t\t\t\tc0.065-0.06,0.093-0.151,0.081-0.273c-0.013-0.141-0.07-0.254-0.17-0.339c-0.101-0.084-0.227-0.175-0.378-0.271
\t\t\t\tc-0.111-0.074-0.216-0.147-0.315-0.221c-0.099-0.073-0.182-0.156-0.249-0.249c-0.067-0.092-0.106-0.202-0.118-0.327
\t\t\t\tc-0.014-0.145,0.014-0.271,0.081-0.377c0.068-0.106,0.163-0.191,0.285-0.252C428.105,202.396,428.238,202.358,428.383,202.345z"
\t\t\t\t/>
\t\t\t<path fill="#431347" d="M430.938,202.063c0.233-0.03,0.443-0.013,0.631,0.051c0.187,0.064,0.343,0.186,0.466,0.365
\t\t\t\tc0.124,0.18,0.206,0.428,0.246,0.745c0.041,0.317,0.023,0.577-0.052,0.78c-0.074,0.204-0.194,0.359-0.359,0.468
\t\t\t\ts-0.364,0.178-0.599,0.208c-0.227,0.029-0.435,0.012-0.624-0.052c-0.188-0.063-0.345-0.185-0.471-0.362
\t\t\t\tc-0.125-0.178-0.207-0.425-0.248-0.742c-0.04-0.317-0.022-0.578,0.054-0.783c0.077-0.205,0.198-0.362,0.365-0.471
\t\t\t\tC430.514,202.161,430.711,202.092,430.938,202.063z M430.95,202.159c-0.128,0.017-0.223,0.125-0.285,0.327
\t\t\t\tc-0.063,0.202-0.065,0.517-0.011,0.946c0.055,0.429,0.137,0.732,0.247,0.91c0.111,0.178,0.23,0.259,0.358,0.242
\t\t\t\tc0.131-0.017,0.228-0.125,0.288-0.325c0.061-0.199,0.063-0.514,0.009-0.943c-0.055-0.429-0.137-0.733-0.246-0.913
\t\t\t\tC431.202,202.224,431.082,202.143,430.95,202.159z"/>
\t\t\t<path fill="#431347" d="M434.171,201.58c0.134-0.024,0.244-0.026,0.333-0.007c0.089,0.019,0.163,0.052,0.225,0.101
\t\t\t\tc0.061,0.051,0.111,0.124,0.15,0.217c0.04,0.093,0.073,0.219,0.102,0.378l0.226,1.282c0.024,0.133,0.061,0.22,0.11,0.261
\t\t\t\ts0.125,0.052,0.228,0.034l0.018,0.1c-0.058,0.007-0.146,0.018-0.263,0.034c-0.116,0.016-0.23,0.034-0.342,0.053
\t\t\t\tc-0.114,0.02-0.227,0.042-0.337,0.067s-0.194,0.043-0.251,0.056l-0.018-0.1c0.086-0.015,0.142-0.049,0.166-0.103
\t\t\t\tc0.025-0.053,0.026-0.147,0.003-0.28l-0.262-1.483c-0.015-0.079-0.033-0.147-0.056-0.204c-0.023-0.057-0.056-0.099-0.099-0.125
\t\t\t\tc-0.042-0.027-0.1-0.034-0.173-0.021c-0.083,0.015-0.155,0.052-0.217,0.112s-0.105,0.136-0.134,0.228
\t\t\t\tc-0.028,0.092-0.033,0.19-0.015,0.295l0.233,1.325c0.024,0.133,0.058,0.221,0.101,0.262c0.044,0.042,0.106,0.055,0.189,0.041
\t\t\t\tl0.018,0.1c-0.052,0.006-0.131,0.016-0.238,0.03c-0.107,0.014-0.217,0.031-0.328,0.05c-0.12,0.021-0.244,0.045-0.37,0.073
\t\t\t\ts-0.221,0.048-0.284,0.063l-0.018-0.1c0.111-0.02,0.186-0.058,0.224-0.113c0.037-0.056,0.045-0.15,0.021-0.284l-0.266-1.502
\t\t\t\tc-0.024-0.143-0.065-0.245-0.12-0.306c-0.055-0.061-0.141-0.081-0.259-0.06l-0.018-0.1c0.104-0.008,0.203-0.021,0.298-0.038
\t\t\t\tc0.134-0.023,0.258-0.051,0.372-0.083c0.113-0.031,0.218-0.067,0.313-0.106l0.075,0.429c0.049-0.172,0.132-0.304,0.25-0.396
\t\t\t\tC433.878,201.667,434.016,201.607,434.171,201.58z M435.605,201.327c0.134-0.023,0.245-0.026,0.333-0.007
\t\t\t\tc0.089,0.019,0.163,0.052,0.225,0.101c0.062,0.051,0.111,0.124,0.151,0.216c0.039,0.093,0.073,0.219,0.101,0.378l0.227,1.283
\t\t\t\tc0.023,0.133,0.063,0.22,0.117,0.259c0.054,0.04,0.137,0.049,0.248,0.03l0.018,0.1c-0.061,0.008-0.152,0.02-0.273,0.036
\t\t\t\tc-0.122,0.017-0.241,0.035-0.359,0.056c-0.114,0.021-0.227,0.042-0.337,0.067s-0.193,0.043-0.25,0.057l-0.018-0.1
\t\t\t\tc0.085-0.015,0.141-0.049,0.166-0.103c0.024-0.054,0.025-0.147,0.002-0.281l-0.262-1.482c-0.014-0.079-0.033-0.147-0.058-0.204
\t\t\t\tc-0.025-0.056-0.061-0.098-0.105-0.124c-0.046-0.026-0.11-0.032-0.192-0.018c-0.124,0.022-0.223,0.096-0.295,0.222
\t\t\t\tc-0.073,0.126-0.098,0.269-0.072,0.428l-0.075-0.287c0.052-0.206,0.143-0.354,0.272-0.446S435.443,201.355,435.605,201.327z"/>
\t\t\t<path fill="#431347" d="M437.965,200.826c0.277-0.062,0.513-0.029,0.707,0.1c0.193,0.129,0.332,0.379,0.415,0.75l-1.554,0.348
\t\t\t\tl-0.029-0.088l0.968-0.216c-0.031-0.155-0.072-0.295-0.124-0.421c-0.051-0.125-0.109-0.223-0.174-0.291
\t\t\t\tc-0.064-0.068-0.137-0.093-0.215-0.076c-0.11,0.024-0.188,0.115-0.235,0.271c-0.046,0.156-0.041,0.396,0.015,0.721l0.021,0.025
\t\t\t\tc0.006,0.039,0.013,0.079,0.02,0.12c0.008,0.042,0.017,0.084,0.026,0.128c0.049,0.217,0.119,0.387,0.211,0.508
\t\t\t\tc0.091,0.122,0.191,0.203,0.3,0.243c0.107,0.04,0.211,0.049,0.309,0.027c0.094-0.021,0.192-0.068,0.293-0.14
\t\t\t\tc0.102-0.072,0.184-0.198,0.246-0.377l0.091,0.009c-0.011,0.128-0.045,0.258-0.101,0.39c-0.057,0.132-0.141,0.248-0.25,0.349
\t\t\t\ts-0.253,0.17-0.429,0.21c-0.215,0.048-0.414,0.044-0.598-0.01c-0.185-0.055-0.344-0.165-0.479-0.33
\t\t\t\tc-0.134-0.165-0.233-0.391-0.298-0.678c-0.065-0.293-0.07-0.548-0.015-0.765c0.056-0.218,0.16-0.394,0.313-0.529
\t\t\t\tC437.553,200.968,437.741,200.875,437.965,200.826z"/>
\t\t\t<path fill="#431347" d="M440.88,200.108c0.131-0.034,0.242-0.046,0.334-0.035c0.091,0.011,0.168,0.039,0.229,0.083
\t\t\t\tc0.065,0.046,0.122,0.113,0.17,0.201s0.093,0.211,0.134,0.37l0.33,1.26c0.034,0.131,0.08,0.214,0.138,0.249
\t\t\t\tc0.058,0.035,0.142,0.038,0.25,0.009l0.026,0.099c-0.061,0.012-0.15,0.032-0.271,0.058c-0.119,0.026-0.237,0.055-0.353,0.085
\t\t\t\tc-0.119,0.031-0.234,0.064-0.347,0.098c-0.113,0.035-0.199,0.062-0.257,0.08l-0.026-0.099c0.094-0.024,0.153-0.065,0.179-0.122
\t\t\t\ts0.021-0.15-0.014-0.282l-0.382-1.456c-0.021-0.078-0.046-0.144-0.076-0.198c-0.031-0.054-0.071-0.092-0.12-0.114
\t\t\t\tc-0.05-0.022-0.114-0.022-0.195-0.001c-0.132,0.035-0.228,0.115-0.29,0.241c-0.062,0.126-0.072,0.268-0.031,0.423l0.341,1.302
\t\t\t\tc0.034,0.131,0.077,0.215,0.128,0.252c0.052,0.037,0.122,0.043,0.213,0.019l0.025,0.099c-0.057,0.012-0.14,0.029-0.248,0.053
\t\t\t\tc-0.109,0.023-0.222,0.05-0.337,0.081c-0.119,0.031-0.24,0.065-0.363,0.103c-0.124,0.038-0.217,0.066-0.278,0.085l-0.025-0.099
\t\t\t\tc0.109-0.028,0.18-0.072,0.213-0.131s0.032-0.153-0.002-0.285l-0.386-1.475c-0.037-0.141-0.085-0.239-0.146-0.295
\t\t\t\tc-0.06-0.056-0.146-0.069-0.263-0.039l-0.025-0.098c0.103-0.017,0.2-0.038,0.294-0.062c0.131-0.035,0.252-0.072,0.363-0.113
\t\t\t\tc0.111-0.041,0.212-0.085,0.303-0.132l0.11,0.421c0.037-0.18,0.113-0.32,0.23-0.42
\t\t\t\tC440.573,200.223,440.715,200.151,440.88,200.108z"/>
\t\t\t<path fill="#431347" d="M443.227,199.437c0.272-0.082,0.51-0.066,0.712,0.048c0.203,0.114,0.359,0.354,0.47,0.718l-1.525,0.459
\t\t\t\tl-0.036-0.085l0.95-0.286c-0.042-0.152-0.094-0.29-0.153-0.411c-0.061-0.122-0.125-0.214-0.195-0.278
\t\t\t\tc-0.069-0.063-0.143-0.083-0.22-0.06c-0.108,0.033-0.18,0.128-0.215,0.287s-0.013,0.398,0.067,0.718l0.021,0.023
\t\t\t\tc0.009,0.038,0.018,0.078,0.028,0.118s0.022,0.083,0.035,0.126c0.064,0.213,0.146,0.377,0.247,0.492
\t\t\t\tc0.1,0.115,0.205,0.188,0.316,0.221c0.11,0.033,0.214,0.034,0.31,0.005c0.093-0.028,0.188-0.082,0.283-0.161
\t\t\t\tc0.096-0.08,0.168-0.211,0.218-0.394l0.092,0.002c-0.002,0.129-0.026,0.261-0.073,0.396c-0.047,0.136-0.121,0.257-0.224,0.366
\t\t\t\ts-0.24,0.188-0.413,0.24c-0.21,0.063-0.409,0.074-0.597,0.033c-0.188-0.042-0.354-0.139-0.5-0.294
\t\t\t\tc-0.146-0.155-0.262-0.373-0.347-0.654c-0.086-0.288-0.109-0.542-0.07-0.763c0.04-0.221,0.132-0.404,0.274-0.55
\t\t\t\tC442.826,199.609,443.007,199.503,443.227,199.437z"/>
\t\t\t<path fill="#431347" d="M445.313,198.771c0.129-0.043,0.247-0.064,0.354-0.065c0.107,0,0.188,0.008,0.24,0.024
\t\t\t\tc0.128,0.035,0.177-0.032,0.147-0.203l0.096-0.032c0.022,0.088,0.054,0.194,0.092,0.319c0.039,0.125,0.092,0.29,0.161,0.495
\t\t\t\tl-0.097,0.032c-0.053-0.108-0.12-0.21-0.199-0.306c-0.08-0.096-0.172-0.166-0.276-0.208c-0.105-0.043-0.222-0.043-0.351,0
\t\t\t\tc-0.089,0.03-0.155,0.08-0.199,0.151c-0.044,0.071-0.051,0.154-0.019,0.249c0.03,0.092,0.085,0.163,0.163,0.213
\t\t\t\tc0.078,0.051,0.172,0.093,0.28,0.126c0.107,0.034,0.221,0.069,0.34,0.108c0.19,0.062,0.358,0.133,0.502,0.212
\t\t\t\tc0.145,0.08,0.247,0.211,0.309,0.395c0.046,0.138,0.047,0.271,0.002,0.4c-0.046,0.129-0.124,0.243-0.235,0.342
\t\t\t\tc-0.111,0.099-0.247,0.174-0.406,0.228c-0.092,0.031-0.177,0.049-0.255,0.055s-0.147,0.003-0.209-0.006
\t\t\t\tc-0.046-0.005-0.089-0.01-0.13-0.015s-0.078-0.009-0.113-0.011c-0.033,0.001-0.057,0.021-0.069,0.059
\t\t\t\tc-0.013,0.039-0.017,0.087-0.01,0.146l-0.097,0.032c-0.026-0.1-0.062-0.221-0.106-0.363c-0.044-0.142-0.104-0.329-0.183-0.562
\t\t\t\tl0.096-0.032c0.074,0.159,0.157,0.294,0.248,0.404c0.092,0.111,0.191,0.188,0.3,0.231c0.108,0.044,0.227,0.044,0.355,0.001
\t\t\t\tc0.079-0.027,0.145-0.077,0.194-0.149c0.051-0.073,0.057-0.168,0.018-0.284c-0.045-0.135-0.126-0.231-0.243-0.291
\t\t\t\ts-0.26-0.119-0.43-0.178c-0.125-0.046-0.243-0.094-0.357-0.143c-0.113-0.049-0.213-0.111-0.299-0.186
\t\t\t\tc-0.087-0.075-0.149-0.172-0.189-0.291c-0.047-0.138-0.049-0.267-0.007-0.386c0.041-0.12,0.114-0.223,0.22-0.312
\t\t\t\tC445.055,198.884,445.176,198.817,445.313,198.771z"/>
\t\t\t<path fill="#431347" d="M447.455,198.02c0.127-0.047,0.244-0.072,0.352-0.076c0.106-0.003,0.188,0.002,0.241,0.017
\t\t\t\tc0.128,0.032,0.175-0.037,0.14-0.207l0.096-0.035c0.025,0.087,0.059,0.192,0.102,0.316c0.042,0.124,0.101,0.287,0.175,0.49
\t\t\t\tl-0.095,0.035c-0.057-0.106-0.126-0.207-0.209-0.3c-0.083-0.094-0.177-0.16-0.282-0.2c-0.106-0.04-0.223-0.037-0.35,0.01
\t\t\t\tc-0.088,0.032-0.153,0.084-0.195,0.157s-0.046,0.156-0.012,0.25c0.034,0.091,0.091,0.16,0.17,0.208
\t\t\t\tc0.08,0.048,0.174,0.087,0.283,0.118s0.224,0.063,0.343,0.098c0.193,0.057,0.362,0.122,0.509,0.197
\t\t\t\tc0.146,0.075,0.253,0.203,0.32,0.385c0.05,0.136,0.055,0.27,0.013,0.4c-0.041,0.131-0.116,0.247-0.225,0.349
\t\t\t\tc-0.108,0.102-0.241,0.182-0.398,0.24c-0.091,0.033-0.176,0.054-0.254,0.063c-0.077,0.008-0.147,0.008-0.209,0
\t\t\t\tc-0.046-0.004-0.089-0.007-0.13-0.011s-0.079-0.006-0.114-0.007c-0.033,0.002-0.056,0.022-0.067,0.061
\t\t\t\tc-0.012,0.039-0.014,0.088-0.005,0.146l-0.096,0.035c-0.029-0.099-0.068-0.219-0.117-0.359c-0.048-0.141-0.115-0.326-0.199-0.556
\t\t\t\tl0.095-0.035c0.079,0.157,0.165,0.29,0.26,0.397c0.095,0.108,0.197,0.182,0.307,0.223s0.228,0.037,0.355-0.01
\t\t\t\tc0.078-0.029,0.142-0.081,0.19-0.155c0.048-0.075,0.051-0.169,0.008-0.285c-0.049-0.133-0.133-0.228-0.251-0.284
\t\t\t\tc-0.119-0.056-0.264-0.111-0.435-0.165c-0.126-0.043-0.247-0.087-0.361-0.133c-0.115-0.045-0.217-0.104-0.305-0.176
\t\t\t\tc-0.089-0.072-0.155-0.167-0.198-0.286c-0.051-0.136-0.057-0.265-0.019-0.385s0.108-0.227,0.211-0.317
\t\t\t\tC447.199,198.141,447.318,198.07,447.455,198.02z"/>
\t\t</g>
\t\t<g>
\t\t\t<path fill="none" d="M387.771,197.909c0,0,30.305,15.951,65.244,0.428"/>
\t\t\t<path fill="#FFFFFF" d="M394.179,196.926l-0.035,0.09c-0.098-0.034-0.175-0.051-0.232-0.05c-0.057,0-0.104,0.024-0.139,0.071
\t\t\t\ts-0.074,0.126-0.117,0.238l-0.861,2.241c-0.042,0.108-0.064,0.193-0.069,0.253s0.014,0.109,0.056,0.146
\t\t\t\tc0.041,0.037,0.109,0.077,0.203,0.12l-0.035,0.09c-0.079-0.037-0.18-0.08-0.303-0.129c-0.123-0.049-0.248-0.098-0.374-0.146
\t\t\t\tc-0.148-0.057-0.285-0.108-0.41-0.155c-0.125-0.047-0.229-0.082-0.309-0.106l0.035-0.09c0.099,0.031,0.175,0.046,0.23,0.047
\t\t\t\ts0.102-0.023,0.139-0.071c0.037-0.048,0.077-0.126,0.119-0.235l0.861-2.241c0.042-0.111,0.066-0.197,0.069-0.256
\t\t\t\tc0.004-0.059-0.015-0.108-0.058-0.146c-0.042-0.039-0.109-0.078-0.202-0.116l0.035-0.09c0.077,0.033,0.178,0.075,0.302,0.126
\t\t\t\tc0.124,0.051,0.26,0.105,0.407,0.162c0.126,0.049,0.252,0.095,0.377,0.14C393.993,196.862,394.096,196.898,394.179,196.926z"/>
\t\t\t<path fill="#FFFFFF" d="M395.857,198.426c0.128,0.044,0.227,0.096,0.297,0.156c0.07,0.06,0.119,0.125,0.146,0.196
\t\t\t\tc0.029,0.075,0.039,0.162,0.03,0.261c-0.009,0.099-0.04,0.227-0.093,0.382l-0.424,1.231c-0.044,0.128-0.052,0.223-0.023,0.284
\t\t\t\tc0.028,0.061,0.096,0.11,0.203,0.146l-0.033,0.096c-0.057-0.023-0.143-0.057-0.257-0.101c-0.114-0.044-0.228-0.086-0.341-0.125
\t\t\t\tc-0.116-0.04-0.23-0.077-0.343-0.111s-0.199-0.059-0.258-0.076l0.033-0.096c0.091,0.031,0.163,0.031,0.216-0.002
\t\t\t\tc0.052-0.033,0.101-0.114,0.145-0.242l0.49-1.423c0.026-0.076,0.042-0.146,0.046-0.207c0.004-0.062-0.008-0.115-0.037-0.161
\t\t\t\tc-0.029-0.046-0.083-0.082-0.162-0.109c-0.128-0.044-0.253-0.031-0.375,0.04s-0.209,0.182-0.261,0.335l-0.438,1.272
\t\t\t\tc-0.044,0.128-0.055,0.222-0.033,0.28c0.022,0.059,0.078,0.104,0.167,0.134l-0.033,0.096c-0.054-0.022-0.133-0.054-0.236-0.094
\t\t\t\tc-0.104-0.041-0.212-0.081-0.325-0.12c-0.116-0.04-0.235-0.079-0.359-0.116c-0.124-0.038-0.216-0.065-0.278-0.083l0.033-0.096
\t\t\t\tc0.106,0.037,0.19,0.04,0.25,0.009c0.06-0.031,0.112-0.11,0.156-0.238l0.496-1.442c0.047-0.137,0.062-0.246,0.043-0.326
\t\t\t\tc-0.019-0.08-0.084-0.139-0.197-0.178l0.033-0.096c0.094,0.043,0.188,0.08,0.279,0.111c0.128,0.044,0.25,0.08,0.365,0.108
\t\t\t\tc0.115,0.027,0.223,0.047,0.325,0.058l-0.142,0.412c0.13-0.129,0.271-0.204,0.425-0.223
\t\t\t\tC395.538,198.352,395.695,198.37,395.857,198.426z"/>
\t\t\t<path fill="#FFFFFF" d="M397.921,199.087c0.129,0.04,0.238,0.09,0.325,0.152s0.148,0.115,0.182,0.159
\t\t\t\tc0.083,0.103,0.163,0.077,0.237-0.08l0.098,0.03c-0.033,0.084-0.07,0.188-0.111,0.313c-0.041,0.124-0.093,0.29-0.156,0.496
\t\t\t\tl-0.097-0.03c0.019-0.119,0.024-0.24,0.015-0.365c-0.01-0.125-0.044-0.234-0.104-0.33c-0.06-0.096-0.155-0.164-0.285-0.204
\t\t\t\tc-0.089-0.027-0.173-0.025-0.25,0.007c-0.078,0.032-0.131,0.096-0.16,0.192c-0.028,0.092-0.025,0.182,0.009,0.269
\t\t\t\tc0.034,0.086,0.085,0.174,0.154,0.265c0.069,0.09,0.14,0.185,0.214,0.285c0.119,0.162,0.215,0.316,0.286,0.464
\t\t\t\tc0.071,0.148,0.079,0.315,0.022,0.5c-0.042,0.139-0.12,0.248-0.231,0.327c-0.112,0.079-0.242,0.126-0.39,0.142
\t\t\t\tc-0.148,0.016-0.303-0.001-0.463-0.05c-0.092-0.028-0.172-0.063-0.239-0.103c-0.067-0.041-0.122-0.083-0.166-0.127
\t\t\t\tc-0.035-0.031-0.067-0.06-0.097-0.088c-0.03-0.027-0.059-0.052-0.086-0.074c-0.028-0.019-0.058-0.016-0.091,0.008
\t\t\t\ts-0.063,0.062-0.093,0.113l-0.097-0.03c0.036-0.097,0.078-0.216,0.124-0.357c0.046-0.141,0.105-0.329,0.177-0.563l0.097,0.03
\t\t\t\tc-0.032,0.172-0.043,0.33-0.033,0.473c0.01,0.144,0.047,0.264,0.109,0.363c0.063,0.098,0.159,0.167,0.289,0.207
\t\t\t\tc0.081,0.024,0.162,0.022,0.246-0.008s0.143-0.104,0.179-0.221c0.042-0.136,0.032-0.262-0.029-0.378
\t\t\t\tc-0.061-0.116-0.143-0.248-0.247-0.394c-0.074-0.11-0.144-0.218-0.208-0.324c-0.064-0.106-0.109-0.214-0.136-0.325
\t\t\t\tc-0.027-0.111-0.022-0.227,0.015-0.347c0.042-0.139,0.115-0.245,0.218-0.318c0.103-0.073,0.223-0.115,0.36-0.126
\t\t\t\tC397.644,199.029,397.782,199.045,397.921,199.087z"/>
\t\t\t<path fill="#FFFFFF" d="M400.32,199.026l-0.211,0.765l0.55,0.152l-0.026,0.093l-0.55-0.152l-0.522,1.885
\t\t\t\tc-0.026,0.093-0.027,0.164-0.004,0.212s0.07,0.082,0.138,0.101c0.056,0.016,0.118,0.01,0.185-0.017
\t\t\t\tc0.068-0.026,0.138-0.085,0.209-0.175l0.068,0.063c-0.089,0.126-0.196,0.221-0.322,0.283c-0.126,0.063-0.276,0.069-0.451,0.021
\t\t\t\tc-0.103-0.028-0.189-0.066-0.259-0.115c-0.071-0.048-0.125-0.105-0.163-0.173c-0.05-0.087-0.071-0.185-0.065-0.292
\t\t\t\tc0.006-0.107,0.032-0.241,0.077-0.403l0.437-1.577l-0.392-0.108l0.026-0.093l0.392,0.108l0.17-0.616
\t\t\t\tc0.13,0.036,0.254,0.058,0.371,0.065S400.209,199.053,400.32,199.026z"/>
\t\t\t<path fill="#FFFFFF" d="M401.826,200.187l-0.52,2.041c-0.033,0.131-0.033,0.226,0,0.285c0.034,0.059,0.105,0.102,0.214,0.129
\t\t\t\tl-0.025,0.099c-0.056-0.018-0.142-0.043-0.26-0.079s-0.237-0.068-0.359-0.099c-0.122-0.031-0.246-0.06-0.373-0.087
\t\t\t\tc-0.126-0.027-0.221-0.047-0.284-0.06l0.025-0.099c0.109,0.028,0.193,0.024,0.25-0.011c0.057-0.036,0.103-0.119,0.136-0.25
\t\t\t\tl0.376-1.478c0.036-0.141,0.042-0.25,0.016-0.328c-0.025-0.078-0.095-0.132-0.211-0.161l0.025-0.099
\t\t\t\tc0.098,0.035,0.193,0.064,0.287,0.088c0.131,0.034,0.255,0.06,0.373,0.078C401.614,200.173,401.724,200.184,401.826,200.187z
\t\t\t\t M401.755,198.935c0.131,0.034,0.227,0.092,0.288,0.176c0.06,0.083,0.076,0.182,0.047,0.294
\t\t\t\tc-0.029,0.113-0.089,0.191-0.183,0.236c-0.093,0.045-0.205,0.05-0.336,0.017c-0.131-0.034-0.227-0.092-0.287-0.176
\t\t\t\tc-0.061-0.083-0.076-0.182-0.048-0.294c0.029-0.112,0.09-0.191,0.183-0.236C401.512,198.907,401.624,198.901,401.755,198.935z"/>
\t\t\t<path fill="#FFFFFF" d="M403.507,199.827l-0.18,0.773l0.557,0.129l-0.022,0.094l-0.557-0.129l-0.442,1.905
\t\t\t\tc-0.022,0.095-0.021,0.166,0.005,0.213c0.025,0.047,0.072,0.079,0.142,0.095c0.057,0.013,0.118,0.005,0.185-0.024
\t\t\t\ts0.134-0.091,0.202-0.184l0.07,0.061c-0.083,0.129-0.187,0.228-0.31,0.295c-0.124,0.067-0.273,0.081-0.449,0.04
\t\t\t\tc-0.104-0.024-0.192-0.059-0.264-0.104s-0.129-0.1-0.17-0.166c-0.053-0.085-0.079-0.182-0.077-0.289
\t\t\t\tc0.002-0.107,0.022-0.243,0.06-0.406l0.371-1.594l-0.396-0.092l0.022-0.094l0.396,0.092l0.145-0.623
\t\t\t\tc0.132,0.031,0.256,0.047,0.374,0.05S403.397,199.858,403.507,199.827z"/>
\t\t\t<path fill="#FFFFFF" d="M406.417,201.198l-0.402,1.988c-0.029,0.142-0.028,0.252,0.003,0.329c0.03,0.077,0.103,0.127,0.217,0.15
\t\t\t\tl-0.021,0.1c-0.096-0.029-0.193-0.054-0.291-0.074c-0.133-0.027-0.257-0.047-0.373-0.061c-0.116-0.014-0.227-0.018-0.333-0.013
\t\t\t\tl0.086-0.427c-0.108,0.143-0.236,0.234-0.384,0.275c-0.148,0.041-0.305,0.044-0.469,0.011c-0.133-0.027-0.237-0.066-0.312-0.117
\t\t\t\tc-0.075-0.052-0.131-0.109-0.167-0.172c-0.042-0.071-0.064-0.158-0.066-0.26c-0.002-0.103,0.012-0.231,0.043-0.386l0.242-1.2
\t\t\t\tc0.029-0.143,0.029-0.252,0-0.329c-0.029-0.077-0.102-0.127-0.219-0.15l0.02-0.1c0.1,0.03,0.197,0.054,0.292,0.074
\t\t\t\tc0.133,0.027,0.258,0.046,0.376,0.059s0.228,0.017,0.33,0.015l-0.397,1.964c-0.016,0.079-0.023,0.15-0.021,0.213
\t\t\t\tc0.002,0.063,0.02,0.115,0.055,0.157c0.034,0.042,0.091,0.07,0.17,0.086c0.126,0.026,0.246-0.005,0.357-0.093
\t\t\t\tc0.111-0.087,0.183-0.209,0.214-0.364l0.251-1.243c0.029-0.143,0.029-0.252,0-0.329c-0.029-0.077-0.102-0.127-0.219-0.15
\t\t\t\tl0.02-0.1c0.1,0.03,0.197,0.054,0.292,0.074c0.133,0.027,0.258,0.046,0.376,0.059C406.204,201.195,406.314,201.2,406.417,201.198
\t\t\t\tz"/>
\t\t\t<path fill="#FFFFFF" d="M408.058,200.732l-0.134,0.782l0.563,0.097l-0.017,0.095l-0.563-0.097l-0.331,1.928
\t\t\t\tc-0.017,0.095-0.011,0.166,0.018,0.211c0.028,0.046,0.077,0.075,0.147,0.087c0.057,0.01,0.118-0.002,0.183-0.035
\t\t\t\tc0.064-0.033,0.128-0.098,0.191-0.195l0.074,0.057c-0.076,0.134-0.173,0.239-0.292,0.313c-0.12,0.074-0.268,0.096-0.446,0.066
\t\t\t\tc-0.105-0.018-0.195-0.048-0.27-0.088c-0.075-0.041-0.135-0.093-0.18-0.156c-0.058-0.082-0.089-0.176-0.094-0.284
\t\t\t\tc-0.004-0.107,0.008-0.243,0.036-0.409l0.277-1.612l-0.401-0.069l0.016-0.095l0.401,0.069l0.108-0.63
\t\t\t\tc0.133,0.023,0.259,0.032,0.376,0.028C407.837,200.79,407.95,200.769,408.058,200.732z"/>
\t\t\t<path fill="#FFFFFF" d="M409.911,201.755c0.281,0.041,0.49,0.156,0.625,0.345c0.136,0.189,0.176,0.473,0.122,0.849l-1.576-0.229
\t\t\t\tl0.004-0.092l0.982,0.143c0.026-0.156,0.037-0.302,0.034-0.438c-0.003-0.136-0.023-0.247-0.059-0.334
\t\t\t\tc-0.037-0.086-0.095-0.136-0.174-0.147c-0.112-0.016-0.217,0.041-0.316,0.169c-0.099,0.129-0.179,0.355-0.243,0.679l0.01,0.031
\t\t\t\tc-0.009,0.038-0.017,0.077-0.024,0.119c-0.008,0.042-0.015,0.084-0.021,0.129c-0.032,0.22-0.027,0.404,0.016,0.55
\t\t\t\tc0.042,0.146,0.107,0.257,0.194,0.333s0.18,0.122,0.279,0.136c0.096,0.014,0.204,0.005,0.324-0.026
\t\t\t\tc0.12-0.032,0.241-0.12,0.364-0.266l0.082,0.042c-0.056,0.116-0.134,0.225-0.234,0.328c-0.1,0.103-0.219,0.182-0.357,0.237
\t\t\t\tc-0.139,0.055-0.297,0.069-0.476,0.043c-0.217-0.032-0.402-0.106-0.555-0.223c-0.152-0.116-0.262-0.276-0.329-0.478
\t\t\t\ts-0.079-0.448-0.037-0.739c0.043-0.297,0.129-0.538,0.259-0.721c0.129-0.184,0.29-0.311,0.481-0.383
\t\t\t\tS409.685,201.722,409.911,201.755z"/>
\t\t\t<path fill="#FFFFFF" d="M413.521,202.188c0.234,0.023,0.436,0.086,0.604,0.19c0.168,0.104,0.293,0.257,0.374,0.459
\t\t\t\tc0.081,0.203,0.106,0.463,0.075,0.781s-0.105,0.568-0.224,0.749c-0.118,0.182-0.27,0.307-0.455,0.376
\t\t\t\tc-0.185,0.07-0.395,0.093-0.629,0.07c-0.228-0.022-0.427-0.085-0.597-0.189s-0.296-0.257-0.379-0.458
\t\t\t\tc-0.083-0.201-0.108-0.46-0.078-0.778c0.031-0.318,0.106-0.568,0.226-0.751c0.12-0.183,0.273-0.309,0.46-0.378
\t\t\t\tS413.293,202.166,413.521,202.188z M413.512,202.284c-0.128-0.013-0.246,0.073-0.351,0.255c-0.105,0.183-0.179,0.489-0.221,0.919
\t\t\t\tc-0.042,0.431-0.028,0.745,0.04,0.942c0.068,0.198,0.167,0.303,0.295,0.316c0.132,0.013,0.25-0.071,0.353-0.253
\t\t\t\tc0.104-0.181,0.176-0.487,0.218-0.917c0.042-0.43,0.029-0.745-0.037-0.944C413.742,202.403,413.643,202.297,413.512,202.284z"/>
\t\t\t<path fill="#FFFFFF" d="M416.401,201.199c0.113,0.008,0.21,0.029,0.292,0.063c0.082,0.035,0.153,0.079,0.214,0.131
\t\t\t\tc0.043,0.039,0.078,0.09,0.106,0.155c0.028,0.065,0.039,0.138,0.034,0.218c-0.006,0.087-0.042,0.162-0.11,0.226
\t\t\t\ts-0.153,0.091-0.256,0.084c-0.106-0.007-0.192-0.044-0.257-0.11s-0.094-0.152-0.086-0.258c0.005-0.083,0.035-0.156,0.087-0.217
\t\t\t\tc0.053-0.062,0.126-0.102,0.222-0.121c-0.008-0.02-0.026-0.037-0.055-0.052c-0.028-0.015-0.066-0.024-0.115-0.027
\t\t\t\tc-0.064-0.004-0.118,0.003-0.162,0.022c-0.043,0.02-0.079,0.048-0.107,0.085c-0.039,0.046-0.068,0.114-0.087,0.205
\t\t\t\ts-0.035,0.231-0.048,0.421l-0.03,0.439l0.517,0.035l-0.006,0.097l-0.517-0.035l-0.13,1.898c-0.011,0.158,0.022,0.266,0.099,0.325
\t\t\t\tc0.077,0.059,0.186,0.093,0.328,0.102l-0.007,0.102c-0.083-0.009-0.201-0.021-0.352-0.036s-0.31-0.028-0.478-0.04
\t\t\t\tc-0.123-0.008-0.243-0.014-0.36-0.017c-0.118-0.003-0.208-0.005-0.269-0.006l0.007-0.101c0.112,0.007,0.194-0.011,0.244-0.056
\t\t\t\tc0.05-0.045,0.08-0.135,0.089-0.271l0.133-1.946l-0.328-0.022l0.006-0.097l0.329,0.022c0.02-0.287,0.057-0.503,0.112-0.65
\t\t\t\tc0.055-0.146,0.143-0.271,0.263-0.373c0.075-0.063,0.173-0.113,0.294-0.152C416.138,201.205,416.266,201.19,416.401,201.199z"/>
\t\t\t<path fill="#FFFFFF" d="M419.437,201.672l1.104,3.058c0.042,0.117,0.091,0.198,0.147,0.243c0.055,0.045,0.105,0.069,0.151,0.074
\t\t\t\tl-0.002,0.097c-0.097-0.009-0.21-0.016-0.341-0.02c-0.131-0.005-0.262-0.009-0.395-0.012c-0.148-0.003-0.289-0.006-0.421-0.007
\t\t\t\ts-0.237,0-0.315,0.004l0.002-0.097c0.165-0.002,0.269-0.028,0.314-0.077s0.037-0.157-0.024-0.323l-0.769-2.27l0.09-0.124
\t\t\t\tl-0.781,1.899c-0.096,0.237-0.145,0.416-0.146,0.539c-0.001,0.123,0.036,0.207,0.113,0.252c0.076,0.045,0.179,0.073,0.308,0.082
\t\t\t\tl-0.002,0.097c-0.106-0.009-0.208-0.016-0.304-0.02s-0.195-0.007-0.295-0.009c-0.071-0.002-0.138-0.002-0.201-0.002
\t\t\t\ts-0.119,0.003-0.167,0.008l0.002-0.097c0.068-0.011,0.139-0.053,0.211-0.125c0.072-0.073,0.145-0.197,0.217-0.373l1.171-2.805
\t\t\t\tc0.051,0.004,0.107,0.007,0.167,0.009C419.329,201.674,419.385,201.674,419.437,201.672z M419.727,203.722l-0.002,0.097
\t\t\t\tl-1.442-0.034l0.051-0.096L419.727,203.722z"/>
\t\t\t<path fill="#FFFFFF" d="M422.158,202.62l0.002,0.102c-0.07,0.001-0.133,0.007-0.186,0.016s-0.088,0.033-0.105,0.072
\t\t\t\tc-0.017,0.039-0.01,0.102,0.021,0.188l0.491,1.427l-0.062,0.069l0.525-1.895l0.098,0.042l-0.687,2.489
\t\t\t\tc-0.052-0.002-0.104-0.003-0.155-0.001c-0.051,0.001-0.104,0.004-0.159,0.008l-0.812-2.123c-0.048-0.128-0.099-0.205-0.152-0.229
\t\t\t\tc-0.054-0.025-0.099-0.037-0.134-0.036l-0.002-0.102c0.104,0.008,0.211,0.013,0.322,0.015s0.233,0.002,0.366-0.001
\t\t\t\tc0.1-0.002,0.206-0.008,0.316-0.017C421.958,202.637,422.063,202.628,422.158,202.62z M424.685,202.565l0.002,0.097
\t\t\t\tc-0.061,0.018-0.118,0.056-0.172,0.115c-0.053,0.059-0.103,0.162-0.147,0.308l-0.601,2.013c-0.052-0.002-0.104-0.002-0.157-0.001
\t\t\t\tc-0.053,0.001-0.105,0.004-0.157,0.008l-0.723-1.848l0.116-0.656c0.058,0.002,0.117,0.001,0.177-0.001s0.118-0.005,0.177-0.006
\t\t\t\tl0.705,1.941l-0.054-0.052l0.302-1.033c0.078-0.266,0.082-0.458,0.014-0.576c-0.068-0.118-0.184-0.182-0.346-0.191l-0.002-0.097
\t\t\t\tc0.052,0.002,0.107,0.003,0.167,0.004s0.12,0,0.18,0.001s0.11,0,0.152-0.001c0.064-0.001,0.131-0.004,0.2-0.009
\t\t\t\tS424.643,202.569,424.685,202.565z"/>
\t\t\t<path fill="#FFFFFF" d="M426.015,202.399c0.284-0.018,0.511,0.052,0.683,0.21c0.172,0.157,0.27,0.426,0.293,0.806l-1.59,0.099
\t\t\t\tl-0.015-0.091l0.99-0.062c-0.007-0.158-0.025-0.303-0.057-0.435s-0.073-0.237-0.126-0.315c-0.054-0.077-0.12-0.114-0.201-0.108
\t\t\t\tc-0.112,0.007-0.204,0.083-0.274,0.23c-0.069,0.146-0.103,0.385-0.099,0.714l0.017,0.028c-0.001,0.039-0.001,0.079,0,0.121
\t\t\t\ts0.003,0.085,0.006,0.131c0.014,0.222,0.057,0.4,0.128,0.535s0.157,0.23,0.258,0.287c0.101,0.057,0.201,0.082,0.301,0.076
\t\t\t\tc0.097-0.006,0.201-0.037,0.313-0.092c0.111-0.055,0.212-0.167,0.302-0.334l0.089,0.023c-0.031,0.125-0.085,0.248-0.162,0.369
\t\t\t\tc-0.076,0.121-0.177,0.223-0.301,0.305c-0.124,0.083-0.276,0.129-0.457,0.14c-0.219,0.014-0.415-0.021-0.588-0.104
\t\t\t\tc-0.174-0.083-0.313-0.216-0.421-0.4c-0.106-0.184-0.169-0.423-0.188-0.716c-0.019-0.3,0.017-0.552,0.105-0.758
\t\t\t\ts0.22-0.364,0.393-0.473S425.786,202.413,426.015,202.399z"/>
\t\t\t<path fill="#FFFFFF" d="M428.185,202.229c0.135-0.013,0.255-0.007,0.359,0.017c0.104,0.024,0.181,0.05,0.229,0.079
\t\t\t\tc0.116,0.063,0.179,0.009,0.188-0.164l0.102-0.01c0.002,0.091,0.007,0.201,0.017,0.332c0.009,0.13,0.023,0.303,0.043,0.519
\t\t\t\tl-0.101,0.009c-0.027-0.117-0.069-0.231-0.125-0.343c-0.056-0.111-0.13-0.2-0.222-0.266s-0.205-0.093-0.341-0.08
\t\t\t\tc-0.093,0.009-0.169,0.042-0.229,0.102s-0.084,0.139-0.075,0.238c0.01,0.097,0.046,0.178,0.111,0.245
\t\t\t\tc0.064,0.067,0.146,0.129,0.243,0.186c0.098,0.058,0.2,0.119,0.307,0.183c0.172,0.104,0.318,0.21,0.44,0.321
\t\t\t\tc0.122,0.11,0.192,0.262,0.211,0.454c0.013,0.145-0.017,0.275-0.091,0.39c-0.073,0.116-0.176,0.208-0.307,0.279
\t\t\t\ts-0.28,0.114-0.447,0.129c-0.097,0.009-0.184,0.007-0.261-0.005s-0.145-0.03-0.201-0.054c-0.044-0.016-0.085-0.03-0.124-0.044
\t\t\t\tc-0.038-0.014-0.074-0.026-0.107-0.036c-0.033-0.007-0.061,0.007-0.081,0.042c-0.021,0.035-0.036,0.081-0.043,0.14l-0.102,0.01
\t\t\t\tc-0.003-0.104-0.01-0.229-0.021-0.377s-0.027-0.344-0.05-0.588l0.101-0.01c0.036,0.172,0.085,0.322,0.149,0.451
\t\t\t\tc0.063,0.128,0.144,0.226,0.239,0.293c0.095,0.067,0.21,0.095,0.345,0.082c0.084-0.008,0.158-0.042,0.225-0.102
\t\t\t\tc0.065-0.06,0.093-0.15,0.081-0.272c-0.013-0.141-0.07-0.254-0.17-0.339c-0.101-0.084-0.227-0.175-0.378-0.271
\t\t\t\tc-0.111-0.074-0.216-0.147-0.315-0.221c-0.099-0.073-0.182-0.156-0.249-0.249c-0.067-0.092-0.106-0.202-0.118-0.327
\t\t\t\tc-0.014-0.145,0.014-0.271,0.081-0.377c0.068-0.106,0.163-0.191,0.285-0.252C427.906,202.28,428.04,202.242,428.185,202.229z"/>
\t\t\t<path fill="#FFFFFF" d="M430.739,201.946c0.234-0.03,0.444-0.013,0.632,0.051c0.187,0.064,0.342,0.186,0.466,0.365
\t\t\t\ts0.206,0.428,0.246,0.745c0.041,0.317,0.023,0.577-0.052,0.78c-0.074,0.203-0.194,0.359-0.359,0.468s-0.364,0.178-0.599,0.208
\t\t\t\tc-0.227,0.029-0.435,0.012-0.624-0.052c-0.188-0.064-0.345-0.185-0.471-0.362c-0.125-0.178-0.208-0.425-0.248-0.742
\t\t\t\ts-0.022-0.578,0.054-0.783c0.076-0.205,0.198-0.362,0.364-0.471C430.315,202.044,430.513,201.976,430.739,201.946z
\t\t\t\t M430.752,202.042c-0.128,0.017-0.223,0.125-0.285,0.327c-0.063,0.202-0.065,0.517-0.011,0.946
\t\t\t\tc0.055,0.429,0.137,0.732,0.247,0.91c0.111,0.178,0.23,0.259,0.358,0.242c0.131-0.017,0.227-0.125,0.288-0.325
\t\t\t\tc0.061-0.2,0.063-0.514,0.009-0.943c-0.055-0.429-0.137-0.733-0.246-0.913C431.003,202.107,430.883,202.026,430.752,202.042z"/>
\t\t\t<path fill="#FFFFFF" d="M433.973,201.464c0.134-0.023,0.244-0.026,0.333-0.007c0.089,0.019,0.163,0.052,0.224,0.101
\t\t\t\tc0.062,0.051,0.112,0.124,0.151,0.216c0.04,0.093,0.073,0.219,0.102,0.378l0.226,1.283c0.024,0.133,0.061,0.22,0.11,0.261
\t\t\t\tc0.05,0.04,0.125,0.051,0.228,0.034l0.018,0.1c-0.058,0.007-0.146,0.018-0.263,0.034c-0.116,0.016-0.23,0.033-0.342,0.053
\t\t\t\tc-0.114,0.02-0.227,0.042-0.337,0.066c-0.11,0.024-0.194,0.043-0.251,0.057l-0.018-0.1c0.086-0.015,0.142-0.049,0.166-0.103
\t\t\t\tc0.025-0.054,0.026-0.147,0.003-0.28l-0.262-1.483c-0.015-0.079-0.033-0.147-0.056-0.204c-0.023-0.056-0.056-0.098-0.099-0.125
\t\t\t\tc-0.042-0.027-0.1-0.034-0.173-0.021c-0.083,0.015-0.155,0.052-0.217,0.112s-0.106,0.136-0.134,0.228
\t\t\t\tc-0.028,0.092-0.033,0.19-0.015,0.295l0.233,1.326c0.024,0.133,0.058,0.221,0.101,0.262c0.044,0.042,0.106,0.055,0.189,0.041
\t\t\t\tl0.018,0.1c-0.052,0.006-0.131,0.016-0.238,0.03c-0.107,0.014-0.217,0.031-0.328,0.05c-0.12,0.021-0.244,0.045-0.37,0.073
\t\t\t\ts-0.221,0.048-0.284,0.063l-0.018-0.1c0.111-0.02,0.186-0.058,0.224-0.113c0.037-0.056,0.045-0.15,0.021-0.284l-0.265-1.502
\t\t\t\tc-0.025-0.143-0.065-0.245-0.12-0.306c-0.055-0.061-0.142-0.081-0.259-0.06l-0.018-0.1c0.104-0.008,0.203-0.021,0.298-0.038
\t\t\t\tc0.134-0.023,0.258-0.051,0.371-0.083c0.114-0.031,0.219-0.067,0.313-0.106l0.075,0.429c0.049-0.172,0.132-0.304,0.25-0.396
\t\t\t\tS433.816,201.492,433.973,201.464z M435.407,201.211c0.134-0.023,0.245-0.026,0.333-0.007c0.089,0.019,0.163,0.052,0.225,0.101
\t\t\t\tc0.062,0.051,0.111,0.124,0.151,0.216c0.039,0.093,0.072,0.219,0.101,0.378l0.227,1.282c0.023,0.134,0.063,0.22,0.117,0.26
\t\t\t\tc0.054,0.04,0.137,0.049,0.248,0.03l0.018,0.1c-0.061,0.007-0.152,0.02-0.273,0.036c-0.122,0.017-0.241,0.035-0.359,0.056
\t\t\t\tc-0.114,0.021-0.227,0.042-0.337,0.067s-0.193,0.043-0.25,0.057l-0.018-0.1c0.085-0.015,0.141-0.05,0.166-0.103
\t\t\t\tc0.024-0.054,0.025-0.147,0.002-0.281l-0.262-1.482c-0.014-0.08-0.033-0.147-0.058-0.204c-0.025-0.056-0.061-0.098-0.105-0.124
\t\t\t\tc-0.046-0.026-0.11-0.032-0.192-0.018c-0.124,0.022-0.223,0.096-0.296,0.222c-0.072,0.126-0.097,0.269-0.071,0.428l-0.075-0.286
\t\t\t\tc0.052-0.206,0.143-0.355,0.272-0.446C435.1,201.3,435.245,201.24,435.407,201.211z"/>
\t\t\t<path fill="#FFFFFF" d="M437.766,200.709c0.276-0.062,0.513-0.029,0.706,0.1c0.194,0.128,0.333,0.379,0.416,0.75l-1.554,0.347
\t\t\t\tl-0.029-0.087l0.968-0.217c-0.031-0.155-0.072-0.295-0.124-0.421c-0.051-0.126-0.109-0.223-0.174-0.291
\t\t\t\tc-0.064-0.068-0.137-0.093-0.216-0.076c-0.11,0.024-0.188,0.115-0.234,0.271s-0.041,0.396,0.015,0.721l0.021,0.025
\t\t\t\tc0.006,0.039,0.012,0.079,0.02,0.12c0.008,0.042,0.017,0.084,0.026,0.128c0.049,0.218,0.119,0.387,0.21,0.509
\t\t\t\tc0.092,0.122,0.192,0.203,0.301,0.243c0.107,0.041,0.211,0.05,0.309,0.028c0.094-0.021,0.192-0.068,0.293-0.14
\t\t\t\tc0.102-0.072,0.184-0.198,0.246-0.377l0.091,0.009c-0.011,0.128-0.045,0.258-0.102,0.39c-0.056,0.131-0.14,0.248-0.249,0.348
\t\t\t\ts-0.253,0.171-0.429,0.21c-0.215,0.048-0.414,0.044-0.598-0.01c-0.185-0.055-0.344-0.165-0.479-0.33
\t\t\t\tc-0.134-0.165-0.233-0.391-0.298-0.678c-0.065-0.293-0.07-0.548-0.015-0.765c0.056-0.218,0.16-0.394,0.313-0.529
\t\t\t\tC437.354,200.852,437.542,200.759,437.766,200.709z"/>
\t\t\t<path fill="#FFFFFF" d="M440.682,199.991c0.131-0.034,0.242-0.046,0.334-0.035c0.091,0.011,0.167,0.039,0.229,0.083
\t\t\t\tc0.065,0.046,0.123,0.113,0.171,0.201c0.048,0.087,0.093,0.21,0.134,0.37l0.33,1.259c0.034,0.131,0.08,0.214,0.138,0.25
\t\t\t\tc0.058,0.035,0.142,0.038,0.25,0.009l0.026,0.098c-0.061,0.013-0.15,0.032-0.271,0.059c-0.119,0.026-0.237,0.055-0.353,0.085
\t\t\t\tc-0.119,0.031-0.234,0.063-0.347,0.098c-0.113,0.035-0.199,0.061-0.257,0.08l-0.026-0.099c0.094-0.024,0.153-0.065,0.179-0.122
\t\t\t\ts0.021-0.15-0.014-0.282l-0.382-1.457c-0.021-0.078-0.046-0.144-0.077-0.197c-0.03-0.054-0.07-0.092-0.119-0.114
\t\t\t\tc-0.05-0.022-0.114-0.022-0.195-0.001c-0.132,0.035-0.229,0.115-0.29,0.241c-0.063,0.126-0.072,0.268-0.031,0.423l0.341,1.302
\t\t\t\tc0.034,0.131,0.077,0.215,0.128,0.252c0.052,0.037,0.122,0.043,0.213,0.019l0.025,0.099c-0.057,0.011-0.14,0.029-0.248,0.053
\t\t\t\tc-0.109,0.023-0.222,0.05-0.337,0.081c-0.119,0.031-0.24,0.065-0.363,0.103c-0.124,0.038-0.217,0.066-0.278,0.085l-0.025-0.099
\t\t\t\tc0.109-0.028,0.18-0.072,0.213-0.131s0.032-0.153-0.002-0.285l-0.386-1.475c-0.037-0.141-0.086-0.239-0.146-0.295
\t\t\t\tc-0.06-0.056-0.147-0.069-0.263-0.039l-0.025-0.098c0.103-0.017,0.2-0.038,0.294-0.062c0.131-0.035,0.252-0.072,0.363-0.113
\t\t\t\ts0.212-0.085,0.303-0.132l0.11,0.421c0.037-0.18,0.113-0.32,0.23-0.42C440.375,200.106,440.516,200.035,440.682,199.991z"/>
\t\t\t<path fill="#FFFFFF" d="M443.028,199.32c0.271-0.082,0.51-0.066,0.712,0.048c0.203,0.114,0.359,0.354,0.469,0.718l-1.524,0.459
\t\t\t\tl-0.036-0.085l0.95-0.286c-0.042-0.152-0.094-0.29-0.153-0.411c-0.061-0.122-0.126-0.214-0.195-0.278
\t\t\t\tc-0.069-0.063-0.143-0.083-0.22-0.06c-0.108,0.033-0.18,0.128-0.215,0.287s-0.013,0.398,0.067,0.718l0.021,0.023
\t\t\t\tc0.009,0.038,0.018,0.078,0.028,0.118s0.022,0.083,0.035,0.126c0.064,0.213,0.146,0.377,0.247,0.492
\t\t\t\tc0.1,0.115,0.205,0.188,0.316,0.221c0.11,0.032,0.214,0.034,0.31,0.005c0.093-0.028,0.188-0.082,0.283-0.161
\t\t\t\tc0.096-0.079,0.168-0.21,0.218-0.394l0.092,0.003c-0.002,0.128-0.026,0.261-0.073,0.396s-0.121,0.257-0.224,0.366
\t\t\t\tc-0.103,0.108-0.24,0.188-0.413,0.24c-0.21,0.063-0.409,0.074-0.597,0.033c-0.188-0.042-0.354-0.139-0.5-0.294
\t\t\t\tc-0.146-0.155-0.262-0.373-0.347-0.654c-0.086-0.288-0.109-0.542-0.07-0.763c0.04-0.221,0.132-0.404,0.274-0.55
\t\t\t\tC442.628,199.492,442.809,199.386,443.028,199.32z"/>
\t\t\t<path fill="#FFFFFF" d="M445.115,198.654c0.129-0.043,0.246-0.064,0.354-0.065c0.107,0,0.188,0.008,0.24,0.024
\t\t\t\tc0.128,0.036,0.177-0.032,0.146-0.202l0.097-0.032c0.022,0.088,0.054,0.194,0.092,0.319s0.092,0.29,0.161,0.495l-0.097,0.032
\t\t\t\tc-0.054-0.108-0.12-0.21-0.199-0.306c-0.08-0.096-0.172-0.166-0.277-0.208c-0.104-0.043-0.221-0.043-0.35,0
\t\t\t\tc-0.089,0.03-0.155,0.08-0.199,0.151c-0.044,0.071-0.051,0.154-0.019,0.249c0.03,0.092,0.085,0.163,0.163,0.213
\t\t\t\tc0.078,0.051,0.172,0.092,0.279,0.126c0.108,0.034,0.222,0.069,0.341,0.108c0.19,0.062,0.358,0.133,0.502,0.212
\t\t\t\tc0.145,0.079,0.247,0.211,0.309,0.395c0.046,0.138,0.047,0.271,0.002,0.4c-0.046,0.129-0.124,0.243-0.235,0.342
\t\t\t\tc-0.111,0.099-0.247,0.175-0.406,0.229c-0.092,0.031-0.177,0.049-0.255,0.055s-0.147,0.003-0.209-0.006
\t\t\t\tc-0.046-0.005-0.089-0.01-0.13-0.015c-0.041-0.005-0.078-0.009-0.113-0.011c-0.033,0.001-0.057,0.021-0.069,0.059
\t\t\t\tc-0.013,0.039-0.017,0.087-0.01,0.146l-0.097,0.032c-0.026-0.1-0.062-0.221-0.106-0.363c-0.044-0.142-0.104-0.329-0.183-0.562
\t\t\t\tl0.096-0.032c0.074,0.159,0.157,0.294,0.248,0.404c0.092,0.111,0.191,0.188,0.3,0.231c0.108,0.044,0.227,0.044,0.355,0.001
\t\t\t\tc0.079-0.027,0.145-0.077,0.194-0.149c0.051-0.073,0.057-0.168,0.018-0.284c-0.045-0.135-0.126-0.231-0.243-0.291
\t\t\t\ts-0.261-0.119-0.43-0.178c-0.125-0.046-0.243-0.094-0.357-0.143c-0.113-0.049-0.213-0.111-0.299-0.186
\t\t\t\tc-0.087-0.075-0.15-0.172-0.19-0.291c-0.046-0.138-0.048-0.267-0.007-0.386c0.042-0.12,0.115-0.223,0.221-0.312
\t\t\t\tC444.856,198.768,444.978,198.701,445.115,198.654z"/>
\t\t\t<path fill="#FFFFFF" d="M447.256,197.903c0.127-0.047,0.244-0.072,0.352-0.076c0.106-0.003,0.187,0.002,0.24,0.017
\t\t\t\tc0.129,0.032,0.176-0.037,0.141-0.207l0.096-0.035c0.025,0.087,0.059,0.192,0.101,0.316c0.043,0.124,0.102,0.287,0.176,0.49
\t\t\t\tl-0.096,0.035c-0.056-0.106-0.126-0.206-0.208-0.299c-0.083-0.094-0.177-0.16-0.283-0.2c-0.105-0.04-0.222-0.037-0.35,0.01
\t\t\t\tc-0.088,0.032-0.152,0.084-0.194,0.157s-0.046,0.156-0.012,0.25c0.034,0.091,0.09,0.16,0.17,0.208
\t\t\t\tc0.08,0.048,0.174,0.087,0.283,0.118s0.224,0.063,0.343,0.098c0.193,0.056,0.362,0.122,0.509,0.197
\t\t\t\tc0.146,0.075,0.253,0.204,0.32,0.385c0.05,0.136,0.055,0.27,0.013,0.4c-0.041,0.131-0.116,0.247-0.225,0.349
\t\t\t\ts-0.241,0.182-0.398,0.24c-0.091,0.033-0.176,0.054-0.254,0.062c-0.077,0.008-0.147,0.008-0.209,0
\t\t\t\tc-0.046-0.004-0.089-0.007-0.13-0.011s-0.079-0.006-0.114-0.007c-0.033,0.002-0.056,0.022-0.067,0.061
\t\t\t\tc-0.012,0.039-0.014,0.088-0.005,0.146l-0.096,0.035c-0.029-0.099-0.068-0.219-0.117-0.359c-0.048-0.141-0.115-0.326-0.199-0.556
\t\t\t\tl0.095-0.035c0.079,0.157,0.165,0.289,0.26,0.397s0.197,0.182,0.307,0.223c0.109,0.04,0.228,0.037,0.355-0.01
\t\t\t\tc0.078-0.029,0.142-0.081,0.19-0.155c0.048-0.075,0.051-0.169,0.008-0.285c-0.049-0.133-0.133-0.228-0.251-0.284
\t\t\t\tc-0.119-0.056-0.264-0.111-0.436-0.165c-0.126-0.043-0.246-0.087-0.36-0.133c-0.115-0.045-0.217-0.104-0.306-0.176
\t\t\t\tc-0.088-0.072-0.154-0.167-0.197-0.286c-0.051-0.136-0.057-0.265-0.019-0.385s0.108-0.227,0.21-0.317
\t\t\t\tC447,198.023,447.119,197.953,447.256,197.903z"/>
\t\t</g>
\t</g>
\t<polyline fill="#440047" points="388.302,203.57 390.92,208.028 390.92,204.775 \t"/>
\t<polyline fill="#440047" points="453.178,203.965 450.143,208.423 450.143,205.168 \t"/>
</g>
<g>
</g>
<g>
</g>
<g>
</g>
<g>
</g>
<g>
</g>
<g>
</g>
<text x="50%" y="50%" dominant-baseline="middle" text-anchor="middle" fill="#000000" style="font-family: 'Playfair Display';font-weight:700;" font-size="1.7em">${event.queryStringParameters.name}</text>
<text x="85%" y="85%" dominant-baseline="middle" text-anchor="middle" fill="#000000" style="font-family: 'Playfair Display';font-weight:400;" font-size="0.8em">${event.queryStringParameters.orderId}</text>
<image x="80%" y="70%" width="80" height="80" href="${lambdaUrl}generate-qr?url=${baseUrl}/cert?id=${event.queryStringParameters.orderId}" />
</svg>
`
  });
};