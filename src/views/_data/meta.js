require('dotenv').config();

module.exports = {
    url: process.env.URL || "http://localhost:8080",
    lambdaUrl: (process.env.URL ? process.env.URL + '/.netlify/functions' : 'http://localhost:9000'),
    siteName: "Institute of Awesomeness: your path to Certified Awesomeness",
    siteDescription:
        "Institute of Awesomeness: Your path to proven Awesomeness. Get your certificate proving you are Awesome now!",
    authorName: "Jens Kooij",
    twitterUsername: "jenskooij", // no `@`
    socialImage: "https://res.cloudinary.com/dexsmqaw4/image/upload/v1600863771/Assets/institute-of-awesomeness_xeutge.png",
    splashImage: "https://res.cloudinary.com/dexsmqaw4/image/upload/w_512/v1600863771/Assets/institute-of-awesomeness_xeutge.png", // The same as social, only 512px
    appleTouchIcon: "https://res.cloudinary.com/dexsmqaw4/image/upload/w_192/v1600863771/Assets/institute-of-awesomeness_xeutge.png"
};