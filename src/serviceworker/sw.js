const BUILD_ID = 'ENV_BUILD_ID';

const cachableUrls = [
  '/',
  '/confirm',
  '/script.js',
  '/style.css',
  '/sitemap.xml',
  '/robots.txt',
  '/site.webmanifest'
];

self.addEventListener('install', function (e) {
  e.waitUntil(
    caches.open(BUILD_ID).then(function (cache) {
      return cache.addAll(cachableUrls);
    })
  );
});

self.addEventListener('fetch', function (event) {

  // Ignore Google's requests
  if (event.request.url.indexOf('google') !== -1) {
    return;
  }

  event.respondWith(
    fetch(event.request).then(response => {
      const responseClone = response.clone();

      if (response.ok) {
        if (event.request.method === 'GET') {
          caches.open(BUILD_ID).then(cache => cache.put(event.request, responseClone));
        }

        return response;
      } else {
        return caches.match(event.request).then(function (cacheResponse) {
          return cacheResponse || response;
        })
      }
    }).catch(err => {
      return caches.match(event.request).then(function (cacheResponse) {
        return cacheResponse;
      })
    })
  );

});

self.addEventListener('activate', (event) => {
  const cacheKeeplist = [BUILD_ID];

  event.waitUntil(
    caches.keys().then((keyList) => {
      return Promise.all(keyList.map((key) => {
        if (cacheKeeplist.indexOf(key) === -1) {
          return caches.delete(key);
        }
      }));
    })
  );
});