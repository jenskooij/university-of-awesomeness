function initOrderFlow () {
  if (typeof fetch !== "function") {
        return; // This is need so as to not break Uncss, which is run in Node, which means fetch isnt defined.
  }
  const orderForm = document.querySelector('form');
  const modal = document.querySelector('.modal');
  const sumitBtn = document.querySelector('[type="submit"]');

  sumitBtn.classList.remove('is-loading');

  function setupCheckout (certificateCloudinaryUrl, orderParams, orderId) {
    let checkoutParams = new URLSearchParams();
    checkoutParams.append('certificateCloudinaryUrl', certificateCloudinaryUrl);
    checkoutParams.append('orderId', orderId);

    fetch(window.LAMBDA_URL + '/setup-checkout?' + checkoutParams.toString())
      .then(response => handleJsonResponse(response))
      .then(data => {
        setTimeout(() => {
          window.location = data.paymentUrl;
        }, 1000);
      })
      .catch(err => cancelOrder(err));
  }

  /**
   * Orders the certificate
   * @param orderParams URLSearchParams
   */
  function orderCertificate (name, orderParams) {
    fetch(window.LAMBDA_URL + '/order-certificate?' + orderParams.toString(), {
      method: 'GET'
    })
      .then(response => handleJsonResponse(response))
      .then(data => {
        document.getElementById('callbackContainer').innerHTML = `<progress class="progress is-small is-primary" max="100"></progress>Processing certificate`;
        convertCertificate(name, data.orderId, orderParams);
      })
      .catch(err => cancelOrder(err));
  }

  /**
   * When something goes wrong, display an error message
   * Enable the close button on the modal
   * Disable the submit button
   * @param err
   */
  function cancelOrder (err) {
    const closeBtn = document.querySelector('.modal-close');
    const modalBg = document.querySelector('.modal-background');

    closeBtn.classList.remove('is-hidden');
    closeBtn.addEventListener('click', () => {
      modal.classList.remove('is-active');
    });
    modalBg.addEventListener('click', () => {
      modal.classList.remove('is-active');
    });
    sumitBtn.setAttribute('disabled', 'disabled');
    document.getElementById('callbackContainer').innerHTML = 'Something went wrong trying to process your order, please try again later';
    console.error(err);
  }

  /**
   * Converts the response to json if it's OK
   * @param response
   * @returns {Promise<never>|any|Promise<any>}
   */
  function handleJsonResponse (response) {
    if (response.ok) {
      return response.json()
    } else {
      return Promise.reject('Something went wrong');
    }
  }

  /**
   * This is a crucial step, because the first call of the asset takes long, because it gets
   * generated, but consequetive calls are signigicanly faster (well kinda because it's a CDN, right?)
   *
   * @param orderParams
   */
  function warmupCloudinary (orderParams) {
    fetch(orderParams.get('certificateUrl'))
      .then(response => {
        if (response.ok) {
          document.getElementById('callbackContainer').innerHTML = `<progress class="progress is-small is-primary" max="100"></progress>Preparing checkout.`;
          console.log(orderParams);
          setupCheckout(orderParams.get('certificateUrl'), orderParams, orderParams.get('orderId'));
        } else {
          return Promise.reject('Something went warming up cloudinary');
        }
      })
      .catch(err => cancelOrder(err));
  }

  /**
   * Convert the certificate from svg to png using cloudinary
   * @param name
   * @param orderParams
   */
  function convertCertificate (name, orderId, orderParams) {
    let convertParams = new URLSearchParams();
    convertParams.append('name', name);
    convertParams.append('orderId', orderId);

    fetch(window.LAMBDA_URL + '/convert-certificate-to-png?' + convertParams.toString(), {
      method: 'GET'
    })
      .then(response => handleJsonResponse(response))
      .then(data => {
        document.getElementById('callbackContainer').innerHTML = `<progress class="progress is-small is-primary" max="100"></progress>Warming up certificate delivery network.`;
        orderParams.append('certificateUrl', data.certificateUrl);
        orderParams.append('orderId', orderId);

        warmupCloudinary(orderParams);
      })
      .catch(err => cancelOrder(err));
  }

  orderForm.addEventListener('submit', (event) => {
    event.preventDefault();
    modal.classList.add('is-active');

    const formData = new FormData(orderForm);
    let orderParams = new URLSearchParams(formData);



    if (dataLayer !== undefined) {
      dataLayer.push({'event': 'order-form-submit'});
    }
    //convertCertificate(formData.get('name'), orderParams);
    orderCertificate(formData.get('name'), orderParams);
    return false;
  });
}