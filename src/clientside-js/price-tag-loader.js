function loadPriceTag () {
  if (typeof fetch !== "function") {
    return; // This is need so as to not break Uncss, which is run in Node, which means fetch isnt defined.
  }
  const prices = document.querySelectorAll('.price');
  const countrySelector = document.getElementById('country');

  function handleJsonResponse (response) {
    if (response.ok) {
      return response.json()
    } else {
      return Promise.reject('Something went wrong');
    }
  }

  function setPriceLabels (data) {
    countrySelector.querySelector(`option[value="${data.country}"]`).setAttribute('selected', 'selected');
    document.querySelector('.select.is-loading').classList.remove('is-loading');

    const formatter = new Intl.NumberFormat(data.country, {
      style: 'currency',
      currency: data.currency,

      // These options are needed to round to whole numbers if that's what you want.
      minimumFractionDigits: 2,
      maximumFractionDigits: 2,
    });
    const priceText = formatter.format(data.totalExchanged);
    prices.forEach(el => {
      el.innerHTML = priceText;
      el.setAttribute('title', `Get your certificate now for only ${priceText}`)
    });
  }

  fetch(window.LAMBDA_URL + '/get-price')
    .then(response => handleJsonResponse(response))
    .then(data => {
      setPriceLabels(data);
      countrySelector.addEventListener('change', () => {
        document.querySelector('.select').classList.add('is-loading');
          fetch(window.LAMBDA_URL + '/get-price?country=' + countrySelector.value)
            .then(response => handleJsonResponse(response))
            .then(priceLabelData => {
                setPriceLabels(priceLabelData);
            })
            .catch(err => console.error(err));
      });
    })
    .catch(err => {
      document.querySelector('.select.is-loading').classList.remove('is-loading');
      console.error(err)
    });
}

