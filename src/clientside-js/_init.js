function init() {
  initOrderFlow();
  loadPriceTag();
}

(function () {
  if (document.readyState === "interactive" || document.readyState === "complete") {
    init();
  } else if (document.addEventListener) {
    document.addEventListener("DOMContentLoaded", init);
  } else if (document.attachEvent) {
    document.attachEvent("onreadystatechange", function () {
      if (document.readyState !== "loading") {
        init();
      }
    });
  }
})();